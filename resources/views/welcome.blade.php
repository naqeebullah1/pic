<!DOCTYPE html>
<html>

    <!-- Mirrored from effortthemes.com/html/spring/spring/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Apr 2019 11:26:46 GMT -->
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="{{ asset('public/assets/img/logo-fav.png') }}">

        <title>Abdan Traders</title>
        <!-- Stylesheets -->
        <link href="{{ asset('public/frontend/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('public/frontend/css/style.css') }}" rel="stylesheet">
        <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <!-- Responsive -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{ asset('public/frontend/css/responsive.css') }}" rel="stylesheet">
        <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
        <style>
            .aboutusCompanyName{
                font-weight: bold;
            }
            .aboutusWelcome{
                font-family: Raleway;
                font-size: 28px !important;
            }
        </style>
        <script src="{{asset('public/slider/js/jssor.slider.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
jssor_1_slider_init = function () {

    var jssor_1_options = {
        $AutoPlay: 1,
        $SlideDuration: 800,
        $SlideEasing: $Jease$.$OutQuint,
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
        $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$
        }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

    /*#region responsive code begin*/

    var MAX_WIDTH = 3000;

    function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;

        if (containerWidth) {

            var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

            jssor_1_slider.$ScaleWidth(expectedWidth);
        } else {
            window.setTimeout(ScaleSlider, 30);
        }
    }

    ScaleSlider();

    $Jssor$.$AddEvent(window, "load", ScaleSlider);
    $Jssor$.$AddEvent(window, "resize", ScaleSlider);
    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    /*#endregion responsive code end*/
};
        </script>
        <style>
            /*jssor slider loading skin spin css*/
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-009-spin {
                from { transform: rotate(0deg); }
                to { transform: rotate(360deg); }
            }

            /*jssor slider bullet skin 032 css*/
            .jssorb032 {position:absolute;}
            .jssorb032 .i {position:absolute;cursor:pointer;}
            .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
            .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
            .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
            .jssorb032 .i.idn {opacity:.3;}

            /*jssor slider arrow skin 051 css*/
            .jssora051 {display:block;position:absolute;cursor:pointer;}
            .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
            .jssora051:hover {opacity:.8;}
            .jssora051.jssora051dn {opacity:.5;}
            .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
        </style>
    </head>

    <body>

        <div class="page-wrapper">

            <!-- Preloader -->
            <div class="preloader"></div>

            <!-- Main Header-->
            <header class="main-header">

                <!-- Main Box -->
                <div class="main-box" style="top: 0px;">
                    <div class="auto-container">
                        <div class="outer-container clearfix">
                            <!--Logo Box-->
                            <div class="logo-box" style="padding:5px 0px;">
                                <div class="logo"><a href="{{asset('/')}}"><img src="{{ asset('public/assets/img/logo.jpeg') }}" alt="" style="width:140px;"></a></div>
                            </div>

                            <!--Nav Outer-->
                            <div class="nav-outer clearfix">
                                <!-- Main Menu -->
                                <nav class="main-menu">

                                    <div class="navbar-header">
                                        <!-- Toggle Button -->    	
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                    <div class="navbar-collapse collapse scroll-nav clearfix">
                                        <ul class="navigation clearfix">
                                            <!--                                            <li class="dropdown"><a href="#">Extra Pages</a>
                                                                                            <ul>
                                                                                                <li><a href="about.html">About Us</a></li>
                                                                                                <li><a href="price.html">Pricing</a></li>
                                                                                                <li class="dropdown"><a href="#">Services</a>
                                                                                                    <ul>
                                                                                                        <li><a href="services.html">All Services</a></li>
                                                                                                        <li><a href="service-detail.html">Service Details</a></li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li class="dropdown"><a href="#">Blog</a>
                                                                                                    <ul>
                                                                                                        <li><a href="blog.html">Blog Page</a></li>
                                                                                                        <li><a href="blog-single.html">Blog Post Details</a></li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li><a href="contact.html">Contact Us</a></li>
                                                                                            </ul>
                                                                                        </li>-->
                                            <li><a href="#home">Home</a></li>
<!--                                            <li><a href="#">Events</a></li>
                                            <li><a href="#">Jobs</a></li>-->
                                            <li><a href="#aboutus">About Us</a></li>
                                            <li><a href="#products">Products</a></li>
                                            <li><a href="#contact">Contact Us</a></li>                                      
                                            <li><a href="#findus">Find Us</a></li>                                      
                                        </ul>
                                    </div>

                                </nav>

                            </div>
                            <!--Nav Outer End-->

                        </div>    
                    </div>
                </div>

            </header>
            <!--End Main Header -->

            <!--Slider Section-->
            <section class="blog-section" id="home" style="padding: 110px 0px;">
                <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
                    <!-- Loading Screen -->
                    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?= asset('public/slider/svg/loading/static-svg/spin.svg'); ?>" />
                    </div>
                    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
                        <div>
                            <img data-u="image" src="<?php echo asset('public/slider/my_img/001.jpg'); ?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?php echo asset('public/slider/my_img/002.jpg'); ?>" />
                        </div>
                        <!--                        <div>
                                                    <img data-u="image" src="../img/gallery/1300x500/003.jpg" />
                                                    <div style="position:absolute;top:30px;left:30px;width:480px;height:130px;z-index:0;background-color:rgba(255,188,5,0.8);font-size:40px;font-weight:100;color:#000000;line-height:60px;padding:5px;box-sizing:border-box;">TOUCH SWIPE
                                                        <br />
                                                        RESPONSIVE SLIDER
                                                    </div>
                                                    <div style="position:absolute;top:300px;left:30px;width:480px;height:130px;z-index:0;background-color:rgba(255,188,5,0.8);font-size:30px;color:#000000;line-height:38px;padding:5px;box-sizing:border-box;">Build your slider with anything, includes image, svg, text, html, photo, picture content</div>
                                                </div>
                                                <div style="background-color:#ff7c28;">
                                                    <div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">Photos in this slider are to demostrate jssor slider,<br />
                                                        which are not licensed for any other purpose.
                                                    </div>
                                                </div>-->
                    </div>
                    <!-- Bullet Navigator -->
                    <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                        <div data-u="prototype" class="i" style="width:16px;height:16px;">
                            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                            </svg>
                        </div>
                    </div>
                    <!-- Arrow Navigator -->
                    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                        </svg>
                    </div>
                    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                        </svg>
                    </div>
                </div>
            </section>
            <section class="blog-section" style="padding-bottom: 0!important;padding:0px;">
                <div class="auto-container" id="aboutus">
                    <!--Heading-->
                    <div class="sec-title-two centered">
                        <h2> About Us </h2>
                    </div>

                    <div class="row clearfix">

                        <!--News Block-->
                        <div class="news-style-one col-md-12 col-sm-12 col-xs-12">
                            <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="lower-content">
                                    <h3 class="aboutusWelcome">
                                        WELCOME TO <span class="aboutusCompanyName">SUN TECH SEEDS</span>
                                    </h3>
                                    <div class="text">Sun Tech Seeds was established in 2010 and now is the leading company of Pakistan around <?php echo date('Y') - 2010; ?> years importing hybrid vegetables seeds, field & fodder crop and Hybrid rice seeds. We invite each day new opportunities and correspond with esteemed organizations for fruitful business involving in the globalize networks and striving for the excellence in the dynamic environment.

                                        Customers are considered the most significant person and highly taken care off. Well supported environment is provided to our customers; we enrich their knowledge and educate our customer about the new researches and technological advancements.

                                        In addition, it’s a proactive organization for its strong understanding of seeds industry issues of the Asian Seed Market and in cause working for the growth and glory of the Seed Industry in Asia Pacific Region.

                                        Sun Tech Seeds has an enriched culture to embellish talents and intends to nourish the futures where people are empowered to think bigger, preparing ambitions to contribute in all sectors of life and specifically for the Economic Growth of the country. This is a place where excellence begins.

                                        We admire the customers’ wand always prove the best option for them. We ourselves invite our customers, offering best for them, we make easy access to our researchers and technical experts for their training, counseling targeting the high yields and benefiting them.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="text-center wow tada" data-wow-delay="0ms" data-wow-duration="1500ms"><a href="blog.html" class="theme-btn btn-style-seven">Go To Blog</a></div>-->
                </div>
            </section>
            <section class="blog-section" style="padding:0px;">
                <div class="auto-container" id="products">
                    <!--Heading-->
                    <div class="sec-title-two centered">
                        <h2> Our Products </h2>
                    </div>

                    <div class="row clearfix">
                        @foreach($products as $product)
                        @if($product->product_img)
                        <!--News Block-->
                        <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                            <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="image-box"><img src="{{ asset($product->product_img) }}" alt=""></figure>
                                <div class="lower-content">
                                    <!--<div class="post-meta">26 March 2017 By Author</div>-->
                                    <h3>{{$product->title}}</h3>
                                    <div class="text">{{$product->product_description}}</div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach

                        <!--                        News Block
                                                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                                                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                                        <figure class="image-box"><a href="blog-single.html"><img src="{{ asset('public/frontend/images/resource/blog-image-2.jpg') }}" alt=""></a></figure>
                                                        <div class="lower-content">
                                                            <div class="post-meta">26 March 2017 By Author</div>
                                                            <h3><a href="blog-single.html">Excepteur sint occaecat cupidatat</a></h3>
                                                            <div class="text">Accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</div>
                                                        </div>
                                                    </div>
                                                </div>
                        
                                                News Block
                                                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                                                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                                                        <figure class="image-box"><a href="blog-single.html"><img src="{{ asset('public/frontend/images/resource/blog-image-3.jpg') }}" alt=""></a></figure>
                                                        <div class="lower-content">
                                                            <div class="post-meta">26 March 2017 By Author</div>
                                                            <h3><a href="blog-single.html">Excepteur sint occaecat cupidatat</a></h3>
                                                            <div class="text">Accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</div>
                                                        </div>
                                                    </div>
                                                </div>-->

                    </div>
                    <!--<div class="text-center wow tada" data-wow-delay="0ms" data-wow-duration="1500ms"><a href="blog.html" class="theme-btn btn-style-seven">Go To Blog</a></div>-->
                </div>
            </section>

            <!--Contact Section-->
            <section class="contact-section" id="contact" style="padding:0px;">
                <div class="auto-container">
                    <div class="row clearfix">
                        <!--Form Column-->
                        <div class="form-column col-md-2 col-sm-0 col-xs-0"></div>
                        <div class="form-column col-md-8 col-sm-12 col-xs-12">
                            <div class="sec-title-two centered">
                                <h2> Contact Us</h2>
                            </div>

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                                <div class="icon"><span class="mdi mdi-check"></span></div>
                                <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                            </div>
                            @endif

                            @if(Session::has('error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                                <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                                <div class="message"><strong>{{ Session::get('error') }}</strong></div>
                            </div>
                            @endif
                            <!--Contact Form-->
                            <div class="default-form">
                                <form method="post" action="{{route('contactus.store')}}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <input type="text" name="name" value="" placeholder="Name" required>
                                    </div>

                                    <div class="form-group">
                                        <input type="email" name="email" value="" placeholder="Email" required>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" name="phone" value="" placeholder="Phone Number" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea name="message" placeholder="Message"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="theme-btn btn-style-two">Submit<span class="icon ti-arrow-circle-right"></span></button>
                                    </div>

                                </form>
                            </div><!--End Contact Form-->
                        </div>
                        <div class="form-column col-md-2 col-sm-0 col-xs-0"></div>

                    </div>
                </div>
            </section>
            <section class="contact-section" id="findus" style="padding:20px;">
                <div class="row clearfix">
                    <!--Form Column-->
                    <div class="form-column col-md-12 col-sm-12 col-xs-12">
                        <div class="sec-title-two centered">
                            <h2> Find Us</h2>
                        </div>

                        <div id="map-view" class="is-vcentered" style="width: 100%; height:500px;"></div>

                    </div>

                </div>
            </section>

            <!--Footer Style One-->
            <footer class="footer-style-one">
                <!--Footer Upper-->
                <div class="footer-upper" style="padding:20px;">
                    <div class="row clearfix">
                        <!--Big Column-->
                        <div class="big-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="footer-column" style="margin-bottom:0;">
                                <div class="footer-widget about-widget">
                                    <div class="widget-inner">
                                        <div class="copyright">&copy; <?=date('Y')?> All Rights Reserved</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </footer>


        </div>
        <!--End pagewrapper-->

        <!--Scroll to top-->
        <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCe0I76FCBsgJP2dh193EWuX2IPST4gn0k&sensor=false&libraries=places"></script>

        <script src="{{ asset('public/frontend/js/jquery.js') }}"></script> 
        <script src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/frontend/js/pagenav.js') }}"></script>
        <script src="{{ asset('public/frontend/js/jquery.scrollTo.js') }}"></script>
        <script src="{{ asset('public/frontend/js/jquery.fancybox.pack.js') }}"></script>
        <script src="{{ asset('public/frontend/js/owl.js') }}"></script>
        <script src="{{ asset('public/frontend/js/wow.js') }}"></script>
        <script src="{{ asset('public/frontend/js/validate.js') }}"></script>
        <script src="{{ asset('public/frontend/js/script.js') }}"></script>

        <script type="text/javascript">jssor_1_slider_init();</script>

        <script type="text/javascript">
            function get_addresses() {
                var map = new google.maps.Map(document.getElementById('map-view'), {
                    zoom: 12,
                    center: new google.maps.LatLng(34.0151, 71.5249),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var infowindow = new google.maps.InfoWindow();

                // if single user  is  offline

                console.log('locations.users[index]');
                var getLat = '34.017114';
                var getLon = '71.579485';
                var marker = '';
                var i = '';
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(getLat, getLon),
                    map: map
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent('Sun Tech Seeds');
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
            get_addresses();
        </script>

    </body>

    <!-- Mirrored from effortthemes.com/html/spring/spring/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Apr 2019 11:27:15 GMT -->
</html>