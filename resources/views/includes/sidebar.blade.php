<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="#">Menus</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li class="<?= (Request::segment(1) == 'home') ? 'active' : ''; ?> ">
                            <a href="{{ url('home') }}">
                                <i class="icon mdi mdi-home mr-1"></i><span>Dashboard</span>
                            </a>
                        </li>
                        <?php
                        if (Auth::user()->user_type == 1) {
                            ?>
                            <li class="<?= (Request::segment(1) == 'users') ? 'active' : ''; ?> ">
                                <a href="{{ url('users') }}">
                                    <i class="fab fa-product-hunt mr-1"></i><span> Users</span>
                                </a>
                            </li>
                            <li class="<?= (Request::segment(1) == 'assets') ? 'active' : ''; ?> ">
                                <a href="{{ url('assets') }}">
                                    <i class="fas fa-hand-holding-usd mr-1"></i><span> Personal Assets</span>
                                </a>
                            </li>
                            <?php
                        }
                        $productsActiveArray = ['products', 'add-products', 'edit-product', 'product-ledger'];
                        ?>
                        <li class="<?= in_array(Request::segment(1), $productsActiveArray) ? 'active' : ''; ?> ">
                            <a href="{{ url('products') }}">
                                <i class="fab fa-product-hunt mr-1"></i><span> Products</span>
                            </a>
                        </li>
                        <?php
                        if (Auth::user()->user_type == 1) {
                            ?>
                            <li class="<?= (Request::segment(1) == 'product_qty') ? 'active' : ''; ?> ">
                                <a href="{{ url('product_qty') }}">
                                    <i class="fas fa-layer-group mr-1"></i><span> Products Type</span>
                                </a>
                            </li>
                            <?php
                        }
                        $salesManActiveArray = ['sale_man', 'area_person'];
                        ?>
                        <li class="<?= in_array(Request::segment(1), $salesManActiveArray) ? 'active' : ''; ?> ">
                            <a href="{{ route('area_person.index',['status'=>1]) }}">
                                <i class="fas fa-street-view mr-1"></i> Sales man
                            </a>
                        </li>
                        <li class="<?= (Request::segment(1) == 'employee') ? 'active' : ''; ?> ">
                            <a href="{{ route('employee.index',['status'=>1]) }}">
                                <i class="fas fa-people-carry mr-1"></i> Employees
                            </a>
                        </li>
                        <?php
                        $companiesActiveArray = array('supplier', 'supplier-payment', 'supplierPurchase', 'supplierReturnPurchase', 'supplierReturnPurchase', 'return_purchase_details');
                        ?>
                        <li class="<?= in_array(Request::segment(1), $companiesActiveArray) ? 'active' : ''; ?> ">
                            <a href="{{ url('supplier?status=1') }}">
                                <i class="fas fa-users mr-1"></i> Companies
                            </a>
                        </li>
                        <li class="<?= (Request::segment(1) == 'supplier_cheques') ? 'active' : ''; ?> ">
                            <a href="{{ url('supplier_cheques') }}">
                                <i class="fas fa-users mr-1"></i> Company Cheques
                            </a>
                        </li>
                        <?php
                        $customerActiveArray = array('companies', 'company', 'sales', 'sale', 'return_sale');
                        ?>
                        <li class="<?= in_array(Request::segment(1), $customerActiveArray) ? 'active' : ''; ?>">
                            <a href="{{ route('companies',['status'=>1]) }}">
                                <i class="fas fa-user-tag mr-1"></i>
                                <span>Regular Customers</span>
                            </a>

                        </li>

                        <li class="<?= (Request::segment(1) == 'customer_cheques') ? 'active' : ''; ?> ">
                            <a href="{{ url('customer_cheques') }}">
                                <i class="fas fa-users mr-1"></i> Customer Cheques
                            </a>
                        </li>

                        <li class="parent <?= (Request::segment(1) == 'walking_customers') ? 'open' : '' ?>">
                            <a href="#">
                                <i class="fas fa-user-tag mr-1"></i><span>Walking Customers</span>
                            </a>
                            <ul class="sub-menu">
                                <li class="<?= (Request::segment(1) == 'customers') ? 'active' : ''; ?> ">
                                    <a href="{{ route('walking_customers') }}">
                                        <span>Sale Details</span>
                                    </a>
                                </li>

                                <li class="<?= (Request::segment(1) == 'customers') ? 'active' : ''; ?> ">
                                    <a href="{{ route('walking_customers/sale') }}">
                                        <span>Sale</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'customers') ? 'active' : ''; ?> ">
                                    <a href="{{ route('walking_customers/returnsale') }}">
                                        <span>Return Sale</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'customers') ? 'active' : ''; ?> ">
                                    <a href="{{ route('walking_customers/return') }}">
                                        <span>Return Sale details</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?= (Request::segment(1) == 'accounts') ? 'active' : ''; ?> ">
                            <a href="{{ route('bank_acc',['status'=>1]) }}">
                                <i class="fas fa-university mr-1"></i><span> Bank Accounts</span>
                            </a>
                        </li>
                        <li class="<?= (Request::segment(1) == 'transfer_amount') ? 'active' : ''; ?> ">
                            <a href="{{ url('transfer_amount') }}">
                                <i class="fas fa-university mr-1"></i><span>Transfer Amount</span>
                            </a>
                        </li>
                        <li class="<?= (Request::segment(1) == 'cash') ? 'active' : ''; ?> ">
                            <a href="{{ url('cash_in_hand') }}">
                                <i class="fas fa-hand-holding-usd mr-1"></i><span>Cash</span>
                            </a>
                        </li>
                        <li class="<?= (Request::segment(1) == 'cities') ? 'active' : ''; ?> ">
                            <a href="{{ url('cities') }}">
                                <i class="fas fa-city mr-1"></i><span>Cities</span>
                            </a>
                        </li>
                       <!--<li class="<?= (Request::segment(1) == 'manufacturing_companies') ? 'active' : ''; ?> ">
                            <a href="{{ route('manufacturing_companies.index') }}">
                                <i class="fas fa-building mr-1"></i><span>Manufacturing Companies</span>
                            </a>
                        </li>-->
                        <?php
                        if (auth()->user()->user_type == 1) {
                            ?>
                            <li class="<?= (Request::segment(1) == 'current-summary') ? 'active' : ''; ?> ">
                                <a href="{{ route('current-summary') }}">
                                    <i class="fas fa-funnel-dollar mr-1"></i><span>Current Summary</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php
                        if (auth()->user()->user_type == 1) {
                            ?>
                        <li class="<?= (Request::segment(1) == 'zakat_persons') ? 'active' : ''; ?> ">
                            <a href="{{ route('zakat_persons.index') }}">
                                <i class="fas fa-funnel-dollar mr-1"></i><span>Personal Expenses</span>
                            </a>
                        </li>
                        <?php } ?>
                        <li class="parent">
                            <a href="#">
                                <i class="fas fa-address-card mr-1"></i><span> Expenses</span>
                            </a>
                            <ul class="sub-menu">
                                <li class="<?= (Request::segment(1) == 'expenses') ? 'active' : ''; ?> ">
                                    <a href="{{ url('expenses') }}">
                                        <span>Add Expenses</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'subexpenses') ? 'active' : ''; ?> ">
                                    <a href="{{ url('subexpenses') }}">
                                        <span>All Expenses</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent">
                            <a href="#">
                                <i class="fas fa-address-card mr-1"></i><span> Reports</span>
                            </a>
                            <ul class="sub-menu">
                                <?php
                                if (Auth::user()->user_type == 1) {
                                    ?>
                                    <li class="parent">
                                        <a href="#">
                                            <i class="fas fa-address-card mr-1"></i><span> Daily Reports</span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="<?= (Request::segment(1) == 'daily_cash' || Request::segment(1) == 'get_daily_cash') ? 'active' : ''; ?> ">
                                                <a href="{{ url('daily_cash') }}">
                                                    <span>Daily cash report</span>
                                                </a>
                                            </li>
                                            <li class="<?= (Request::segment(1) == 'daily_bank' || Request::segment(1) == 'get_daily_bank') ? 'active' : ''; ?> ">
                                                <a href="{{url('daily_bank')}}">
                                                    <span>Daily bank report</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="parent">
                                        <a href="#">
                                            <i class="fas fa-file-invoice-dollar mr-1"></i><span> Profit Report</span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="<?= (Request::segment(1) == 'profit' || Request::segment(1) == 'get-profit-report') ? 'active' : ''; ?> ">
                                                <a href="{{ url('profit') }}">
                                                    <span>Profit</span>
                                                </a>
                                            </li>
                                            <li class="<?= (Request::segment(1) == 'item-profit') ? 'active' : ''; ?>">
                                                <a href="{{url('item-profit')}}">
                                                    <span>Items profit</span>
                                                </a>
                                            </li>
                                             <li class="<?= (Request::segment(1) == 'item-profit') ? 'active' : ''; ?>">
                                                <a href="{{url('item-discount')}}">
                                                    <span>Discount Report</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="<?= (Request::segment(1) == 'company-current-status') ? 'active' : ''; ?> ">
                                        <a href="{{ url('company-current-status') }}">
                                            <span>Company Current Status</span>
                                        </a>
                                    </li>
                                <?php } ?>

                                <li class="<?= (Request::segment(1) == 'supplier-city-report' || Request::segment(1) == 'get-supplier-city-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('supplier-city-report') }}">
                                        <span>Company General Report</span>
                                    </a>
                                </li>
                                @php
                                if (Auth::user()->user_type == 1) {
                                @endphp
                                <li class="<?= (Request::segment(1) == 'supplier-report' || Request::segment(1) == 'supplier-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('supplier-report') }}">
                                        <span>Company Ledger Report</span>
                                    </a>
                                </li>
                                @php } @endphp
                                <li class="<?= (Request::segment(1) == 'customer-city-report' || Request::segment(1) == 'get-customer-city-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('customer-city-report') }}">
                                        <span>Customer General Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'customer-report' || Request::segment(1) == 'customer-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('customer-report') }}">
                                        <span>Customer Ledger Report</span>
                                    </a>
                                </li>
                                <?php
                                if (Auth::user()->user_type == 1) {
                                    ?>
                                    <li class="<?= (Request::segment(1) == 'product-quotation' || Request::segment(1) == 'get-product-quotation') ? 'active' : ''; ?> ">
                                        <a href="{{ url('product-quotation') }}">
                                            <span>Quotation</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li class="<?= (Request::segment(1) == 'product-general-report' || Request::segment(1) == 'get-product-general-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('product-general-report') }}">
                                        <span>Product General Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'product-report' || Request::segment(1) == 'product-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('product-report') }}">
                                        <span>Product Report</span>
                                    </a>
                                </li>
                                <?php
                                if (Auth::user()->user_type == 1) {
                                    ?>
                                    <li class="<?= (Request::segment(1) == 'purchase-report' || Request::segment(1) == 'purchase-ledger-report') ? 'active' : ''; ?> ">
                                        <a href="{{ url('purchase-report') }}">
                                            <span>Purchase Report</span>
                                        </a>
                                    </li>
                                    <li class="<?= (Request::segment(1) == 'return-purchase-report' || Request::segment(1) == 'return-purchase-ledger-report') ? 'active' : ''; ?> ">
                                        <a href="{{ url('return-purchase-report') }}">
                                            <span>Return Purchase Report</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li class="<?= (Request::segment(1) == 'sale-report' || Request::segment(1) == 'sale-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('sale-report') }}">
                                        <span>Sale Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'return-sale-report' || Request::segment(1) == 'return-sale-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('return-sale-report') }}">
                                        <span>Return Sale Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'expenses-report' || Request::segment(1) == 'expense-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('expenses-report') }}">
                                        <span>Expenses Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'employee-report' || Request::segment(1) == 'employee-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('employee-report') }}">
                                        <span>Employee Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'bank-report' || Request::segment(1) == 'bank-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('bank-report') }}">
                                        <span>Banks Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'cash-report' || Request::segment(1) == 'cash-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('cash-report') }}">
                                        <span>Cash Ledger Report</span>
                                    </a>
                                </li>
                                <li class="<?= (Request::segment(1) == 'area-report' || Request::segment(1) == 'area-ledger-report') ? 'active' : ''; ?> ">
                                    <a href="{{ url('area-report') }}">
                                        <span>Sales man Report</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
<!--                        <li class="<?= (Request::segment(1) == 'map') ? 'active' : ''; ?> ">
            <a href="{{ url('users/map') }}">
                <i class="fas fa-map-marker-alt"></i><span>Map</span>
            </a>
        </li>-->
                        <li class="">
                            <a href="{{ route('our_backup_database') }}">
                                <i class="fas fa-people-carry mr-1"></i> Backup
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>