<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="shortcut icon" href="{{asset('public/images/logo2.png')}}">-->
        <title>Abdan Traders</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/assets/css/app.css') }}" type="text/css" />

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

        <meta name="csrf_token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css">
        @yield('style')

        <style>
            @media print { 
                .noprint { 
                    visibility: hidden; 
                }

                .row{
                    display: flex;

                }
                .col-lg-3{
                    width:25%;
                }
                .col-lg-3 .card{
                    /*border:1px solid red;*/
                }
                .col-lg-3 .card-header{
                    font-size: 26px;
                }
                table{
                    font-size:22px;
                }
            }
            .required{
                font-weight: bold;
                color: red;
                margin: 0 5px;
                font-size: 16px;
            }
            .custom-select.custom-select-sm{
                min-width:45px !important;
            }
            @keyframes mymove
            {
                0%   {
                    font-size:1.077rem;


                }
                25%  {
                    font-size:1.3rem;

                }
                50%  {
                    font-size:1.2rem;

                }
                75%  {
                    font-size:1.3rem;

                }
                100%  {
                    font-size:1.1rem;

                }
            }

            .cheque_div{
                color:red;
                font-weight:600;
                animation: mymove ease 3s infinite;
            }
            .cheque_Upper_div:hover{
                box-shadow: 1px 1px 5px  black !important;
                transition: all ease 0.3s;
            }
            .desc {
                line-height: 26px !important;
            }
            .table{
                z-index: 99999;
            }
            .be-datatable-header{
                width: 100%;
            }
            .be-datatable-header .col-sm-6{
                display: flex;
                align-items: flex-end;
            }
            #table1_filter{ 
                width: 100%;
                display: flex;
                justify-content: flex-end;
            }
            #table1_filter label{ 
                display: flex;
                align-items:  center;
            }
            @media only screen and (max-width: 576px) {
                #table1_filter{
                    margin-top: 10px;
                    justify-content: flex-start;
                }
            }
            /*576*/
            @media 
            only screen and (max-width: 760px),
            (min-device-width: 768px) and (max-device-width: 1024px)  {

                .form-control{
                    height: 2.5rem;
                    padding: 0 10px;
                }
                .form-group{
                    margin-bottom: 5px;
                }
                .form-group.row {
                    margin-bottom: 0;
                    padding: 0;
                }

                /* Force table to not be like tables anymore */
                table, thead, tbody, th, td, tr { 
                    display: block; 
                }

                /* Hide table headers (but not display: none;, for accessibility) */
                thead tr { 
                    position: absolute;
                    top: -9999px;
                    left: -9999px;
                }

                tr { border: 1px solid #ccc; }

                td { 
                    /* Behave  like a "row" */
                    border: none;
                    border-bottom: 1px solid #eee; 
                    position: relative;
                    padding-left: 50%; 
                }

                td:before { 
                    width: 45%; 
                    white-space: nowrap;
                    font-weight: bold;
                    text-transform: capitalize;
                }
                td{ 
                    display: flex;
                    padding: 11px 6px !important;
                }

            }


            @media only screen and (max-width: 800px) {

                /* Force table to not be like tables anymore */
                .no-more-tables table, 
                .no-more-tables thead, 
                .no-more-tables tbody, 
                .no-more-tables th, 
                .no-more-tables td, 
                .no-more-tables tr { 
                    display: block; 
                }
                .no-more-tables td.hide-sm{
                    display: none;
                }
                
                /* Hide table headers (but not display: none;, for accessibility) */
                .no-more-tables thead tr { 
                    position: absolute;
                    top: -9999px;
                    left: -9999px;
                }

                .no-more-tables tr { border: 1px solid #ccc; }

                .no-more-tables td { 
                    /* Behave  like a "row" */
                    border: none;
                    border-bottom: 1px solid #eee; 
                    position: relative;
                    padding-left: 50% !important; 
                    white-space: normal;
                    text-align:left;
                }

                .no-more-tables td:before { 
                    /* Now like a table header */
                    position: absolute;
                    /* Top/left values mimic padding */
                    top: 6px;
                    left: 6px;
                    width: 45%; 
                    padding-right: 10px; 
                    white-space: nowrap;
                    text-align:left;
                    font-weight: bold;
                }

                /*
                Label the data
                */
                .no-more-tables td:before { content: attr(data-title); }
                .no-more-tables td.full-width-td{
                    padding: 0px !important;
                }
            }
        </style>
    </head>

    <body>
        <div class="be-wrapper">
            <nav class="navbar navbar-expand fixed-top be-top-header">
                <div class="container-fluid">
                    <div class="be-navbar-header pb-2">
                        <a class="navbar-brand" href="<?= route('home'); ?>">
                            <img src="{{asset('public/images/logo2.jpg')}}" alt="LOGO" style="height:95%;font-weight: bold;">
                        </a>
                    </div>
                    <div class="be-right-navbar">
                        <ul class="nav navbar-nav float-right be-user-nav">
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><img src="{{ asset('public/assets/img/avatar.png') }}" alt="Avatar"><span class="user-name"><?php
                                        echo ucwords(auth()->user()->name);
                                        ?></span></a>
                                <div class="dropdown-menu" role="menu">
                                    <div class="user-info">
                                        <div class="user-name">{{ Auth::user()->username }}</div>
                                        <div class="user-position online">Available</div>
                                    </div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();"><span class="icon mdi mdi-power"></span>Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>  
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>