<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="#">Blank Page</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li class="<?= (Request::segment(2) == 'add-country') ? 'active' : ''; ?> ">
                            <a href="{{ route('my_dashboard') }}">
                                <i class="icon mdi mdi-home mr-1"></i><span>Dashboard</span>
                            </a>
                        </li>
                        <li class="<?= (Request::segment(2) == 'stock') ? 'active' : ''; ?> ">
                            <a href="{{ route('stock') }}">
                                <i class="fas fa-layer-group mr-1"></i><span> Stock</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('saleman.sale') }}">
                                <i class="fas fa-file-invoice mr-1"></i><span> Create Invoice</span>
                            </a>
                        </li>

                        <li class="<?= (Request::segment(2) == 'customers') ? 'active' : ''; ?> ">
                            <a href="{{ route('saleman.customers') }}">
                                <i class="fas fa-users mr-1"></i><span>Customers</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>