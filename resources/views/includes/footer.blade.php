</div>
<script src="{{ asset('public/assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app.js') }}" type="text/javascript"></script>

@yield('script')

<script type="text/javascript">
$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});
$('body').on('keydown', 'input, select', function (e) {
    if (e.key === "Enter") {
        var self = $(this), form = self.parents('form:eq(0)'), focusable, next;
        focusable = form.find('input,a,select,button,textarea').filter(':visible');
        next = focusable.eq(focusable.index(this) + 1);
        if (next.length) {
            next.focus();
        } else {
            form.submit();
        }
        return false;
    }
});

$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});


$(document).ready(function () {
    //-initialize the javascript
    App.init();
    App.formElements();

});
$(function () {
    if ($('[required]').parent().siblings('label').length != 0) {
        $('[required]').parent().siblings('label').append('<span class="required">*</span>');
    } else {
        $('[required]').parent().prepend('<span class="text-danger" style="line-height:2">Required</span><span class="required"> *</span>');
    }
});
</script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/jszip/jszip.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/pdfmake/pdfmake.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/pdfmake/vfs_fonts.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<script src="{{ asset('public/assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
<script type="text/javascript">
                                                    function fetchHistory(invoiceNo){
                                                    $.ajax({
                                                    data: {
                                                    invoiceNo : invoiceNo
                                                    },
                                                            type: 'GET',
                                                            url: "{{ route('purchase/history') }}",
                                                            success: function(return_data) {
                                                            $('#purchaseDetailBody').html(return_data);
                                                            $('#purchaseDetailModal').modal('show');
                                                            },
                                                    });
                                                    }
                                                    $(document).ready(function() {
                                                    //-initialize the javascript
                                                    App.dataTables();
                                                    });
</script>

</body>

</html>