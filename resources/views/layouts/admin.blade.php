@include('includes.header')
<?php
if (Auth::user()->user_type == 3) {
    ?>
    @include('includes.sales_man_sidebar')
<?php } else { ?>
    @include('includes.sidebar')
<?php } ?>

@yield('content')

@include('includes.footer')

