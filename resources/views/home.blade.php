@extends('layouts.admin')

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Dashboard</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item active">Home Page</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
            <div class="icon"><span class="mdi mdi-close"></span></div>
            <div class="message"><strong> {{ Session::get('error') }} </strong></div>
        </div>
        @endif
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message"><strong> {{ Session::get('success') }} </strong></div>
        </div>
        @endif
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{ url('products') }}">

                    <div class="widget widget-tile">
                        <div class="chart sparkline" id="spark1">
                            <i class="fab fa-product-hunt fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <div class="desc">Products</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="113">{{ $product }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{ url('area_person') }}">

                    <div class="widget widget-tile">
                        <div class="chart sparkline" id="spark2">
                            <i class="fas fa-street-view fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <div class="desc">Sales man</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="80" data-suffix="%">{{ $area_person }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{ url('employee') }}">

                    <div class="widget widget-tile">
                        <div class="chart sparkline" id="spark3">
                            <i class="fas fa-people-carry fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <div class="desc">Employees</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="532">{{ $employees }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{ url('supplier') }}">
                    <div class="widget widget-tile">
                        <div class="chart sparkline" id="spark4">
                            <i class="fas fa-users fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <div class="desc">Companies</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="113">{{ $supplier }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{ url('companies') }}">
                    <div class="widget widget-tile">
                        <div class="chart sparkline" id="spark1">
                            <i class="fas fa-user-tag fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <div class="desc">Customers</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="113">{{ $company }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{ url('bank_acc') }}">
                    <div class="widget widget-tile">
                        <div class="chart sparkline" id="spark2">
                            <i class="fas fa-university fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <div class="desc">Bank Accounts</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="80" data-suffix="%">{{ $bank_acc }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{ url('zero-products') }}">
                    <div class="widget widget-tile">
                        <div class="chart sparkline" id="spark2">
                            <i class="fab fa-product-hunt fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <div class="desc">Zero Qty Products</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="80" data-suffix="%">{{ $less_qty }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{url('customer_cheques')}}">
                    <div class="cheque_Upper_div widget widget-tile">
                        <div class="chart sparkline" id="spark4">
                            <i class="fas fa-money-check-alt fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <?php
                            $class = '';
                            if ($cheques) {
                                $class = 'cheque_div';
                            }
                            ?>
                            <div class="desc <?= $class; ?>">Customer Cheques</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{ $cheques }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <a class="text-dark" href="{{url('supplier_cheques')}}">
                    <div class="cheque_Upper_div widget widget-tile">
                        <div class="chart sparkline" id="spark4">
                            <i class="fas fa-money-check-alt fa-5x"></i>
                        </div>
                        <div class="data-info">
                            <?php
                            $class = '';
                            if ($suppliercheques) {
                                $class = 'cheque_div';
                            }
                            ?>
                            <div class="desc <?= $class; ?>">Company Cheques</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{ $suppliercheques }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

@endsection
