@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Cash In Hand</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Cash Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong>{{ Session::get('error') }}</strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">
                        <form>
                            <div class="row pb-3">
                                <div class="col-lg-5">
                                    <label>From Date</label>
                                    <div>
                                        <input value="<?= isset($_GET['from_date'])?$_GET['from_date']:date('Y-m-d', strtotime('-1 month')) ?>" type="date" class="form-control form-control-sm"  name="from_date" required="">
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <label>From Date</label>
                                    <div class="">
                                        <input value="<?= isset($_GET['to_date'])?$_GET['to_date']:date('Y-m-d') ?>" type="date" class="form-control form-control-sm" name="to_date" required="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <p>&nbsp;</p>
                                    <input type="submit" class="btn btn-primary" value="Search">
                                </div>
                            </div>
                        </form>
                        <b>{{ ($accountDetails->count() != 0)?'Total Cash : '.$cashInHand->total_amount.' RS/-':'Cash In Hand' }} </b>
                        <div class="tools dropdown">

                            <?php if ($accountDetails->count() == 0) { ?>
                                <a href="{{ route('cash_in_hand.create') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add New Cash</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Bank</th>
                                    <th>Customer</th>
                                    <th>Company</th>
                                    <th>Expense</th>
                                    <th>Cash In</th>
                                    <th>Cash Out</th>
                                    <th>Remaining</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accountDetails as $product)
                                <tr class="<?= ($product->is_deleted) ? 'text-white bg-danger' : ''; ?>">
                                    <td data-title="S.No">{{ $product->id }}</td>
                                    <td data-title="Bank"><?php
                                        if ($product->bank_id) {
                                            if (isset($banks[$product->bank_id])) {
                                                echo $banks[$product->bank_id];
                                            } else {
                                                echo "bank Deleted";
                                            }
                                        }
                                        if ($product->transfer_from_bank_id) {
                                            if (isset($banks[$product->transfer_from_bank_id])) {
                                                echo $banks[$product->transfer_from_bank_id];
                                            } else {
                                                echo "bank Deleted";
                                            }
                                        }
                                        ?></td>
                                    <td data-title="Customer"><?php
                                        if ($product->company_id == '0') {
                                            echo '<span class="text-danger">Walking Customer</span>';
                                        }
                                        if ($product->company_id) {
                                            if (isset($companies[$product->company_id])) {
                                                echo $companies[$product->company_id];
                                            }
                                        }
                                        ?></td>
                                    <td data-title="Company">
                                        <?php
                                        if ($product->supplier_id) {
                                            if (isset($suppliers[$product->supplier_id])) {
                                                echo $suppliers[$product->supplier_id];
//                                    echo ;
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Expense">
                                        <?php
                                        if ($product->parent_id) {
                                            if ($expenses[$product->parent_id]) {
                                                echo $expenses[$product->parent_id];
//                                    echo ;
                                            }
                                        }

                                        if ($product->zakat_person_id) {
                                            if (isset($personal_expenses[$product->zakat_person_id])) {
                                                echo $personal_expenses[$product->zakat_person_id] . ' (P.Expense)';
//                                    echo ;
                                            } else {
                                                echo 'Person Deleted';
                                            }
                                        }
                                        ?>

                                    </td>
                                    <td data-title="Cash In">{{ $product->deposit }}</td>
                                    <td data-title="Cash Out">{{ $product->withdrawal }}</td>
                                    <td data-title="Remaining">{{ $product->remaining }}</td>
                                    <td data-title="Description">{{ $product->description }}</td>
                                    <td data-title="Date">@if($product->created_at)
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('d-m-Y') }}
                                        @endif
                                    </td>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection