@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Bank Account Detail</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Accounts</li>
                <li class="breadcrumb-item active">Account Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Accounts Detail
                        <div class="tools dropdown">
                            <a href="{{ url('bank_acc') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-chevron-left"></i> Go back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Account No</th>
                                    <th>Current Amount</th>
                                    <th>Deposit</th>
                                    <th>Withdraw</th>
                                    <th>Remaining</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accountDetails as $product)
                                <tr>
                                    <td>{{ $product->bank_name }}</td>
                                    <td>{{ $product->account_no }}</td>
                                    <td>{{ $product->current_amount }}</td>
                                    <td>{{ $product->deposit }}</td>
                                    <td>{{ $product->withdrawal }}</td>
                                    <td>{{ $product->remaining }}</td>
                                    <td>{{ $product->description }}</td>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection