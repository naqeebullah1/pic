@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Head Expenses</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Head Expenses List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header"> Head Expenses List
                        <div class="tools dropdown">
                            <a href="{{ route('employee.create') }}" class="btn btn-rounded btn-space btn-primary" data-toggle="modal" data-target="#form-bp1"><i class="fas fa-plus-circle"></i> Add Head</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Head Expense</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($head_expenses as $head_expense)
                                <tr>
                                    <td data-title="ID">{{ $head_expense->id }}</td>
                                    <td data-title="Head Expense">{{ $head_expense->expenses }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a href="{{ route('expenses.edit',['id' => $head_expense->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('expenses.destroy',['id' => $head_expense->id]) }}" style="display:inline;">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                            </form>
                                        </td>
                                    <?php } ?>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Head Expenses Modal -->

        <div class="modal fade colored-header colored-header-primary" id="form-bp1" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">Add New Head</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
                    </div>
                    <form method="post" action="{{ route('expenses.store') }}">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Head Name.</label>
                                <div>
                                    <input class="form-control" type="text" name="head_expense" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary md-close" type="button" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary md-close" type="button">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')

@endsection