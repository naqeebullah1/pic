@extends('layouts.admin')

@section('style')


<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Expenses</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Expenses List</li>
            </ol>
        </nav>
    </div>

    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Expenses List
                        <div class="tools dropdown">
                            <a href="{{ route('employee.create') }}" class="btn btn-rounded btn-space btn-primary" data-toggle="modal" data-target="#expense"><i class="fas fa-plus-circle"></i> Add Expenses</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Expense Name</th>
                                    <th>Description</th>
                                    <th>Payment Type</th>
                                    <th>Amount Paid</th>
                                    <th>Bank Name</th>
                                    <th>Date</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th> 
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expneses as $expnese)

                                <tr>
                                    <td data-title="Expense Name">{{ $expnese->expenses}}</td>
                                    <td data-title="Description">{{ $expnese->description }}</td>
                                    <td data-title="Payment Type">{{ $expnese->payment_type }}</td>
                                    <td data-title="Amount">{{ $expnese->amount }}</td>
                                    <td data-title="Bank Name">
                                        @if($expnese->payment_type == 'bank')
                                        {{ str_replace("|"," | ",$banks[$expnese->bank_id]) }}
                                        @else
                                        cash
                                        @endif
                                    </td>
                                    <td>@if($expnese->date)
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d', $expnese->date)->format('d-m-Y') }}
                                        @endif
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td> 
                                            <a onclick="return confirm('are you sure')" href="{{ route('expense/delete', ['id' => $expnese->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                Delete
                                            </a>

                                        </td>
                                    <?php } ?>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Head Expenses Modal -->
    </div>
</div>

<!-- Add Expenses Modal -->
<div id="expense" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--<div class="modal fade colored-header colored-header-primary" id="expense" role="dialog">-->
    <div class="modal-dialog modal-lg">
        <div class="modal-content " style="max-width: 100%">
            <div class="modal-header modal-header-colored">
                <h3 class="modal-title">Add Expense</h3>
                <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
            </div>
            <form action="{{ route('subexpenses.store') }}" method="post">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Select Head.</label>
                        <div>

                            <select name="head" class="select2" required>
                                <option value="">Select Head</option>
                                @foreach($head_expenses as $head_expense)
                                <option value="{{ $head_expense->id }}">{{ $head_expense->expenses }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Expense Date</label>
                        <div>
                            <input name="created_at" class="form-control" type="date" value="<?= date('Y-m-d') ?>" required>
                        </div>
                    </div>
                    <table class="table table-bordered no-more-tables table-striped table-sm multiple-expenses">
                        <thead>

                            <tr>
                                <th>Title</th>
                                <th>Amount</th>
                                <th>Description</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-title="Title">
                                    <input name="expense_title[]" class="form-control p-2" required placeholder="Title" style="height:3rem;">
                                </td>
                                <td data-title="Amount">
                                    <input class="form-control p-2" type="text" name="pay[]" required placeholder="Amount"style="height:3rem;">
                                </td>
                                <td data-title="Description">
                                    <input class="form-control p-2" type="text" name="desc[]" required placeholder="Description" style="height:3rem;">
                                </td>
                                <td data-title="Delete">
                                    <button type="button" class="btn btn-danger btn-sm delete-row"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-success btn-sm add-more">Add More</button>
                    </div>

                    <div class="form-group">
                        <label>Payment Type</label>
                        <div>
                            <select name="pay_type" class="form-control" id="pay" required>
                                <option value="">Select Payment Type</option>
                                <option value="bank">Bank</option>
                                <option value="cash" selected="">Cash</option>
                            </select>
                        </div>
                    </div>
                    <div id="bank">

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary md-close" type="button" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary md-close" type="button">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')



<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>



<script>
                                            $('.add-more').click(function () {
                                                var html = '<tr>\n\
                            <td data-title="Title">\n\
                                <input name="expense_title[]" class="form-control p-2" required placeholder="Title" style="height:3rem;">\n\
                            </td>\n\
                            <td data-title="Amount">\n\
                                <input class="form-control p-2" type="text" name="pay[]" required placeholder="Amount"style="height:3rem;">\n\
                            </td>\n\
                            <td data-title="Description">\n\
                                <input class="form-control p-2" type="text" name="desc[]" required placeholder="Description" style="height:3rem;">\n\
                            </td>\n\
<td data-title="Delete">\n\
                                <button type="button" class="btn btn-danger btn-sm delete-row"><i class="fas fa-trash"></i></button>\n\
                            </td>\n\
                        </tr>';
                                                $('.multiple-expenses').append(html);
                                            });
                                            var pay_type = $('#pay').val();
                                            var csrf = $('meta[name="csrf_token"]').attr('content');
//alert(pay_type);

                                            $.ajax({
                                                url: '{{ route("payment-type") }}',
                                                type: 'POST',
                                                data: {
                                                    pay_type: pay_type,
                                                    '_token': csrf
                                                },
                                                success: function (data) {
                                                    $('#bank').html(data);
                                                }
                                            });
                                            $('body').on('change', '#pay', function () {
                                                var pay_type = $('#pay').val();

                                                var csrf = $('meta[name="csrf_token"]').attr('content');

                                                $.ajax({
                                                    url: '{{ route("payment-type") }}',
                                                    type: 'POST',
                                                    data: {
                                                        pay_type: pay_type,
                                                        '_token': csrf
                                                    },
                                                    success: function (data) {
                                                        $('#bank').html(data);
                                                    }
                                                });
                                            });
                                            $('body').on('click', '.delete-row', function () {
                                                $(this).closest('tr').remove();
                                            });

</script>
@endsection