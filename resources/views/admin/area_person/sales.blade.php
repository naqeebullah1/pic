@extends('layouts.admin')

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />

<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        .invoice-table td:nth-of-type(1){width: auto;}
        .invoice-table td:nth-of-type(1):before { content: "P-Id";}
        .invoice-table td:nth-of-type(2):before { content: "Name"; }
        .invoice-table td:nth-of-type(4):before { content: "Sale Price"; }
        .invoice-table td:nth-of-type(5):before { content: "Current Qty"; }
        .invoice-table td:nth-of-type(6):before { content: "Qty"; }
        .invoice-table td:nth-of-type(8):before { content: "Total Amount"; }
        .invoice-table td:nth-of-type(9):before { content: "Action"; }
       
        .table-description td:nth-of-type(1):before { content: "Description"; }
        .table-description td:nth-of-type(2):before { content: "Vr No"; }
        .table-description td:nth-of-type(3):before { content: "Date"; }
        
        
    }
</style>

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Sales man</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('area_person')}}">Sales man</a></li>

                <li class="breadcrumb-item active">Sales man Sale</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong>{{ Session::get('error') }}</strong></div>
                </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-header"> Sale man: <b> {{ $supplier->name }}</b></div>
                            <div class="card-body">
                                <hr>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-2 col-form-label text-sm-center" style="color:black;font-size:20px">Select Product</label>
                                    <div class="col-12 col-sm-8 col-lg-4">
                                        <select class="select2 product" name='product[]' data-placeholder="Select Product">
                                            <option value="">Select Product </option>
                                            @foreach($products as $product)
                                            <option value="{{ $product->id }}"><?php echo ($product->qty) ? $product->product_name . ' | ' . ($product->qty) : $product->product_name; ?></option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <br>
                                <hr>
                                <br>

                                <form action="{{ route('sale_man/store_sale',$supplier->id) }}" method="post" id="saleForm">
                                    <input type="hidden" name="supplier_id" value="{{ $supplier->id }}">
                                    {{ csrf_field() }}
                                    <table class="table invoice-table table-striped">
                                        <thead class="border">
                                            <tr>
                                                <th>
                                                    P-ID
                                                </th>

                                                <th>
                                                    Product Name
                                                </th>
                                                <th class="d-none">
                                                    Whole Rate
                                                </th>
                                                <th>
                                                    Sale Price
                                                </th>
                                                <th>
                                                    Current Qty
                                                </th>
                                                <th>
                                                    Qty
                                                </th>
                                                <th class="d-none">
                                                    Discount (in %)
                                                </th>
                                                <th>
                                                    Total Amount
                                                </th>
                                                <th>
                                                    Delete
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="product_detail" class="border">
                                            <tr id="initial_row">
                                                <td colspan="9" class="text-center text-danger" style="font-size:16px;">
                                                    Please select an item from the list
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="row justify-content-end">
                                        <div class="col-lg-4">
                                            <h4 style="font-weight:bold">Summary</h4>

                                            <table class="table table-sm table-bordered" style="background-color:#eeeeee4d">
                                                <tr>
                                                    <th>
                                                        <label> Total Amount</label>
                                                        <div>
                                                            <input class="form-control line_cost_total form-control-sm" type="text" name="line_cost_total" readonly value="0">
                                                        </div>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <label>Discount</label> 
                                                        <div>
                                                            <input class="form-control discount form-control-sm" type="text" value="0" name="discount" required>
                                                        </div>
                                                    </th>
                                                </tr>
                                                <tr>

                                                    <th>
                                                        <label> Net Amount</label>

                                                        <div>
                                                            <input class="form-control total form-control-sm" type="text" readonly value="0" name="net_amount">
                                                        </div>
                                                    </th>

                                                </tr>
                                                <tr>

                                                    <th>
                                                        <label> Paid Amount</label>

                                                        <div>
                                                            <input class="form-control paidamount form-control-sm" type="number" min="0" value="0" name="paidamount" required>
                                                        </div>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <label> Due Amount</label>
                                                        <div>
                                                            <input class="form-control due_amount form-control-sm" type="text" readonly value="0" name="due_amount">
                                                        </div>
                                                    </th>
                                                </tr>
                                                <tbody>
                                                       <!--                                            <tr>
                                                                                                       <td>
                                                                                                           <div class="form-group pt-2">
                                                                                                               <input class="form-control line_cost_total form-control-sm" type="text" name="line_cost_total" readonly value="0">
                                                                                                           </div>
                                                                                                       </td>
                                                                                                       <td>
                                                                                                           <div class="form-group pt-2">
                                                                                                               <input class="form-control discount form-control-sm" type="text" value="0" name="discount" required>
                                                                                                           </div>
                                                                                                       </td>
                                                                                                       <td>
                                                                                                           <div class="form-group pt-2">
                                                                                                               <input class="form-control total form-control-sm" type="text" readonly value="0" name="net_amount">
                                                                                                           </div>
                                                                                                       </td>
                                                                                                       <td>
                                                                                                           <div class="form-group pt-2">
                                                                                                               <input class="form-control paidamount form-control-sm" type="number" min="0" value="0" name="paidamount" required>
                                                                                                           </div>
                                                                                                       </td>
                                                                                                       <td>
                                                                                                           <div class="form-group pt-2">
                                                                                                               <input class="form-control due_amount form-control-sm" type="text" readonly value="0" name="due_amount">
                                                                                                           </div>
                                                                                                       </td>
                                                                                                   </tr>-->
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <table class="table table-description">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Description
                                                </th>
                                                <th>
                                                    VR NO
                                                </th>
                                                <th>
                                                    Date
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input name="description" class="form-control form-control-sm" type="text">
                                                </td>
                                                <td>
                                                    <input class="form-control form-control-sm" type="text" name="vr_no">
                                                </td>
                                                <td>
                                                    <input class="form-control form-control-sm" type="date" name="created_at" value="{{date('Y-m-d')}}">
                                                </td>
                                            </tr>
                                        </tbody>

                                    </table>
                                    <div class="col text-center">
                                        <button name="submit" value="submit" class="btn btn-space btn-primary">
                                            Submit
                                        </button>
                                        <button name="print" value="print" class="btn btn-space btn-primary">
                                            Submit and print
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>


<script>
$('#add_more_exp').click(function () {
    var html = '<tr>\n\
                                                        <td>\n\
                                                            <div>\n\
                                                                <input class="form-control form-control-sm line_cost" type="text" name="extra_title[]">\n\
                                                            </div>\n\
                                                        </td>\n\
                                                        <td>\n\
                                                            <div>\n\
                                                                <input class="form-control form-control-sm" type="text" name="extra_description[]">\n\
                                                            </div>\n\
                                                        </td>\n\
                                                        <td>\n\
                                                            <button class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>\n\
                                                        </td>\n\
                                                    </tr>';
    $('#extra_exp_body').append(html);
});
// Add Product To Table
$('body').on('focus', '.unit_price', function () {
//    $('.unit_price').focus(function () {
    $(this).val('');
});
$('body').on('focus', '.qty', function () {
//    $('.unit_price').focus(function () {
    $(this).val('');
});

$('body').on('change', '.product', function () {
    var channel = $(this);
    var product = $(this).val();


    $.ajaxSetup({
        beforeSend: function (xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf_token"]').attr('content'));
            }
        },
    });
    $.ajax({
        data: {
            id: product
        },
        type: 'POST',
        url: "{{ route('get-product-details') }}",
        success: function (return_data) {
            var unitPrice = return_data.product_sale_price;
            if ($('select[name=sale_type]').val() == 'whole') {
                unitPrice = return_data.product_whole_sale_price;
            }
            $('#initial_row').remove();
            $('#product_detail').prepend('<tr><td width="5%"><div class="form-group pt-2"><input class="form-control form-control-sm" type="text" readonly name="product_id[]" value="' + return_data.id + '"></div></td><td><div class="form-group pt-2">' + return_data.product_name + '</div></td><td class="d-none"><div class="form-group pt-2"><input class="form-control whole_price form-control-sm" type="text" name="whole_price[]" value="' + return_data.product_whole_sale_price + '"></div></td><td><div class="form-group pt-2"><input class="form-control unit_price form-control-sm" type="text" name="retail_price[]" value="' + return_data.product_sale_price + '"></div></td><td><div class="form-group pt-2"><input class="form-control form-control-sm" type="text" readonly name="current_qty[]" value="' + return_data.stock_qty + '"></div></td><td><div class="form-group pt-2"><input class="form-control qty form-control-sm" type="number" name="qty[]" value="0"></div></td><td class="d-none"><div class="form-group pt-2"><input class="form-control qty form-control-sm" type="text"  name="discount_per_item[]" value="0"></div></td><td><div class="form-group pt-2"><input class="form-control line_cost form-control-sm" type="text" readonly name="line_cost[]" value=""></div></td><td><a href="javascript:;" class="btn btn-danger delete btn-sm"><i class="fa fa fa-trash"></i></a></td></tr>');
        },
    });
});
$('body').on('change', 'select[name=sale_type]', function () {
    var saleType = "unit_price";
    if ($('select[name=sale_type]').val() == 'whole') {
        saleType = "whole_price";
    }
    $('#product_detail  > tr').each(function () {
        change_qty_cost($(this));
    });
});
// Add Quantity to Product
$('body').on('keyup', '.qty', function () {
    change_qty_cost($(this).closest('tr'));
});
$('body').on('keyup', '.line_cost', function () {
    update_calculation();
});
$('body').on('keyup', '.unit_price', function () {
    change_qty_cost($(this).closest('tr'));
});
$('body').on('keyup', '[name="discount_per_item[]"]', function () {
    change_qty_cost($(this).closest('tr'));
//alert($(this).val());
});
$('body').on('keyup', '.discount', function () {
    update_calculation();
});
$('body').on('keyup', '.paidamount', function () {
    update_calculation();
});

function change_qty_cost(row) {
    var saleType = "unit_price";
    if ($('select[name=sale_type]').val() == 'whole') {
        saleType = "whole_price";
    }
    var discount = row.find('[name="discount_per_item[]"]').val() / 100;
//    console.log(discount);
    if (row.find('.' + saleType).val() >= 0 && row.find('.qty').val() >= 0) {
        var unit_price = +row.find('.' + saleType).val();
        var qty = +row.find('.qty').val();
        if (row.find('.' + saleType).val() == 0 || row.find('.qty').val() == 0) {
            var line_cost = row.find('.line_cost').val(0);
        } else {
            var actual_val = unit_price * qty;
//alert(unit_price);
//alert(qty);
//alert(actual_val);
            if (discount != 0) {

                actual_val = actual_val - (actual_val * discount);
            }
            var line_cost = row.find('.line_cost').val(actual_val);
        }
        update_calculation();
    }
}

// Delete Product from List
$('body').on('click', '.delete', function () {
    var rowCount = $('#product_detail tr').length;
    if (rowCount == 7) {
        message("error", 'You can\'t delete because there are must be at least one row');
    } else {
        var row = $(this).closest('tr');
        var line_cost = row.find('.line_cost').val();
        var cost = $('.line_cost_total').val();
        $('.line_cost_total').val(cost - line_cost);

        $(this).closest('tr').remove();

        update_calculation();
    }
});

// calculation of line_cost_total, discount, total, paidamount and due amount
function update_calculation() {

    var discount = $(".discount").val();
    // if the discount field is null then put in discount 0
    if (discount == '') {
        $(".discount").val(0);
    }

    var paidamount = $(".paidamount").val();
    // if the paidamount field is null then put in paidamount 0
    if (paidamount == '') {
        $(".paidamount").val(0);
    }

    var line_cost = $(".line_cost");
    var total_line_cost = 0;
    for (var i = 0; i < line_cost.length; i++) {
        if ($.isNumeric($(line_cost[i]).val()))
            total_line_cost += parseInt($(line_cost[i]).val());
    }

    $(".line_cost_total").val(total_line_cost);
    $(".total").val(total_line_cost);

    var line_cost_total = parseInt($(".line_cost_total").val());

    var discount = parseInt($(".discount").val());


    $(".total").val(line_cost_total - discount);

    var total = parseInt($(".total").val());

    var paidamount = parseInt($(".paidamount").val());

    $(".due_amount").val(total - paidamount);
}
</script>

@endsection