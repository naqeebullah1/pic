@extends('layouts.admin')

@section('style')

<style>
    .currentMonth{
        font-weight: bold;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Total Stock</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>

                <li class="breadcrumb-item active">Stock</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Stock List

                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <!--<th>Sale Details</th>-->
                                    <th>Stock Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <tr>
                                    <td data-title="Product Name">{{ $product->product_name }}</td>
                                    <td data-title="Quantity">{{ $product->qty }}</td>
                                   
                                    <td data-title="Stock Details">
                                        <a href="{{ route('stock-details', ['id'=>$product->id]) }}" class="btn btn-outline-success btn-space btn-sm">Stock Details</a>
                                    </td>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="addCityModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Add Sales man</h4>
                    </div>
                    <form method="POST" id="advancePaymentForm" action="{{route('area_person.store')}}">
                        {{csrf_field()}}
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12"></label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Person Name" name="name" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12"></label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Cnic" name="cnic" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Contact" name="contact" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12">Email</label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Email" name="email" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12">Address</label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Address" name="address" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">

                            <div class="form-group row">
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <!--<label class="col-lg-12">City</label>-->
                                    <select name="city" class="form-control" required>
                                        <option value="">Select City</option>
                                        @foreach($cities as $city)
                                        <option value="{{$city->city}}">{{ucwords($city->city)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="advancePaymentForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@section('script')

<script type="text/javascript">

function editCity(cityId) {
    $('input[name=city_id]').val(cityId);
    $.ajax({
        url: '{{ route("fetch_city") }}',
        type: 'Get',
        data: {id: cityId},
        success: function (data) {
            $('input[name=city]').val(data);
            $('#editCityModal').modal('show');
        }
    });
}

</script>

@endsection