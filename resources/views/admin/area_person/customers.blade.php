@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Customers List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Customers List

                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Type</th>
                                    <th>Payment</th>
                                    <th>Sale History</th>
                                    <!--<th>Return Sale</th>-->
                                    <!--<th>Return History</th>-->
                                    <th>Ledger</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customers as $product)
                                <tr>
                                    <td data-title="Name">{{ $product->name }}</td>
                                    <td data-title="Contact">{{ $product->contact }}</td>
                                    <td data-title="Type">{{ ucwords($product->type) }}</td>

                                    <td data-title="Sale History">
                                        <a href="{{ route('customer-payment', ['id' => $product->id]) }}" class="btn btn-outline-secondary btn-space btn-sm">
                                            Pay
                                        </a>
                                    </td>
                                    <td data-title="Ledger">
                                        <a href="{{ route('sale-details', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View
                                        </a>
                                    </td>
<!--                                    <td>
                                        <a href="{{ route('return_sale.show', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Return
                                        </a>
                                    </td>-->
<!--                                    <td>
                                        <a href="{{ route('return_sale.edit', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            <a href="#" class="btn btn-outline-success btn-space btn-sm">
                                            View
                                        </a>
                                    </td>-->
                                    <td>
                                        <a href="{{ route('saleman.customer_ledger', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View
                                        </a>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="requestPayment">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Request For Payment</h3>
                        <a class="close" data-dismiss="modal">&times;</a>
                    </div>
                    <div class="modal-body">
                        <form method="post" id="messageForm" action="{{route('company/request_payment')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Enter Message</label>
                                <input type="hidden" name="customer_id">
                                <textarea class="form-control" name="message"></textarea> 
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-outline-success">Send</button>
                                <button class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
function requestPayment(num) {
    $('input[name=customer_id]').val(num);
    $('#requestPayment').modal('show');
}

</script>

@endsection