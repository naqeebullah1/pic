@extends('layouts.admin')

@section('style')

<style>
    .currentMonth{
        font-weight: bold;
    }
      @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Product"; }
        td:nth-of-type(2):before { content: "Qty"; }
        td:nth-of-type(3):before { content: "Invoice No"; }
        td:nth-of-type(4):before { content: "Description"; }
        td:nth-of-type(5):before { content: "Date"; }

    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Stock Details</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('area_person')}}">Sales man</a></li>
                <li class="breadcrumb-item"><a href="{{url('sale_man/sales/history',Request::segment(5))}}">Stock</a></li>
                <li class="breadcrumb-item active">Stock Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Product Stock Details

                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Invoice No</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <tr>
                                    <td>{{ $product->product_name }}</td>
                                    <td>{{ $product->qty }}</td>
                                    <td>{{ $product->vr_no }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('d-m-Y') }}</td>
                               </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection

@section('script')

<script type="text/javascript">

function editCity(cityId) {
    $('input[name=city_id]').val(cityId);
    $.ajax({
        url: '{{ route("fetch_city") }}',
        type: 'Get',
        data: {id: cityId},
        success: function (data) {
            $('input[name=city]').val(data);
            $('#editCityModal').modal('show');
        }
    });
}

</script>

@endsection