@extends('layouts.admin')

@section('style')

<style>
    .currentMonth{
        font-weight: bold;
    }
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Name"; }
        td:nth-of-type(2):before { content: "Contact"; }
        td:nth-of-type(3):before { content: "Email"; }
        td:nth-of-type(4):before { content: "Address"; }
        td:nth-of-type(5):before { content: "Add Stock"; }
        td:nth-of-type(6):before { content: "Stock History"; }
        td:nth-of-type(7):before { content: "Sales"; }
        td:nth-of-type(8):before { content: "Recovery"; }
        td:nth-of-type(9):before { content: "Action"; }

    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Sales man</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>

                <li class="breadcrumb-item active">Sales man</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('error'))
                @foreach(Session::get('error')->all() as $e)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message">
                        <strong> {{ $e }} </strong>
                        <br>
                    </div>
                </div>
                @endforeach
                @endif
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table p-1">
                    <div class="dropdown d-md-none">
                        <button type="button" class="float-right btn btn-outline-default btn-sm dropdown-toggle " data-toggle="dropdown" id="5b4f95e9a64b5" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div style="border:1px solid #ccc;" class="dropdown-menu" aria-labelledby="5b4f95e9a64b5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(63px, 31px, 0px);">
                            <a href="{{ url('area_person?status=1') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active text-white' : '' ?>">Active Salesmen</a>
                            <a href="{{ url('area_person?status=0') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active text-white' : '' ?>">Inactive Salesmen</a>
                        </div>
                    </div>

                    <!-- button group for desktop -->
                    <div class="d-none d-md-flex btn-group">
                        <a href="{{ url('area_person?status=1') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active' : '' ?>">Active Salesmen</a>
                        <a href="{{ url('area_person?status=0') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active' : '' ?>">Inactive Salesmen</a>
                    </div>
                    <div class="card-header">Sales men List
                        <div class="tools dropdown">
                            <a href="#" data-toggle="modal" data-target="#addCityModal"  class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add Sales man</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Add Stock</th>
                                    <th>Stock History</th>
                                    <th>Sales</th>
                                    <th>Recovery</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->contact }}</td>
                                    <td>{{ $product->email }}</td>
                                    <td>{{ $product->address }}, {{ $product->city }}</td>
                                    <td><a class="btn btn-outline-success btn-space btn-sm" href="{{url('sale_man/sales',$product->id)}}">Add Stock</a></td>
                                    <td><a class="btn btn-outline-success btn-space btn-sm" href="{{url('sale_man/sales/history',$product->id)}}">Stock History</a></td>
                                    <td><a class="btn btn-outline-success btn-space btn-sm" href="{{url('sale_man/sales/invoices',$product->id)}}">Sales</a></td>
                                    <td><a class="btn btn-outline-success btn-space btn-sm" href="{{url('sale_man/recovery',$product->id)}}">Recovery</a></td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td>
                                            <a href="{{ route('area_person.edit', ['id'=>$product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <!--                                            <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('area_person.destroy', ['id'=>$product->id]) }}" style="display:inline;">
                                                                                            {{ csrf_field() }}
                                                                                            {{ method_field('DELETE') }}
                                                                                            <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                                                                        </form>-->
                                            <?php
                                            if ($product->status) {
                                                ?>
                                                <a href="{{route('change.saleman.status',['id'=>$product->id])}}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Move to Inactive
                                                </a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="{{route('change.saleman.status',['id'=>$product->id])}}" class="btn btn-outline-success btn-space btn-sm">
                                                    Move to Active
                                                </a>
                                                <?php
                                            }
                                            ?>

                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="addCityModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Add Sales man</h4>
                    </div>
                    <form method="POST" id="advancePaymentForm" action="{{route('area_person.store')}}">
                        {{csrf_field()}}
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12"></label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Person Name" name="name" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12"></label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Cnic" name="cnic" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Contact" name="contact" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12">Email</label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Email" name="email" type="text" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12">Email</label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Password" name="password" type="password" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <!--<label class="col-lg-12">Address</label>-->
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input placeholder="Address" name="address" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="padding:10px">

                            <div class="form-group row">
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <!--<label class="col-lg-12">City</label>-->
                                    <select name="city" class="form-control" required>
                                        <option value="">Select City</option>
                                        @foreach($cities as $city)
                                        <option value="{{$city->city}}">{{ucwords($city->city)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="advancePaymentForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@section('script')

<script type="text/javascript">

    function editCity(cityId) {
        $('input[name=city_id]').val(cityId);
        $.ajax({
            url: '{{ route("fetch_city") }}',
            type: 'Get',
            data: {id: cityId},
            success: function (data) {
                $('input[name=city]').val(data);
                $('#editCityModal').modal('show');
            }
        });
    }

</script>

@endsection