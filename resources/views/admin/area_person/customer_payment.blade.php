@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>

                <li class="breadcrumb-item active">Customer Payment</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Customer Payment</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('saleman.save_payment') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Name</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" name="name" type="text" placeholder="Company Name" required value="{{ $supplier->name }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Contact No</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="contact_no" placeholder="Add Contact No" required value="{{ $supplier->contact }}" readonly>
                                </div>
                                <input type="hidden" name="customer_id" value="{{ $supplier->id }}">
                            </div>

                            <div class="form-group row d-none">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Dues</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="dues" required value="{{ $supplier->dues }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Description</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Payment Type</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select class="form-control" name="pay_from" id="pay" required>
                                        <option value="">Select Payment Type</option>
                                        <option value="bank">Bank</span></option>
                                        <option value="cash" selected>Cash</option>
                                        <!--<option value="cheque">Cheque</option>-->
                                    </select>
                                </div>
                            </div>
                            <div id="bank">

                            </div>
                            <div id="cheque">

                            </div>
                            <div id="dollor_conversion">
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Amount</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="pay" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Discount</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="discount" value="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    var pay_type = $('#pay').val();

    var csrf = $('meta[name="csrf_token"]').attr('content');

    $.ajax({
        url: '{{ route("company/payment_type") }}',
        type: 'POST',
        data: {
            pay_type: pay_type,
            '_token': csrf
        },
        success: function (data) {
            // console.log(data)
            $('#bank').html(data);
            $('#cheque').html('');
        }
    });
    $('body').on('change', '#pay', function () {
        var pay_type = $('#pay').val();

        var csrf = $('meta[name="csrf_token"]').attr('content');

        $.ajax({
            url: '{{ route("saleman.payment-type") }}',
            type: 'POST',
            data: {
                pay_type: pay_type,
                '_token': csrf
            },
            success: function (data) {
                // console.log(data)
                $('#bank').html(data);
                $('#cheque').html('');
            }
        });
    });
    $('body').on('change', '#transfer_to', function () {
        var pay_type = $(this).val();
        var csrf = $('meta[name="csrf_token"]').attr('content');
        $.ajax({
            url: '{{ route("get_cheque_fields_customer") }}',
            type: 'POST',
            data: {
                pay_type: pay_type,
                '_token': csrf
            },
            success: function (data) {
                // console.log(data)
                $('#cheque').html(data);
            }
        });
    });
</script>
@endsection