@extends('layouts.admin')

@section('style')

<style>
    .currentMonth{
        font-weight: bold;
    }
      @media
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Id"; }

        td:nth-of-type(2):before { content: "Date"; }
        td:nth-of-type(3):before { content: "Invoice No"; }
        td:nth-of-type(4):before { content: "Description"; }
        td:nth-of-type(5):before { content: "Total Stock"; }

        td:nth-of-type(6):before { content: "Assign Stock"; }
        td:nth-of-type(7):before { content: "Sale Stock"; }
        td:nth-of-type(8):before { content: "Return Stock"; }
        td:nth-of-type(9):before { content: "Remaining"; }



    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Stock Details</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('area_person')}}">Sales man</a></li>
                <li class="breadcrumb-item"><a href="{{url('sale_man/sales/history',Request::segment(5))}}">Stock</a></li>
                <li class="breadcrumb-item active">Stock Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Product Stock Details

                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Date</th>

                                    <th>Vr No</th>
                                    <th>Description</th>
                                    <th>Total Stock</th>
                                    <th>Assign Stock</th>
                                    <th>Sale Stock</th>
                                    <th>Return Stock</th>
                                    <th>Remaining</th>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->date)->format('d-m-Y') }}</td>

                                    <td>{{ $product->invoice_no }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>{{ $product->total_stock }}</td>
                                    <td>{{ $product->assign_stock }}</td>
                                    <td>{{ $product->sale_stock }}</td>
                                    <td>{{ $product->return_stock }}</td>
                                    <td>{{ $product->remaining}}</td>

                               </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection

@section('script')

<script type="text/javascript">

function editCity(cityId) {
    $('input[name=city_id]').val(cityId);
    $.ajax({
        url: '{{ route("fetch_city") }}',
        type: 'Get',
        data: {id: cityId},
        success: function (data) {
            $('input[name=city]').val(data);
            $('#editCityModal').modal('show');
        }
    });
}

</script>

@endsection
