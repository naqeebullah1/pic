@extends('layouts.admin')

@section('style')
<style>
    @media
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Saleman"; }
        td:nth-of-type(2):before { content: "Product"; }
        td:nth-of-type(3):before { content: "Qty"; }
        td:nth-of-type(4):before { content: "Sale Details"; }
        td:nth-of-type(5):before { content: "Details"; }
        td:nth-of-type(6):before { content: "Action"; }

    }

</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Sales man</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('area_person')}}">Sales man</a></li>
                <li class="breadcrumb-item active">Sales man Stock</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Sales man Stock
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Sale man</th>
                                    <th>Product</th>
                                    <th>Qty</th>
                                    <th>Sale Details</th>
                                    <th>Details</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $ledger)
                                <tr>
                                    <td data-title="Saleman">{{ $ledger->name }}</td>
                                    <td data-title="Product">{{ $ledger->product_name }}</td>
                                    <td data-title="Quantity">{{ $ledger->qty }}</td>
                                    <td data-title="Sale Details">
                                        <a href="{{ route('saleman/sale-details', ['id' => $ledger->id,Request::segment(4)]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Stock Sale Details
                                        </a>
                                    </td>
                                    <td data-title="Details">

                                        <a href="{{ route('saleman/stock/details', ['id' => $ledger->id,Request::segment(4)]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Details
                                        </a>

                                        <a href="{{ route('saleman/stock/details', ['id' => $ledger->id,Request::segment(4)]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Ledger
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a onclick="return confirm('are You sure')" href="{{ route('sale/delete_sale', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                Delete
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection
