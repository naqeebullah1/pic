@extends('layouts.admin')

@section('style')


<style>
    .currentMonth{
        font-weight: bold;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Stock Details</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('my_dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('stock')}}">Stock</a></li>
                <!--<li class="breadcrumb-item active">Stock</li>-->
                <li class="breadcrumb-item active">Stock Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Product Stock Details

                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <tr>
                                    <td data-title="Product Name">{{ $product->product_name }}</td>
                                    <td data-title="Quantity">{{ $product->qty }}</td>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('d-m-Y') }}</td>
<!--                                    <td>
                                        <a href="{{ route('stock-details', ['id'=>$product->id]) }}" class="btn btn-outline-success btn-space btn-sm">Stock Details</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('area_person.edit', ['id'=>$product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                        <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('area_person.destroy', ['id'=>$product->id]) }}" style="display:inline;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                        </form>
                                    </td>-->
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection

@section('script')

<script type="text/javascript">

function editCity(cityId) {
    $('input[name=city_id]').val(cityId);
    $.ajax({
        url: '{{ route("fetch_city") }}',
        type: 'Get',
        data: {id: cityId},
        success: function (data) {
            $('input[name=city]').val(data);
            $('#editCityModal').modal('show');
        }
    });
}

</script>

@endsection