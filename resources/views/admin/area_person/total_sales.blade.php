@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customer</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>

                <li class="breadcrumb-item active">Customer Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Customer Ledger
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Sale</th>
                                    <th>Return Sale</th>
                                    <th>Discount</th>
                                    <th>Payment</th>
                                    <th>Remaining</th>                                    
                                    <th>Date</th>    
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Action</th> 
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer as $ledger)

                                <tr>
                                    <td>{{ $ledger->name }}</td>
                                    <td>
                                        <?php
                                        if ($ledger->sale != 0) {
                                            ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }}, 'sale')"><?= $ledger->sale ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->sale;
                                        }
                                        ?> 
                                    </td>
                                    <td>
                                        <?php
                                        if ($ledger->return_sale != 0) {
                                            ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }}, 'return_sale')"><?= $ledger->return_sale ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->return_sale;
                                        }
                                        ?> 
                                    </td>
                                    <td>{{ $ledger->discount }}</td>
                                    <td>{{ $ledger->payment }}</td>
                                    <td>{{ $ledger->remaining }}</td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <td>
                                            <?php
                                            if ($ledger->return_sale) {
                                                ?>
                                                <a onclick="" href="{{ route('return_sale.edit', ['id' => $ledger->company_id]) }}" class="btn btn-link btn-space btn-sm">
                                                    Go to return sale
                                                </a>
                                            <?php } elseif ($ledger->sale) {
                                                ?>
                                                <a onclick="" href="{{ route('sales.edit', ['id' => $ledger->company_id]) }}" class="btn btn-link btn-space btn-sm">
                                                    Go to sale
                                                </a>
                                                <?php
                                            } elseif ($ledger->payment) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ route('customer/delete_payment', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete
                                                </a>
                                                <?php
                                            } else {
                                                
                                            }
                                            ?>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="purchaseDetailModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content" style="max-width: 100% !important;">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;" id="changeHeader"></h4>
                    </div>

                    <div class="modal-body" id="purchaseDetailBody" style="padding:10px">
                    </div>
                    <div class="modal-footer" style="padding:10px">
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
                                                function fetchHistory(invoiceNo, type){
                                                $.ajax({
                                                data: {
                                                invoiceNo : invoiceNo,
                                                        type : type,
                                                },
                                                        type: 'GET',
                                                        url: "{{ route('sale/history') }}",
                                                        success: function(return_data) {
                                                        if (type == 'sale'){
                                                        $('#changeHeader').text('Sale Details');
                                                        } else{
                                                        $('#changeHeader').text('Return Sale Details');
                                                        }
                                                        $('#purchaseDetailBody').html(return_data);
                                                        $('#purchaseDetailModal').modal('show');
                                                        },
                                                });
                                                }
                                                
</script>

@endsection