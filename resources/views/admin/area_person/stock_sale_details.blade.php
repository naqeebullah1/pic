@extends('layouts.admin')

@section('style')
<style>
    @media
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Invoice No"; }
        td:nth-of-type(2):before { content: "Customer"; }
        td:nth-of-type(3):before { content: "Product"; }
        td:nth-of-type(4):before { content: "Unit Price"; }
        td:nth-of-type(5):before { content: "Purchase Qty"; }
        td:nth-of-type(6):before { content: "Total Amount"; }
        td:nth-of-type(7):before { content: "Discount"; }
        td:nth-of-type(8):before { content: "Sale Price"; }
        td:nth-of-type(9):before { content: "Origional Price"; }
        td:nth-of-type(10):before { content: "Net Amount"; }
        td:nth-of-type(11):before { content: "Paid"; }
        td:nth-of-type(12):before { content: "Description"; }
        td:nth-of-type(13):before { content: "Date"; }
        td:nth-of-type(14):before { content: "Action"; }
        tr{
            margin-bottom: 10px;
        }

    }

</style>

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Saleman</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('area_person')}}">Sales Man</a></li>
                <li class="breadcrumb-item"><a href="{{url('sale_man/sales/history',['id'=>Request::segment(4)])}}">Sale man Stock</a></li>
                <li class="breadcrumb-item active">Stock Sale History</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table pb-4">
                    <div class="card-header" style="font-weight: bold">Saleman Sale Details</div>
                    <div class="card-body" style="margin: 0 1rem;">
                        <form>
                            <div class="row pb-3">
                                <div class="col-lg-5">
                                    <label>From Date</label>
                                    <div>
                                        <input value="<?= isset($_GET['from_date'])?$_GET['from_date']:date('Y-m-d', strtotime('-1 month')) ?>" type="date" class="form-control form-control-sm"  name="from_date" required="">
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <label>From Date</label>
                                    <div class="">
                                        <input value="<?= isset($_GET['to_date'])?$_GET['to_date']:date('Y-m-d') ?>" type="date" class="form-control form-control-sm" name="to_date" required="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <p>&nbsp;</p>
                                    <input type="submit" class="btn btn-primary" value="Search">
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover table-fw-widget table-bordered" id="table1">
                            <thead>
                                <tr>
                                    <th>Invoice No</th>
                                    <th>Customer</th>
                                    <th>Product Name</th>
                                    <th>Unit Price</th>
                                    <th>Purchase Qty</th>
                                    <th>Total Amount</th>
                                    <th>Discount (%)</th>
                                    <th>Sale Price</th>
                                    <th>Original Price</th>
                                    <th>Net Amount</th>
                                    <th>Paid</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $ledger)

                                <tr>
                                    <td>{{ $ledger->vr_no }}</td>
                                    <td>{{ $ledger->company_id }}</td>
                                    <td>{{ $ledger->product_name.' | '.($ledger->pack) }}</td>
                                    <td>{{ round($ledger->unit_price,2) }}</td>
                                    <td>{{ $ledger->qty }}</td>
                                    <td>{{ round($ledger->actual_price,2) }}</td>
                                    <td>{{ round($ledger->discount,2) }}</td>
                                    <td>{{ round($ledger->total_price,2) }}</td>
                                    <td>{{ round($ledger->total_amount,2) }}</td>
                                    <td>{{ round($ledger->net_amount,2) }}</td>
                                    <td>{{ $ledger->paid }}</td>
                                    <td>{{ $ledger->narration }}</td>
                                    <td> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection
