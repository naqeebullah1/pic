@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Sales</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('my_dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Sales</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Sales
                    </div>
                    <div class="card-body p-1">
                        <table class="table  table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Total Amount</th>
                                    <th>Discount</th>
                                    <th>Net Amount</th>
                                    <th>Paid</th>
                                    <th>Date</th>
                                    <th>Details</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $ledger)

                                <tr>
                                    <td data-title="Customer Name">{{ $ledger->name }}</td>

                                    <td data-title="Total Amount">{{ $ledger->total_amount }}</td>
                                    <td data-title="Discount">{{ $ledger->discount }}</td>
                                    <td data-title="Net Amount">{{ $ledger->net_amount }}</td>
                                    <td data-title="Paid">{{ $ledger->paid }}</td>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="Details">
                                        <a href="{{ route('saleman.saledetails', ['id' => $ledger->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View Details
                                        </a>
                                        <a href="{{ route('saleman.print_invoice', ['id' => $ledger->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Generate Invoice
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <!--<a onclick="return confirm('are You sure')" href="{{ route('sale/delete_sale', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">-->
                                            <a onclick="return confirm('are You sure')" href="#" class="btn btn-outline-danger btn-space btn-sm">
                                                Delete
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection