@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Sales man Sales</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Sales man</a></li>
                <li class="breadcrumb-item active">Sales man sale</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Sales man Sale details</div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Sale Man</th>
                                    <th>Customer Name</th>
                                    <th>Total Amount</th>
                                    <th>Discount</th>
                                    <th>Net Amount</th>
                                    <th>Paid</th>
                                    <th>Date</th>
                                    <th>Details</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $ledger)

                                <tr>
                                    <td>{{ $ledger->area_person_name }}</td>
                                    <td>{{ $ledger->name }}</td>

                                    <td>{{ $ledger->total_amount }}</td>
                                    <td>{{ $ledger->discount }}</td>
                                    <td>{{ $ledger->net_amount }}</td>
                                    <td>{{ $ledger->paid }}</td>
                                    <td> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <td>
                                        <a target="_blank" href="{{ route('sale/details', ['id' => $ledger->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View Details
                                        </a>
                                        <a target="_blank" href="{{ route('sale/print_invoice', ['id' => $ledger->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Generate Invoice
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td>
                                            <a onclick="return confirm('are You sure')" href="{{ route('sale/delete_sale', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                Delete
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection