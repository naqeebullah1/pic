@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customer</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('salesman/customers')}}">Customers</a></li>

                <li class="breadcrumb-item active">Customer Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Customer Ledger
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Sale</th>
                                    <th>Discount</th>
                                    <th>Payment</th>
                                    <th>Remaining</th>                                    
                                    <th>Date</th>                                    
                                    <th>Sale details</th>                                    
                                    <!--<th>Action</th>-->                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer as $ledger)

                                <tr>
                                    <td data-title="Customer Name">{{ $ledger->name }}</td>
                                    <td data-title="Sale">
                                        <?php
                                        echo $ledger->sale;
                                        ?> 
                                    </td>
                                    <td data-title="Discount">{{ $ledger->discount }}</td>
                                    <td data-title="Payment">{{ $ledger->payment }}</td>
                                    <td data-title="Remaining">{{ $ledger->remaining }}</td>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($ledger->sale_details) {
                                            $sale_details = json_decode($ledger->sale_details);
//                                            dump($sale_details);
                                            ?>
                                            <table class="table table-sm table-bordered table-grey">
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                </tr>
                                                @foreach($sale_details as $sd)
                                                <tr>
                                                    <td>{{$sd->product}}</td>
                                                    <td>{{$sd->unit_price}}</td>
                                                    <td>{{$sd->qty}}</td>
                                                </tr>

                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="purchaseDetailModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content" style="max-width: 100% !important;">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;" id="changeHeader"></h4>
                    </div>

                    <div class="modal-body" id="purchaseDetailBody" style="padding:10px">
                    </div>
                    <div class="modal-footer" style="padding:10px">
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    function fetchHistory(invoiceNo, type) {
        $.ajax({
            data: {
                invoiceNo: invoiceNo,
                type: type,
            },
            type: 'GET',
            url: "{{ route('sale/history') }}",
            success: function (return_data) {
                if (type == 'sale') {
                    $('#changeHeader').text('Sale Details');
                } else {
                    $('#changeHeader').text('Return Sale Details');
                }
                $('#purchaseDetailBody').html(return_data);
                $('#purchaseDetailModal').modal('show');
            },
        });
    }

</script>

@endsection