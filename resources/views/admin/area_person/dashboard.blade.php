@extends('layouts.admin')

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Dashboard</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item active">Home Page</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message"><strong> {{ Session::get('success') }} </strong></div>
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
            <div class="icon"><span class="mdi mdi-close"></span></div>
            <div class="message"><strong> {{ Session::get('error') }} </strong></div>
        </div>
        @endif
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
                <div class="widget widget-tile">
                    <div class="chart sparkline" id="spark1">
                        <i class="fab fa-product-hunt fa-5x"></i>
                    </div>
                    <div class="data-info">
                        <div class="desc">Products</div>
                        <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="113">{{ $product }}</span>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col-12 col-lg-6 col-xl-3">
                <div class="widget widget-tile">
                    <div class="chart sparkline" id="spark1">
                        <i class="fas fa-user-tag fa-5x"></i>
                    </div>
                    <div class="data-info">
                        <div class="desc">Customers</div>
                        <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="113">{{ $company }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                <div class="widget widget-tile">
                    <div class="chart sparkline" id="spark1">
                        <i class="fas fa-layer-group fa-5x"></i>
                    </div>
                    <div class="data-info">
                        <div class="desc">Total Stock Qty</div>
                        <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="113">{{ $stock->stock }}</span>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection