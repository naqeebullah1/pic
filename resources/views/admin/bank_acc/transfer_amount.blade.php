@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Transfer Amount</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('bank_acc')}}">Transfer Amount</a></li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Transfer amount from bank to cash or cash to bank</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('transfer_amount/store') }}">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Transfer amount from</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select class="form-control" name="pay_from" id="pay" required="">
                                        <option value="">Transfer Amount from</option>
                                        <option value="bank">Bank to cash</option>
                                        <option value="cash">Cash to bank</option>
                                        <!--<option value="cheque">Cheque</option>-->
                                    </select>
                                </div>
                            </div>
                            <div id="only_bank">

                            </div>
                            <div id="bank">

                            </div>

                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Description</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <textarea class="form-control" name="description" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Date</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control form-control-sm" type="date" name="created_at" value="{{date('Y-m-d')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $('body').on('change', '#pay', function () {
    var pay_type = $('#pay').val();
    var csrf = $('meta[name="csrf_token"]').attr('content');
    $.ajax({
    url: '{{url("/get-payment-type")}}',
            type: 'POST',
            data: {
            pay_type: pay_type,
                    '_token': csrf
            },
            success: function (data) {
            // console.log(data)
            $('#bank').html(data);
            }
    });
    });
    $('body').on('change', '#pay', function () {
    if ($(this).val() == 'cash') {
    var html = '<div class="form-group row">\n\
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Select bank to transfer amount<span class="required">*</span></label>\n\
                                <div class="col-12 col-sm-8 col-lg-6">\n\
                                    <select class="form-control" name="transfer_to" required="">\n\
                                        <option value="">Select Bank</option>';
    @foreach($banks as $bank)
            html += '<option value="{{$bank->id}}">{{$bank->bank_name." | Acc No:".$bank->account_no." | Balance: ".$bank->current_amount}}</option>';
    @endforeach


            html += '</select>\n\
                                </div>\n\
                            </div>';
    $('#only_bank').html(html);
    }else{
   $('#only_bank').html('');
    }
    });
</script>
@endsection
