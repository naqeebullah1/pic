@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Bank Accounts</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Accounts List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table p-1">
                    <div class="dropdown d-md-none">
                        <button type="button" class="float-right btn btn-outline-default btn-sm dropdown-toggle " data-toggle="dropdown" id="5b4f95e9a64b5" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div style="border:1px solid #ccc;" class="dropdown-menu" aria-labelledby="5b4f95e9a64b5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(63px, 31px, 0px);">
                            <a href="{{ url('bank_acc?status=1') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active text-white' : '' ?>">Active Banks</a>
                            <a href="{{ url('bank_acc?status=0') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active text-white' : '' ?>">Inactive Banks</a>
                        </div>
                    </div>

                    <!-- button group for desktop -->
                    <div class="d-none d-md-flex btn-group">
                        <a href="{{ url('bank_acc?status=1') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active' : '' ?>">Active Banks</a>
                        <a href="{{ url('bank_acc?status=0') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active' : '' ?>">Inactive Banks</a>
                    </div>
                    <div class="card-header">Accounts List
                        <div class="tools dropdown">
                            <a href="{{ url('bank_acc/add') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add New Bank Account</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Account No</th>
                                    <th>Current Amount</th>
                                    <th>Transfer Amount</th>
                                    <th>Account Details</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accounts as $product)
                                <tr>
                                    <td data-title="Name">{{ $product->bank_name }}</td>
                                    <td data-title="Account No">{{ $product->account_no }}</td>
                                    <td data-title="Current Amount">{{ $product->current_amount }}</td>
                                    <td data-title="Transfer Amount">
                                        <a href="#" onclick="transferAmount(<?= $product->id ?>, '<?= $product->bank_name . ' | ' . $product->account_no ?>')" class="btn btn-outline-dark btn-space btn-sm">
                                            Transfer Amount
                                        </a>
                                    </td>
                                    <td data-title="Account Details">
                                        <a href="{{ route('bank_acc/show', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            <i class="fas fa-eye"></i>&nbsp; View
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <td data-title="Action">
                                            <a href="{{ route('bank_acc/edit', ['id' => $product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <?php
                                            if ($product->status) {
                                                ?>
                                                <a href="{{route('change.bank.status',['id'=>$product->id])}}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Move to Inactive
                                                </a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="{{route('change.bank.status',['id'=>$product->id])}}" class="btn btn-outline-success btn-space btn-sm">
                                                    Move to Active
                                                </a>
                                                <?php
                                            }
                                            ?>

                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="paymentModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Transfer Payment</h4>
                    </div>
                    <form method="POST" id="updateCityForm" action="{{route('transferPayment')}}">
                        {{csrf_field()}}

                        <div class="modal-body" style="padding:10px">
                            <div class="modal-body" style="padding:10px">
                                <div class="form-group row" style="padding:0">
                                    <label class="col-lg-12">Select Bank from which you want to transfer amount</label>
                                    <div class="col-12 col-sm-8 col-lg-12">

 <!--<input name="bank_name" type="hidden">-->
                                        <select name="transfer_from" class="form-control" required>
                                            <option value="">Select bank</option>
                                            @foreach($accounts as $a)
                                            <option value="{{$a->id}}">{{ $a->bank_name }} | {{$a->account_no}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" style="padding:0">
                                    <label class="col-lg-12">Transfer To</label>
                                    <div class="col-12 col-sm-8 col-lg-12">
                                        <input name="transfer_to" type="hidden">
                                        <input name="bank_name" type="text" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group row" style="padding:0">
                                    <label class="col-lg-12">Enter Amount to transfer</label>
                                    <div class="col-12 col-sm-8 col-lg-12">
                                        <input name="amount" type="number" class="form-control" min="0" required>
                                    </div>
                                </div>
                                <div class="form-group row" style="padding:0">
                                    <label class="col-lg-12">Please Enter Description</label>
                                    <div class="col-12 col-sm-8 col-lg-12">
                                        <textarea name="description" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="updateCityForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    function transferAmount(bankId, bankName) {
        $('form').trigger("reset");
        $('input[name=transfer_to]').val(bankId);
        $('input[name=bank_name]').val(bankName);
        $('select[name=transfer_from] option').show()
        $('select[name=transfer_from] option[value=' + bankId + ']').hide()
        $('#paymentModal').modal('show');
    }
    $(document).ready(function () {
        $('input[name=amount]').change(function () {
            if ($('select[name=transfer_from]').val()) {
                var transferFrom = $('select[name=transfer_from] option:selected').text();
                var transferTo = $('input[name=bank_name]').val();
                var description = $('input[name=amount]').val() + ' PKR hasbeen transfered from ' + transferFrom + ' to ' + transferTo;
                $('textarea[name=description]').val(description);
            }
        });
        $('select[name=transfer_from]').change(function () {
            if ($('input[name=amount]').val()) {
                var transferFrom = $('select[name=transfer_from] option:selected').text();
                var transferTo = $('input[name=bank_name]').val();
                var description = $('input[name=amount]').val() + ' PKR hasbeen transfered from ' + transferFrom + ' to ' + transferTo;
                $('textarea[name=description]').val(description);
            }
        });
        //-initialize the javascript

    });

</script>

@endsection