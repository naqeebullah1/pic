@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Bank Account Detail</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('bank_acc')}}">Accounts</a></li>
                <li class="breadcrumb-item active">Account Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">
                        Account: <b>( <?= $bank->bank_name ?> )</b><br/>
                        Current Amount: <b>( <?= $bank->current_amount . ' RS/-' ?> )</b>
                        <a style="float:right;" href="{{ url('bank_acc') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-chevron-left"></i> Go back</a>
                    </div>
                    <?php
                    if (Request::segment(3)) {
                        ?>
                    <div class="text-right p-3">
                        <a  onclick="return confirm('Are You sure you want to update Ledger.')" href="{{url('bank_acc/update-ledger',Request::segment(3))}}" class="btn btn-outline-primary btn-space btn-sm">Update Ledger</a>
                    </div>

                        <?php
                    }
                    ?>

                    <div class="card-body p-1">
                        <table class="table  table-striped table-bordered table no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Cash</th>
                                    <th>Customer</th>
                                    <th>Company</th>
                                    <th>Expense</th>
                                    <th>Account No</th>
                                    <th>Deposit</th>
                                    <th>Withdraw</th>
                                    <th>Remaining</th>
                                    <th>Description</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accountDetails as $product)
                                <tr class="<?= ($product->is_deleted) ? 'bg-danger text-white' : '' ?>">
                                    <td data-title="Date">
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('d-m-Y')}}
                                    </td>
                                    <td data-title="Cash"><?php
                                        if ($product->transfer_to_cash || $product->transfer_from_cash) {
                                            echo 'Cash';
                                        }
                                        ?></td>

                                    <td data-title="Customer"><?php
                                        if ($product->company_id) {
                                            echo $companies[$product->company_id];
//                                    echo ;
                                        }
                                        ?></td>
                                    <td data-title="Company"><?php
                                        if ($product->supplier_id) {
                                            if (isset($suppliers[$product->supplier_id])) {
                                                echo $suppliers[$product->supplier_id];
//                                    echo ;
                                            }
                                        }
                                        ?></td>
                                    <td data-title="Expense"><?php
                                        if ($product->parent_id) {
                                            if ($expenses[$product->parent_id]) {
                                                echo $expenses[$product->parent_id];
//                                    echo ;
                                            }
                                        }
                                        ?></td>
                                    <td data-title="Account No">{{ $product->account_no }}</td>

                                    <td data-title="Deposit">{{ $product->deposit }}</td>
                                    <td data-title="Withdraw">{{ $product->withdrawal }}</td>
                                    <td data-title="Remaining">{{ $product->remaining }}</td>
                                    <td data-title="Description">{{ $product->description }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <td data-title="Action">
                                            <?php
                                            if ($product->cihl_id && ($product->transfer_from_cash || $product->transfer_to_cash)):
                                                ?>
                                                <a class="btn btn-outline-danger btn-sm" href="{{route('bank_acc/delete_ledger',$product->id)}}" onclick="return confirm('are you sure')">Delete</a>
                                            <?php endif; ?>
                                        </td>
                                    <?php } ?>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection