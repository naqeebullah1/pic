@extends('layouts.admin')

@section('style')
<style>
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "date"; }
        td:nth-of-type(2):before { content: "v.no"; }
        td:nth-of-type(3):before { content: "customer Name"; }
        td:nth-of-type(4):before { content: "company name"; }
        td:nth-of-type(5):before { content: "p.code"; }
        td:nth-of-type(6):before { content: "total qty"; }
        td:nth-of-type(7):before { content: "purchased qty"; }
        td:nth-of-type(8):before { content: "return purchased qty"; }
        td:nth-of-type(9):before { content: "return purchased"; }
        td:nth-of-type(10):before { content: "sale"; }
        td:nth-of-type(11):before { content: "retrn sale"; }
        td:nth-of-type(12):before { content: "Remaining"; }
        td:nth-of-type(13):before { content: "Adjustments"; }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Products</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('products')}}">Products</a></li>
                <li class="breadcrumb-item active">Product Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Product - <b>( {{ $productRec->product_name }} )</b><br>
                        Stock - <b>( {{ $productRec->stock_qty }} )</b>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Vr No</th>
                                    <th>Customer Name</th>
                                    <th>Company Name</th>
                                    <th>P.code</th>
                                    <th>Total Qty</th>
                                    <th>Purchased Qty</th>
                                    <th>Return Purchased</th>
                                    <th>Sale</th>
                                    <th>Return Sale</th>
                                    <th>Remaning</th>
                                    <th>Adjustment</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <?php
                                $isAdjust = '';
                                if ($product->adjust):
                                    $isAdjust = 'bg-warning text-white';
                                endif;
                                ?>
                                <tr class="<?= $isAdjust ?>">
                                    <td>
                                        @if($product->created_at)
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('d-m-Y') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                        if ($product->sale_no) {
                                            echo $product->sale_no;
                                        }
                                        if ($product->return_sale_no) {
                                            echo $product->return_sale_no;
                                        }
                                        if ($product->purchase_no) {
                                            echo $product->purchase_no;
                                        }
                                        if ($product->return_purchase_no) {
                                            echo $product->return_purchase_no;
                                        }
                                        ?> </td>
                                    <td>
                                        <?php
                                        $val = '';
                                        if ($product->sale) {
                                            if ($product->sale_id == null) {
                                                $val = '<span class="text-danger">Walking Customer</span>';
                                            }
                                        }
                                        if ($customers) {
                                            if ($product->customer_sale_id && isset($customers[$product->customer_sale_id])) {
                                                $val = $customers[$product->customer_sale_id];
                                            }
                                            if ($product->return_sale_customer_id) {
                                                $val = $customers[$product->return_sale_customer_id];
                                            }
                                        }
                                        echo $val;
                                        ?> 
                                    </td>
                                    <td>
                                        <?php
                                        if ($product->supplier_purchase_id) {
                                            if (isset($suppliers[$product->supplier_purchase_id])) {
                                                echo $suppliers[$product->supplier_purchase_id];
                                            } else {
                                                echo 'Supplier Record Deleted';
                                            }
                                        }
                                        if ($product->return_purchase_supplier_id) {
                                            echo $suppliers[$product->return_purchase_supplier_id];
                                        }
                                        ?> 

                                    </td>
                                    <td>{{ $product->product_code }}</td>
                                    <td>{{ $product->total_qty }}</td>
                                    <td>{{ $product->purchased_qty }}</td>
                                    <td>{{ $product->return_purchased }}</td>
                                    <td>{{ $product->sale }}</td>
                                    <td>{{ $product->return_sale }}</td>
                                    <td>{{ $product->remaning }}</td>
                                    <td>{{ $product->adjust }}</td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection