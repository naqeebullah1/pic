@extends('layouts.admin')

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />

<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
    .calculateQty{
        color:dodgerblue;
    }
</style>

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Products</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('products')}}">Products</a></li>
                <li class="breadcrumb-item active">Edit Product</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Edit Product</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('update-product') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Product Name</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" name="product_name" type="text" placeholder="Add Product Name" value="{{ $product->product_name }}" required>
                                    <input type="hidden" name="pro_id" value="{{ $product->id }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Packing Type</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select name="qty" class="form-control">
                                        <option value="">Select Packing Type</option>
                                        @foreach($qty as $q)
                                        <option value="{{$q->quantity}}" <?= ($product->qty == $q->quantity) ? 'selected' : ''; ?> >{{ $q->quantity }}</option>
                                        @endforeach
                                    </select>                               
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Product Code</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="product_code" placeholder="Add Product Code" value="{{ $product->product_code }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Product Cost Price</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" pattern="[0-9.]{0,10}" name="product_cost_price" placeholder="Product Cost Price" value="{{ $product->product_cost_price }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Product Sale Price</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" pattern="[0-9.]{0,10}" name="product_sale_price" placeholder="product sale price" value="{{ $product->product_sale_price }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Product Whole Sale Price</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" pattern="[0-9.]{0,10}" name="product_whole_sale_price" placeholder="Product Whole Sale Price" value="{{ $product->product_whole_sale_price }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Stock Qty in peices</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="stock_qty" placeholder="Stock Quantity" value="{{ $product->stock_qty }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Qty per cotton</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" pattern="[0-9.]{0,10}" value="{{ $product->qty_per_cotton }}" name="qty_per_cotton" placeholder="Qty per cotton" required>
                                    <p class="mt-2 mb-0 calculateQty"></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right" for="inputTextarea3">Company</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <!--<input class="form-control" type="text" name="manufacturing_company" placeholder="Add Company Name" value="" required>-->
                                    <select name="supplier_id" class="select2" data-placeholder="Select Company Name">
                                        <option value=""></option>
                                        @foreach($companies as $company)
                                        <option value="{{$company->id}}" <?= ($product->supplier_id == $company->id) ? 'selected' : ''; ?> >{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Product Description</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <textarea name="product_description" class="form-control">{{$product->product_description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-masks.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>


<script type="text/javascript">
$(document).ready(function () {
    //-initialize the javascript
    App.init();
    App.masks();
});
$(function () {
    $('input[name=qty_per_cotton]').change(function () {
        var stockQty = $('input[name=stock_qty]').val();
        var qty_per_cotton = $('input[name=qty_per_cotton]').val();
        var totalItems = Math.floor(stockQty / qty_per_cotton) + ' cotton and ' + stockQty % qty_per_cotton + ' pieces';
        if (stockQty != '') {
            $('.calculateQty').text('You have ' + totalItems + ' in stock');
        }
    });
});
</script>
@endsection