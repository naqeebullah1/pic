@extends('layouts.admin')

@section('style')
<style>
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Name"; }
        td:nth-of-type(2):before { content: "code"; }
        td:nth-of-type(3):before { content: "cost price"; }
        td:nth-of-type(4):before { content: "sale price"; }
        td:nth-of-type(5):before { content: "whole sale price"; }
        td:nth-of-type(6):before { content: "Stock"; }
        td:nth-of-type(7):before { content: "Company"; }
        td:nth-of-type(8):before { content: "Action"; }
    }
</style>
@endsection

@section('content')
<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Products</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Products List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table p-1">
                    <div class="btn-group btn-group-justified btn-light">
                        <a href="{{ url('products?status=1') }}" class="btn border btn-light <?= ((isset($_GET['status']) && $_GET['status'] == '1') || !isset($_GET['status'])) ? 'active' : '' ?>">Active Products</a>
                        <a href="{{ url('products?status=0') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active' : '' ?>">Inactive Products</a>
                    </div>
                    <div class="card-header">Products List
                        <div class="tools dropdown">
                            <a href="{{ route('add-products') }}">
                                <button class="btn btn-rounded btn-space btn-primary">
                                    <i class="fas fa-plus-circle"></i> Add New Product
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Product Code</th>

                                    <?php
                                    $role_id = auth()->user()->user_type;
                                    if ($role_id == 1) {
                                        ?>
                                        <th>Cost Price</th>
                                    <?php } ?>
                                    <th>Sale Price</th>
                                    <th>Whole Sale Price</th>
                                    <th>Stock Qty (pieces/cottons)</th>
<!--                                    <th>Items per Cottons</th>
                                    <th>Total stock qty</th>-->
                                    <th>Company</th>
                                   
                                        <th>Actions</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td class="p_name"><?= $product->product_name . ' | ' . ($product->qty); ?></td>
                                    <td>{{ $product->product_code }}</td>
                                    <?php
                                    if ($role_id == 1) {
                                        ?>
                                        <td>{{ $product->product_cost_price }}</td>
                                    <?php } ?>
                                    <td>{{ $product->product_sale_price }}</td>
                                    <td>{{ $product->product_whole_sale_price }}</td>
                                    <td class="c_qty">
                                        <?php
                                        $product_qty = 1;
                                        $totalcot = 0;
                                        if ($product->qty_per_cotton) {
                                            $product_qty = round($product->qty_per_cotton, 2);
//                                            dump($product_qty);
                                            $totalcot = floor($product->stock_qty / $product_qty) . ' cotton, ' . $product->stock_qty % $product_qty . ' Pieces';
                                        }
                                        ?>
                                        <?= $product->stock_qty . ' / ' . $product->qty_per_cotton . ' = ' . $totalcot ?>
                                    </td>
                                    <td>{{ $product->name }}</td>
                                    <td nowrap>
                                        <?php
                                        if ($product->status) {
                                            ?>
                                            <a onclick="return confirm('This is an active user. Do you want to move to Inactive.')" href="{{ route('admin.product.change.status', ['id' => $product->id,'status'=>0]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                Inactive
                                            </a>

                                            <?php
                                        } else {
                                            ?>
                                            <a onclick="return confirm('This is an Inactive user. Do you want to move to Active.')" href="{{ route('admin.product.change.status', ['id' => $product->id,'status'=>1]) }}" class="btn btn-outline-success btn-space btn-sm">
                                                Active
                                            </a>

                                            <?php
                                        }
                                        ?>

                                        <a title="View Ledger" href="{{ route('product-ledger', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <?php
                                        if ($role_id == 1) {
                                            ?>
                                            <button productId="{{ $product->id }}" title="Stock Adjustment" onclick="adjustment(this)" class="adjustBtn btn btn-outline-dark btn-space btn-sm">
                                                <i class="fas fa-sliders-h"></i>
                                            </button>
                                            <a title="Edit" href="{{ route('edit-product', ['id' => $product->id]) }}" class="btn btn-outline-primary btn-space btn-sm"><i class="fas fa-edit"></i></a>
                                            <!--<a title="Delete" href="{{ route('delete-product', ['id' => $product->id]) }}" class="btn btn-outline-danger btn-space btn-sm"><i class="fas fa-trash"></i></a>-->
                                        <?php } ?>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Adjust Stock</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <b>Item Name:</b> <span id="itemName"></span>
                <br><br>
                <b>
                    Current Qty:
                </b> <span id="currentQty"></span>
                <br>
                <br>
                <form method="post" action="{{ route('stock.adjustment') }}">
                    <div class="form-group">
                        <label style="font-weight: bold">Adjustment</label>
                        {{ csrf_field() }}
                        <input type="hidden" name="p_id" id="p_id" class="form-control">
                        <div>                        
                            <input type="text" name="adjStock" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <input type="submit" class="btn btn-success" value="Save">
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    function adjustment(ref) {
        $('#itemName').html($(ref).parent().siblings('.p_name').html());
        $('#currentQty').html($(ref).parent().siblings('.c_qty').html());
        $('#p_id').val($(ref).attr('productId'));
        $('#myModal').modal('show');
    }

</script>

@endsection