@extends('layouts.admin')

@section('style')


@endsection

@section('content')
<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Products</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Products List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Products List
                        <div class="tools dropdown">
                            <a href="{{ route('add-products') }}">
                                <button class="btn btn-rounded btn-space btn-primary">
                                    <i class="fas fa-plus-circle"></i> Add New Product
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Product Code</th>

                                    <?php
                                    $role_id = auth()->user()->user_type;
                                    if ($role_id == 1) {
                                        ?>
                                        <th>Cost Price</th>
                                    <?php } ?>
                                    <th>Sale Price</th>
                                    <th>Whole Sale Price</th>
                                    <th>Stock Qty (pieces/cottons)</th>
<!--                                    <th>Items per Cottons</th>
                                    <th>Total stock qty</th>-->
                                    <th>Company</th>
                                    <th>Product Ledger</th>
                                    <?php if ($role_id == 1) {
                                        ?>
                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)

                                <tr>
                                    <td><?= $product->product_name . ' | ' . ($product->qty); ?></td>
                                    <td>{{ $product->product_code }}</td>
                                    <?php
                                    if ($role_id == 1) {
                                        ?>
                                        <td>{{ $product->product_cost_price }}</td>
                                    <?php } ?>
                                    <td>{{ $product->product_sale_price }}</td>
                                    <td>{{ $product->product_whole_sale_price }}</td>
                                    <td>
                                        <?php
                                        $product_qty = 1;
                                        $totalcot = 0;
                                        if ($product->qty_per_cotton) {
                                            $product_qty = round($product->qty_per_cotton, 2);
//                                            dump($product_qty);
                                            $totalcot = floor($product->stock_qty / $product_qty) . ' cotton, ' . $product->stock_qty % $product_qty . ' Pieces';
                                        }
                                        ?>
                                        {{ $product->stock_qty. ' / '.$product->qty_per_cotton  .' = '. $totalcot  }}
                                    </td>
                                    <td>{{ $product->name }}</td>
                                    <td>
                                        <a href="{{ route('product-ledger', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <?php
                                        if ($role_id == 1) {
                                            ?>
                                            <a href="{{ route('edit-product', ['id' => $product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <a href="{{ route('delete-product', ['id' => $product->id]) }}" class="btn btn-outline-danger btn-space btn-sm">Delete</a>
                                        <?php } ?>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection