@extends('layouts.admin')

@section('style')

<style>
    .currentMonth{
        font-weight: bold;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Manufacturing Companies</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Companies List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Companies List
                        <div class="tools dropdown">
                            <a href="#" data-toggle="modal" data-target="#addCityModal"  class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add New Company</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Companies</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <tr>
                                    <td>{{ $product->company_name }}</td>
                                    <td>
                                        <a onclick="editCity({{$product->id}})" href="#" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                        <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('manufacturing_companies.destroy', ['id'=>$product->id]) }}" style="display:inline;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="addCityModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Add Company</h4>
                    </div>
                    <form method="POST" id="advancePaymentForm" action="{{route('manufacturing_companies.store')}}">
                        {{csrf_field()}}
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <label class="col-lg-12">Add Company Name</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="company_name" type="text" class="form-control"  min="0">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="advancePaymentForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="editCityModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Edit Manufacturing Company</h4>
                    </div>
                    <form method="POST" id="updateCityForm" action="{{route('manufacturing_companies/update')}}">
                        {{csrf_field()}}

                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <label class="col-lg-12">Edit Company</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="company_id" type="hidden" class="form-control"  min="0">
                                    <input name="company_name" type="text" class="form-control"  min="0">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="updateCityForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@section('script')

<script type="text/javascript">

                                            function editCity(cityId){
                                            $('input[name=company_id]').val(cityId);
                                            $.ajax({
                                            url: '{{ route("fetch_company") }}',
                                                    type: 'Get',
                                                    data: {id:cityId},
                                                    success: function(data) {
                                                    $('input[name=company_name]').val(data);
                                                    $('#editCityModal').modal('show');
                                                    }
                                            });
                                            }
                                            
</script>

@endsection