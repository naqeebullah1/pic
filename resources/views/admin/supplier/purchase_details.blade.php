@extends('layouts.admin')
@section('style')
@endsection
@section('content')
<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Company</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('supplier.index')}}">Companies</a></li>
               
                <li class="breadcrumb-item"><a href="{{ route('supplierPurchase.edit', ['id' => $purchase_id]) }}">Purchases</a></li>
               
                <li class="breadcrumb-item active">Company Purchase Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Company Purchase Details
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered table-fw-widget no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Prdouct Name</th>
                                    <th>Product Code</th>
                                    <th>Unit Price</th>
                                    <th>Qty</th>
                                    <th>Total Amount</th>
                                    <th>Discount (%)</th>
                                    <th>Sale Price</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_details as $details)
                                <tr>
                                    <td data-title="Product Name">{{ $details->product_name.' | '.($details->pack) }}</td>
                                    <td data-title="Product Code">{{ $details->product_code }}</td>
                                    <td data-title="Unit Price">{{ $details->unit_price }}</td>
                                    <td data-title="Qty">{{ $details->qty }}</td>
                                    <td data-title="Total Amount">{{ $details->actual_amount }}</td>
                                    <td data-title="Discount (%)">{{ $details->item_discount }}</td>
                                    <td data-title="Sale Price">{{ $details->total_price }}</td>
                                    <td data-title="Date"> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $details->created_at)->format('d-m-Y') }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection