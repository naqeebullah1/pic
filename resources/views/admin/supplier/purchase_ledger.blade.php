@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Company</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#">Companies</a></li>
                <li class="breadcrumb-item active">Company Purchase Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Company Purchase Ledger
                    </div>
                    <div class="card-body p-1">
                        <table class="no-more-tables table-bordered table table-striped table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Total Amount</th>
                                    <th>Discount</th>
                                    <th>Net Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Date</th>
                                    <td>Details</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td>Action</td>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($supplier_purchases as $purchases)

                                <tr>
                                    <td data-title="Company Name">{{ $purchases->name }}</td>
                                    <td data-title="Total Amount">{{ $purchases->total_amount }}</td>
                                    <td data-title="Discount">{{ $purchases->discount }}</td>
                                    <td data-title="Net Amount">{{ $purchases->net_amount }}</td>
                                    <td data-title="Paid Amount">{{ $purchases->paid }}</td>
                                    <td data-title="Date"> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $purchases->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="details">
                                        <a href="{{ route('purchase=-details', ['id' => $purchases->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View Details
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a onclick="return confirm('are you sure')" href="{{ route('purchase/delete_purchase', ['id' => $purchases->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                Delete
                                            </a>

                                            <a  href="{{ route('editPurchase', ['id' => $purchases->id]) }}" class="btn btn-outline-info btn-space btn-sm">
                                                Edit
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
@endsection
