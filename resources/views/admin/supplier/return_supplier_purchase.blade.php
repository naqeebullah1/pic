@extends('layouts.admin')

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />

<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
    td span,td .required{
        display: none;
    }
</style>

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Companies</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('supplier.index')}}">Companies</a></li>
                <li class="breadcrumb-item active">Company Return Purchase </li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong>{{ Session::get('error') }}</strong></div>
                </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-header"> <b>Company Return Purchase</b></div>
                            <div class="card-body">
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                        <tr>
                                            <th>
                                                Company Name
                                            </th>
                                            <th>
                                                Balance
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-title="Company Name">
                                                {{ $supplier->name }}
                                            </td>
                                            <td data-title="Balance">
                                                {{ $supplier->dues }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <br>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-2 col-form-label text-sm-center" style="color:black;font-size:20px">Select Product</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <select class="select2 product" name='product[]' data-placeholder="Select Product">
                                            <option value="">Select Product </option>

                                            @foreach($products as $product)
                                            <option value="{{ $product->id }}"><?php echo ($product->qty) ? $product->product_name . ' | ' . ($product->qty) : $product->product_name; ?></option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <hr>
                                <br>

                                <form action="{{ route('supplierReturnPurchase.store') }}" method="post">
                                    <input type="hidden" name="supplier_id" value="{{ $supplier->id }}">
                                    <input type="hidden" name="dues" value="{{ $supplier->dues }}">
                                    <h3 class="m-0">Products</h3>

                                    {{ csrf_field() }}
                                    <table class="table table-bordered table-striped no-more-tables" >
                                        <thead>
                                            <tr>
                                                <th width="5%">
                                                    P-ID
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Unit Price
                                                </th>
                                                <th>
                                                    Current Qty
                                                </th>
                                                <th>
                                                    Qty
                                                </th>
                                                <th>
                                                    Discount (in %)
                                                </th>
                                                <th>
                                                    Total Amount
                                                </th>
                                                <th>
                                                    Delete
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="product_detail">

                                        </tbody>
                                    </table>
                                    <h3 class="m-0">Summary</h3>
                                    <table class="table no-more-tables table-bordered" >
                                        <thead>
                                            <tr>
                                                <th>
                                                    Total Amount
                                                </th>
                                                <th>
                                                    Discount
                                                </th>
                                                <th>
                                                    Net Amount
                                                </th>
                                                <th>
                                                    Paid Amount
                                                </th>
                                                <th>
                                                    Due Amount
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td data-title="Total Amount">
                                                    <input class="form-control line_cost_total form-control-sm" type="text" name="line_cost_total" readonly value="0">
                                                </td>
                                                <td data-title="Discount">
                                                    <input class="form-control discount form-control-sm" type="text" value="0" name="discount" required>
                                                </td>
                                                <td data-title="Net Amount">
                                                    <input class="form-control total form-control-sm" type="text" readonly value="0" name="net_amount">
                                                </td>
                                                <td data-title="Paid Amount">
                                                    <input class="form-control paidamount form-control-sm" type="text" value="0" name="paidamount" required>
                                                </td>
                                                <td data-title="Due Amount">
                                                    <input class="form-control due_amount form-control-sm" type="text" readonly value="0" name="due_amount">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered no-more-tables">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Description
                                                </th>
                                                <th>
                                                    VR NO
                                                </th>
                                                <th>
                                                    Date
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td data-title="Description">
                                                    <input name="remarks" class="form-control form-control-sm" type="text">
                                                </td>
                                                <td data-title="VR No">
                                                    <input class="form-control form-control-sm" type="text" name="vr_no">
                                                </td>
                                                <td data-title="Date">
                                                    <input class="form-control form-control-sm" type="date" name="created_at" value="{{date('Y-m-d')}}">
                                                </td>
                                            </tr>
                                        </tbody>

                                    </table>

                                    <div class="col text-center">
                                        <button class="btn btn-space btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>


<script>
// Add Product To Table
$('body').on('change', '.product', function () {
    var channel = $(this);
    var product = $(this).val();

    $.ajaxSetup({
        beforeSend: function (xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf_token"]').attr('content'));
            }
        },
    });
    $.ajax({
        data: {
            id: product
        },
        type: 'POST',
        url: "{{ route('get-product-details') }}",
        success: function (return_data) {
            $('#product_detail').prepend('<tr><td data-title="P-ID"><input class="form-control form-control-sm" type="text" readonly name="product_id[]" value="' + return_data.id + '"></td><td data-title="Product Name">' + return_data.product_name + '</td><td data-title="Unit Price"><input class="form-control unit_price form-control-sm" type="text" readonly name="unit_price[]" value="' + return_data.product_cost_price + '"></td><td data-title="Current Qty"><input class="form-control form-control-sm" type="text" readonly name="current_qty[]" value="' + return_data.stock_qty + '"></td><td data-title="Qty"><input class="form-control qty form-control-sm" type="number" name="qty[]" min="0" value="0"></td><td data-title="Discount (in %)"><input class="form-control form-control-sm" type="text"  name="discount_per_item[]" value="0"></td><td data-title="Total Amount"><input class="form-control line_cost form-control-sm" type="text" readonly name="line_cost[]" value=""></td><td data-title="Delete"><a href="javascript:;" class="btn btn-danger delete btn-sm"><i class="fa fa fa-trash"></i></a></td></tr>');
        },
    });
});

// Add Quantity to Product
$('body').on('keyup', '.qty', function () {
    change_qty_cost($(this).closest('tr'));
});
$('body').on('keyup', '.unit_price', function () {
    change_qty_cost($(this).closest('tr'));
});
$('body').on('keyup', '.discount', function () {
    update_calculation();
});
$('body').on('keyup', '.paidamount', function () {
    update_calculation();
});
$('body').on('keyup', '[name="discount_per_item[]"]', function () {
    change_qty_cost($(this).closest('tr'));
//alert($(this).val());
});
function change_qty_cost(row) {
    var discount = row.find('[name="discount_per_item[]"]').val() / 100;
    console.log(discount);

    if (row.find('.unit_price').val() >= 0 && row.find('.qty').val() >= 0) {
        var unit_price = +row.find('.unit_price').val();
        var qty = +row.find('.qty').val();
        if (row.find('.unit_price').val() == 0 || row.find('.qty').val() == 0) {
            var line_cost = row.find('.line_cost').val(0);
        } else {
//            var line_cost = row.find('.line_cost').val(unit_price * qty);
            var actual_val = unit_price * qty;
            if (discount != 0) {
                actual_val = actual_val - (actual_val * discount);
            }

            var line_cost = row.find('.line_cost').val(actual_val);
        }

        update_calculation();

    }
}

// Delete Product from List
$('body').on('click', '.delete', function () {
    var rowCount = $('#product_detail tr').length;
    if (rowCount == 7) {
        message("error", 'You can\'t delete because there are must be at least one row');
    } else {
        var row = $(this).closest('tr');
        var line_cost = row.find('.line_cost').val();
        var cost = $('.line_cost_total').val();
        $('.line_cost_total').val(cost - line_cost);

        $(this).closest('tr').remove();

        update_calculation();
    }
});

// calculation of line_cost_total, discount, total, paidamount and due amount
function update_calculation() {

    var discount = $(".discount").val();
    // if the discount field is null then put in discount 0
    if (discount == '') {
        $(".discount").val(0);
    }

    var paidamount = $(".paidamount").val();
    // if the paidamount field is null then put in paidamount 0
    if (paidamount == '') {
        $(".paidamount").val(0);
    }

    var line_cost = $(".line_cost");
    var total_line_cost = 0;
    for (var i = 0; i < line_cost.length; i++) {
        if ($.isNumeric($(line_cost[i]).val()))
            total_line_cost += parseInt($(line_cost[i]).val());
    }

    $(".line_cost_total").val(total_line_cost);
    $(".total").val(total_line_cost);

    var line_cost_total = parseInt($(".line_cost_total").val());

    var discount = parseInt($(".discount").val());


    $(".total").val(line_cost_total - discount);

    var total = parseInt($(".total").val());

    var paidamount = parseInt($(".paidamount").val());

    $(".due_amount").val(total - paidamount);
}
</script>

@endsection