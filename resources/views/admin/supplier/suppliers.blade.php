@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Companies</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Companies List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table p-1">
                    <div class="dropdown d-md-none">
                        <button type="button" class="float-right btn btn-outline-default btn-sm dropdown-toggle " data-toggle="dropdown" id="5b4f95e9a64b5" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div style="border:1px solid #ccc;" class="dropdown-menu" aria-labelledby="5b4f95e9a64b5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(63px, 31px, 0px);">
                            <a href="{{ url('supplier?status=1') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active text-white' : '' ?>">Active Companies</a>
                            <a href="{{ url('supplier?status=0') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active text-white' : '' ?>">Inactive Companies</a>
                            <a href="{{ url('supplier') }}" class="dropdown-item <?= !isset($_GET['status']) ? 'active text-white' : '' ?>">All Companies</a>

                        </div>
                    </div>

                    <!-- button group for desktop -->
                    <div class="d-none d-md-flex btn-group">
                        <a href="{{ url('supplier?status=1') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active' : '' ?>">Active Companies</a>
                        <a href="{{ url('supplier?status=0') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active' : '' ?>">Inactive Companies</a>
                        <a href="{{ url('supplier') }}" class="btn border btn-light <?= !isset($_GET['status']) ? 'active' : '' ?>">All Companies</a>
                    </div>


                    <div class="card-header m-1 mt-3">Companies List
                        <div class="tools dropdown">
                            <a href="{{ route('supplier.create') }}">
                                <button class="btn btn-rounded btn-space btn-primary">
                                    <i class="fas fa-plus-circle"></i> Add
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="no-more-tables table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>City</th>
                                    <th>Dues</th>
                                    <th>Last Payment</th>
                                    <th>Pay</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Purchase</th>
                                        <th>Purchase History</th>
                                        <th>Purchase Return</th>
                                        <th>Return History</th>
                                        <th>Ledger</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    <?php } ?>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($suppliers as $supplier)

                                <tr>
                                    <td  data-title="Name">{{ $supplier->name }}</td>
                                    <td data-title="Contact">{{ $supplier->contact_no }}</td>
                                    <td data-title="City">{{ $supplier->city }}</td>
                                 @if($supplier->dues<0)
                                    
                                    <td data-title="Dues" style="color:red;">{{ $supplier->dues }}</td>
                                    
                                    @endif
                                    @if($supplier->dues>-1)
                                    
                                     <td data-title="Dues" >{{ $supplier->dues }}</td>
                                    
                                    @endif
                                    <td data-title="Last Payment">
                                        <span class="text-danger">
                                            <?php
                                            if ($supplier->last_payment) {
                                                $earlier = new DateTime();
                                                $later = new DateTime($supplier->last_payment);

                                                $diff = $later->diff($earlier)->format("%a");
                                                if ($diff == 0) {
                                                    echo 'Paid Today';
                                                } elseif ($diff == 1) {
                                                    echo '<b>' . abs($diff) . '</b> day ago';
                                                } else {
                                                    echo '<b>' . abs($diff) . '</b> days ago';
                                                }
                                            } else {
                                                echo 'Not Paid yet';
                                            }
                                            ?>
                                        </span>
                                    </td>

                                    <td data-title="Pay">
                                        <a href="{{ route('supplier-payment', ['id' => $supplier->id]) }}" class="btn btn-outline-secondary btn-space btn-sm">
                                            Pay
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Purchase">
                                            <a href="{{ route('supplierPurchase.show', ['id' => $supplier->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                                Purchase
                                            </a>
                                        </td>
                                        <td data-title="Purchase History">
                                            <a href="{{ route('supplierPurchase.edit', ['id' => $supplier->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                                View
                                            </a>
                                        </td>
                                        <td data-title="Purchase Return">
                                            <a href="{{ route('supplierReturnPurchase.show', ['id' => $supplier->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                                Return
                                            </a>
                                        </td>
                                        <td data-title="Return History">
                                            <a href="{{ route('supplierReturnPurchase.edit', ['id' => $supplier->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                                View
                                            </a>
                                        </td>
                                        <td data-title="Ledger">
                                            <a href="{{ route('supplier.show', ['id' => $supplier->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                                View
                                            </a>
                                        </td>
                                        <td data-title="Status">
                                            <?php
                                            if ($supplier->status) {
                                                ?>
                                                <a onclick="return confirm('This is an active user. Do you want to move to Inactive.')" href="{{ route('admin.company.change.status', ['id' => $supplier->id,'status'=>0]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Move to Inactive
                                                </a>

                                                <?php
                                            } else {
                                                ?>
                                                <a onclick="return confirm('This is an Inactive user. Do you want to move to Active.')" href="{{ route('admin.company.change.status', ['id' => $supplier->id,'status'=>1]) }}" class="btn btn-outline-success btn-space btn-sm">
                                                    Move to Active
                                                </a>

                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <td data-title="Action">
                                            <a href="{{ route('supplier.edit', ['id' => $supplier->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')



@endsection