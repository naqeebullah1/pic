@extends('layouts.admin')

@section('style')
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Company</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('supplier.index')}}">Companies</a></li>

                <li class="breadcrumb-item active">Company Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">
                        Company: <b>(<?= $supplierDetails->name ?>)</b><br/>
                        Balance: <b>(<?php echo($supplierDetails->dues) . ' Rs/-' ?>)</b>

                    </div>
                    <div class="text-right pr-4 pb-3">
                        <a onclick="return confirm('Are You sure you want to update Ledger.')" href="{{ url('company/update-ledger',['id'=>$supplierDetails->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Update Ledger</a></div>
                    <div class="card-body p-1">
                        <table class="table no-more-tables table-striped table-bordered table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <!--<th>Company Name</th>-->
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Bank</th>
                                    <th>Purchase Details</th>
                                    <th>Purchase</th>
                                    <th>Return Purchase</th>
                                    <th>DTL</th>
                                    <th>Payment</th>
                                    <th>Balance</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($supplier_ledger as $ledger)
                                
                                
                                @if($ledger->payment>0)
                                    <tr style="background-color:lightblue;">
                                    <td data-title="Date"> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="Description">{{ $ledger->narration}}</td>
                                    
                                   <td data-title="Bank">@if($ledger->bank_name)
                                        {{$ledger->bank_name}}
                                        @endif

                                    </td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($ledger->purchase_details) {
                                            $sale_details = json_decode($ledger->purchase_details);
//                                            dump($sale_details);
                                            ?>
                                            <table class="table table-sm table-bordered table-grey">
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Cotton</th>
                                                    <th>Total</th>
                                                </tr>

                                                @foreach($sale_details as $sd)
                                                <tr>
                                                    <td>{{$sd->product}}</td>
                                                    <td>{{$sd->unit_price}}</td>
                                                    <td>{{$sd->qty}}</td>
                                                    <td>@php if(isset($sd->cotton)){
                                                        echo $sd->cotton;
                                                        }?></td>
                                                    <td>{{$sd->unit_price*$sd->qty}}</td>
                                                </tr>

                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Purchase"> <?php
                                        if ($ledger->purchase != 0) {
                                            ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }})"><?= $ledger->purchase ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->purchase;
                                        }
                                        ?> </td>
                                    <td data-title="Return Purchase">{{ $ledger->return_purchase }}</td>
                                    <td data-title="DTL">{{ $ledger->dtl }}</td>
                                    <td data-title="Payment">{{ $ledger->payment }}</td>
                                    <td data-title="Balance">{{ $ledger->balance }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action" nowrap>
                                            <?php
                                            if ($ledger->return_purchase) {
//                                                dump($ledger->invoice_id);
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ url('purchase/delete_return_purchase', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete Return Purchase
                                                </a>
                                                <?php
                                            } elseif ($ledger->purchase) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ url('purchase/delete_purchase', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete Purchase
                                                </a>
                                            <?php } elseif ($ledger->payment) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ route('supplier/delete_payment', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete
                                                </a>
                                                <?php
                                            } else {
                                                
                                            }
                                            ?>

                                        </td>   
                                    <?php } ?>
                                
                                
                                
                                @else
                                
                                    <tr>
                                    <td data-title="Date"> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="Description">{{ $ledger->narration}}</td>
                                    
                                   <td data-title="Bank">@if($ledger->bank_name)
                                        {{$ledger->bank_name}}
                                        @endif

                                    </td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($ledger->purchase_details) {
                                            $sale_details = json_decode($ledger->purchase_details);
//                                            dump($sale_details);
                                            ?>
                                            <table class="table table-sm table-bordered table-grey">
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Cotton</th>
                                                    <th>Total</th>
                                                </tr>

                                                @foreach($sale_details as $sd)
                                                <tr>
                                                    <td>{{$sd->product}}</td>
                                                    <td>{{$sd->unit_price}}</td>
                                                    <td>{{$sd->qty}}</td>
                                                    <td>@php if(isset($sd->cotton)){
                                                        echo $sd->cotton;
                                                        }?></td>
                                                    <td>{{$sd->unit_price*$sd->qty}}</td>
                                                </tr>

                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Purchase"> <?php
                                        if ($ledger->purchase != 0) {
                                            ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }})"><?= $ledger->purchase ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->purchase;
                                        }
                                        ?> </td>
                                    <td data-title="Return Purchase">{{ $ledger->return_purchase }}</td>
                                    <td data-title="DTL">{{ $ledger->dtl }}</td>
                                    <td data-title="Payment">{{ $ledger->payment }}</td>
                                    <td data-title="Balance">{{ $ledger->balance }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action" nowrap>
                                            <?php
                                            if ($ledger->return_purchase) {
//                                                dump($ledger->invoice_id);
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ url('purchase/delete_return_purchase', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete Return Purchase
                                                </a>
                                                <?php
                                            } elseif ($ledger->purchase) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ url('purchase/delete_purchase', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete Purchase
                                                </a>
                                            <?php } elseif ($ledger->payment) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ route('supplier/delete_payment', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete
                                                </a>
                                                <?php
                                            } else {
                                                
                                            }
                                            ?>

                                        </td>   
                                    <?php } ?>
                                
                                @endif
                            
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="purchaseDetailModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content" style="max-width: 100% !important;">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Purchase Details</h4>
                    </div>

                    <div class="modal-body" id="purchaseDetailBody" style="padding:10px">
                    </div>
                    <div class="modal-footer" style="padding:10px">
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')



@endsection