@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Personal Expenses</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                 <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('zakat_persons.index')}}">Personal Expenses</a></li>
                <li class="breadcrumb-item active">Edit Person Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Edit Person Details</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('zakat_persons.update',$account->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            

                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Name</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input name="id" type="hidden"  value="{{ $account->id }}">
                                    <input class="form-control" name="name" type="text" required value="{{ $account->name }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Contact Number</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="contact_no" required value="{{ $account->contact_no }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Total Amount</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" min="0" required  readonly value="{{ $account->balance }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection