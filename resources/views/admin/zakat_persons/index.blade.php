@extends('layouts.admin')

@section('style')



@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Personal Expenses</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Personal Expenses</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Personal Expenses
                        <div class="tools dropdown">
                            <a href="{{ route('zakat_persons.create') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add New Personal Expense</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact No</th>
                                    <th>Total Amount</th>
                                    <th>Pay</th>
                                    <th>Account Details</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accounts as $product)
                                <tr>
                                    <td data-title="Name">{{ $product->name }}</td>
                                    <td data-title="Contact No">{{ $product->contact_no }}</td>
                                    <td data-title="Total Amount">{{ $product->balance }}</td>
                                    <td data-title="Pay">
                                        <a href="{{ route('zakat_persons.payment', ['id' => $product->id]) }}" onclick="transferAmount(<?= $product->id ?>, '<?= $product->name ?>')" class="btn btn-outline-dark btn-space btn-sm">
                                            Pay
                                        </a>
                                    </td>
                                    <td data-title="Account Details">
                                        <a href="{{ route('zakat_persons.ledger', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            <i class="fas fa-eye"></i>&nbsp; View
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a href="{{ route('zakat_persons.edit', ['id' => $product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <!--<a href="{{ route('zakat_persons.destroy', ['id' => $product->id]) }}" class="btn btn-outline-danger btn-space btn-sm" onclick="return confirm('Are you sure')">Delete</a>-->
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection

    @section('script')

  
    <script type="text/javascript">
                                            function transferAmount(bankId, bankName) {
                                                $('form').trigger("reset");
                                                $('input[name=transfer_to]').val(bankId);
                                                $('input[name=bank_name]').val(bankName);
                                                $('select[name=transfer_from] option').show()
                                                $('select[name=transfer_from] option[value=' + bankId + ']').hide()
                                                $('#paymentModal').modal('show');
                                            }
                                            $(document).ready(function () {
                                                $('input[name=amount]').change(function () {
                                                    if ($('select[name=transfer_from]').val()) {
                                                        var transferFrom = $('select[name=transfer_from] option:selected').text();
                                                        var transferTo = $('input[name=bank_name]').val();
                                                        var description = $('input[name=amount]').val() + ' PKR hasbeen transfered from ' + transferFrom + ' to ' + transferTo;
                                                        $('textarea[name=description]').val(description);
                                                    }
                                                });
                                                $('select[name=transfer_from]').change(function () {
                                                    if ($('input[name=amount]').val()) {
                                                        var transferFrom = $('select[name=transfer_from] option:selected').text();
                                                        var transferTo = $('input[name=bank_name]').val();
                                                        var description = $('input[name=amount]').val() + ' PKR hasbeen transfered from ' + transferFrom + ' to ' + transferTo;
                                                        $('textarea[name=description]').val(description);
                                                    }
                                                });
                                                //-initialize the javascript
                                                // App.dataTables();
                                            });

    </script>

    @endsection