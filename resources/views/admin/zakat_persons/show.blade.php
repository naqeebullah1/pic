@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Personal Expenses</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('zakat_persons.index')}}">Personal Expenses</a></li>
                <li class="breadcrumb-item active">Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Expense Detail
                        <div class="tools dropdown">
                            <a href="{{route('zakat_persons.index')}}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-chevron-left"></i> Go back</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Withdrawal</th>
                                    <!--<th>Current Amount</th>-->
                                    <th>Description</th>
                                    <th>Date</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accountDetails as $product)
                                <tr>
                                    <td data-title="Name">{{ $product->name }}</td>
                                    <td data-title="Withdrawal">{{ $product->withdrawal }}</td>
                                    <!--<td>{{ $product->balance }}</td>-->
                                    <td data-title="Description">{{ $product->description }}</td>
                                    <td data-title="Date">{{ date('Y-m-d h:i a',strtotime($product->created_at)) }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a href="{{ route('zakat_persons.destroy', ['id' => $product->id]) }}" class="btn btn-outline-danger btn-space btn-sm" onclick="return confirm('Are you sure')">Delete</a>
                                        </td>
                                    <?php } ?>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')



@endsection