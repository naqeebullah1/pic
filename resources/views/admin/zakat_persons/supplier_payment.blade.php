@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Personal Expenses</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('zakat_persons.index')}}">Zakat persons</a></li>
                <li class="breadcrumb-item active">Payment</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong>{{ Session::get('error') }}</strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Pay to Personal Expense</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('zakat_person.save_payment') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Name</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" name="name" type="text" placeholder="Person Name" required value="{{ $supplier->name }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Date</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control form-control-sm" type="date" name="created_at" value="{{date('Y-m-d')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Contact No</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="contact_no" placeholder="Add Contact No" required value="{{ $supplier->contact_no }}" readonly>
                                </div>
                                <input type="hidden" name="supplier_id" value="{{ $supplier->id }}">
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Description</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Payment Type</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select class="form-control" name="pay_from" id="pay" required>
                                        <option value="">Select Payment Type</option>
                                        <option value="bank">Bank</span></option>
                                        <option value="cash">Cash</option>
                                        <!--<option value="cheque">Cheque</option>-->
                                    </select>
                                </div>
                            </div>
                            <div id="bank">

                            </div>
                            <div id="cheque">

                            </div>
                            <div id="dollor_conversion">
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    $('body').on('change', '#pay', function () {
        var pay_type = $('#pay').val();

        var csrf = $('meta[name="csrf_token"]').attr('content');

        $.ajax({
            url: '{{ route("get-payment-type") }}',
            type: 'POST',
            data: {
                pay_type: pay_type,
                '_token': csrf
            },
            success: function (data) {
                // console.log(data)
                $('#bank').html(data);
                 $('#cheque').html('');
            }
        });
    });
//    get_cheque_fields
    $('body').on('change', '#transfer_to', function () {
        var pay_type = $(this).val();
        var csrf = $('meta[name="csrf_token"]').attr('content');
        $.ajax({
            url: '{{ route("get_cheque_fields") }}',
            type: 'POST',
            data: {
                pay_type: pay_type,
                '_token': csrf
            },
            success: function (data) {
                // console.log(data)
                $('#cheque').html(data);
            }
        });
    });

//    $('body').on('focusout', '#pay_price', function() {
//        var currency = $("#get_currency").val();
//        var dollor_rate = {{ Session::get('today_currency')  }};
//
//        if(currency == 'usd')
//        {
//            var total_dollor_amount = dollor_rate * $("#pay_price").val();
//
//            $('#dollor_conversion').html('<div class="form-group row"><label class="col-12 col-sm-3 col-form-label text-sm-right">Dollar in Rupees</label><div class="col-12 col-sm-8 col-lg-6"><input class="form-control" type="text" value="'+total_dollor_amount+'"></div></div>');
//        }
//        else
//        {
//            console.log('not usd');
//        }
//    });
</script>
@endsection