@extends('layouts.admin')

@section('style')

<style>
    .currentMonth{
        font-weight: bold;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Cities</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Cities List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Cities List
                        <div class="tools dropdown">
                            <a href="#" data-toggle="modal" data-target="#addCityModal"  class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add New City</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>City</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <tr>
                                    <td data-title="City">{{ $product->city }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a onclick="editCity({{$product->id}})" href="#" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('cities.destroy', ['id'=>$product->id]) }}" style="display:inline;">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                            </form>
                                        </td>
                                        <?php } ?>
                                </tr>


                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="addCityModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Add City</h4>
                    </div>
                    <form method="POST" id="advancePaymentForm" action="{{route('cities.store')}}">
                        {{csrf_field()}}
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <label class="col-lg-12">Add City Name</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="city" type="text" class="form-control"  min="0">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="advancePaymentForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="editCityModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Edit City</h4>
                    </div>
                    <form method="POST" id="updateCityForm" action="{{route('cities/update_city')}}">
                        {{csrf_field()}}

                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <label class="col-lg-12">Edit City</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="city_id" type="hidden" class="form-control"  min="0">
                                    <input name="city" type="text" class="form-control"  min="0">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="updateCityForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@section('script')


<script type="text/javascript">

                                            function editCity(cityId){
                                            $('input[name=city_id]').val(cityId);
                                            $.ajax({
                                            url: '{{ route("fetch_city") }}',
                                                    type: 'Get',
                                                    data: {id:cityId},
                                                    success: function(data) {
                                                    $('input[name=city]').val(data);
                                                    $('#editCityModal').modal('show');
                                                    }
                                            });
                                            }
                                           
</script>

@endsection