@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>
                <li class="breadcrumb-item"><a href="{{route('sales.edit',['id'=>$customer_id])}}">Sales</a></li>

                <li class="breadcrumb-item active">Customer Purchase Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table pb-4">
                    <div class="card-header" style="font-weight: bold">Customer Sale Ledger</div>
                    <div class="card-body m-1">
                        <table class="table table-striped table-fw-widget table-bordered no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Vr No</th>
                                    <th>Product code</th>
                                    <th>Unit Price</th>
                                    <th>Purchase Qty</th>
                                    <th>Total Amount</th>
                                    <th>Discount (%)</th>
                                    <th>Sale Price</th>
                                    <th>Original Price</th>
                                    <th>Net Amount</th>
                                    <th>Paid</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $ledger)

                                <tr>
                                    <td data-title="Product Name">{{ $ledger->product_name.' | '.($ledger->pack) }}</td>
                                    <td data-title="Vr No">{{ $ledger->vr_no }}</td>
                                    <td data-title="Product Code">{{ $ledger->product_code }}</td>
                                    <td data-title="Unit Price">{{ $ledger->unit_price }}</td>
                                    <td data-title="Purchase Qty">{{ $ledger->qty }}</td>
                                    <td data-title="Total Amount">{{ $ledger->actual_price }}</td>
                                    <td data-title="Discount (%)">{{ $ledger->discount }}</td>
                                    <td data-title="Sale Price">{{ $ledger->total_price }}</td>
                                    <td data-title="Original Price">{{ $ledger->total_amount }}</td>
                                    <td data-title="Net Amount">{{ $ledger->net_amount }}</td>
                                    <td data-title="Paid">{{ $ledger->paid }}</td>
                                    <td data-title="Date"> {{ date('Y-m-d',strtotime($ledger->created_at)) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!--expenses show-->
                    <?php
//                    dump($sale_expenses)
                    ?>
                    @if($sale_expenses->count())
                    <div class="card-header" style="font-weight: bold">Extra Expenses With sale</div>
                    <div class="card-body" style="margin: 0 1rem;">
                        <table class="table table-bordered" style="background-color: #eeeeee4d;">
                            <thead>
                                <tr>
                                    <th>Amount</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sale_expenses as $expenses)

                                <tr>
                                    <td>{{ $expenses->total_price }}</td>
                                    <td>{{ $expenses->description }}</td>
                                    <td> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $expenses->created_at)->format('d-m-Y') }}</td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection