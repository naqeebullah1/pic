@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customer</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>

                <li class="breadcrumb-item active">Customer Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Customer - <b>{{ $customerName->name }}</b><br/>
                        Dues - <b>{{ $customerName->dues }} Rs/-</b>
                        <?php
//                    dump($customer);
                        ?>

                    </div>
                    <?php
                    if (Request::segment(3)) {
                        ?>
                        <div class="text-right pr-4 pb-3"><a onclick="return confirm('Are You sure you want to update Ledger.')" href="{{route('update.ledger_payment',Request::segment(3))}}" class="btn btn-outline-primary btn-space btn-sm">Update Ledger</a>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="card-body p-1">
                        <table class="table no-more-tables table-striped table-bordered table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Date</th>                                    
                                    <th>VR No</th>                                    
                                    <th>Sale - Discount</th>
                                    <th>Return Sale</th>
                                    <th>Payment Discount</th>
                                    <th>Payment</th>
                                    <th>Remaining</th>                                    
                                    <th>Sale details</th>                                    
                                    <th>Payment Type</th>                                    
                                    <th>Description</th>                                    
                                    <th>Created By</th>  
                                    <?php // if (auth()->user()->user_type == 1) { ?>

                                    <th>Action</th>                                    
                                    <?php // } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer as $details)
                                <?php
                                $ledger = array_values($details)[0];
                                ?>
                                
                                @if($ledger->payment>0)
                                   <tr style="background-color:lightblue;">
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="VR No">
                                        @if($ledger->sale != 0)
                                        <a href="#" class="xedit" 
                                           data-pk="{{$ledger->invoice_no}}"
                                           data-name="sales-vr_no" 
                                           data-placement="right">
                                            {{ $ledger->vr_no }}
                                        </a>
                                        @endif

                                    </td>
                                    <td data-title="Sale - Discount">
                                        <?php
                                        if ($ledger->sale != 0) {
                                            ?>
                                            <?= $ledger->total_amount . '-' . $ledger->sale_discount . '=' ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }}, 'sale')"><?= $ledger->total_amount - $ledger->sale_discount ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->sale;
                                        }
                                        ?> 
                                    </td>
                                    <td data-title="Return Sale">
                                        <?php
                                        if ($ledger->return_sale != 0) {
                                            ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }}, 'return_sale')"><?= $ledger->return_sale ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->return_sale;
                                        }
                                        ?> 
                                    </td>
                                    <td data-title="Payment Discount">{{ $ledger->discount }}</td>
                                    <td data-title="Payment">{{ $ledger->payment }}</td>
                                    <td data-title="Remaining">{{ $ledger->remaining }}</td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($ledger->sale_details) {
                                            ?>
                                            <table class="table table-sm table-bordered table-grey">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Cotton</th>
                                                    <th>Total</th>
                                                </tr>
                                                @php $i=1; @endphp
                                                @foreach($details as $sd)

                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$sd->product_name}}</td>
                                                    <td>{{ round($sd->total_price/$sd->qty,2) }}</td>
                                                    <td>{{$sd->qty}}</td>
                                                    <td>@php if(isset($sd->cotton)){
                                                        echo $sd->cotton;
                                                        }?></td>
                                                    <td>{{round($sd->total_price/$sd->qty,2) * $sd->qty }}</td>
                                                </tr>

                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Payment Type">
                                        <?php
                                        if ($ledger->payment) {
                                            if ($ledger->bank_id) {
                                                if (isset($banks[$ledger->bank_id])) {
                                                    echo $banks[$ledger->bank_id];
                                                }
                                            } else {
                                                echo 'Cash';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Description">
                                        <?php
                                        echo $ledger->narration;
                                        ?>
                                    </td>
                                    <td data-title="Created By">
                                        <?php
                                        if ($ledger->table == 'users') {
                                            if (isset($users[$ledger->created_by])) {
                                                echo $users[$ledger->created_by] . ' (Admin)';
                                            } else {
                                                echo '<span class="text-danger">Record Deleted</span>';
                                            }
                                        }
                                        if ($ledger->table == 'salesman') {
                                            //echo $salesman[$ledger->created_by];
                                        }
                                        ?>
                                    </td>

                                    <td nowrap data-title="Action">
                                        <?php
                                        if (auth()->user()->user_type == 1) {
                                            if ($ledger->return_sale) {
//                                                dump($ledger);
                                                ?>
                                               <a onclick="return confirm('are you sure')" href="{{ url('sale/delete_return_sale', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                   Delete Return Sale
                                                </a>
                                            <?php
                                            }
                                            if ($ledger->sale) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ url('sale/delete_sale', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                   Delete Sale
                                                </a>
                                                <?php
                                            }
                                            if ($ledger->payment && $ledger->invoice_no == 0) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ route('customer/delete_payment', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete
                                                </a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </td>

                                </tr>
                                
                                
                                
                                @else
                                
                                   <tr>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="VR No">
                                        @if($ledger->sale != 0)
                                        <a href="#" class="xedit" 
                                           data-pk="{{$ledger->invoice_no}}"
                                           data-name="sales-vr_no" 
                                           data-placement="right">
                                            {{ $ledger->vr_no }}
                                        </a>
                                        @endif

                                    </td>
                                    <td data-title="Sale - Discount">
                                        <?php
                                        if ($ledger->sale != 0) {
                                            ?>
                                            <?= $ledger->total_amount . '-' . $ledger->sale_discount . '=' ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }}, 'sale')"><?= $ledger->total_amount - $ledger->sale_discount ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->sale;
                                        }
                                        ?> 
                                    </td>
                                    <td data-title="Return Sale">
                                        <?php
                                        if ($ledger->return_sale != 0) {
                                            ?>
                                            <a href="#" onclick="fetchHistory({{ $ledger->invoice_no }}, 'return_sale')"><?= $ledger->return_sale ?></a>
                                            <?php
                                        } else {
                                            echo $ledger->return_sale;
                                        }
                                        ?> 
                                    </td>
                                    <td data-title="Payment Discount">{{ $ledger->discount }}</td>
                                    <td data-title="Payment">{{ $ledger->payment }}</td>
                                    <td data-title="Remaining">{{ $ledger->remaining }}</td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($ledger->sale_details) {
                                            ?>
                                            <table class="table table-sm table-bordered table-grey">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Cotton</th>
                                                    <th>Total</th>
                                                </tr>
                                                @php $i=1; @endphp
                                                @foreach($details as $sd)

                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$sd->product_name}}</td>
                                                    <td>{{ round($sd->total_price/$sd->qty,2) }}</td>
                                                    <td>{{$sd->qty}}</td>
                                                    <td>@php if(isset($sd->cotton)){
                                                        echo $sd->cotton;
                                                        }?></td>
                                                    <td>{{round($sd->total_price/$sd->qty,2) * $sd->qty }}</td>
                                                </tr>

                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Payment Type">
                                        <?php
                                        if ($ledger->payment) {
                                            if ($ledger->bank_id) {
                                                if (isset($banks[$ledger->bank_id])) {
                                                    echo $banks[$ledger->bank_id];
                                                }
                                            } else {
                                                echo 'Cash';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Description">
                                        <?php
                                        echo $ledger->narration;
                                        ?>
                                    </td>
                                    <td data-title="Created By">
                                        <?php
                                        if ($ledger->table == 'users') {
                                            if (isset($users[$ledger->created_by])) {
                                                echo $users[$ledger->created_by] . ' (Admin)';
                                            } else {
                                                echo '<span class="text-danger">Record Deleted</span>';
                                            }
                                        }
                                        if ($ledger->table == 'salesman') {
                                            //echo $salesman[$ledger->created_by];
                                        }
                                        ?>
                                    </td>

                                    <td nowrap data-title="Action">
                                        <?php
                                        if (auth()->user()->user_type == 1) {
                                            if ($ledger->return_sale) {
//                                                dump($ledger);
                                                ?>
                                               <a onclick="return confirm('are you sure')" href="{{ url('sale/delete_return_sale', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                   Delete Return Sale
                                                </a>
                                            <?php
                                            }
                                            if ($ledger->sale) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ url('sale/delete_sale', ['id' => $ledger->invoice_no]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                   Delete Sale
                                                </a>
                                                <?php
                                            }
                                            if ($ledger->payment && $ledger->invoice_no == 0) {
                                                ?>
                                                <a onclick="return confirm('are you sure')" href="{{ route('customer/delete_payment', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete
                                                </a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </td>

                                </tr>
                                
                                
                                @endif
                             

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="purchaseDetailModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content" style="max-width: 100% !important;">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;" id="changeHeader"></h4>
                    </div>

                    <div class="modal-body" id="purchaseDetailBody" style="padding:10px">
                    </div>
                    <div class="modal-footer" style="padding:10px">
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<link href="<?= asset('public/editable/css/bootstrap-editable.css') ?>" rel="stylesheet"/>
<script src="<?= asset('public/editable/js/bootstrap-editable.min.js') ?>"></script>
<script type="text/javascript">

                                            $(document).ready(function () {
                                            $.ajaxSetup({
                                            headers: {
                                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                                            }
                                            });
                                            $('.xedit').editable({
                                            url: '{{ route("admin.edit.vr_no") }}',
                                                    title: 'Update',
                                                    success: function (response, newValue) {
                                                    console.log('Updated', response)
                                                    },
                                                    error:function(response){
                                                    console.log(response);
                                                    }
                                            });
                                            });
                                            function fetchHistory(invoiceNo, type){
                                            $.ajax({
                                            data: {
                                            invoiceNo : invoiceNo,
                                                    type : type,
                                            },
                                                    type: 'GET',
                                                    url: "{{ route('sale/history') }}",
                                                    success: function(return_data) {
                                                    if (type == 'sale'){
                                                    $('#changeHeader').text('Sale Details');
                                                    } else{
                                                    $('#changeHeader').text('Return Sale Details');
                                                    }
                                                    $('#purchaseDetailBody').html(return_data);
                                                    $('#purchaseDetailModal').modal('show');
                                                    },
                                            });
                                            }

</script>

@endsection