@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />

<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>

                <li class="breadcrumb-item active">Edit Customer</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Edit New Customer</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('company/update') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Customer Type</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select class="form-control" name="type" required>
                                        <option value="">Select Customer</option>
                                        <option value="dealer" <?= ($company->type == 'dealer') ? 'selected="selected"' : ''; ?> >Dealer</option>
                                        <option value="company" <?= ($company->type == 'company') ? 'selected="selected"' : ''; ?>>Company</option>
                                        <option value="customer" <?= ($company->type == 'customer') ? 'selected="selected"' : ''; ?>>Customer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Sales Man</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select name="area_person_id" class="select2" data-placeholder="Select Sales Man">
                                        <option value=""></option>
                                        @foreach($area_persons as $person)
                                        <option value="{{$person->id}}" <?= ($company->area_person_id == $person->id) ? 'selected="selected"' : ''; ?> >{{ucwords($person->name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Name</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" name="id" type="hidden" value="{{ $company->id }}">
                                    <input class="form-control" name="name" type="text" value="{{ $company->name }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Cnic</label>
                                <div  class="col-12 col-sm-8 col-lg-6">
                                    <input data-mask="cnic" class="form-control" name="cnic" value="{{ $company->cnic }}" type="text" placeholder="Cnic without dashes" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Contact Number</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="contact" value="{{ $company->contact }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Email Address</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="email" name="email" value="{{ $company->email }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Address</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="test" name="address" value="{{ $company->address }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">City</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select name="city" class="select2">
                                        @foreach($cities as $city)
                                        <option value="{{$city->city}}" <?= ($company->city == $city->city) ? 'selected="selected"' : ''; ?> >{{ucwords($city->city)}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Select Country</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select name="country" class="select2" data-placeholder="Select Country">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->country}}" <?= ($company->country == $country->country) ? 'selected="selected"' : ''; ?> >{{$country->country}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--                            <div class="form-group row">
                                                            <label class="col-12 col-sm-3 col-form-label text-sm-right">Dues</label>
                                                            <div class="col-12 col-sm-8 col-lg-6">
                                                                <input class="form-control" type="number" name="dues" min="0" value="{{ $company->dues }}" required>
                                                            </div>
                                                        </div>-->
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-masks.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function () {
    //-initialize the javascript
    App.init();
    App.masks();
});
</script>
@endsection