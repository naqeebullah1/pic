@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customer</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('supplier.index')}}">Customers</a></li>

                <li class="breadcrumb-item active">Customer Cheques</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Customer Cheques
                        <div class="tools dropdown">
                            <a href="javascript:" data-toggle="modal" data-target="#purchaseDetailModal" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Clear Cheques</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table no-more-tables table-striped table-bordered table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Cheque No</th>
                                    <th>Amount</th>
                                    <th>Released On</th>
                                    <th>Payment Type</th>
                                    <th>Is Cleared</th>

                                    <th>Created On</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cheques as $ledger)
                                <tr>
                                    <td data-title="Customer Name" >{{ $ledger->name }}</td>
                                    <td data-title="Cheque No">{{ $ledger->cheque_no }}</td>
                                    <td data-title="Amount">{{ $ledger->amount }}</td>
                                    <?php
                                    ?>
                                    <td data-title="Released On">@if($ledger->release_date)
                                            {{ date('Y-m-d', strtotime($ledger->release_date)) }}
                                        @endif
                                    </td>
                                    <td data-title="Payment Type">{{ $ledger->transfer_to }}</td>

                                    <td data-title="Is Cleared"> <?php
                                        if ($ledger->is_clear == 0) {
                                            echo "<span class='text-warning'>Pending</span>";
                                        }
                                        if ($ledger->is_clear == 1) {
                                            echo "<span class='text-success'>Cleared</span>";
                                        }
                                        if ($ledger->is_clear == 2) {
                                            echo "<span class='text-danger'>Not Cleared</span>";
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Created On"> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <td data-title="Action"> 
                                            <?php
                                            if ($ledger->is_clear == 0 || $ledger->is_clear == 2) {
                                                ?>

                                                <a onclick="return confirm('are you sure')" href="{{ route('customer/cheque/delete', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Delete
                                                </a>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="purchaseDetailModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-xl">

                <!-- Modal content-->
                <div class="modal-content" style="max-width: 100% !important;">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">List Of cheques that needs to be cleared</h4>
                    </div>

                    <div class="modal-body" id="purchaseDetailBody" style="padding:10px">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Cheque No</th>
                                    <th>Amount</th>
                                    <th>Released On</th>
                                    <th>Payment Type</th>
                                    <th>Is Cleared</th>

                                    <th>Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($unclearcheques as $ledger)
                                <tr>
                                    <td>{{ $ledger->name }}</td>
                                    <td>{{ $ledger->cheque_no }}</td>
                                    <td>{{ $ledger->amount }}</td>
                                    <td>{{ date('Y-m-d',strtotime($ledger->release_date)) }}</td>
                                    <td>{{ $ledger->transfer_to }}</td>

                                    <td id="clear<?= $ledger->id ?>"> 
                                        <a href="javascript:" onclick="isclear('clear', '<?= $ledger->id; ?>')"  class="btn btn-success btn-sm">Cleared</a>
                                        <a href="javascript:" onclick="isclear('notclear', '<?= $ledger->id; ?>')" class="btn btn-danger btn-sm">Not Cleared</a>
                                    </td>
                                    <td> {{ date('Y-m-d',strtotime($ledger->created_at)) }}</td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer" style="padding:10px">
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
                                            function isclear(status, id) {

                                                if (confirm('Are You Sure')) {
                                                    $.ajax({
                                                        url: '<?= url('cheque_cleared') ?>',
                                                        type: 'get',
                                                        data: {
                                                            status: status,
                                                            id: id
                                                        },
                                                        success: function (data) {
                                                            data = JSON.parse(data);
                                                            if (data.flag == 1) {
                                                                $('#clear' + id).parents('tr').remove();
                                                                location.reload();
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                           
</script>

@endsection