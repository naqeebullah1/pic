@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>
                <li class="breadcrumb-item"><a href="{{route('return_sale.edit',['id'=> $redirect])}}">Return Sale</a></li>

                <li class="breadcrumb-item active">Return Sale Details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Customer Return Sale Ledger
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Product code</th>
                                    <th>Unit Price</th>
                                    <th>Return Qty</th>
                                    <th>Total Amount</th>
                                    <th>Discount (%)</th>
                                    <th>Sale Price</th>
                                    <th>Original Price</th>
                                    <th>Paid</th>
                                    <th>Remarks</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $ledger)

                                <tr>
                                    <td data-title="Product Name">{{ $ledger->product_name.' | '.($ledger->pack) }}</td>
                                    <td data-title="Product Code">{{ $ledger->product_code }}</td>
                                    <td data-title="Unit Price">{{ $ledger->unit_price }}</td>
                                    <td data-title="Return Qty">{{ $ledger->qty }}</td>
                                    <td data-title="Total Amount">{{ $ledger->actual_amount }}</td>
                                    <td data-title="Discount (%)">{{ $ledger->item_discount }}</td>
                                    <td data-title="Sale Price">{{ $ledger->total_price }}</td>
                                    <td data-title="Original Price">{{ $ledger->net_amount }}</td>
                                    <td data-title="Paid">{{ $ledger->paid }}</td>
                                    <td data-title="Remarks">{{ $ledger->remarks }}</td>
                                    <td data-title="Date"> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection