<table class="table no-more-tables table-bordered table-sm" style="background-color:#eee">
    <thead>
        <tr>
            <th>
                Customer Name
            </th>
            <th>
                Type
            </th>
            <th>
                Dues
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="Customer Name">
                {{ $supplier->name }}
            </td>
            <td data-title="Type">
                {{ ucfirst($supplier->type) }}
            </td>
            <td data-title="Dues">
                {{ $supplier->dues }}
            </td>

        </tr>
    </tbody>
</table>
<!--<hr>-->
<div class="card-header pt-0 m-0"><b>Select Products</b></div>

<table class="table table-bordered table-sm" style="background-color:#eee">
    <thead>
        <tr>
            <th>
                Sales man
            </th>
            <th>
                Product
            </th>
            <th>
                Sale Type
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <select class="select2 salesman" form="saleForm" name='salesman' data-placeholder="Select Sales man">
                    <!--<option value="">Select Sales man</option>-->
                    <option value="0">Admin</option>
                    @foreach($salesman as $product)
                    <option <?= ($getSaleDetails['sale_man_id'] == $product->id) ? 'selected' : '' ?> value="{{ $product->id }}"><?php echo $product->name; ?></option>
                    @endforeach
                </select>
            </td>
            <td>
                <select class="select2 product" name='product[]' data-placeholder="Select Product">
                    <option value="">Select Product </option>
                    @foreach($products as $product)
                    <option value="{{ $product->id }}">
                        <?php echo ($product->qty) ? $product->product_name . ' | ' . ($product->qty) : $product->product_name; ?></option>
                    @endforeach
                </select>
            </td>
            <td>
                <select class="form-control" name='sale_type' style="height: 30px;padding: 0 10px;" form="saleForm">
                    <option <?= ($getSaleDetails['sale_type'] == 'retail') ? 'selected' : '' ?> value='retail'>Retail</option>
                    <option <?= ($getSaleDetails['sale_type'] == 'whole') ? 'selected' : '' ?> value="whole">Whole</option>
                </select>
            </td>

        </tr>
    </tbody>
</table>
<div class="card-header pt-0 m-0 pb-0"><b>List Of Products</b></div>
<br>

<input type="hidden" name="supplier_id" value="{{ $supplier->id }}">
<input type="hidden" name="dues" value="{{ $supplier->dues }}">

{{ csrf_field() }}
<table class="table no-more-tables table-striped">
    <thead class="border">
        <tr>
            <th width="5%">
                P-ID
            </th>

            <th>
                Product Name
            </th>
            <th>
                Whole Rate
            </th>
            <th>
                Retail Rate
            </th>
            <th>
                Total Stock
            </th>
            <th>
                Sales man stock
            </th>
            <th>
                Sale Qty
            </th>
            <th>
                Discount (in %)
            </th>
            <th>
                Total Amount
            </th>
            <th>
                Delete
            </th>
        </tr>
    </thead>
    <tbody id="product_detail" class="border">
        @forelse($getSaleDetails->SaleDetails as $saleDetail)
        <?php
       $product = $saleDetail['product'];

        if ($product) {
            $saleQty=$saleDetail['qty'];
            $discount=$saleDetail['discount'];
            $unitPrice=$saleDetail['unit_price'];
            $wholeSaleRate=$saleDetail['product']['product_whole_sale_price'];
//            $productStock=$saleDetail['product']['stock_qty']+ $saleDetail['qty'];
            $productStock=$saleDetail['product']['stock_qty'];
            ?>
            <tr>
                <td data-title="P-ID">
                    <input class="form-control form-control-sm" type="text" readonly="" name="product_id[]" value="{{ $product['id'] }}">
                </td>
                <td data-title="Product Name">{{ $product['product_name'] }}</td>
                <td data-title="Whole Rate">
                    <input class="form-control whole_price form-control-sm" type="text" name="whole_price[]" value="{{ $wholeSaleRate }}">
                </td>
                <td data-title="Retail Rate">
                    <input class="form-control unit_price form-control-sm" type="text" name="retail_price[]" value="{{ $unitPrice }}">
                </td>
                <td data-title="Total Stock">
                    <input class="form-control form-control-sm" type="text" readonly="" name="current_qty[]" value="{{ $productStock }}">
                </td>
                <td data-title="Saleman Stock">
                    <input class="form-control form-control-sm" type="text" readonly="" name="salesman_stock[]" value="38">
                </td>
                <td data-title="Sale Qty">
                    <input class="form-control qty form-control-sm" type="number" name="qty[]" min="0" value="{{ $saleQty }}">
                </td>
                <td data-title="Discount (In %)">
                    <input class="form-control line-discount form-control-sm" type="text" name="discount_per_item[]" value="{{ $discount }}">
                </td>
                <td data-title="Total Amount">
                    <input class="form-control line_cost form-control-sm" type="text" readonly="" name="line_cost[]" value="">
                </td>
                <td data-title="Delete">
                    <a href="javascript:;" class="btn btn-danger delete btn-sm"><i class="fa fa fa-trash"></i></a>
                </td>
            </tr>
            <?php
        }
        ?>
        @empty
        <tr id="initial_row">
            <td colspan="9" class="text-center text-danger full-width-td" style="font-size:16px;">
                Please select an item from the list
            </td>
        </tr>
        @endforelse


    </tbody>
</table>

<div class="row">
    <div class="col-lg-8">
        <h4 style="font-weight:bold">Extra Expenses</h4>
        <table class="table table-sm table-bordered no-more-tables" style="background-color:#eeeeee4d">
            <thead>
                <tr>
                    <th>
                        Amount
                    </th>
                    <th>
                        Description

                    </th>
                    <th>
                        Delete

                    </th>
                </tr>
            </thead>
            <tbody id="extra_exp_body">

            </tbody>
            <tr>
                <th colspan="4" class="text-right">
                    <button id="add_more_exp" class="btn btn-success  btn-sm" type="button" >Add Expense</button>
                </th>
            </tr>

        </table>
    </div>
    <div class="col-lg-4">
        <h4 style="font-weight:bold">Summary</h4>

        <table class="table table-sm table-bordered" style="background-color:#eeeeee4d">
            <tr>
                <th>
                    <label> Total Amount</label>
                    <div>
                        <input class="form-control line_cost_total form-control-sm" type="text" name="line_cost_total" readonly value="0">
                    </div>
                </th>
            </tr>
            <tr>
                <th>
                    <label>Discount</label>
                    <div>
                        <input class="form-control discount form-control-sm" type="text" value="{{ $getSaleDetails->discount }}" name="discount" required>
                    </div>
                </th>
            </tr>
            <tr>

                <th>
                    <label> Net Amount</label>

                    <div>
                        <input class="form-control total form-control-sm" type="text" readonly value="0" name="net_amount">
                    </div>
                </th>

            </tr>
            <tr>

                <th>
                    <label> Paid Amount</label>

                    <div>
                        <input class="form-control paidamount form-control-sm" type="number" min="0" value="0" name="paidamount" required>
                    </div>
                </th>
            </tr>
            <tr>
                <th>
                    <label> Due Amount</label>
                    <div>
                        <input class="form-control due_amount form-control-sm" type="text" readonly value="0" name="due_amount">
                    </div>
                </th>
            </tr>
            <tbody>
                   <!--                                            <tr>
                                                                   <td>
                                                                       <div class="form-group pt-2">
                                                                           <input class="form-control line_cost_total form-control-sm" type="text" name="line_cost_total" readonly value="0">
                                                                       </div>
                                                                   </td>
                                                                   <td>
                                                                       <div class="form-group pt-2">
                                                                           <input class="form-control discount form-control-sm" type="text" value="0" name="discount" required>
                                                                       </div>
                                                                   </td>
                                                                   <td>
                                                                       <div class="form-group pt-2">
                                                                           <input class="form-control total form-control-sm" type="text" readonly value="0" name="net_amount">
                                                                       </div>
                                                                   </td>
                                                                   <td>
                                                                       <div class="form-group pt-2">
                                                                           <input class="form-control paidamount form-control-sm" type="number" min="0" value="0" name="paidamount" required>
                                                                       </div>
                                                                   </td>
                                                                   <td>
                                                                       <div class="form-group pt-2">
                                                                           <input class="form-control due_amount form-control-sm" type="text" readonly value="0" name="due_amount">
                                                                       </div>
                                                                   </td>
                                                               </tr>-->
            </tbody>
        </table>
    </div>
</div>
<table class="table table-bordered no-more-tables">
    <thead>
        <tr>
            <th>
                Description
            </th>
            <th>
                VR NO
            </th>
            <th>
                Date
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="Description">
                                   <input name="description" value="{{ $getSaleDetails->narration }}" class="form-control form-control-sm" type="text">
            </td>
            <td data-title="VR No">
                <input class="form-control form-control-sm" type="text" value="{{ $getSaleDetails->vr_no }}" name="vr_no">
            </td>
            <td data-title="Date">
                <input class="form-control form-control-sm" type="date" name="created_at" value="{{date('Y-m-d')}}">
            </td>
        </tr>
    </tbody>

</table>
<div class="col text-center">
    <input type="checkbox" name="send_msg" value="1"> Send Message
    <button name="submit" value="submit" class="btn btn-space btn-primary">
        Submit
    </button>
    <button name="print" value="print" class="btn btn-space btn-primary">
        Submit and print
    </button>
</div>
