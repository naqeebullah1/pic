<table class="table no-more-tables table-bordered table-sm" style="background-color:#eee">
    <thead>
        <tr>
            <th>
                Supplier  Name
            </th>

            <th>
                Dues
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>

            <td data-title="Customer Name">
                {{ $supplier->name }}
            </td>

            <td data-title="Dues">
                {{ $supplier->dues }}
            </td>

        </tr>
    </tbody>
</table>
<!--<hr>-->
<div class="card-header pt-0 m-0"><b>Select Products</b></div>

<table class="table table-bordered table-sm" style="background-color:#eee">
    <thead>
        <tr>

            <th>
                Product
            </th>

        </tr>
    </thead>
    <tbody>
        <tr>

            <td>
                <select class="select2 product" name='product[]' data-placeholder="Select Product">
                    <option value="">Select Product </option>
                    @foreach($products as $product)
                    <option value="{{ $product->id }}">
                        <?php echo ($product->qty) ? $product->product_name . ' | ' . ($product->qty) : $product->product_name; ?></option>
                    @endforeach
                </select>
            </td>


        </tr>
    </tbody>
</table>
<div class="card-header pt-0 m-0 pb-0"><b>List Of Products</b></div>
<br>

<input type="hidden" name="supplier_id" value="{{ $supplier->id }}">
<input type="hidden" name="dues" value="{{ $supplier->dues }}">

{{ csrf_field() }}
<table class="table no-more-tables table-striped">
    <thead class="border">
        <tr>
            <th width="5%">
                P-ID
            </th>

            <th>
                Product Name
            </th>

            <th>
               Unit Price
            </th>
            <th>
                Current Stock
            </th>

            <th>
                Purchase Qty
            </th>
            <th>
                Discount (in %)
            </th>
            <th>
                Total Amount
            </th>
            <th>
                Delete
            </th>
        </tr>
    </thead>

    <tbody id="product_detail" class="border">
        @forelse($getSaleDetails->PurchaseDetails as $saleDetail)
        <?php
       $product = $saleDetail['product'];

        if ($product) {
            $saleQty=$saleDetail['qty'];
            $discount=$saleDetail['discount'];
            $unitPrice=$saleDetail['unit_price'];
            $totalprice=$saleDetail['total_price'];


//            $productStock=$saleDetail['product']['stock_qty']+ $saleDetail['qty'];
            $productStock=$saleDetail['product']['stock_qty'];
            ?>

            <tr>
                <td data-title="P-ID">
                    <input class="form-control form-control-sm" type="text" readonly="" name="product_id[]" value="{{ $product['id'] }}">
                </td>
                <td data-title="Product Name">{{ $product['product_name'] }}</td>

                <td data-title="Retail Rate">
                    <input class="form-control unit_price form-control-sm" type="text" name="retail_price[]" value="{{ $unitPrice }}">
                </td>
                <td data-title="Total Stock">
                    <input class="form-control form-control-sm" type="text" readonly="" name="current_qty[]" value="{{ $productStock }}">
                </td>

                <td data-title="Sale Qty">
                    <input class="form-control qty form-control-sm" type="number" name="qty[]" min="0" value="{{ $saleQty }}">
                </td>
                <td data-title="Discount (In %)">
                    <input class="form-control line-discount form-control-sm" type="text" name="discount_per_item[]" value="{{ $discount }}">
                </td>
                <td data-title="Total Amount">
                    <input class="form-control line_cost form-control-sm" type="text" readonly="" name="line_cost[]" value="{{ $totalprice }}">
                </td>
                <td data-title="Delete">
                    <a href="javascript:;" class="btn btn-danger delete btn-sm"><i class="fa fa fa-trash"></i></a>
                </td>
            </tr>
            <?php
        }
        ?>
        @empty
        <tr id="initial_row">
            <td colspan="9" class="text-center text-danger full-width-td" style="font-size:16px;">
                Please select an item from the list
            </td>
        </tr>
        @endforelse


    </tbody>
</table>
<h3 class="m-0">Summary</h3>

<table style="background-color: #f8f8f8;" class="table table-bordered no-more-tables table-small">
    <thead>
        <tr>
            <th class="p-2">
                Total Amount
            </th>
            <th class="p-1">
                Discount <span class="required">*</span>
            </th>
            <th class="p-1">
                Net Amount
            </th>
            <th class="p-1">
                Paid Amount <span class="required">*</span>
            </th>
            <th class="p-1">
                Due Amount
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="Total Amount">
                <input class="form-control line_cost_total form-control-sm" type="text" name="line_cost_total" readonly value="0">
            </td>
            <td data-title="Discount">
                <input class="form-control discount form-control-sm" type="text" value="0" name="discount" required>
            </td>
            <td data-title="Net Amount">
                <input class="form-control total form-control-sm" type="text" readonly value="0" name="net_amount">
            </td>
            <td data-title="Paid Amount">
                <input class="form-control paidamount form-control-sm" type="text" value="0" name="paidamount" required>
            </td>
            <td data-title="Due Amount">
                <input class="form-control due_amount form-control-sm" type="text" readonly value="0" name="due_amount">
            </td>
        </tr>
    </tbody>
</table>
</div>

<table class="table table-bordered no-more-tables">
    <thead>
        <tr>
            <th>
                Description
            </th>
            <th>
                VR NO
            </th>
            <th>
                Date
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="Description">
                                   <input name="description" value="{{ $ledger->narration }}" class="form-control form-control-sm" type="text">
            </td>
            <td data-title="VR No">
                <input class="form-control form-control-sm" type="text" value="{{ $getSaleDetails->vr_no }}" name="vr_no">
            </td>
            <td data-title="Date">
                <input class="form-control form-control-sm" type="date" name="created_at" value="{{ $getSaleDetails->created_at->format('Y-m-d') }}">
            </td>
        </tr>
    </tbody>

</table>
<div class="col text-center">
    <input type="checkbox" name="send_msg" value="1"> Send Message
    <button name="submit" value="submit" class="btn btn-space btn-primary">
        Submit
    </button>
    <button name="print" value="print" class="btn btn-space btn-primary">
        Submit and print
    </button>
</div>
