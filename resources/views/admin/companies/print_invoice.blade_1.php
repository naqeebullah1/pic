<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="{{ asset('public/images/logo2.png') }}">
        <title>S.M Qaisar medicines</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/assets/css/app.css') }}" type="text/css" />

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <style>
            #table1_length select{
                width:60px;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }} " />

        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datatables/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" />
        <style>
            body {
                margin: 0;
                padding: 0;
                font: 12pt "Tahoma";
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 21cm;
                min-height: 29.7cm;
                padding: 2cm;
                margin: 1cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            @page {
                size: A4;
                margin: 0;
            }
            #footer {
                padding:10px;
                width: 100%;   
                text-align: center;
                border-top: 1px solid black;    
            }
            #footer span{
                font-weight: bold;
            }
            @media print {
                .row:before{
                    content:url("{{asset('public/images/color_bg.png')}}") !important;
                    position: absolute !important;
                    top:450px !important;
                    opacity: 0.2 !important;
                }
                body {
                    position: relative;
                }
                #footer {
                    position: absolute;
                    bottom: 0;
                    border-top: 1px solid black;
                    padding: 20px 10px;
                    width: 100%;   
                    text-align: center;
                }
                #footer span{
                    font-weight: bold;
                }
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }
            .c_name{
                font-size: 30px;
                font-family: "Times New Roman";
                font-weight: bold;
                color: #0F6838;
                text-shadow: -1px 1px 1px;
            }
            .no-margin{
                margin-bottom: 5px;

            }
            .import_text{
                font-size: 22px;
                font-family: arial;
                margin-top: 10px;
                font-weight: bold;
            }
            .header_details{
                font-size:16px;
                font-family: arial;
                font-weight: bold;
            }
            .img_logo{
                width:300px;
            }
            .invoice_text{
                font-family: arial;
                font-size: 16px;
                font-weight: bold;
            }
            .invoice_no{
                font-size: 20px;
                font-weight: bold;
                border-bottom: 1px solid black;
                padding: 0 100px 0 20px;
                font-family: "Times New Roman";
            }
            .my_table th{
                border: 1px solid #ccc;
                padding-left: 10px;
                /*border-right: 1px solid black;*/
            }
            .my_table td{
                border: 1px solid #ccc;
                padding-left: 10px;

                /*border-right: 1px solid black;*/
            }
            .my_table .empty_tr td{
                border: none !important;

                /*border-right: 1px solid black;*/
            }
            .my_table{
                border-left: none;
                border-right: none;
                border-bottom: none;

            }
            .detail_tr{
                font-weight: bold;
            }
            .empty_td{
                border: 1px solid transparent !important;
            }
        </style>
    </head>

    <body style="background-color:white;">
        <div class="be-wrapper subpage" style="padding:10px !important">

            <div class="main-content container-fluid">
                <div class="d-flex">
                    <div style="float:left;width:50%">
                        <p class="no-margin c_name">S.M Qaiser</p>
                        <p class="no-margin import_text" >Importers, exporters growers & seed merchants</p>
                        <p class="no-margin header_details">Shop No 4, Qazi Mansion New Grain Market Peshawar City</p>
                        <p class="no-margin header_details">Ph: 0092-91-2550043, 2550044</p>
                        <p class="no-margin header_details">Email: suntechseeds@gmail.com</p>
                        <p class="no-margin header_details">Website: www.suntechseeds.com</p>
                        <p class="no-margin header_details" style="font-size:17px">
                            <span style="font-size:17px">CEO: Abdur Rehman</span> Mob: 0343-9997454
                        </p>
                        <p class="no-margin header_details">
                            <span style="font-size:17px">MD: Farman Khan</span> Mob: 0300-8592886
                        </p>
                    </div>
                    <div class="" style="width:50%">
                        <img class="img_logo" style="float:right;" src="{{asset('public/images/logo.png')}}">
                    </div>
                </div>
                <div style="margin-top: 10px;display: flex;">
                    <div class="" style="width:50%;">
                        <span class="invoice_text">Invoice No </span><span class="invoice_no">{{$sales->id}}</span>
                    </div>
                    <div class="" style="width:50%;text-align: right;">
                        <span class="invoice_text">Date </span><span class="invoice_no">{{ date('d-M-Y',strtotime($sales->created_at)) }}</span>
                    </div>
                </div>
                <!--                                <div style="width:100%;margin-top: 20px;display:flex;">
                                                    <div>
                                                        <span class="invoice_text">Customer Name</span>
                                                    </div>
                                                    <div style="flex-grow:1">
                                                        <span class="invoice_no" style="">{{ucwords($sales->name)}}</span>
                                                    </div>
                                                    <div>
                                                        <span class="invoice_text">Contact NO</span>
                                                    </div>
                                                    <div style="flex-grow:1">
                                                        <span class="invoice_no" style="">{{ucwords($sales->contact)}}</span>
                                                    </div>
                                                </div>-->

                <div style="width:100%;margin-top: 20px;display: flex;">
                    <div style="padding:0 10px 0 0;"> <span class="invoice_text">Customer Name</span></div>
                    <div style="flex-grow:1;padding: 0 10px;border-bottom: 1px solid black;font-weight: bold;">{{ucwords($sales->name)}}</div>
                </div>
                <div style="width:100%;margin-top: 20px;display: flex;">
                    <div style="padding:0 10px 0 0;"> <span class="invoice_text">Address</span></div><div style="flex-grow:1;padding: 0 10px;border-bottom: 1px solid black;font-weight: bold;"><?php echo ucfirst($sales->address) . ',&nbsp;' . ucfirst(strtolower($sales->city)); ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <br/>
                        <br/>
                        <table class="my_table " style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Product</th>
                                    <th>Unit Price</th>
                                    <th>Qty</th>
                                    <th>Actual Amount</th>
                                    <th>Discount (%)</th>
                                    <th>After Discount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $key=>$ledger)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $ledger->product_name }}<?= ($ledger->p_qty) ? ' | ' . $ledger->p_qty : ''; ?></td>
                                    <td>{{ $ledger->unit_price }}</td>
                                    <td>{{ $ledger->qty }}</td>
                                    <td>{{ $ledger->actual_price }}</td>
                                    <td>{{ $ledger->item_discount }}</td>
                                    <td>{{ $ledger->total_price }}</td>
                                </tr>
                                @endforeach
                                <tr class="empty_tr">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr class="detail_tr">
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td style="border-top: none;border-bottom: none;text-align: right;">Total &nbsp; </td>
                                    <td>{{ $sales->total_amount }} </td>
                                </tr>
                                <tr class="detail_tr">
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td style="border-top: none;border-bottom: none;text-align: right;">Discount &nbsp; </td>

                                    <td>{{ $sales->discount }} </td>
                                </tr>
                                <tr class="detail_tr">
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td style="border-top: none;border-bottom: none;text-align: right;">Net Amount &nbsp; </td>

                                    <td>{{ $sales->net_amount }} </td>
                                </tr>
                                <tr class="detail_tr">
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td style="border-top: none;border-bottom: none;text-align: right;">Paid &nbsp;</td>

                                    <td>{{ $sales->paid }} </td>
                                </tr>
                                <tr class="detail_tr">
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td style="border-top: none;border-bottom: none;text-align: right;">New Balance &nbsp;</td>
                                    <td>{{ $sales->net_amount-$sales->paid }} </td>
                                </tr>
                                <tr class="detail_tr">
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td style="border-top: none;border-bottom: none;text-align: right;">Old Balance &nbsp;</td>
                                    <td>{{ $sales->dues-$sales->net_amount-$sales->paid }} PKR</td>
                                </tr>
                                <tr class="detail_tr">
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td class="empty_td"></td>
                                    <td style="border-top: none;border-bottom: none;text-align: right;">Remaining Balance &nbsp;</td>
                                    <td>{{ $sales->dues }} PKR</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <p style="margin-top: 20px">{{$sales->narration}}</p>
                </div>

            </div>


        </div>
        <div id="footer">
            <p>For any queries kindly contact us at <span>(0092-91-2550043, 2550044)</span>
                OR you can visit us on <span>http://www.suntechseeds.com</span>

            </p>
        </div>
    </body>
</html>           