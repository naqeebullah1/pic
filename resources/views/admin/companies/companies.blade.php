@extends('layouts.admin')
@section('style')
@endsection
@section('content')
<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Customers List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif
                <div class="card card-table p-1">
                    <div class="dropdown d-md-none">
                        <button type="button" class="float-right btn btn-outline-default btn-sm dropdown-toggle " data-toggle="dropdown" id="5b4f95e9a64b5" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div style="border:1px solid #ccc;" class="dropdown-menu" aria-labelledby="5b4f95e9a64b5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(63px, 31px, 0px);">
                            <a href="{{ url('companies?status=1') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active text-white' : '' ?>">Active Companies</a>
                            <a href="{{ url('companies?status=0') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active text-white' : '' ?>">Inactive Companies</a>
                        </div>
                    </div>

                    <!-- button group for desktop -->
                    <div class="d-none d-md-flex btn-group">
                        <a href="{{ url('companies?status=1') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active' : '' ?>">Active Customers</a>
                        <a href="{{ url('companies?status=0') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active' : '' ?>">Inactive Customers</a>
                    </div>


                    <div class="card-header">Customers List
                        <div class="tools dropdown">
                            <a href="{{ url('Company/add') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped no-more-tables table-bordered table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Type</th>
                                    <th>Dues</th>
                                    <th>Last Payment</th>
                                    <th>Payment</th>
                                    <th style="white-space:nowrap">Status</th>
                                    <th>Sale</th>
                                    <th>Sale History</th>
                                    <th>Return Sale</th>
                                    <th>Return History</th>
                                    <th>Ledger</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customers as $product)
                                <tr>
                                    <td data-title="Name">{{ $product->name }}</td>
                                    <td data-title="Contact">{{ $product->contact }}</td>
<!--                                    <td>{{ $product->email }}</td>
                                    <td>{{ $product->address }}</td>
                                    <td>{{ $product->city }}</td>-->
                                    <td data-title="Type">{{ ucwords($product->type) }}</td>
                                  
                                   @if($product->dues<0)
                                    
                                    <td data-title="Dues" style="color:Red;">{{ $product->dues }}</td>
 @endif
                                    @if($product->dues>-1)
                                    
                                     <td data-title="Dues">{{ $product->dues }}</td>
                                     @endif
                                    <td data-title="Last Payment">
                                        <span class="text-danger">
                                            <?php
                                            if ($product->last_payment) {
                                                $earlier = new DateTime();
                                                $later = new DateTime($product->last_payment);

                                                $diff = $later->diff($earlier)->format("%a");
                                                if ($diff == 0) {
                                                    echo 'Paid Today';
                                                } elseif ($diff == 1) {
                                                    echo '<b>' . abs($diff) . '</b> day ago';
                                                } else {
                                                    echo '<b>' . abs($diff) . '</b> days ago';
                                                }
                                            } else {
                                                echo 'Not Paid yet';
                                            }
                                            ?>
                                        </span>
                                    </td>
                                    <td data-title="Payment">
                                        <a href="{{ route('company/payment', ['id' => $product->id]) }}" class="btn btn-outline-secondary btn-space btn-sm">
                                            Pay
                                        </a>
                                    </td>
                                    <td data-title="Status">
                                        <?php
                                        if ($product->status) {
                                            ?>
                                            <a href="{{route('change.company.status',['id'=>$product->id])}}" class="btn btn-outline-danger btn-space btn-sm">
                                                Move to Inactive
                                            </a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="{{route('change.company.status',['id'=>$product->id])}}" class="btn btn-outline-success btn-space btn-sm">
                                                Move to Active
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Sale">
                                        <a href="{{ route('sales.show', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Sale
                                        </a>
                                    </td>
                                    <td data-title="Sale History">
                                        <a href="{{ route('sales.edit', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View
                                        </a>
                                    </td>
                                    <td data-title="Return Sale">
                                        <a href="{{ route('return_sale.show', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Return
                                        </a>
                                    </td>
                                    <td data-title="Return History">
                                        <a href="{{ route('return_sale.edit', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            <!--<a href="#" class="btn btn-outline-success btn-space btn-sm">-->
                                            View
                                        </a>
                                    </td>
                                    <td data-title="Ledger">
                                        <a href="{{ route('customer/ledger', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a href="{{ route('company/edit', ['id' => $product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="requestPayment">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Request For Payment</h3>
                        <a class="close" data-dismiss="modal">&times;</a>
                    </div>
                    <div class="modal-body">
                        <form method="post" id="messageForm" action="{{route('company/request_payment')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Enter Message</label>
                                <input type="hidden" name="customer_id">
                                <textarea class="form-control" name="message"></textarea> 
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-outline-success">Send</button>
                                <button class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    function requestPayment(num) {
        $('input[name=customer_id]').val(num);
        $('#requestPayment').modal('show');
    }

</script>

@endsection