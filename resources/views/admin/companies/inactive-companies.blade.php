@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Customers List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Customers List
                        <div class="tools dropdown">
                            <a href="{{ url('Company/add') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add New Customer</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Type</th>
                                    <th>Dues</th>
                                    <th>Last Payment</th>
                                    <th>Payment</th>
                                    <th style="white-space:nowrap">Status</th>
                                    <th>Sale</th>
                                    <th>Sale History</th>
                                    <th>Return Sale</th>
                                    <th>Return History</th>
                                    <th>Ledger</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>
                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customers as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->contact }}</td>
<!--                                    <td>{{ $product->email }}</td>
                                    <td>{{ $product->address }}</td>
                                    <td>{{ $product->city }}</td>-->
                                    <td>{{ ucwords($product->type) }}</td>
                                    <td>{{ $product->dues }}</td>

                                    <td>
                                        <span class="text-danger">
                                            <?php
                                            if ($product->last_payment) {
                                                $earlier = new DateTime();
                                                $later = new DateTime($product->last_payment);

                                                $diff = $later->diff($earlier)->format("%a");
                                                if ($diff == 0) {
                                                    echo 'Paid Today';
                                                } elseif ($diff == 1) {
                                                    echo '<b>' . abs($diff) . '</b> day ago';
                                                } else {
                                                    echo '<b>' . abs($diff) . '</b> days ago';
                                                }
                                            } else {
                                                echo 'Not Paid yet';
                                            }
                                            ?>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="{{ route('company/payment', ['id' => $product->id]) }}" class="btn btn-outline-secondary btn-space btn-sm">
                                            Pay
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{route('change.company.status',['id'=>$product->id])}}" class="btn btn-outline-dark btn-space btn-sm">
                                            Move to Active
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('sales.show', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Sale
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('sales.edit', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('return_sale.show', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Return
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('return_sale.edit', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            <!--<a href="#" class="btn btn-outline-success btn-space btn-sm">-->
                                            View
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('customer/ledger', ['id' => $product->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td>
                                            <a href="{{ route('company/edit', ['id' => $product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="requestPayment">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Request For Payment</h3>
                        <a class="close" data-dismiss="modal">&times;</a>
                    </div>
                    <div class="modal-body">
                        <form method="post" id="messageForm" action="{{route('company/request_payment')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Enter Message</label>
                                <input type="hidden" name="customer_id">
                                <textarea class="form-control" name="message"></textarea> 
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-outline-success">Send</button>
                                <button class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
                                                function requestPayment(num){
                                                $('input[name=customer_id]').val(num);
                                                $('#requestPayment').modal('show');
                                                }
                                               
</script>

@endsection