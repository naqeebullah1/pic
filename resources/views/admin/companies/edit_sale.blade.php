@extends('layouts.admin')

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />

<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>

                <li class="breadcrumb-item active">Customer Sale</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                    <div class="message"><strong>{{ Session::get('error') }}</strong></div>
                </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-header"><b>Customer Details</b></div>
                            <div class="card-body">
                                <form action="{{ route('sales.update',$getSaleDetails->id) }}" method="post" id="saleForm">
                                     {{ method_field('PATCH') }}
                                    @include('admin.companies.includes.sale_form',['isEdit'=>true])
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script>
$('.salesman').change(function () {
    var saleman = $(this).val();
    $('#product_detail').html('<tr id="initial_row">\n\
                                                <td colspan="9" class="text-center text-danger" style="font-size:16px;">\n\
                                                    Please select an item from the list\n\
                                                </td>\n\
                                            </tr>');
    $.ajax({
        type: 'GET',
        url: "{{ url('get-product-list') }}/" + saleman,
        success: function (return_data) {
            return_data = JSON.parse(return_data);
            var html = '<option value=""></option>';
            $.each(return_data, function (i, item) {
                html += '<option value="' + item.id + '">' + item.product_name + ' | Qty: ' + item.qty + '</option>';
            });
            $('select[name="product[]"]').html(html);
        },
    });
});

$('#add_more_exp').click(function () {
    var html = '<tr>\n\
                                                        <td data-title="Amount">\n\
                                                            <div>\n\
                                                                <input class="form-control form-control-sm line_cost" type="text" name="extra_title[]">\n\
                                                            </div>\n\
                                                        </td>\n\
                                                        <td data-title="Description">\n\
                                                            <div>\n\
                                                                <input class="form-control form-control-sm" type="text" name="extra_description[]">\n\
                                                            </div>\n\
                                                        </td>\n\
                                                        <td data-title="Delete">\n\
                                                            <button class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>\n\
                                                        </td>\n\
                                                    </tr>';
    $('#extra_exp_body').append(html);
});

$('body').on('focus', '.qty, .unit_price', function () {
    $(this).select();
});

$('body').on('change', '.product', function () {
    var channel = $(this);
    var product = $(this).val();
    var saleman = $('.salesman').val();

    $.ajaxSetup({
        beforeSend: function (xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf_token"]').attr('content'));
            }
        },
    });
    $.ajax({
        data: {
            id: product,
            saleman: saleman
        },
        type: 'POST',
        url: "{{ route('get-product-details') }}",
        success: function (return_data) {
            var unitPrice = return_data.product_sale_price;
            if ($('select[name=sale_type]').val() == 'whole') {
                unitPrice = return_data.product_whole_sale_price;
            }
            var sales = return_data.saleman_stock;
            if (typeof return_data.saleman_stock == 'undefined') {
                sales = 0;
            }
            $('#initial_row').remove();
            $('#product_detail').prepend('<tr><td data-title="P-ID"><input class="form-control form-control-sm" type="text" readonly name="product_id[]" value="' + return_data.id + '"></td><td  data-title="Product Name">' + return_data.product_name + '</td><td  data-title="Whole Rate"><input class="form-control whole_price form-control-sm" type="text" name="whole_price[]" value="' + return_data.product_whole_sale_price + '"></td><td  data-title="Retail Rate"><input class="form-control unit_price form-control-sm" type="text" name="retail_price[]" value="' + return_data.product_sale_price + '"></td><td  data-title="Total Stock"><input class="form-control form-control-sm" type="text" readonly name="current_qty[]" value="' + return_data.stock_qty + '"></td><td  data-title="Saleman Stock"><input class="form-control form-control-sm" type="text" readonly name="salesman_stock[]" value="' + sales + '"></td><td  data-title="Sale Qty"><input class="form-control qty form-control-sm" type="number" name="qty[]" min="0" value="0"></td><td data-title="Discount (In %)" ><input class="form-control qty form-control-sm" type="text"  name="discount_per_item[]" value="0"></td><td  data-title="Total Amount"><input class="form-control line_cost form-control-sm" type="text" readonly name="line_cost[]" value=""></td><td data-title="Delete" ><a href="javascript:;" class="btn btn-danger delete btn-sm"><i class="fa fa fa-trash"></i></a></td></tr>');
        },
    });
});
$('body').on('change', 'select[name=sale_type]', function () {
    var saleType = "unit_price";
    if ($('select[name=sale_type]').val() == 'whole') {
        saleType = "whole_price";
    }
    $('#product_detail  > tr').each(function () {
        change_qty_cost($(this));
    });
});
$('.qty').each(function () {
    change_qty_cost($(this).closest('tr'));
});
// Add Quantity to Product
$('body').on('keyup', '.qty', function () {
    change_qty_cost($(this).closest('tr'));
});
$('body').on('keyup', '.line_cost', function () {
    update_calculation();
});
$('body').on('keyup', '.unit_price', function () {
    change_qty_cost($(this).closest('tr'));
});
$('body').on('keyup', '[name="discount_per_item[]"]', function () {
    change_qty_cost($(this).closest('tr'));
//alert($(this).val());
});
$('body').on('keyup', '.discount', function () {
    update_calculation();
});
$('body').on('keyup', '.paidamount', function () {
    update_calculation();
});

function change_qty_cost(row) {
    var saleType = "unit_price";
    if ($('select[name=sale_type]').val() == 'whole') {
        saleType = "whole_price";
    }
    var discount = row.find('[name="discount_per_item[]"]').val() / 100;
//    console.log(discount);
    if (row.find('.' + saleType).val() >= 0 && row.find('.qty').val() >= 0) {
        var unit_price = +row.find('.' + saleType).val();
        var qty = +row.find('.qty').val();
        if (row.find('.' + saleType).val() == 0 || row.find('.qty').val() == 0) {
            var line_cost = row.find('.line_cost').val(0);
        } else {
            var actual_val = unit_price * qty;
//alert(unit_price);
//alert(qty);
//alert(actual_val);
            if (discount != 0) {

                actual_val = actual_val - (actual_val * discount);
            }
            var line_cost = row.find('.line_cost').val(actual_val.toFixed(2));
        }
        update_calculation();
    }
}

// Delete Product from List
$('body').on('click', '.delete', function () {
    var rowCount = $('#product_detail tr').length;
    if (rowCount == 7) {
        message("error", 'You can\'t delete because there are must be at least one row');
    } else {
        var row = $(this).closest('tr');
        var line_cost = row.find('.line_cost').val();
        var cost = $('.line_cost_total').val();
        $('.line_cost_total').val(cost - line_cost);

        $(this).closest('tr').remove();

        update_calculation();
    }
});

// calculation of line_cost_total, discount, total, paidamount and due amount
function update_calculation() {
    var discount = $(".discount").val();

    // if the discount field is null then put in discount 0
    if (discount == '') {
        $(".discount").val();
    }

    var paidamount = $(".paidamount").val();
    // if the paidamount field is null then put in paidamount 0
    if (paidamount == '') {
        $(".paidamount").val();
    }

    var line_cost = $(".line_cost");
    var total_line_cost = 0;
    for (var i = 0; i < line_cost.length; i++) {
        if ($.isNumeric($(line_cost[i]).val()))
            total_line_cost += parseInt($(line_cost[i]).val());
    }

    $(".line_cost_total").val(total_line_cost);
    $(".total").val(total_line_cost);

    var line_cost_total = parseInt($(".line_cost_total").val());

    var discount = parseInt($(".discount").val());


    $(".total").val(line_cost_total - discount);

    var total = parseInt($(".total").val());

    var paidamount = parseInt($(".paidamount").val());

    $(".due_amount").val(total - paidamount);
}
</script>

@endsection
