@extends('layouts.admin')

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }} " />

<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datatables/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" />

@endsection

@section('content')
<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Users Locations</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Users Locations</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <form action="{{ route('users/map') }}" method="post" style="width:100%;">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-8" style="margin-bottom:20px;">
                    <select name="user_id" id="filter_users" class="form-control">
                        <option value="">All Users</option>
                        @foreach($users as $u)
                        <option value="{{$u->id}}" <?= (isset($user->id) && $user->id == $u->id) ? 'selected' : ''; ?> >{{$u->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <button class="btn btn-success btn-lg" type="submit">Search</button>
                </div>   
            </div>
            <div class="row">
                <div class="col-lg-12" style="margin-bottom:20px;">
                    <ul id="offlineUsers" style="font-size: 16px;color: red;">

                    </ul>
                </div>   
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div id="map-view" class="is-vcentered" style="width: 100%; height:500px;"></div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCe0I76FCBsgJP2dh193EWuX2IPST4gn0k&sensor=false"></script>-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCe0I76FCBsgJP2dh193EWuX2IPST4gn0k&sensor=false&libraries=places"></script>
<!--<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>-->
<script src="{{ asset('public/assets/js/locationpicker.jquery.js') }}"></script>

<script src="{{ asset('public/assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/jszip/jszip.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/pdfmake/pdfmake.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/pdfmake/vfs_fonts.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/datatables/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>


<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-app.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-firestore.js"></script>

<script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-database.js"></script>

<script type="text/javascript">
$(document).ready(function () {
//-initialize the javascript
    App.dataTables();
});
function errData(error) {
    console.log('error');
    console.log(error);

}
var firebaseConfig = {
    apiKey: "AIzaSyApv6uTT7SxewFc2jX9nMsuWB3BOOndl9g",
    authDomain: "suntechseeds-f209c.firebaseapp.com",
    databaseURL: "https://suntechseeds-f209c.firebaseio.com",
    projectId: "suntechseeds-f209c",
    storageBucket: "",
    messagingSenderId: "727347849588",
    appId: "1:727347849588:web:e58e8ac751498bb7"
};
var app = firebase.initializeApp(firebaseConfig);
</script>
<?php if (empty($user)) { ?>
    <script type="text/javascript">
        function gotData(data) {
            var myusers = <?php echo $myusers; ?>;
            $('#offlineUsers').empty();
            var locations = data.val();
            console.log(locations);
            var map = new google.maps.Map(document.getElementById('map-view'), {
                zoom: 10,
                center: new google.maps.LatLng(34.0151, 71.5249),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            if (locations) {
                $.each(locations, function (index, element) {
                    if (element['lat'] == 0 || element['lon'] == 0) {
                        $('#offlineUsers').append('<li>' + myusers[index] + ' is offline </li>');
                    } else {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(element['lat'], element['lon']),
                            map: map
                        });
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(myusers[index]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                    }
                });
            } else {

            }
        }

        function get_addresses() {
            var db = firebase.database().ref('/users');
            db.on('value', gotData, errData);
        }
        setInterval(function () {
            get_addresses();
        }, 60000); // 30 seconds
        get_addresses();
    </script>
<?php } else {
    ?>
    <script type="text/javascript">
        function gotData(data) {
            $('#offlineUsers').empty();
            var locations = data.val();
            console.log(locations);
            if (!locations || locations['lat'] == 0 || locations['lon'] == 0) {
                var name = $('select[name=user_id] option:selected').text();
                $('#offlineUsers').append('<li>' + name + ' is offline </li>');
            } else {
                $('#map-view').locationpicker({
                    location: {latitude: locations['lat'], longitude: locations['lon']},
                    enableAutocomplete: true,
                    radius: 0,
                });
            }
        }
        function get_addresses() {
            var phone_no = '0' + <?php echo $user->phone_no ?>;
            var db = firebase.database().ref('/users/' + phone_no);
            db.on('value', gotData, errData);
        }
        setInterval(function () {
            get_addresses();
        }, 10000); // 30 seconds
        get_addresses();
    </script>
<?php } ?>
@endsection