@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>
                <li class="breadcrumb-item active">Customer Sale Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Customer Sale Ledger
                    </div>
                    <div class="card-body p-1">
                        <table class="table no-more-tables table-striped table-bordered table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Total Amount</th>
                                    <th>Discount</th>
                                    <th>Net Amount</th>
                                    <th>Paid</th>
                                    <th>Date</th>
<!--                                    <th>Previous Balance</th>
                                    <th>New Balance</th>-->
                                    <th>Created By</th>
                                    <th>Details</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $ledger)

                                <tr>
                                    <td data-title="Customer Name">{{ $ledger->name }}</td>

                                    <td data-title="Total Amount">{{ $ledger->total_amount }}</td>
                                    <td data-title="Discount">{{ $ledger->discount }}</td>
                                    <td data-title="Net Amount">{{ $ledger->net_amount }}</td>
                                    <td data-title="Paid">{{ $ledger->paid }}</td>
                                    <td data-title="Date"> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>
<!--                                    <td> {{ $ledger->prev_bal }}</td>
                                    <td> {{ $ledger->new_bal }}</td>-->
                                    <td data-title="Created By">
                                        <?php
                                        if ($ledger->table == 'users') {
                                            echo $users[$ledger->created_by] . ' (Admin)';
                                        }
                                        if ($ledger->table == 'salesman') {
                                            echo $salesman[$ledger->created_by];
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Details">
                                        <a href="{{ route('sale/details', ['id' => $ledger->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            View Details
                                        </a>
                                        <a href="{{ route('sale/print_invoice', ['id' => $ledger->id]) }}" class="btn btn-outline-success btn-space btn-sm">
                                            Generate Invoice
                                        </a>
                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td data-title="Action">
                                            <a title="Edit" href="{{ route('sale/edit_sale', ['id' => $ledger->id]) }}" class="btn btn-outline-primary btn-space btn-sm">
                                                <span class="fas fa-edit"></span>
                                            </a>
                                            <a title="Delete" onclick="return confirm('are You sure')" href="{{ route('sale/delete_sale', ['id' => $ledger->id]) }}" class="btn btn-outline-danger btn-space btn-sm">
                                                <span class="fas fa-trash"></span>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection