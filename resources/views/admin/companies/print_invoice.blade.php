<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="{{ asset('public/images/logo2.jpg') }}">
        <!--<title>S.M Qaisar medicines</title>-->
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/assets/css/app.css') }}" type="text/css" />

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <style>
            #table1_length select{
                width:60px;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }} " />

        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datatables/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" />
        <style>
            body {
                margin: 0;
                padding: 0;
                font: 12pt "Tahoma";
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 21cm;
                min-height: 29.7cm;
                padding: 2cm;
                margin: 1cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            @page {
                size: A4;
                margin: 0;
            }
            #footer {
                padding:10px;
                width: 100%;   
                text-align: center;
                border-top: 1px solid black;    
            }
            #footer span{
                font-weight: bold;
            }
            @media print {
                body {
                    position: relative;
                }
                #footer {
                    position: absolute;
                    bottom: 0;
                    border-top: 1px solid black;
                    padding: 20px 10px;
                    width: 100%;   
                    text-align: center;
                }
                #footer span{
                    font-weight: bold;
                }
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }
            .c_name{
                font-size: 30px;
                font-family: "Times New Roman";
                font-weight: bold;
                /*color: #EB1D27;*/
                text-shadow: -1px 1px 1px;
                padding-bottom: 20px;
            }
            .no-margin{
                margin-bottom: 5px;

            }
            .import_text{
                font-size: 22px;
                font-family: arial;
                margin-top: 10px;
                font-weight: bold;
            }
            .header_details{
                font-size:16px;
                font-family: arial;
                font-weight: bold;
                /*color:#FBA01C;*/
            }
            .img_logo{
                width:200px;
            }
            .invoice_text{
                font-family: arial;
                font-size: 16px;
                font-weight: bold;
            }
            .invoice_no{
                font-size: 20px;
                font-weight: bold;
                border-bottom: 1px solid black;
                padding: 0 100px 0 20px;
                font-family: "Times New Roman";
            }
            .detail_tr{
                font-weight: bold;
            }
        </style>
    </head>

    <body style="background-color:white;">
        <div class="be-wrapper subpage" style="padding:10px !important">

            <div class="main-content container-fluid">
                <div class="d-flex">
                    <div style="float:left;width:50%">
                        <p class="no-margin c_name">ABBAS TRADERS</p>
                        <!--<p class="no-margin import_text" >Importers, exporters growers & seed merchants</p>-->
                        <p class="no-margin header_details">Kumail CNG Near Achini Chowk Ring Road Peshawar</p>
                        <!--<p class="no-margin header_details">Ph: 0092-91-2550043, 2550044</p>-->
                        <!--<p class="no-margin header_details">Email: suntechseeds@gmail.com</p>-->
                        <p class="no-margin header_details" style="font-size:17px;">
                            <span style="font-size:17px">Mia Mir</span> Mob: 0333-9102526
                        </p>
                        <p class="no-margin header_details">
                       
                        </p>
                    </div>
                    <div class="" style="width:50%">
                        <img class="img_logo" style="float:right;" src="{{asset('public/images/logo2.jpg')}}">
                    </div>
                </div>
                <div style="margin-top: 10px;display: flex;">
                    <div class="" style="width:50%;">
                        <span class="invoice_text">Invoice No </span><span class="invoice_no">{{$sales->id}}</span>
                    </div>
                    <div class="" style="width:50%;text-align: right;">
                        <span class="invoice_text">Date </span><span class="invoice_no">{{ date('d-M-Y',strtotime($sales->created_at)) }}</span>
                    </div>
                </div>
                <div style="width:100%;margin-top: 20px;display: flex;">
                    <div style="padding:0 10px 0 0;"> <span class="invoice_text">Customer Name</span></div>
                    <div style="flex-grow:1;padding: 0 10px;border-bottom: 1px solid black;font-weight: bold;">{{ucwords($sales->name)}}</div>
                </div>
                <div style="width:100%;margin-top: 20px;display: flex;">
                    <div style="padding:0 10px 0 0;"> <span class="invoice_text">Address</span></div><div style="flex-grow:1;padding: 0 10px;border-bottom: 1px solid black;font-weight: bold;"><?php echo ucfirst($sales->address) . ',&nbsp;' . ucfirst(strtolower($sales->city)); ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table cellpadding="10" border="1" style="width: 100%;margin-top: 20px">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Product</th>
                                    <th>Unit Price</th>
                                    <th>CTN</th>
                                    <th>Qty/pcs</th>
                                    <th>Actual Amount</th>
                                    <th>Discount (%)</th>
                                    <th>After Discount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_ledger as $key=>$ledger)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $ledger->product_name }}<?= ($ledger->p_qty) ? ' | ' . $ledger->p_qty : ''; ?></td>
                                    <td>{{ $ledger->unit_price }}</td>
                                    <td>{{ round($ledger->qty/$ledger->qty_per_cotton,1) }}</td>
                                    <td>{{ $ledger->qty }}</td>
                                    <td>{{ $ledger->actual_price }}</td>
                                    <td>{{ $ledger->item_discount }}</td>
                                    <td>{{ $ledger->total_price }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if($sale_expenses->count())
                        <h3>Extra Expenses</h3>
                        <table border="1" cellpadding="10" style="width: 100%;margin-top: 10px">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sale_expenses as $key=>$exp)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $exp->description }}</td>
                                    <td>{{ $exp->total_price }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        <table border="1"  cellpadding='7' align="right" style="margin-top: 20px;">
                            <tr class="detail_tr">
                                <td style="">Total &nbsp; </td>
                                <td>{{ $sales->total_amount }} </td>
                            </tr>
                            <tr class="detail_tr">
                                <td style="">Discount &nbsp; </td>

                                <td>{{ $sales->discount }} </td>
                            </tr>
                            <tr class="detail_tr">
                                <td style="">Net Amount &nbsp; </td>

                                <td>{{ $sales->net_amount }} </td>
                            </tr>
                            <tr class="detail_tr">
                                <td style="">Paid &nbsp;</td>

                                <td>{{ $sales->paid }} </td>
                            </tr>
                            <tr class="detail_tr">
                                <td style="">New Balance &nbsp;</td>
                                <td>{{ $sales->net_amount-$sales->paid }} </td>
                            </tr>
                            <tr class="detail_tr">
                                <td style="">Old Balance &nbsp;</td>
                                <td>{{ $sales->dues-$sales->net_amount-$sales->paid }} PKR</td>
                            </tr>
                            <tr class="detail_tr">
                                <td style="">Remaining Balance &nbsp;</td>
                                <td>{{ $sales->dues }} PKR</td>
                            </tr>
                        </table>

                    </div>
                    <p style="margin-top: 20px">{{$sales->narration}}</p>
                </div>

            </div>


        </div>
        <div id="footer">
            <p>For any queries kindly contact us at <span>(0333-9102526, 0334-9002728)</span>
                OR you can visit Us.

            </p>
        </div>
    </body>
</html>           