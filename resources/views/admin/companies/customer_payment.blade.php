@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Customers</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('companies')}}">Customers</a></li>

                <li class="breadcrumb-item active">Customer Payment</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Customer Payment</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('company/save_payment') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Date</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="date" name="created_at" value="{{date('Y-m-d')}}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Name</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" name="name" type="text" placeholder="Company Name" required value="{{ $supplier->name }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Contact No</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="contact_no" placeholder="Add Contact No" required value="{{ $supplier->contact }}" readonly>
                                </div>
                                <input type="hidden" name="customer_id" value="{{ $supplier->id }}">
                            </div>

                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Dues</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="dues" required value="{{ $supplier->dues }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Description</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Payment Type</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select class="form-control" name="pay_from" id="pay" required>
                                        <option value="">Select Payment Type</option>
                                        <option value="bank">Bank</span></option>
                                        <option value="cash" selected>Cash</option>
                                        <option value="cheque">Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div id="bank">

                            </div>
                            <div id="cheque">

                            </div>
                            <div id="dollor_conversion">
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Amount</label>
                              
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text"  id="number" name="pay"   required>
                                     <span id="words" style="color:Red;"></span>

                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Discount</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="discount" value="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right pt-0">Send Message</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input type="checkbox" name="send_msg" value="1">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary">Submit</button>
                                    <button class="btn btn-warning" name="print" value="print">Save & Print</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    var pay_type = $('#pay').val();

    var csrf = $('meta[name="csrf_token"]').attr('content');

    $.ajax({
        url: '{{ route("company/payment_type") }}',
        type: 'POST',
        data: {
            pay_type: pay_type,
            '_token': csrf
        },
        success: function (data) {
            // console.log(data)
            $('#bank').html(data);
            $('#cheque').html('');
        }
    });
    $('body').on('change', '#pay', function () {
        var pay_type = $('#pay').val();

        var csrf = $('meta[name="csrf_token"]').attr('content');

        $.ajax({
            url: '{{ route("company/payment_type") }}',
            type: 'POST',
            data: {
                pay_type: pay_type,
                '_token': csrf
            },
            success: function (data) {
                // console.log(data)
                $('#bank').html(data);
                $('#cheque').html('');
            }
        });
    });
    $('body').on('change', '#transfer_to', function () {
        var pay_type = $(this).val();
        var csrf = $('meta[name="csrf_token"]').attr('content');
        $.ajax({
            url: '{{ route("get_cheque_fields_customer") }}',
            type: 'POST',
            data: {
                pay_type: pay_type,
                '_token': csrf
            },
            success: function (data) {
                // console.log(data)
                $('#cheque').html(data);
            }
        });
    });
</script>


<script>

   var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

function inWords (num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}

document.getElementById('number').onkeyup = function () {
    document.getElementById('words').innerHTML = inWords(document.getElementById('number').value);
};
</script>
@endsection