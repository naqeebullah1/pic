@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Current Summary Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Current Summary</a></li>
                <li class="breadcrumb-item active">Current Summay Report</li>
            </ol>
        </nav>
        <div style="text-align: right;"><h3>Date: {{date("d-m-Y")}}</h3></div>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                
                @php
                    $products_total = 0;
                    $supplier_total = 0;
                    $customer_total =  $customers[1]->customer_total + $customers[2]->customer_total;
                @endphp

                @foreach($products as $product)
                       @php $products_total += $product->products_price; @endphp
                @endforeach

                @foreach($suppliers as $supplier)
                      <?php

                            if($supplier->currency == 'usd')
                            {
                                $get_currency_price = $supplier->supplier_total * Session::get('today_currency');
                            }
                            else
                            {
                                $get_currency_price = $supplier->supplier_total;
                            }

                            $supplier_total += $get_currency_price;

                      ?>
                @endforeach

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Dealer</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $customers[2]->customer_total }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Customer</div>
                            <div class="card-body">
                                <h2><b>
                                    {{ $customers[1]->customer_total }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Products Total</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $products_total }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Companies</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $supplier_total }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header"> Current Amount in Company</div>
                            <div class="card-body">
                                <h2><b>
                                        @php
                                            $total = $customer_total + $products_total;

                                            $profit = $total - $supplier_total;
                                        @endphp

                                        {{ $profit }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection