@extends('layouts.admin')

@section('style')


@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Employee</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('employee.index')}}">Employees</a></li>

                <li class="breadcrumb-item active">Employee Ledger</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Employee Ledger
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Salary</th>
                                    <th>Advance</th>
                                    <th>Payment</th>
                                    <th>Remaining</th>                                    
                                    <th>Description</th>                                    
                                    <th>Date</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer as $ledger)

                                <tr>
                                    <td>{{ $ledger->name }}</td>
                                    <td>{{ $ledger->salary }}</td>

                                    <td>{{ $ledger->advance }}</td>
                                    <td>{{ $ledger->payment }}</td>
                                    <td>{{ $ledger->remaining }}</td>
                                    <td>{{ $ledger->narration }}</td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ledger->created_at)->format('d-m-Y') }}</td>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="purchaseDetailModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content" style="max-width: 100% !important;">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Sale Details</h4>
                    </div>

                    <div class="modal-body" id="purchaseDetailBody" style="padding:10px">
                    </div>
                    <div class="modal-footer" style="padding:10px">
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
function fetchHistory(invoiceNo) {
    $.ajax({
        data: {
            invoiceNo: invoiceNo
        },
        type: 'GET',
        url: "{{ route('sale/history') }}",
        success: function (return_data) {
            $('#purchaseDetailBody').html(return_data);
            $('#purchaseDetailModal').modal('show');
        },
    });
}

</script>

@endsection