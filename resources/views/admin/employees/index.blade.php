@extends('layouts.admin')

@section('style')
<style>
    .currentMonth {
        font-weight: bold;
    }
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        .employees td:nth-of-type(1):before { content: "Name"; }
        .employees td:nth-of-type(2):before { content: "Contact"; }
        .employees td:nth-of-type(3):before { content: "Email"; }
        .employees td:nth-of-type(4):before { content: "Address"; }
        .employees td:nth-of-type(5):before { content: "Designaton"; }
        .employees td:nth-of-type(6):before { content: "Joining Date"; }
        .employees td:nth-of-type(7):before { content: "Salary"; }
        .employees td:nth-of-type(8):before { content: "Remaining Amount"; }
        .employees td:nth-of-type(9):before { content: "Payment"; }
        .employees td:nth-of-type(10):before { content: "Ledger"; }
        .employees td:nth-of-type(11):before { content: "Action"; }

    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Employees</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Employees List</li>

            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message"><strong> {{ Session::get('error') }} </strong></div>
                </div>
                @endif
                <div class="card card-table p-1">
                    <div class="dropdown d-md-none">
                        <button type="button" class="float-right btn btn-outline-default btn-sm dropdown-toggle " data-toggle="dropdown" id="5b4f95e9a64b5" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div style="border:1px solid #ccc;" class="dropdown-menu" aria-labelledby="5b4f95e9a64b5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(63px, 31px, 0px);">
                            <a href="{{ url('employee?status=1') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active text-white' : '' ?>">Active Employees</a>
                            <a href="{{ url('employee?status=0') }}" class="dropdown-item <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active text-white' : '' ?>">Inactive Employees</a>
                        </div>
                    </div>

                    <!-- button group for desktop -->
                    <div class="d-none d-md-flex btn-group">
                        <a href="{{ url('employee?status=1') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '1') ? 'active' : '' ?>">Active Employees</a>
                        <a href="{{ url('employee?status=0') }}" class="btn border btn-light <?= (isset($_GET['status']) && $_GET['status'] == '0') ? 'active' : '' ?>">Inactive Employees</a>
                    </div>
                    <div class="card-header">Employees List
                        <div class="tools dropdown">
                            <a href="{{ route('employee.create') }}" class="btn btn-rounded btn-space btn-primary"><i class="fas fa-plus-circle"></i> Add New Employee</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="employees table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Designation</th>
                                    <th>Joining Date</th>
                                    <th>Salary</th>
                                    <th>Remaining Amount</th>
                                    <th>Payments</th>
                                    <th>Ledger</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $product)
                                <?php
                                $ledger = App\EmployeeLedger::where('employee_id', '=', $product->id)
                                        ->orderBy('id', 'desc')
                                        ->first();
//                                echo $ledger->remaining;
                                ?>
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->contact }}</td>
                                    <td>{{ $product->email }}</td>
                                    <td>{{ $product->address.', '.$product->city }}</td>
                                    <td>{{ $product->designation }}</td>
                                    <td>
                                        @if($product->joining_date)
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d', $product->joining_date)->format('d-m-Y') }}
                                        @endif
                                    </td>
                                    <!--<td>{{ ($product->last_salary_date)?date('d M, Y',strtotime($product->last_salary_date)):'' }}</td>-->
                                    <td>{{ $product->salary }}</td>
                                    <td>{{ $product->remainings }}</td>
                                    <td nowrap>
                                        <?php if (!$product->leaving_date) { ?>

                                            <a href="#" onclick="advancePayment({{$product->id}})" class="btn btn-outline-success btn-space btn-sm">Advance</a>
                                            <?php
                                        } else {
                                            echo 'Job Leaved';
                                        }

                                        if ($ledger->remaining > 0) {
                                            ?>
                                            <a href="#" onclick="paySalary({{$product->id}})" class="btn btn-outline-success btn-space btn-sm">Pay</a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <a href="{{route('employee/employee_ledger',['id'=> $product->id])}}" class="btn btn-outline-success btn-space btn-sm">View</a>

                                    </td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td nowrap>
                                            <?php if (!$product->leaving_date) { ?>
                                                <a href="javascript:" onclick="leave_job_modal({{$product->id}})" class="btn btn-outline-danger btn-space btn-sm" style="white-space:nowrap">Leave Job</a>
                                            <?php } ?>
                                            <a href="{{ route('employee.edit', ['id' => $product->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <?php
                                            if ($product->status) {
                                                ?>
                                                <a href="{{route('change.employee.status',['id'=>$product->id])}}" class="btn btn-outline-danger btn-space btn-sm">
                                                    Move to Inactive
                                                </a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="{{route('change.employee.status',['id'=>$product->id])}}" class="btn btn-outline-success btn-space btn-sm">
                                                    Move to Active
                                                </a>
                                                <?php
                                            }
                                            ?>
                                            <!--                                            <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('employee.destroy', ['id'=>$product->id]) }}" style="display:inline;">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                            </form>-->
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="advancePaymentModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Add Advance Payment</h4>
                    </div>
                    <form method="POST" id="advancePaymentForm" action="{{route('employee/advance')}}">
                        {{csrf_field()}}
                        <div class="modal-body" style="padding:10px">

                            <p style="font-size:14px;color:red">Add advance payment for the month of <span class="currentMonth"></span></p>
                            <div class="form-group row">
                                <label class="col-lg-12">Payment Type</label>
                                <div class="col-lg-12">
                                    <select class="form-control pay" name="pay_from" id="pay" required>
                                        <option value="">Select Payment Type</option>
                                        <option value="bank">Bank</span></option>
                                        <option value="cash">Cash</option>
                                    </select>
                                </div>
                            </div>
                            <div class="bank">

                            </div>
                            <div class="form-group row" style="padding:0">
                                <label class="col-lg-12">Date</label>

                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="employee_id" type="hidden">
                                    <input name="created_at" type="date" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row" style="padding:0">
                                <label class="col-lg-12">Amount to pay</label>

                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="advance" type="number" class="form-control" id="number" >
                                     <span id="words" style="color:Red;"></span>
                                </div>
                                
                            </div>
                            <div class="form-group row" style="padding:0">
                                <label class="col-12 col-sm-3 col-form-label" style="margin-bottom:10px;">Description</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <textarea name="narration" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="advancePaymentForm">Save</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="paySalary" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Payment details</h4>
                    </div>
                    <form method="POST" id="PaymentForm" action="{{route('employee/save_paid_payment')}}">
                        {{csrf_field()}}
                        <div class="modal-body" style="padding:10px">

                            <p style="font-size:14px;color:red;">Employee Pending Payment Details</p>
                            <div class="form-group row" style="padding:0">
                                <div class="col-12 col-sm-8 col-lg-12" id="paymentDetailDiv">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-12">Payment Type</label>
                                <div class="col-lg-12">
                                    <select class="form-control pay" name="pay_from" required>
                                        <option value="">Select Payment Type</option>
                                        <option value="bank">Bank</span></option>
                                        <option value="cash">Cash</option>
                                    </select>
                                </div>
                            </div>
                            <div class="bank">

                            </div>
                            <div class="form-group row" style="padding:0">
                                <label class="col-12 col-sm-3 col-form-label" style="margin-bottom:10px;">Amount</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="amount" type="number" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group row" style="padding:0">
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <input name="employee_id" type="hidden">
                                </div>
                            </div>
                            <div class="form-group row" style="padding:0">
                                <label class="col-12 col-sm-3 col-form-label" style="margin-bottom:10px;">Description</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <textarea name="narration" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="PaymentForm">Click To Pay</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="leave_job" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content" style="max-width:100%">
                    <div style='padding: 15px;'>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="margin: 0;">Job Leaving details</h4>
                    </div>
                    <form method="POST" id="leave_job_form" action="{{route('save_last_payment')}}">
                        {{csrf_field()}}
                        <div class="modal-body" style="padding:10px">
                            <div class="form-group row" style="padding:0">
                                <div class="col-12 col-sm-8 col-lg-12" id="lastDetails">

                                </div>
                            </div>
                            <p class="text-dark last_payment_info" style="font-weight:bold;font-size:16px;"></p>
                            <div class="form-group row">
                                <label class="col-lg-12">Last date of job</label>
                                <div class="col-lg-12">
                                    <input type="date" class="form-control" name="leaving_date" required="">
                                    <input type="hidden" class="form-control" name="employee_id">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-12">Payment Type</label>
                                <div class="col-lg-12">
                                    <select class="form-control pay" name="pay_from" required>
                                        <option value="">Select Payment Type</option>
                                        <option value="bank">Bank</span></option>
                                        <option value="cash">Cash</option>
                                    </select>
                                </div>
                            </div>
                            <div class="bank">

                            </div>
                            <div class="form-group row" style="padding:0">
                                <div class="col-lg-4">

                                    <div class="form-group row" style="padding:0">
                                        <label class="col-12 col-sm-3 col-form-label" style="margin-bottom:10px;">New Amount</label>
                                        <div class="col-12 col-sm-8 col-lg-12">
                                            <input name="new_amount" type="number" class="form-control add_amount" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group row" style="padding:0">
                                        <label class="col-12 col-sm-3 col-form-label" style="margin-bottom:10px;">Old Amount</label>
                                        <div class="col-12 col-sm-8 col-lg-12">
                                            <input name="old_amount" type="number" class="form-control add_amount" required="" readonly="">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group row" style="padding:0">
                                        <label class="col-12 col-sm-3 col-form-label" style="margin-bottom:10px;">Total Amount</label>
                                        <div class="col-12 col-sm-8 col-lg-12">
                                            <input name="amount" type="number" class="form-control" required="" readonly="">
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="form-group row" style="padding:0">
                                <label class="col-12 col-sm-3 col-form-label" style="margin-bottom:10px;">Description</label>
                                <div class="col-12 col-sm-8 col-lg-12">
                                    <textarea name="narration" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer" style="padding:10px">
                        <button type="submit" class="btn btn-outline-success btn-space" form="leave_job_form">Click To Pay</button>
                        <button type="button" class="btn btn-outline-danger btn-space" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $('input[name=leaving_date]').change(function(){
    var emp = $('#leave_job_form input[name=employee_id]').val();
    var leaving_date = $(this).val();
    $.ajax({
    url:"{{url('get_date_diff')}}",
            data:{
            emp: emp,
                    leaving_date: leaving_date,
            },
            success:function(data){
            data = JSON.parse(data);
            var html = "<span>Last salary: " + data.last_salary + "<br>Days: " + data.days + "<br>Amount: " + data.amount + "<br>Old Balances: " + data.remainings + "<br>Remainings: " + ( + data.remainings + + data.amount);
            console.log(html);
            $('.last_payment_info').html(html);
            $('#leave_job_form input[name=amount]').val( + data.remainings + + data.amount);
            $('#leave_job_form input[name=new_amount]').val(data.amount);
            $('#leave_job_form input[name=old_amount]').val(data.remainings);
            }
    });
    });
    function leave_job_modal(id){
    $('#leave_job_form input[name=employee_id]').val(id);
    $('#leave_job').modal('show');
    var employeeId = id;
    $.ajax({
    data: {
    id: employeeId
    },
            type: 'GET',
            url: "{{ route('salary_details') }}",
            success: function(data) {
            $('#lastDetails').html(data);
            },
    });
    }
    function advancePayment(employeeId) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var d = new Date();
    $('.currentMonth').text(months[d.getMonth()]);
    $('input[name=employee_id]').val(employeeId);
    $('#advancePaymentModal').modal('show');
    }

    function paySalary(employeeId) {
    $.ajax({
    data: {
    id: employeeId
    },
            type: 'GET',
            url: "{{ route('salary_details') }}",
            success: function(data) {
            $('#paymentDetailDiv').html(data);
            var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var d = new Date();
            var previousMonth = d.getMonth() - 1;
            if (previousMonth == '-1'){
            previousMonth = 11;
            }

            $('.currentMonth').text(months[previousMonth]);
            $('input[name=employee_id]').val(employeeId);
            $('#paySalary').modal('show');
            },
    });
    }
    $(document).ready(function() {
    //-initialize the javascript
    $('body').on('change', '.pay', function() {
    var pay_type = $(this).val();
    var csrf = $('meta[name="csrf_token"]').attr('content');
    $.ajax({
    url: '{{ route("employee/payment_type") }}',
            type: 'POST',
            data: {
            pay_type: pay_type,
                    '_token': csrf
            },
            success: function(data) {
            // console.log(data)
            $('.bank').html(data);
            }
    });
    });
    });
</script>


<script>

   var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

function inWords (num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}

document.getElementById('number').onkeyup = function () {
    document.getElementById('words').innerHTML = inWords(document.getElementById('number').value);
};
</script>
@endsection