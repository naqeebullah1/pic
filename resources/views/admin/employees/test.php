@extends('layouts.master')
@section('title')
LineUp:Serials
@endsection
@push('cssCode')
<style>

    #save:hover{
        box-shadow: 4px 4px #b3e6ff;
    }

    #myTable {
        text-align: center;
    }

    table>tbody>tr>td>button {
        padding: 5px !important;
        margin: 2px !important;
    }

    #myTable_filter>label>input{
        padding:5px !important;
    }

</style>
@endpush
@section('content')
<div style="margin:25px auto;">
    <h2 id="pageHeader">Serials</h2>
    <table id="myTable" class="display responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="width:5% !important">S.No</th>
                <th>Serial Number</th>
                <th>Name</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 0;
            @endphp
            @foreach ($members as $member) 
                <tr>
                    <td>{{++$i}}</td>
                    <td>
                        <a href="#" onclick="memberWindow_{{ $member->queue_id }}=window.open('{{ url("queue/member/$member->queue_id/serve/$member->id") }}', 'memberWindow_{{ $member->queue_id }}', width=60, height=60); return false">
                            {{ $member->serial_number }}
                        </a>
                    </td>
                    <td>
                        @if ( ! is_null( $member->user_id) )
                            {{ $member->user_name }}
                        @else
                            {{ $member->name }}
                        @endif
                    </td>
                    <td>{{ $member->status }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="addEntityModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div style="background:#29248c;" class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Queue</h5>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url("queue/insert") }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Queue Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Queue Name"
                                required>
                        @if($errors->has('name'))
                            <p class="text text-danger">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Select Status</option>
                            <option value="INACTIVE">INACTIVE</option>
                            <option value="ACTIVE">ACTIVE</option>
                        </select>
                        @if($errors->has('status'))
                            <p class="text text-danger">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select class="form-control" name="type" id="type">
                            <option value="">Select Type</option>
                            <option value="DOMESTIC">DOMESTIC</option>
                            <option value="INTERNATIONAL">INTERNATIONAL</option>
                        </select>
                        @if($errors->has('type'))
                            <p class="text text-danger">{{ $errors->first('type') }}</p>
                        @endif
                    </div>
                    <div style="text-align:center;margin-top:35px;">
                        <button style="width:20%;font-family:Montserrat;padding:10px;color:#fff;background:#29248c;" id="save" type="submit"
                            class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
