@extends('layouts.admin')

@section('style')
<style>
    .currentMonth {
        font-weight: bold;
    }
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Title"; }
        td:nth-of-type(2):before { content: "Worth"; }
        td:nth-of-type(3):before { content: "Description"; }
        td:nth-of-type(4):before { content: "created"; }
        td:nth-of-type(5):before { content: "Action"; }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Assets</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">All Assets</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('error'))
                @foreach(Session::get('error')->all() as $e)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message">
                        <strong> {{ $e }} </strong>
                        <br>
                    </div>
                </div>
                @endforeach
                @endif
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Assets List
                        <div class="tools dropdown">
                            <a href="{{ route('employee.create') }}" class="btn btn-rounded btn-space btn-primary" data-toggle="modal" data-target="#form-bp1"><i class="fas fa-plus-circle"></i> Add Asset</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Worth</th>
                                    <th>Description</th>
                                    <th>Created at</th>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->title }}</td>
                                    <td>{{ $user->price }}</td>
                                    <td>{{ $user->description }}</td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d-m-Y') }}</td>
                                    <?php if (auth()->user()->user_type == 1) { ?>

                                        <td>
                                            <a href="{{ route('assets.edit', ['id' => $user->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                            <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('assets.destroy', ['id'=>$user->id]) }}" style="display:inline;">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                            </form>
                                        </td>
                                    <?php } ?>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Head Expenses Modal -->
        <div class="modal fade colored-header colored-header-primary" id="form-bp1" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">Add Personal Asset</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
                    </div>
                    <form action="{{ route('assets.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Title</label>
                                <div>
                                    <input class="form-control" type="text" name="title" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Asset worth</label>
                                <div>
                                    <input class="form-control" type="text" name="price" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <div>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary md-close" type="button" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-primary md-close" type="submit">Proceed</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection