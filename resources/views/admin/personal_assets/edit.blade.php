@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Assets</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('assets')}}">Assets</a></li>
                <li class="breadcrumb-item active">Edit Asset details</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('error'))
                @foreach(Session::get('error')->all() as $e)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message">
                        <strong> {{ $e }} </strong>
                        <br>
                    </div>
                </div>
                @endforeach
                @endif
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Edit Asset Details</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('assets.update', ['id' => $user->id ]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Title</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="title" value="{{ $user->title }}" required>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Asset Worth</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="price" value="{{ $user->price }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Description</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <textarea class="form-control" name="description">{{ $user->description }}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-masks.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
//    alert($('input[name=phone_no]').val());
    //-initialize the javascript
    App.init();
    App.masks();
});
</script>
@endsection