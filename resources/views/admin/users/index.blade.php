@extends('layouts.admin')

@section('style')
<style>
    .currentMonth {
        font-weight: bold;
    }
    /* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
    */
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
        td:nth-of-type(1):before { content: "Name"; }
        td:nth-of-type(2):before { content: "Email"; }
        td:nth-of-type(3):before { content: "Phone"; }
        td:nth-of-type(4):before { content: "User Type"; }
        td:nth-of-type(5):before { content: "Action"; }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Users</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">All Users List</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('error'))
                @foreach(Session::get('error')->all() as $e)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message">
                        <strong> {{ $e }} </strong>
                        <br>
                    </div>
                </div>
                @endforeach
                @endif
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif
                <div class="card card-table">
                    <div class="card-header">Users List
                        <div class="tools dropdown">
                            <a href="{{ route('employee.create') }}" class="btn btn-rounded btn-space btn-primary" data-toggle="modal" data-target="#form-bp1"><i class="fas fa-plus-circle"></i> Add User</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="">
                            <table class="table table-striped table-hover table-fw-widget" id="table1">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone No</th>
                                        <th>User Type</th>
                                        <?php if (auth()->user()->user_type == 1) { ?>

                                            <th>Actions</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone_no }}</td>
                                        <td>
                                            <?php
                                            if ($user->user_type == 1) {
                                                echo 'Admin';
                                            }
                                            if ($user->user_type == 2) {
                                                echo 'Operator';
                                            }

                                            if ($user->user_type == 3) {
                                                echo 'Sales man';
                                            }
                                            ?>
                                        </td>
                                        <?php if (auth()->user()->user_type == 1) { ?>

                                            <td>
                                                <a href="{{ route('users.edit', ['id' => $user->id]) }}" class="btn btn-outline-primary btn-space btn-sm">Edit</a>
                                                <?php
                                                if (Auth::user()->id != $user->id) {
                                                    ?>
                                                    <form onsubmit="return confirm('Are you sure')" method="POST" action="{{ route('users.destroy', ['id'=>$user->id]) }}" style="display:inline;">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button type="submit" class="btn btn-outline-danger btn-space btn-sm">Delete</button>
                                                    </form>
                                                    <?php
                                                }
//                                        if(Auth::user())
                                                ?>
                                            </td>
                                        <?php } ?>
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Head Expenses Modal -->
        <div class="modal fade colored-header colored-header-primary" id="form-bp1" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">Add User</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
                    </div>
                    <form action="{{ route('users.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>User Type</label>
                                <div>
                                    <select class="form-control" name="role">
                                        <option value="1">Super Admin</option>
                                        <option value="2">Computer Operator</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <div>
                                    <input class="form-control" type="text" name="name" placeholder="John Doe" required>
                                </div>

                            </div>
                            <div class="form-group">
                                <label>Email address.</label>
                                <div>
                                    <input class="form-control" type="email" name="email" placeholder="username@example.com">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone No</label>
                                <div >
                                    <input class="form-control" type="text" name="phone_no" placeholder="03xx xxxxxxx" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="form-control" type="password" name="pass" placeholder="********">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary md-close" type="button" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-primary md-close" type="submit">Proceed</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')



@endsection