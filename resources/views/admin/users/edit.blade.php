@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Employees</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#">SalesMan</a></li>
                <li class="breadcrumb-item active">Edit Sales Man</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('error'))
                @foreach(Session::get('error')->all() as $e)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-close"></span></div>
                    <div class="message">
                        <strong> {{ $e }} </strong>
                        <br>
                    </div>
                </div>
                @endforeach
                @endif
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">Edit SalesMan</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('users.update', ['id' => $user->id ]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">User Type</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <select class="form-control" name="role" required="">
                                        <option value="1" <?= ($user->user_type == 1) ? 'selected' : '' ?>>Super Admin</option>
                                        <option value="2" <?= ($user->user_type == 2) ? 'selected' : '' ?>>Computer Operator</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Name</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="name" value="{{ $user->name }}" placeholder="John Doe" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Email address.</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="email" name="email" placeholder="username@example.com" value="{{ $user->email }}" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Phone No</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="text" name="phone_no" placeholder="John Doe" required value="{{ $user->phone_no }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Password</label>
                                <div class="col-12 col-sm-8 col-lg-6">
                                    <input class="form-control" type="password" name="pass" placeholder="*****" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-form-label text-sm-right"></label>
                                <div class="col-12 col-sm-8 col-lg-6 text-center">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-masks.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
//    alert($('input[name=phone_no]').val());
    //-initialize the javascript
    App.init();
    App.masks();
});
</script>
@endsection