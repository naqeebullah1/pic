@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Employees</a></li>
                <li class="breadcrumb-item active">Employee Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Employees Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                             function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Salary</th>
                                    <th>Advance</th>
                                    <th>Loan</th>
                                    <th>Payment</th>
                                    <th>Remaning</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php

                                $advance = 0;
                                $loan = 0;
                                $payment = 0;
                                $remaining = 0;

                                @endphp

                                @if(count($employee_report) > 0)

                                @foreach($employee_report as $report)

                                @php
                                $advance += $report->advance;
                                $loan += $report->loan;
                                $payment += $report->payment;
                                $remaining += $report->remaining;
                                @endphp
                                <tr>
                                    <td data-title="Name">{{ $report->name }}</td>
                                    <td data-title="Date">{{ \Carbon\Carbon::parse($report->date)->format('d-m-Y')}}</td>
                                    <td data-title="Description">{{ $report->narration }}</td>
                                    <td data-title="Salary">{{ $report->salary }}</td>
                                    <td data-title="Advance">{{ $report->advance }}</td>
                                    <td data-title="Loan">{{ $report->loan }}</td>
                                    <td data-title="Payment">{{ $report->payment }}</td>
                                    <td data-title="Remaining">{{ $report->remaining }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Employee Ledger Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Advance</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $advance }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Loan</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $loan }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Payment</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $payment }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Rmaining</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $remaining }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection
