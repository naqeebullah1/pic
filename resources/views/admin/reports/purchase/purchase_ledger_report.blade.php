@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Purchase</a></li>
                <li class="breadcrumb-item active">Purchase Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Purchase Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                             function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Company Name</th>
                                    <th>VR #</th>
                                    <th>Total Amount</th>
                                    <th>Discount</th>
                                    <th>Paid</th>
                                    <th>Balance</th>
                                    <th>Net Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $total_amount = 0;
                                $discount = 0;
                                $paid = 0;
                                $balance = 0;
                                $net_amount = 0;
                                @endphp

                                @if(count($ledger_report) > 0)

                                @foreach($ledger_report as $report)

                                @php

                                $total_amount += $report->total_amount;
                                $discount += $report->discount;
                                $paid += $report->paid;
                                $balance += $report->balance;
                                $net_amount += $report->net_amount;

                                @endphp
                                <tr>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="Name">{{ $report->name }}</td>
                                    <td data-title="VR #">{{ $report->vr_no }}</td>
                                    <td data-title="Total Amount">{{ $report->total_amount }}</td>
                                    <td data-title="Discount">{{ $report->discount }}</td>
                                    <td data-title="Paid">{{ $report->paid }}</td>
                                    <td data-title="Balance">{{ $report->balance }}</td>
                                    <td data-title="Net Amount">{{ $report->net_amount }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Purchase Ledger Data Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $total_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Discount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $discount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Paid</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $paid }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Remaning Balance</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $balance }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Net Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $net_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection