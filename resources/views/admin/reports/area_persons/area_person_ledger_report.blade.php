@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
    .sticky-top{
        z-index: 1111111;top: 70px;background-color: #f8f8f8;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Saleman</a></li>
                <li class="breadcrumb-item active">Saleman Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Saleman Report <b>({{$pName}})</b>

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered no-more-tables table-striped" id="table1">
                            <thead>
                                <tr>
                                    <th class="sticky-top">VR_NO#</th>
                                    <th class="sticky-top">Customer</th>
                                    <th class="sticky-top">Description</th>
                                    <th class="sticky-top">Sale</th>
                                    <th class="sticky-top">Sale Details</th>
                                    <th class="sticky-top">Return&nbsp;Sale</th>
                                    <th class="sticky-top">Balance</th>
                                    <th class="sticky-top">Payment</th>
                                    <th class="sticky-top">Remaining</th>
                                    <th class="sticky-top">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php

                                $sale = 0;
                                $return_sale = 0;
                                $payment = 0;

                                @endphp

                                @if(count($area_person) > 0)

                                @foreach($area_person as $report)

                                @php

                                $sale += $report->sale;
                                $return_sale += $report->return_sale;
                                $payment += $report->payment;

                                @endphp
                                <tr>
                                    <td data-title="Invoice#">{{ $report->vr_no }}</td>
                                    <td data-title="Customer">{{ $report->company_name }}</td>
                                    <td data-title="Description">{{ $report->narration }}</td>
                                    <td data-title="Sale">{{ $report->sale }}</td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($report->sale_details) {
                                            $sale_details = json_decode($report->sale_details);
//                                            dump($sale_details);
                                            ?>
                                            <table class="table table-sm table-bordered table-grey">
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                </tr>
                                                @foreach($sale_details as $sd)
                                                <tr>
                                                    <td>{{$sd->product}}</td>
                                                    <td>{{$sd->unit_price}}</td>
                                                    <td>{{$sd->qty}}</td>
                                                </tr>

                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Return Sale">{{ $report->return_sale }}</td>
                                    <td data-title="Balance">{{ $report->remaining + $report->payment}}</td>
                                    <td data-title="Payment">{{ $report->payment }}</td>
                                    <td data-title="Remaining">{{ $report->remaining }}</td>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)->format('d-m-Y') }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Saleman Ledger Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Sale</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $sale }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Return Sale</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $return_sale }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Payment</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $payment }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    @if(!empty($balance))
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total dues to customer</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $balance->total_dues }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection
