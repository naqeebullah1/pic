@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Expenses</a></li>
                <li class="breadcrumb-item active">Expenses Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        <div style="text-align: right;"><h3> Date : {{date('d-m-Y')}}</h3></div>
                        Expenses Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables">
                            <thead>
                                <tr>
                                    <th>Expense Name</th>
                                    <th>Description</th>
                                    <th>Payment Type</th>
                                    <th>Total Amount</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $amount = 0;
                                @endphp

                                @if(count($expense_report) > 0)

                                @foreach($expense_report as $report)

                                @php
                                $amount += $report->amount;
                                @endphp
                                <tr>
                                    <td data-title="Expense Name">{{ $report->expenses }}</td>
                                    <td data-title="Description">{{ $report->description }}</td>
                                    <td data-title="Payment Type">{{ $report->payment_type }}</td>
                                    <td data-title="Total Amount">{{ $report->amount }}</td>
                                    <td data-title="Date">
                                        @if($report->date)
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d', $report->date)->format('d-m-Y') }}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Expense Ledger Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Amount</div>
                            <div class="card-body">
                                <h2>
                                    <b>
                                        {{ $amount }}
                                    </b>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection