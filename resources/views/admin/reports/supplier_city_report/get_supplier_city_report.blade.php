@extends('layouts.admin')

@section('style')
<style>
    @media print { 
        .noprint { 
            visibility: hidden; 
        } 
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Companies</a></li>
                <li class="breadcrumb-item active">Companies Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>

        <div class="row">
            <div class="col-sm-12">
                <div class="card card-table">
                    <div class="card-header">
                        Companies General Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print" ></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Contact No</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>Current Dues</th>
                                    <th>Last payment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $amount = 0;
                                ?>
                                @if(count($cities) > 0)

                                @foreach($cities as $city)

                                <tr>
                                    <td data-title="Company Name">{{ $city->name }}</td>
                                    <td data-title="Contact No">{{ $city->contact_no }}</td>
                                    <td data-title="Address">{{ $city->address }}</td>
                                    <td data-title="City">{{ $city->city }}</td>
                                    <td data-title="Country">{{ $city->country }}</td>
                                    <td data-title="Current Dues"><?php
                                        echo $city->dues.' Rs/-';
                                        $amount += $city->dues;
                                        ?></td>
                                    <td data-title="Last Payment">{{ $city->last_payment }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Companies Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Company</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ count($cities) }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Company dues</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection