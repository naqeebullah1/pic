@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Banks</a></li>
                <li class="breadcrumb-item active">Bank Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Bank Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                             function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Bank Name</th>
                                     <th>Customer</th>
                                    <th>Company</th>
                                    <th>Expense</th>
                                    <th>Date</th>
                                    <th>Description</th>

                                    <th>Deposit Amount</th>
                                    <th>Withdrawal Amount</th>
                                    <th>Remaning Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $total_amount = 0;
                                $deposit = 0;
                                $withdrawal = 0;
                                $remaining = 0;
                                @endphp

                                @if(count($bank_report) > 0)

                                @foreach($bank_report as $report)

                                @php
                                $total_amount += $report->total_amount;
                                $deposit += $report->deposit;
                                $withdrawal += $report->withdrawal;
                                $remaining += $report->remaining;
                                @endphp
                                <tr>
                                    <td data-title="Bank Name">{{ $report->bank_name }}</td>
                                    <td data-title="Customer"><?php
                                        if ($report->company_id) {
                                            echo $companies[$report->company_id];
//                                    echo ;
                                        }
                                        ?></td>
                                    <td data-title="Company"><?php
                                        if ($report->supplier_id) {
                                            if (isset($suppliers[$report->supplier_id])) {
                                                echo $suppliers[$report->supplier_id];
//                                    echo ;
                                            }
                                        }
                                        ?></td>
                                    <td data-title="Expense"><?php
                                        if ($report->parent_id) {
                                            if ($expenses[$report->parent_id]) {
                                                echo $expenses[$report->parent_id];
//                                    echo ;
                                            }
                                        }
                                        ?></td>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="Description">{{ $report->description }}</td>

                                    <td data-title="Deposit">{{ $report->deposit }}</td>
                                    <td data-title="Withdrawal">{{ $report->withdrawal }}</td>
                                     
                                      
                                      @if($report->remaining>0)
                                    <td data-title="Remaining">{{ $report->remaining }}</td>
                                    @else
                                      
                                        <td data-title="Remaining" style="color:red;font-weight:bold;">{{ $report->remaining }}</td>
                                    
                                    @endif
                                    
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Bank Ledger Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $total_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Deposit Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $deposit }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Withdrawal Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $withdrawal }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Current Remaining Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $amount[0]}}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection
