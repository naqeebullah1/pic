@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Customers</a></li>
                <li class="breadcrumb-item active">Customer Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Customer Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables table-sm"  id="table1">
                            <thead>
                                <tr>
                                    <th>Date </th>
                                    <th>Customer Name</th>
                                    <th>VR #</th>
                                    <th>Description</th>
                                    <th>Sale Details</th>
                                    <th>Sale Amount</th>
                                    <!--<th>Sales Discount</th>-->
                                    <th>Return Sale Amount</th>
                                    <th>Discount</th>
                                    <th>Payment</th>

                                    <th>Remaning Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $sale_amount = 0;
                                $return_sale_amount = 0;
                                $payment = 0;
                                $balance = 0;
                                @endphp

                                @if(count($ledger_report) > 0)

                                @foreach($ledger_report as $key=>$report)

                                @php
                                $sale_amount += $report->sale;
                                $return_sale_amount += $report->return_sale;
                                $payment += $report->payment;
                                if(count($ledger_report)-1== $key){
                                $balance = $report->remaining;
                                }

                                @endphp
                                <tr>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="Name">{{ $report->name }}</td>
                                    <td data-title="VR #">{{ $report->vr_no }}</td>
                                    <td data-title="Description">{{ $report->narration }}</td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($report->sale_details) {
                                            $sale_details = json_decode($report->sale_details);
//                                            dump($sale_details);
                                            ?>
                                            <table class="table table-bordered table-sm">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Carton </th>
                                                    <th>Total</th>
                                                </tr>
                                                @php $i = 1; @endphp
                                                @foreach($sale_details as $sd)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$sd->product}}</td>
                                                    <td>{{$sd->unit_price}}</td>
                                                    <td>{{$sd->qty}}</td>
                                                     <td>@php if(isset($sd->cotton)){
                                                        echo $sd->cotton;
                                                    }?></td>
                                                     <!--<td>{{$sd->cotton}}</td>-->
                                                    <td>{{$sd->unit_price*$sd->qty}}</td>
                                                </tr>
                                                @php $i++; @endphp
                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Sale Amount">{{ $report->sale }}</td>
                                    <!--<td data-title="Sale Discount">{{ $report->sale_discount }}</td>-->
                                    <td data-title="Return sale">{{ $report->return_sale }}</td>
                                    <td data-title="Discount">{{ $report->discount }}</td>
                                    <td data-title="Payment">{{ $report->payment }}</td>

                                    <td data-title="Balance">{{ $report->remaining }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Customer Ledger Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Opening Balance</div>
                            <div class="card-body">
                                <h2>
                                    <b>
                                        {{$remaining_balance->remaining}}

                                    </b>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Sale Amount</div>
                            <div class="card-body">
                                <h2>
                                    <b>
                                        {{ $sale_amount }}

                                    </b>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Return Sale Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $return_sale_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Paid Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $payment }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Paid in %</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $new_amount = ($sale_amount+$remaining_balance->remaining) - $return_sale_amount;
                                        if($new_amount==0){
                                          $per = 0;


                                        }
                                        else{
                                           $per = $payment / $new_amount;
                                        }

                                        echo round($per * 100, 2) . '%';
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Current Balance</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $balance }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection
