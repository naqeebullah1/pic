@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Daily Cash Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Daily Cash Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>
                        <!--<img class="logo_img" src="<?= asset('public/images/logo.png') ?>" width="200px">-->
                        <!--<h3 class="daily_heading" style="padding-top:20px;">Daily Cash Report</h3>-->
                        Daily Cash Report

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card" style="">
                            <div class="card-header">Opening Balance</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php echo $opening_bal->remaining; ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Withdrawal for suppliers</div>
                            <div class="card-body">
                                <h2>
                                    <b>
                                        <?php
                                        $withdrawal = $supplier_bal->total_amount;
                                        echo $supplier_bal->total_amount;
                                        ?>
                                    </b>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Deposit for regular customer</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $deposited = $customer_bal->total_amount;
                                        echo $customer_bal->total_amount;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Deposit for walking customers</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $deposited += $walking_customer_bal->total_amount;
                                        echo $walking_customer_bal->total_amount;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Deposit for expenses</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $deposited += $exp_bal->total_amount;
                                        echo $exp_bal->total_amount;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Deposit to bank</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $deposited += $bank_bal->total_amount;
                                        echo $bank_bal->total_amount;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Deposit for employee salaries</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $deposited += $emp_bal->total_amount;
                                        echo $emp_bal->total_amount;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Withdrawal from banks</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $withdrawal += $cash_bal->total_amount;
                                        echo $cash_bal->total_amount;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Deposit for zakat</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        $deposited += $zakat_bal->total_amount;
                                        echo $zakat_bal->total_amount;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Deposited</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        echo $deposited;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Withdrawal</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        echo $withdrawal;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Closing Balance</div>
                            <div class="card-body">
                                <h2><b>
                                        <?php
                                        echo isset($closing_bal->remaining) ? $closing_bal->remaining : 0;
                                        ?>
                                    </b></h2>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    @endsection

    @section('script')


    @endsection