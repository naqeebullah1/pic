@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Companies</a></li>
                <li class="breadcrumb-item active">Company Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Company Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                             function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>VR #</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Purchase Details</th>
                                    <th>Purchase Amount</th>
                                    <th>Return Purchase Amount</th>
                                    <th>Payment</th>
                                    <th>Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $purchase_amount = 0;
                                $return_purchase_amount = 0;
                                $payment = 0;
                                $balance = 0;
                                @endphp

                                @if(count($ledger_report) > 0)

                                @foreach($ledger_report as $key=>$report)

                                @php
                                $purchase_amount += $report->purchase;
                                $return_purchase_amount += $report->return_purchase;
                                $payment += $report->payment;
                                if(count($ledger_report)-1== $key){
                                $balance = $report->balance;
                                }
                                @endphp
                                <tr>
                                    <td data-title="Company Name">{{ $report->name }}</td>
                                    <td data-title="VR #">{{ $report->vr_no }}</td>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)->format('d-m-Y') }}</td>
                                    <td data-title="Description">{{ $report->narration }}</td>
                                    <td class="hide-sm">
                                        <?php
                                        if ($report->purchase_details) {
                                            $sale_details = json_decode($report->purchase_details);
//                                            dump($sale_details);
                                            ?>
                                            <table class="table table-sm table-bordered table-grey">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Product</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Carton</th>
                                                    <th>Total</th>
                                                </tr>
                                                @php $i=1; @endphp
                                                @foreach($sale_details as $sd)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$sd->product}}</td>
                                                    <td>{{$sd->unit_price}}</td>
                                                    <td>{{$sd->qty}}</td>
                                                    <td>@php if(isset($sd->cotton)){
                                                        echo $sd->cotton;
                                                    }?></td>
                                                    <td>{{$sd->unit_price*$sd->qty}}</td>
                                                </tr>
                                                @php $i++; @endphp
                                                @endforeach
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td data-title="Purchase Amount">{{ $report->purchase }}</td>
                                    <td data-title="Return Purchase">{{ $report->return_purchase }}</td>
                                    <td data-title="Payment">{{ $report->payment }}</td>

                                    <td data-title="Balance">{{ $report->balance }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Company Ledger Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Purchase Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $purchase_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Return Purchase Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $return_purchase_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Paid Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $payment }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Balance</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $balance }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection
