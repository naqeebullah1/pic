@extends('layouts.admin')
@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Company Current Status</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Company Current Status</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>
                        <!--<img class="logo_img" src="<?= asset('public/images/logo.png') ?>" width="200px">-->
                        <!--<h3 class="daily_heading" style="padding-top:20px;">Daily Cash Report</h3>-->
                        <p>Company Current Status</p>
                        <p>Date:  <?php echo date('d-m-Y') ?></p>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-2">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped no-more-tables table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-title="Description">Assets</td>
                                            <td data-title="Balance">
                                                <?php
                                                $credit = 0;
                                                if ($assets->assets) {
                                                    $credit += $assets->assets;
                                                    echo $assets->assets;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-title="Description">Cash In Hand</td>
                                            <td data-title="Balance">
                                                <?php
                                                if ($opening_bal) {
                                                    $credit += $opening_bal->total_amount;

                                                    echo $opening_bal->total_amount;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-title="Description">Banks</td>
                                            <td data-title="Balance">
                                                <?php
                                                if ($banks) {
                                                    $credit += $banks->amount;
                                                    echo $banks->amount;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-title="Description">Stock</td>
                                            <td data-title="Balance">
                                                <?php
                                                if ($stock->amount) {
                                                    $credit += $stock->amount;

                                                    echo $stock->amount;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-title="Description">Customer Receivables</td>
                                            <td data-title="Balance">
                                                <?php
                                                if ($customers_reciveable->amount) {
                                                    $credit += $customers_reciveable->amount;

                                                    echo $customers_reciveable->amount;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 2px solid lightblue; background-color: lightblue">
                                            <td data-title="Description">Total Credit</td>
                                            <td data-title="Balance">
                                                <?php
                                                if ($customers_reciveable->amount) {
                                                    echo $credit;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class = "row mt-3">
                            <div class = "col-lg-12">
                                <table class = "table table-striped no-more-tables table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-title="Description">Supplier Payable</td>
                                            <td data-title="Balance">
                                                <?php
                                                $debit = 0;
                                                if ($s_payables->amount) {
                                                    $debit += $s_payables->amount;
                                                    echo $s_payables->amount;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                                <p class="pt-5 text-right pr-3" style="font-size: 20px;">
                                    <?php
                                    $calculate = $credit - $debit;
                                    if ($calculate < 0) {
                                        echo 'Debit: ' . -1 * ($calculate);
                                    } else {
                                        echo 'Credit: ' . $calculate;
                                    }
                                    ?>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @endsection

    @section('script')


    @endsection