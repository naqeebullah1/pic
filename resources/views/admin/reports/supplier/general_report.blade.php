@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />

<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Reports</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Companies</a></li>
                <li class="breadcrumb-item active">Generate Company Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Companies Report
                    </div>
                    <form method="post" action="{{ route('generate-supplier-general-report') }}">
                        {{ csrf_field() }}
                        <div class="card-body pl-8">
                            <div class="row align-items-center">
                                <div class="col-sm-3">
                                    <label>Alphabetical wise </label>
                                    <input class="form-control" type="text" name="name" placeholder="Alphabetical wise">
                                </div>
                                <div class="col-lg-3">
                                    <label>Payment less then</label>
                                    <input class="form-control" type="number" name="less" placeholder="Payment less then">

                                </div>
                                <div class="col-lg-3">
                                    <label>Payment grater then</label>
                                    <input class="form-control" type="number" name="grater" placeholder="Payment grater then">

                                </div>
                                <div class="col-lg-3">
                                    <label data-toggle="collapse" data-target="#date_field">
                                        <input  type="checkbox" name="not_paid" value="1">
                                        &nbsp;&nbsp; Not Paid
                                    </label>
                                </div>
                            </div>
                            <div id="date_field" class="row pt-3 pb-3 collapse">
                                <div class="col-sm-3">
                                    <label>Before given date</label>
                                    <input class="form-control" type="date" name="from_date" placeholder="Alphabetical wise">
                                </div>
                                <div class="col-lg-3">
                                    <label>After given date</label>
                                    <input class="form-control" type="date" name="to_date" placeholder="Payment less then">
                                </div>
                                <div class="col-lg-3">
                                    <label>Enter Days</label>
                                    <input class="form-control" type="number" min="0" name="days" placeholder="Enter days of payment">
                                </div>
                            </div>
                            <div class="row pt-3 pb-3" >
                                <div class="col-sm-12">
                                    <button class="btn btn-space btn-primary" type="submit">Submit</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>
@endsection