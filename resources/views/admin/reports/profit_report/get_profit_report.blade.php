@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Product</a></li>
                <li class="breadcrumb-item active">Product Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        <div style="text-align: left;"><h3> From Date : <b>{{date('d-M-Y',strtotime($from_date))}}</b> - To Date: <b>{{date('d-M-Y',strtotime($to_date))}}</b></h3></div>
                        Expense Details

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables">
                            <thead>
                                <tr>
                                    <th>Expense</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total_exp = 0;
                                ?>
                                @if(count($exp) > 0)
                                @foreach($exp as $e)
                                <?php
                                $total_exp += $e->exp_amount;
                                if ($e->exp_amount) {
                                    ?>
                                    <tr>
                                        <td data-title="Expense">{{ $e->expenses }}</td>
                                        <td data-title="Amount">{{ $e->exp_amount }} Rs/-</td>
                                    </tr>
                                <?php } ?>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        No Record Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <!--Employees sallaries-->

                <div class="card card-table">
                    <div class="card-header">
                        Employees Salaries
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables">
                            <thead>
                                <tr>
                                    <th>Employee</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($employeeSallaries) > 0)
                                @foreach($employeeSallaries as $es)
                                <?php
                                $total_exp += $es->payment + $es->advance;
                                ?>
                                <tr>
                                    <td data-title="Employee">{{ ucwords($es->name) }}</td>
                                    <td data-title="Type"><?php
                                        if ($es->payment > 0) {
                                            echo 'Sallary';
                                        }
                                        if ($es->advance) {
                                            echo 'Advance';
                                        }
                                        ?></td>
                                    <td data-title="Amount Paid">
                                        <?php
                                        echo $es->payment + $es->advance;
                                        ?>
                                        Rs/-</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        No Record Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>



                <div class="card card-table">
                    <div class="card-header">
                        Product Details

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped table-bordered no-more-tables">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Total sale Qty</th>
                                    <th>Cost Price</th>
                                    <th>Sale Price</th>
                                    <th>Discount</th>
                                    <th>Profit</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $c_p = 0;
                                $s_p = 0;
                                $saleDiscounts = 0;
                                ?>
                                @if(count($products) > 0)

                                @foreach($products as $report)
                                <?php
                                $tcp = round($report->tcp);
                                $tsp = round($report->tsp);
                                $tsd = round($tsp - $report->total_price);
                                $c_p += $tcp;
                                $s_p += $tsp;
                                $lineProfit = $tsp - $tcp - $tsd;
                                $saleDiscounts += $tsd;
                                ?>
                                <tr>
                                    <td data-title="Product" >{{ $report->product_name }}</td>
                                    <td data-title="Total Sale Qty" class="text-right">{{ ($report->t_qty)?$report->t_qty:0 }}</td>
                                    <td data-title="Cost Price" class="text-right">{{ ($tcp)?$tcp:0 }}</td>
                                    <td data-title="Sale Price" class="text-right">{{ ($tsp)?$tsp:0 }}</td>
                                    <td data-title="Discount" class="text-right">{{ $tsd }}</td>
                                    <td data-title="Profit" class="text-right">{{ $lineProfit }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        No Record Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Sale Costs</div>
                            <div class="card-body">
                                <h3><b>
                                        {{ number_format($c_p,2) }} Rs/-
                                    </b>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Sale</div>
                            <div class="card-body">
                                <h3>
                                    <b>
                                        {{ number_format($s_p,2) }} Rs/-
                                    </b>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Sale Profit</div>
                            <div class="card-body">
                                <h3>
                                    <b>
                                        <?php
                                        $profit = $s_p - $c_p;
                                        echo number_format($profit, 2)
                                        ?> Rs/-
                                    </b>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Expenses + Sallaries</div>
                            <div class="card-body">
                                <h3><b>
                                        {{ number_format($total_exp,2) }} Rs/-


                                    </b>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total DTL</div>
                            <div class="card-body">
                                <h3><b>{{ number_format($supplier_dtl->total,2) }} Rs/-</b>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Discounts

                                <small>(Sales)</small>
                            </div>
                            <div class="card-body">
                                <h3>
                                    <b>
                                        <?php
                                        $saleTableDisc = $sales[0]->discount;

                                        $paymentAndSaleDiscounts = $saleDiscounts + $saleTableDisc;
                                        ?>
                                        {{ number_format($paymentAndSaleDiscounts,2) }} Rs/-
                                    </b>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Discounts

                                <small>(Payments)</small>
                            </div>
                            <div class="card-body">
                                <h3>
                                    <b>
                                        <?php
                                        $saleTableDisc = $sales[0]->discount;

                                        $paymentAndSaleDiscounts = $customer_discount->total;
                                        ?>
                                        {{ number_format($paymentAndSaleDiscounts,2) }} Rs/-
                                    </b>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Remaining Profit</div>
                            <div class="card-body">
                                <h3>
                                    <b>
                                        <?php
                                        $remainingProfit = ($profit + $supplier_dtl->total) - ($total_exp + $paymentAndSaleDiscounts+$saleDiscounts + $saleTableDisc);
                                        echo number_format($remainingProfit, 2);
                                        ?> Rs/-
                                    </b>
                                </h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection
