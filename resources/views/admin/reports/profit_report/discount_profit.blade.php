@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />
<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Reports</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Item-Profit Report</a></li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Discount Reports
                    </div>
                    <form method="post" action="{{ route('discount_profit') }}">
                        {{ csrf_field() }}
                        <div class="card-body pl-8">
                            <div class="row">

                                <div class="col-sm-6">
                                    <br>
                                    <div class="form-group row">
                                       

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 col-sm-8 col-lg-6">
                                            <label>From date</label>
                                            <div>
                                                <input type="date" class="form-control" name="from_date" value="<?= isset($_POST['from_date']) ? $_POST['from_date'] : '' ?>" required>                                            
                                            </div>
                                        </div> 

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 col-sm-8 col-lg-6">
                                            <label>To date</label>
                                            <div>
                                                <input type="date" class="form-control" name="to_date" value="<?= isset($_POST['to_date']) ? $_POST['to_date'] : '' ?>" required>                                            
                                            </div>
                                        </div> 

                                    </div>


                                    <div class="row pt-3">
                                        <div class="col-sm-6">
                                            <button class="btn btn-space btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
                if (!empty($purchase_ledger)) {
                    ?>
                    <div class="main-content container-fluid p-0" id='printMe'>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-table">
                                    <div class="card-header">
                                        <div style="text-align: left;"><h3> From Date : {{date('d-m-Y',strtotime($from_date))}} - To Date: {{date('d-m-Y',strtotime($to_date))}}</h3></div>
                                        Profit Report
                                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>
                                        <script type="text/javascript">
                                            function printDiv(divName) {
                                                //                                    alert(divName);
                                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                                $('#' + divName).prepend(html);
                                                var printContents = document.getElementById(divName).innerHTML;
                                                var originalContents = document.body.innerHTML;
                                                document.body.innerHTML = printContents;
                                                window.print();
                                                document.body.innerHTML = originalContents;
                                                $('.logo-img').remove();
                                                window.location.reload();
                                            }
                                        </script>
                                    </div>

                                    <div class="card-body">
                                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                                            <thead>
                                                <tr>
                                                    <th>Company Name</th>
                                                    <th>Discount</th>
                                                    <th>Date</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $total = 0; ?>
                                                @foreach($purchase_ledger as $report)
                                                <?php
                                               
                                                ?>
                                                <tr>
                                                    <td data-title="Product Name">{{ $report->name }}</td>
                                                    <td data-title="Total Sale Qty">{{$report->discount  }}</td>
                                                    <td data-title="Cost Price">{{ $report->created_at  }}</td>
                                                   
                                                </tr>

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                       
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>
@endsection