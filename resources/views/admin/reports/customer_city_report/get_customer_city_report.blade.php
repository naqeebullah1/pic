@extends('layouts.admin')

@section('style')

@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Customers</a></li>
                <li class="breadcrumb-item active">Customer in Cities</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Customers in City

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print" ></i> Print</button>

                        <script type="text/javascript">

                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table no-more-tables table-striped table-bordered" id="table1">
                            <thead>
                                <tr>
                                    <th>Last Payment</th>
                                    <th>Customer Name</th>
                                    <th>Current Dues</th>
                                    <th>City</th>
                                    <th>Contact No</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $amount = 0; ?>
                                @if(count($cities) > 0)

                                @foreach($cities as $city)

                                <tr>
                                    <td data-title="Last Payment">{{ ($city->last_payment)?date('d M Y',strtotime($city->last_payment)):''  }}</td>
                                    <td data-title="Name">{{ $city->name }}</td>
                                    <td data-title="Dues"><?php
                                        echo $city->dues;
                                        $amount += $city->dues;
                                        ?></td>
                                    <td data-title="City">{{ $city->city }}</td>
                                    <td data-title="Contact No">{{ $city->contact }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Customers Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Customer</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ count($cities) }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Customer dues</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection