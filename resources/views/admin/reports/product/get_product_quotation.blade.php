@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
        .noprinttd{
            display: none;
        }
    }
    .selectable{

        background-color: #eee6;

    }
    .logo-center{
        width:100%;
        display:flex;
        justify-content:center;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Product</a></li>
                <li class="breadcrumb-item active">Product Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Product Quotation Report
                        
                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                            var html = '<div class="logo-img logo-center"><img src="<?= asset('public/images/logo2.jpg') ?> " width="150px"></div><br><div>{{date("l jS \of F Y")}}</div>';
                            $('#' + divName).prepend(html);
                            var printContents = document.getElementById(divName).innerHTML;
                            var originalContents = document.body.innerHTML;
                            document.body.innerHTML = printContents;
                            window.print();
                            document.body.innerHTML = originalContents;
                            //$('.logo-img').remove();
                            window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-striped no-more-tables table-bordered">
                            <thead>
                                <tr>
                                    <th >Product Name</th>
                                    <th>Product Type</th>
                                    <th class="noprinttd">Purchase price</th>
                                    <th class="noprinttd">sale price</th>
                                    <th>Price/Liter</th>
                                    <th>Price/can</th>
                                    <th>Price/carton</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $items = 0;
                                $total_amount = 0;
                                ?>
                                @if(count($products) > 0)
                                @foreach($products as $key=>$report)
                                <tr>
                                    <td data-title="Product Name">{{ $report->product_name }}</td>
                                    <td data-title="Product Type">{{ $report->qty }}</td>
                                    <td data-title="Purchase Price" class="noprinttd">{{ $report->product_cost_price }}</td>
                                    <td data-title="Sale Price" class="noprinttd">{{ $report->product_sale_price }}</td>
                                    <td data-title="Price/Liter" contenteditable=""></td>
                                    <td data-title="Price/can" contenteditable="" onkeyup="changeOilPrice(this,{{ $report->qty_per_cotton }})">{{ floor($report->product_sale_price) }}</td>
                                    <td data-title="Price/Carton" contenteditable="">
                                        {{ floor($report->product_sale_price*$report->qty_per_cotton) }} Rs/-
                                    </td>


                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        No Product Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Products</div>
                            <div class="card-body">
                                <h2>
                                    <b>
                                        {{ count($products) }}
                                    </b>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Items</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $items }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $total_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    function changeOilPrice(ele, qty){
    var price = $(ele).text() * qty;
    price = price || 0;
    price= price+' Rs/-'
    $(ele).next('td').text(price);
    }
</script>

@endsection