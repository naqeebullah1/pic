@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Product</a></li>
                <li class="breadcrumb-item active">Product Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Product Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>VR #</th>
                                    <th>Customer</th>
                                    <th>Company</th>
                                    <th>Description</th>
                                    <th>Total Qty</th>
                                    <th>Purchased</th>
                                    <th>Return Purchase</th>
                                    <th>Sale</th>
                                    <th>Return Sale</th>
                                    <th>Remaining</th>
                                    <th>Remaining carton</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $total_qty = 0;
                                $purchase_qty = 0;
                                $return_purchase_qty = 0;
                                $sale_qty = 0;
                                $return_sale = 0;
                                $remaning = 0;
                                @endphp

                                @if(count($ledger_report) > 0)

                                @foreach($ledger_report as $key=>$report)

                                @php

                                $total_qty += $report->total_qty;
                                $purchase_qty += $report->purchased_qty;
                                $return_purchase_qty += $report->return_purchased;
                                $sale_qty += $report->sale;
                                $return_sale += $report->return_sale;

                                if(count($ledger_report)-1== $key){
                                $remaning = $report->remaning;
                                }


                                @endphp
                                <?php
                                $isAdjust = '';
                                if ($report->adjust):
                                    $isAdjust = 'bg-warning text-white';
                                endif;
                                ?>
                                <tr class="<?= $isAdjust ?>">
                                    <td data-title="Product Name">{{ $report->product_name }}</td>
                                    <td data-title="VR #">
                                        <?php
                                        if ($report->sale_no) {
                                            echo $report->sale_no;
                                        }
                                        if ($report->return_sale_no) {
                                            echo $report->return_sale_no;
                                        }
                                        if ($report->purchase_no) {
                                            echo $report->purchase_no;
                                        }
                                        if ($report->return_purchase_no) {
                                            echo $report->return_purchase_no;
                                        }
                                        ?> </td>
                                    <td data-title="Customer Name">
                                        <?php
                                        if ($customers) {
                                            if ($report->customer_sale_id) {
                                                echo $customers[$report->customer_sale_id];
                                            }

                                            if ($report->return_sale_customer_id) {
                                                echo $customers[$report->return_sale_customer_id];
                                            }
                                        }
                                        if ($report->customer_sale_id === 0) {
                                            echo '<span class="text-danger">Walking Customer</span>';
                                        }
                                        ?> 
                                    </td>
                                    <td data-title="Company Name">
                                        <?php
                                        if ($report->supplier_purchase_id) {
                                            if (isset($suppliers[$report->supplier_purchase_id])) {
                                                echo $suppliers[$report->supplier_purchase_id];
                                            } else {
                                                echo "Supplier Deleted";
                                            }
                                        }
                                        if ($report->return_purchase_supplier_id) {
                                            echo $suppliers[$report->return_purchase_supplier_id];
                                        }
                                        if ($report->sale_id == null && $report->purchase_id == null) {
                                            echo " ";
                                        }
                                        ?> 

                                    </td>
                                    <td data-title="Description">{{ $report->description }}</td>
                                    <td data-title="Totel Qty">{{ $report->total_qty }}</td>
                                    <td data-title="Purchased">{{ $report->purchased_qty }}</td>
                                    <td data-title="return Purchase">{{ $report->return_purchased }}</td>
                                    <td data-title="Sale">{{ $report->sale }}</td>
                                    <td data-title="Return Sale">{{ $report->return_sale }}</td>
                                    <td data-title="Remaining">{{ $report->remaning }}</td>
                                    <td data-title="Remaining carton">{{ round($report->remaning/$report->qty_per_cotton,1) }}</td>
                                    <td data-title="Date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)->format('d-m-Y') }}</td>
                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        Product Ledger Data Not Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

                <div class="row">
                    <!--                    <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header">Total Qty</div>
                                                <div class="card-body">
                                                    <h2><b>
                                                            {{ $total_qty }}
                                                        </b></h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header">Purchase Qty</div>
                                                <div class="card-body">
                                                    <h2><b>
                                                            {{ $purchase_qty }}
                                                        </b></h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header">Return Purchased Qty</div>
                                                <div class="card-body">
                                                    <h2><b>
                                                            {{ $return_purchase_qty }}
                                                        </b></h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header">Sale Qty</div>
                                                <div class="card-body">
                                                    <h2><b>
                                                            {{ $sale_qty }}
                                                        </b></h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header">Return Sale Qty</div>
                                                <div class="card-body">
                                                    <h2><b>
                                                            {{ $return_sale }}
                                                        </b></h2>
                                                </div>
                                            </div>
                                        </div>-->
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Remaining Qty</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $remaning }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection