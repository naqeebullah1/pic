@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}" />

<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b:after {
        content: none;
        font-family: none;
        font-size: 1.923rem;
        font-weight: normal;
        line-height: 3.5382rem;
        color: #404040;
    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Reports</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Products</a></li>
                <li class="breadcrumb-item active">Generate Product Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">Filter Products
                    </div>
                    <form method="post" action="{{ route('get-product-general-report') }}">
                        {{ csrf_field() }}
                        <div class="card-body pl-8">
                            <div class="row">

                                <div class="col-sm-6">
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-12 col-sm-8 col-lg-6">
                                            <label>Product Name</label>
                                            <select class="select2" name="product_id" data-placeholder="All Products">
                                                <option value=""> Select Product </option>
                                                @foreach($products as $product)
                                                <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 col-sm-8 col-lg-6">
                                            <label>Company</label>
                                            <select class="select2" name="supplier_id" data-placeholder="Select Company">
                                                <option value=""></option>
                                                @foreach($suppliers as $product)
                                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
            
                                    
                                    <div class="row pt-3">
                                        <div class="col-sm-6">
                                            <button class="btn btn-space btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('script')
<script src="{{ asset('public/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/app-form-elements.js') }}" type="text/javascript"></script>
@endsection