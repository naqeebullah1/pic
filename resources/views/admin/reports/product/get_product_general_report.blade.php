@extends('layouts.admin')

@section('style')
<style>
    @media print {
        .noprint {
            visibility: hidden;
        }
    }
    .selectable{

        background-color: #eee6;

    }
</style>
@endsection

@section('content')

<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Report</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="#">Product</a></li>
                <li class="breadcrumb-item active">Product Report</li>
            </ol>
        </nav>
    </div>
    <div class="main-content container-fluid" id='printMe'>
        <div class="row">
            <div class="col-sm-12">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message"><strong> {{ Session::get('success') }} </strong></div>
                </div>
                @endif

                <div class="card card-table">
                    <div class="card-header">
                        Product Report

                        <button class="btn btn-sm btn-info float-right noprint" onclick="printDiv('printMe')"><i class="fas fa-print"></i> Print</button>

                        <script type="text/javascript">
                            function printDiv(divName) {
                                var html = '<div class="logo-img"><img src="<?= asset('public/images/logo.png') ?> " width="150px"></div>';
                                $('#' + divName).prepend(html);
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;
                                document.body.innerHTML = printContents;
                                window.print();
                                document.body.innerHTML = originalContents;
                                $('.logo-img').remove();
                                window.location.reload();
                            }
                        </script>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-bordered table-striped no-more-tables" id="table1">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Product Type</th>
                                    <?php
                                    if (Auth::user()->user_type == 1) {
                                        ?>
                                        <th>Purchase price</th>
                                    <?php } ?>
                                    <th>sale price</th>
                                    <th>whole sale price</th>
                                    <th>Stock (Pieces)</th>
                                    <th>Stock (cartons)</th>

                                    <th>Supplier</th>
                                    <?php
                                    if (Auth::user()->user_type == 1) {
                                        ?>
                                        <th>Amount</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $items = 0;
                                $total_amount = 0;
                                ?>
                                @if(count($products) > 0)

                                @foreach($products as $report)
                                <tr>
                                    <td data-title="Product Name">{{ $report->product_name }}</td>
                                    <td data-title="Product Type">{{ $report->qty }}</td>
                                    <?php
                                    if (Auth::user()->user_type == 1) {
                                        ?>
                                        <td class="selectable" data-title="Purchase Price" contenteditable>{{ $report->product_cost_price }}</td>
                                    <?php } ?>
                                    <td data-title="Sale Price" class="selectable" contenteditable>{{ $report->product_sale_price }}</td>
                                    <td data-title="Whole Sale Price" class="selectable" contenteditable>{{ $report->product_whole_sale_price }}</td>
                                    <td data-title="Stock (pieces)">{{ $report->stock_qty }}</td>
                                    <td data-title="Stock (carton)">
                                        <?php
                                        $product_qty = 1;
                                        if ($report->qty_per_cotton) {
                                            $product_qty = round($report->qty_per_cotton, 2);
//                                            dump($product_qty);
                                            $totalcot = floor($report->stock_qty / $product_qty) . ' cotton, ' . $report->stock_qty % $product_qty . ' Pieces';
                                        }
                                        ?>
                                        {{ $report->stock_qty. ' / '.$report->qty_per_cotton  .' = '. $totalcot  }}
                                    </td>
                                    <td data-title="Supplier">{{ $report->name }}</td>
                                    <?php
                                    if (Auth::user()->user_type == 1) {
                                        ?>
                                        <td data-title="Amount"><?php
                                            echo $report->product_cost_price * $report->stock_qty;
                                            $items += $report->stock_qty;
                                            $total_amount += $report->product_cost_price * $report->stock_qty;
                                            ?></td>
                                    <?php } ?>

                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td>
                                        No Product Found
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Products</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ count($products) }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Items</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $items }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">Total Amount</div>
                            <div class="card-body">
                                <h2><b>
                                        {{ $total_amount }}
                                    </b></h2>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')


@endsection