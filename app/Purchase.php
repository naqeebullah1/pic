<?php

namespace App;

use App\PurchaseDetail;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table='supplier_purchase';
    public function purchaseDetails() {
        return $this->hasMany('App\PurchaseDetail');
    }

}
