<?php

namespace App\Http\Traits;

use DB;

trait UpdateLedgersTrait {
    /*
     * This trait Will Update following ledgers if necessary on delete of any record
     * 1. Cash Ledger
     * 2. Bank Ledger
     * 3. Customer Ledger
     * 4. Company/supplier Ledger
     * 5. Product Ledger
     * 
     */

    public function updateCashLedger($cashRec) {
        /*
         * Delete $cashId before ledger updation;
         */
        $getInfo = $cashRec->first();
        $cashRec->delete();
        $remainingAmount = $getInfo->remaining;
        if ($getInfo->deposit > 0) {
            $remainingAmount = $remainingAmount - $getInfo->deposit;
        }
        if ($getInfo->withdrawal > 0) {
            $remainingAmount = $remainingAmount + $getInfo->withdrawal;
        }
        $getNextRecords = DB::select("select id,deposit,withdrawal,is_deleted, remaining from cash_in_hand_ledger where id > " . $getInfo->id);
        if ($getNextRecords) {
            foreach ($getNextRecords as $record) {
                if ($record->is_deleted == 1) {
                    DB::table('cash_in_hand_ledger')->where('id', $record->id)->delete();
                } else {
                    if ($record->deposit > 0) {
                        $remainingAmount = $remainingAmount + $record->deposit;
                    }
                    if ($record->withdrawal > 0) {
                        $remainingAmount = $remainingAmount - $record->withdrawal;
                    }
                    DB::table('cash_in_hand_ledger')->where('id', $record->id)->update([
                        'remaining' => $remainingAmount
                    ]);
                }
            }
        }
    }

    public function updateCustomerLedger($customerLedger) {
        /*
         * Get Prev Amount
         */
        $getInfo = $customerLedger->first();
        $remainingAmount = $getInfo->remaining;
        $payment = $getInfo->payment;


        $payment = $getInfo->payment + $getInfo->discount;
        /*
         * + Payment
         * - sale
         * + return sale
         */

        if ($getInfo->sale) {
            $remainingAmount = $remainingAmount - $getInfo->sale + $payment;
        } elseif ($getInfo->return_sale) {
            $remainingAmount = $remainingAmount + $getInfo->return_sale - $payment;
        } else {
            $remainingAmount = $remainingAmount + $payment;
        }

        /*
         * Delete Current Ledger
         */
        $customerLedger->delete();

        $this->customerLedgerNextRecords($getInfo->id, $getInfo->company_id, $remainingAmount);
    }

    public function customerLedgerNextRecords($ledgerId, $customerId, $remainingAmount) {
        $getNextRecords = DB::select("select id,company_id,remaining,payment,discount,sale,return_sale  from company_ledger where id > " . $ledgerId . " and company_id=" . $customerId);
        if ($getNextRecords) {
            foreach ($getNextRecords as $record) {

                $payment = $record->payment + $record->discount;
                /*
                 * - Payment
                 * + sale
                 * - return sale
                 */

                if ($record->sale) {
                    $remainingAmount = $remainingAmount + $record->sale - $payment;
                } elseif ($record->return_sale) {
                    $remainingAmount = $remainingAmount - $record->return_sale + $payment;
                } else {
                    $remainingAmount = $remainingAmount - $payment;
                }
                DB::table('company_ledger')->where('id', $record->id)->update([
                    'remaining' => $remainingAmount
                ]);
            }
        }
        return $remainingAmount;
    }

    public function updateSupplierLedger($record) {
        $getInfo = $record->first();
        $getPrevRec = DB::select('select * from supplier_ledger where id < ? and supplier_id = ? order by id desc limit 1', [$getInfo->id, $getInfo->supplier_id]);
        if ($getPrevRec) {
            $remainingAmount = $getPrevRec[0]->balance;
        }
        $record->delete();
        $this->supplierLedgerNextRecords($remainingAmount, $getInfo->supplier_id, $getInfo->id);
    }

    public function supplierLedgerNextRecords($remainingAmount, $supplierId, $ledgerId) {
        $getNextRecords = DB::select("select id,supplier_id,purchase,return_purchase,payment,balance,dtl  from supplier_ledger where id > " . $ledgerId . " and supplier_id=" . $supplierId);
        if ($getNextRecords) {
            foreach ($getNextRecords as $record) {
                $payment = $record->payment + $record->dtl;
                if ($record->purchase) {
                    $remainingAmount = $remainingAmount + $record->purchase - $payment;
                } elseif ($record->return_purchase) {
                    $remainingAmount = $remainingAmount - $record->return_purchase + $payment;
                } else {
                    $remainingAmount = $remainingAmount - $payment;
                }
                DB::table('supplier_ledger')->where('id', $record->id)->update([
                    'balance' => $remainingAmount
                ]);
            }
        }
        return $remainingAmount;
    }

    public function updateBankLedger($bankRec) {
        $getInfo = $bankRec->first();
        $bankId = $getInfo->bank_id;
        $bankRec->delete();
        $remainingAmount = $getInfo->remaining;
        if ($getInfo->deposit > 0) {
            $remainingAmount = $remainingAmount - $getInfo->deposit;
        }
        if ($getInfo->withdrawal > 0) {
            $remainingAmount = $remainingAmount + $getInfo->withdrawal;
        }
        $this->bankLedgerNextRecords($getInfo->id, $bankId, $remainingAmount);
    }

    public function bankLedgerNextRecords($ledgerId, $bankId, $remainingAmount) {
        $getNextRecords = DB::select("select id,bank_id,description,is_deleted,deposit,withdrawal,description,  remaining from bank_ledger where id > " . $ledgerId . " and bank_id=$bankId order by id asc");
        if ($getNextRecords) {
            foreach ($getNextRecords as $record) {
                if ($record->is_deleted == 1) {
                    DB::table('bank_ledger')->where('id', $record->id)->delete();
                } else {
                    if ($record->deposit > 0) {
                        $remainingAmount = $remainingAmount + $record->deposit;
                    }
                    if ($record->withdrawal > 0) {
                        $remainingAmount = $remainingAmount - $record->withdrawal;
                    }

                     DB::table('bank_ledger')->where('id', $record->id)->update([
                      'remaining' => $remainingAmount
                      ]); 
                }
            }
        }
        return $remainingAmount;
    }

    public function updateProductLedger($productLedger) {
        $productId = $productLedger->product_id;
        $remaining = $productLedger->remaning;
        if ($productLedger->sale > 0) {
            $remaining = $remaining + $productLedger->sale;
        }
        if ($productLedger->adjust > 0) {
            $remaining = $remaining + $productLedger->adjust;
        }
        if ($productLedger->return_purchased > 0) {
            $remaining = $remaining + $productLedger->return_purchased;
        }
        if ($productLedger->return_sale > 0) {
            $remaining = $remaining - $productLedger->return_sale;
        }
        if ($productLedger->purchased_qty > 0) {
            $remaining = $remaining - $productLedger->purchased_qty;
        }

        $getNextRecords = DB::select("select id,product_id,remaning,sale,return_sale,purchased_qty, adjust,return_purchased  from product_ledger where id > " . $productLedger->id . " and product_id=$productId");
        if ($getNextRecords) {
            foreach ($getNextRecords as $record) {
//                dump($remaining);
                if ($record->sale > 0) {
                    $remaining = $remaining - $record->sale;
                }
                if ($record->adjust > 0) {
                    $remaining = $remaining - $record->adjust;
                }
                if ($record->return_purchased > 0) {
                    $remaining = $remaining - $record->return_purchased;
                }
                if ($record->return_sale > 0) {
                    $remaining = $remaining + $record->return_sale;
                }
                if ($record->purchased_qty > 0) {
                    $remaining = $remaining + $record->purchased_qty;
                }
                DB::table('product_ledger')->where('id', $record->id)->update([
                    'remaning' => $remaining
                ]);
            }
        }
    }

}
