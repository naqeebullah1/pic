<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;

class CheckRoleMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $identifyRoute = request()->segment(1);
        if (Auth::user()->user_type == 3) {
//                dd($_SERVER);
            if ($identifyRoute != 'salesman') {
                Session::flash('error', 'You are not allowed to access that location');
                return redirect()->route('my_dashboard');
            }
        }
        return $next($request);
    }

}
