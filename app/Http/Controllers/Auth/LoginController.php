<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function redirectPath() {

        if (Auth::guard('saleman')->check()) {
            return route('my_dashboard');
        } else {
            return route('home');
        }
    }

    protected function authenticated() {
        $this->employee_payments();
    }

    public function employee_payments() {
//        dd($arrayToSave);
        try {
            DB::beginTransaction();
            $date = date('Y-m-01', strtotime('-1 month'));
            /*
             *  get users that have joined newly
             */
            $new_employees = DB::table('employees')
                    ->whereDate('last_salary_date', '>', $date)
                    ->whereDate('last_salary_date', '<', date('Y-m-01'))
                    ->where('leaving_date', null);
            $get_new_employees = $new_employees->get();

            if ($get_new_employees) {
                $arrayToSave = array();
                foreach ($get_new_employees as $key => $emp):

                    $calculate_days = date('d', strtotime(date('Y-m-01', strtotime('-1 day'))) - strtotime($emp->last_salary_date));
                    $amount = round(($emp->salary / 30) * $calculate_days);
//dd($calculate_days);
                    $ledger = DB::table('employee_ledger')
                                    ->where('employee_id', '=', $emp->id)
                                    ->orderBy('id', 'desc')->first();

                    $empRec = DB::table('employees')
                            ->where('id', '=', $emp->id)
                            ->update([
                        'remainings' => $ledger->remaining + $amount,
                        'last_salary_date' => date('y-m-01')
                    ]);

                    $arrayToSave[$key]['employee_id'] = $emp->id;
                    $arrayToSave[$key]['date'] = date('Y-m-d');
                    $arrayToSave[$key]['narration'] = 'salary to pay';
                    $arrayToSave[$key]['salary'] = $emp->salary;
                    $arrayToSave[$key]['advance'] = 0;
                    $arrayToSave[$key]['loan'] = 0;
                    $arrayToSave[$key]['payment'] = 0;
                    $arrayToSave[$key]['remaining'] = $ledger->remaining + $amount;
                    $arrayToSave[$key]['created_at'] = date('Y-m-d H:i:s');
                    $arrayToSave[$key]['updated_at'] = date('Y-m-d H:i:s');
                endforeach;
//            dd($arrayToSave);
                DB::table('employee_ledger')->insert($arrayToSave);
            }

            /*
             * if sallary date exceded and payment is not stored
             */

            $today = date('Y-m-d');
            $check = true;
            while ($check) {

                $pendingSallaries = "select `x`.* from("
                        . "select `e`.*, "
                        . "DATE_ADD(`e`.`last_salary_date`, INTERVAL 1 MONTH) "
                        . "as `next_date` from `employees` as `e` where `e`.`leaving_date` is null ) as `x` where `x`.`next_date`<='$today';";

                $employees = DB::select($pendingSallaries);

                if ($employees) {
                    $arrayToSave = array();
                    foreach ($employees as $key => $emp):
                        $ledger = DB::table('employee_ledger')
                                ->where('employee_id', '=', $emp->id)
                                ->orderBy('id', 'desc')
                                ->first();
                        $nextDate = date('Y-m-01', strtotime($emp->next_date));
                        $empRec = DB::table('employees')
                                ->where('id', '=', $emp->id)
                                ->update([
                            'remainings' => $ledger->remaining + $emp->salary,
                            'last_salary_date' => $nextDate
                        ]);

                        $arrayToSave[$key]['employee_id'] = $emp->id;
                        $arrayToSave[$key]['date'] = $nextDate;
                        $arrayToSave[$key]['narration'] = 'salary to pay';
                        $arrayToSave[$key]['salary'] = $emp->salary;
                        $arrayToSave[$key]['advance'] = 0;
                        $arrayToSave[$key]['loan'] = 0;
                        $arrayToSave[$key]['payment'] = 0;
                        $arrayToSave[$key]['remaining'] = $ledger->remaining + $emp->salary;
                        $arrayToSave[$key]['created_at'] = date('Y-m-d H:i:s');
                        $arrayToSave[$key]['updated_at'] = date('Y-m-d H:i:s');
                    endforeach;
                    DB::table('employee_ledger')->insert($arrayToSave);
                }else {
                    $check = false;
                }
//                    dd($empRec);
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            dd($e);
        }
    }

}
