<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use DB;
use Session;
use Redirect;

class UsersController extends Controller {

    public function index(Request $request) {
        $validator = Validator::make($request->all(), [
                    'phone_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => 'false',
                        'status' => '401',
                        'message' => 'Please Review All Fields',
                        'errors' => $validator->errors()
            ]);
        }

        if (Auth::attempt(['phone_no' => $request->phone_number, 'password' => $request->password])) {
            //Authentication passed

            $user = Auth::user();

            return response()->json([
                        'success' => 'true',
                        'status' => '200',
                        'message' => 'User LoggedIn Successfully',
                        'user_data' => $user
            ]);
        }

        return response()->json([
                    'success' => 'false',
                    'status' => '401',
                    'message' => 'Your Phone No is Incorrect'
        ]);
    }

    public function verify_phone_number(Request $request) {
        $validator = Validator::make($request->all(), [
                    'phone_no' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => 'false',
                        'status' => '401',
                        'message' => 'Please Review All Fields',
                        'errors' => $validator->errors()
            ]);
        }

        $result = DB::table('users')->where('phone_no', $request->phone_no)->first();

        if ($result) {
            return response()->json([
                        'success' => 'true',
                        'status' => '200',
                        'message' => 'Phone Number Verified',
            ]);
        } else {
            return response()->json([
                        'success' => 'false',
                        'status' => '401',
                        'message' => 'Phone Number does not exits in our records',
            ]);
        }
    }

    public function update_password(Request $request) {
        $validator = Validator::make($request->all(), [
                    'new_password' => 'required',
                    'phone_no' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => 'false',
                        'status' => '401',
                        'message' => 'Please Review All Fields',
                        'errors' => $validator->errors()
            ]);
        }

        $result = DB::table('users')->where('phone_no', $request->phone_no)->update([
            'password' => bcrypt($request->new_password)
        ]);

        if ($result) {
            return response()->json([
                        'success' => 'true',
                        'status' => '200',
                        'message' => 'Password Updated Successfully',
            ]);
        } else {
            return response()->json([
                        'success' => 'false',
                        'status' => '401',
                        'message' => 'Please check your phone number, Phone does not exists in our records',
            ]);
        }
    }

    public function login(Request $request) {
//        dd($request->get('email'));
        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );
//        if (Auth::attempt($credentials)) {
//            return Redirect::back();
//        } else
        if (Auth::guard('saleman')->attempt($credentials)) {
            session([
                'name' => $request->get('username')
            ]);
//            dd(Auth)
        } else {
            Session::flash('message', "Invalid Credentials , Please try again.");
            return Redirect::back();
        }
    }

}
