<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FrontendController extends Controller {

    public function index() {
        $products = DB::table('products')->get();
//        dd($products);
        return view('welcome')->with('products', $products);
    }

}
