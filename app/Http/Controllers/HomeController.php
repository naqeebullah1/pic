<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if (Auth::user()->user_type == 'sale_man') {
            Auth::logout();
            return redirect('/login');
        }
        $curency = DB::table('currency')->latest('created_at')->first();

        if ($curency) {
            session(['today_currency' => $curency->today_currency]);
        }

        $count_products = DB::table('products')
                ->where('status', 1)
                ->count();
        $count_area_person = DB::table('area_person')
                ->where('status', 1)
                ->count();
        $count_employees = DB::table('employees')
                ->where('status', 1)
                ->count();
        $count_supplier = DB::table('supplier')
                ->where('status', 1)
                ->count();
        $count_company = DB::table('company')
                ->where('status', 1)
                ->count();
                $less_qty = DB::table('products')
                ->where('qty','<', 5)
                ->count();
        $count_bank_acc = DB::table('bank_acc')
                ->where('status', 1)
                ->count();
        $count_manufacturing_companies = DB::table('manufacturing_companies')->count();
//        $count_cities = DB::table('cities')->count();
        $count_user = DB::table('users')->where('user_type', 'sale_man')->count();

        $date = date('Y-m-d');

        $cheques = DB::table('cheques')
                ->where('is_clear', 0)
                ->where('release_date', '<=', $date)
                ->where('customer_id', '!=', null)
                ->orderBy('id', 'DESC')
                ->count();
        $suppliercheques = DB::table('cheques')
                ->where('is_clear', 0)
                ->where('release_date', '<=', $date)
                ->where('supplier_id', '!=', null)
                ->orderBy('id', 'DESC')
                ->count();
//        dd($cheques);

        return view('home')
                        ->with('product', $count_products)
                        ->with('area_person', $count_area_person)
                        ->with('employees', $count_employees)
                        ->with('supplier', $count_supplier)
                        ->with('company', $count_company)
                        ->with('bank_acc', $count_bank_acc)
                        ->with('manufacturing_companies', $count_manufacturing_companies)
                        ->with('user', $count_user)
                        ->with('cheques', $cheques)
                        ->with('suppliercheques', $suppliercheques)
                        ->with('less_qty', $less_qty);
    }

    public function add_currency(Request $request) {
        DB::table('currency')->insert([
            'today_currency' => $request->currency,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Session::flash('success', 'Currency Updated Successfully');
        return redirect()->back();
    }

}
