<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class HeadExpensesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $head_expenses = DB::table('expenses')
                ->select('id', 'expenses')
                ->where('level',0)
                ->get();
//        dd($head_expenses);
        return view('admin.expenses.head_expenses')
                        ->with('head_expenses', $head_expenses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        DB::table('expenses')->insert([
            'expenses' => $request->head_expense,
            'expenses_slug' => str_slug($request->head_expense),
            'level' => 0,
            'parent_id' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        Session::flash('success', 'Head Expense Added Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $head_expense = DB::table('expenses')->where('id', $id)->select('id', 'expenses')->first();
        return view('admin.expenses.head_edit')
                        ->with('head_expense', $head_expense);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        DB::table('expenses')->where('id', $id)->update([
            'expenses' => $request->expense
        ]);

        Session::flash('success', 'Head Expense Updated Successfully');
        return redirect()->route('expenses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('expenses')->where('id', $id)->delete();

        Session::flash('success', 'Head Expense Deleted Successfully');
        return redirect()->back();
    }

}
