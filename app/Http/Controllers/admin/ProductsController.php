<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ProductsController extends Controller {

    public function index() {
        $products = DB::table('products')->orderBy('product_code', 'asc')
                ->select('products.*', 'supplier.name')
                ->leftJoin('supplier', 'supplier.id', '=', 'products.supplier_id');
        $status = 1;
        if (isset($_GET['status']) && ($_GET['status'] == 0 || $_GET['status'] == 1)) {
            $status = $_GET['status'];
        }
        $products = $products->where('products.status', $status);
        $products = $products->get();

        return view('admin.products.products')->with('products', $products);
    }
    public function zero_products() {


        $products = DB::table('products')->where('status',1)->orderBy('product_code', 'asc')->get();
        return view('admin.products.zero_product')->with('products', $products);


    }
    public function add_products() {
        $companies = DB::table('manufacturing_companies')->get();
        $suppliers = DB::table('supplier')->get();
        $qty = DB::table('product_qty')->get();

        return view('admin.products.add_products')
                        ->with('qty', $qty)
                        ->with('suppliers', $suppliers)
                        ->with('companies', $companies);
    }

    public function stockAdjustment(Request $request) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $product_id = $request->p_id;
            $product = DB::table('products')->where('id', $product_id)->first();

//            dump($request->all());
            $remaining = $product->stock_qty - $request->adjStock;
            $updateProduct = DB::table('products')->where('id', $product_id)->update(['stock_qty' => $remaining]);
//            dd($remaining);
            DB::table('product_ledger')->insert([
                'product_id' => $product_id,
                'invoice_id' => 0,
                'total_qty' => 0,
                'purchased_qty' => 0,
                'return_purchased' => 0,
                'sale' => 0,
                'return_sale' => 0,
                'remaning' => $remaining,
                'adjust' => $request->adjStock,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            Session::flash('success', 'Product Added Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function store(Request $request) {

        DB::table('products')->insert([
            'product_name' => $request->product_name,
            'qty' => $request->qty,
            'product_code' => $request->product_code,
            'product_cost_price' => $request->product_cost_price,
            'product_sale_price' => $request->product_sale_price,
            'product_whole_sale_price' => $request->product_whole_sale_price,
            'product_description' => $request->product_description,
            'supplier_id' => $request->supplier_id,
            'stock_qty' => $request->stock_qty,
            'qty_per_cotton' => $request->qty_per_cotton,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $product_id = DB::getPdo()->lastInsertId();

        DB::table('product_ledger')->insert([
            'product_id' => $product_id,
            'invoice_id' => 0,
            'total_qty' => 0,
            'purchased_qty' => 0,
            'return_purchased' => 0,
            'sale' => 0,
            'return_sale' => 0,
            'remaning' => $request->stock_qty,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        Session::flash('success', 'Product Added Successfully');
        return redirect()->back();
    }

    public function delete_product($id) {
        DB::table('products')->delete($id);
        DB::table('product_ledger')->where('product_id', $id)->delete();

        Session::flash('success', 'Product & Product Ledger Deleted Successfully');
        return redirect()->back();
    }

    public function edit_product($id) {
        $product = DB::table('products')->where('id', $id)->first();
        $companies = DB::table('supplier')->get();

        $qty = DB::table('product_qty')->get();
        return view('admin.products.edit_product')
                        ->with('qty', $qty)
                        ->with('companies', $companies)
                        ->with('product', $product);
    }

    public function update_product(Request $request) {
        $product = DB::table('products')->where('id', $request->pro_id)->first();
        DB::table('products')->where('id', $request->pro_id)->update([
            'product_name' => $request->product_name,
            'qty' => $request->qty,
            'product_code' => $request->product_code,
            'product_cost_price' => $request->product_cost_price,
            'product_sale_price' => $request->product_sale_price,
            'product_whole_sale_price' => $request->product_whole_sale_price,
            'supplier_id' => $request->supplier_id,
            'stock_qty' => $request->stock_qty,
            'qty_per_cotton' => $request->qty_per_cotton,
            'product_description' => $request->product_description,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        Session::flash('success', 'Product Updated Successfully');
        return redirect()->route('products');
    }

    public function update_ledger($id) {
        $products = DB::table('products')
                ->select('sales.vr_no as sale_no', 'return_sales.vr_no as return_sale_no', 'supplier_purchase.vr_no as purchase_no', 'return_purchase.vr_no as return_purchase_no', 'product_ledger.id', 'product_ledger.sale_id', 'sales.company_id as customer_sale_id', 'supplier_purchase.supplier_id as supplier_purchase_id', 'return_sales.company_id as return_sale_customer_id', 'return_purchase.supplier_id as return_purchase_supplier_id', 'products.product_name', 'products.product_code', 'product_ledger.total_qty', 'product_ledger.purchased_qty', 'product_ledger.return_purchased', 'product_ledger.sale', 'product_ledger.return_sale', 'product_ledger.remaning', 'product_ledger.adjust', 'product_ledger.created_at')
                ->join('product_ledger', 'product_ledger.product_id', '=', 'products.id')
                ->leftjoin('sales', 'sales.id', '=', 'product_ledger.sale_id')
                ->leftjoin('return_sales', 'return_sales.id', '=', 'product_ledger.return_sale_id')
                ->leftjoin('supplier_purchase', 'supplier_purchase.id', '=', 'product_ledger.purchase_id')
                ->leftjoin('return_purchase', 'return_purchase.id', '=', 'product_ledger.return_purchase_id')
                ->where('product_ledger.product_id', $id)
                ->orderBy('product_ledger.id', 'desc')
                ->get();
//        dd($products);
        $suppliers = DB::table('supplier')->pluck('name', 'id');
        $customers = DB::table('company')->pluck('name', 'id');
        $productRec = DB::table('products')->where('id', $id)->first();
//        dd($suppliers[1]);
        return view('admin.products.product_ledger')
                        ->with('products', $products)
                        ->with('productRec', $productRec)
                        ->with('suppliers', $suppliers)
                        ->with('customers', $customers);
    }

    public function change_status() {
        $id = $_GET['id'];
        $status = $_GET['status'];
        $msg = ($_GET['status']) ? 'Product moved to Active Successfully.' : 'Product moved to Inactive Successfully.';
        $cheque = DB::table('products')->where('id', $id)->update(['status' => $status]);
        if ($cheque) {
            Session::flash('success', $msg);
            return redirect()->back();
        }
    }

}
