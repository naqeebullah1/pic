<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Http\Traits\UpdateLedgersTrait;

class SupplierController extends Controller {

    use UpdateLedgersTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $suppliers = DB::table('supplier');

        if (isset($_GET['status']) && ($_GET['status'] == 0 || $_GET['status'] == 1)) {
            $suppliers = $suppliers->where('status', $_GET['status']);
        }
        $suppliers = $suppliers->get();

        return view('admin.supplier.suppliers')->with('suppliers', $suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $cities = DB::table('cities')->get();
        $countries = DB::table('countries')->get();


        return view('admin.supplier.add_supplier')
                        ->with('countries', $countries)
                        ->with('cities', $cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (empty(Session::get('today_currency'))) {
            Session::flash('error', 'Please Add Today Currency Rate Today');
            return redirect()->back();
        }

        DB::table('supplier')->insert([
            'name' => $request->name,
            'cnic' => $request->cnic,
            'contact_no' => $request->contact_no,
            'address' => $request->address,
            'city' => $request->city,
            'country' => $request->country,
            'dues' => $request->dues,
            'currency' => $request->currency,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $supplier_id = DB::getPdo()->lastInsertId();

        DB::table('supplier_ledger')->insert([
            'supplier_id' => $supplier_id,
            'invoice_id' => 0,
            'purchase' => 0,
            'return_purchase' => 0,
            'payment' => 0,
            'narration' => 'Opening Balance',
            'balance' => $request->dues,
            'currency' => $request->currency,
            'currency_rate' => Session::get('today_currency'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        Session::flash('success', 'Supplier Added Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $supplier = DB::table('supplier')
                ->select('supplier_ledger.*', 'supplier_ledger.invoice_id as invoice_no', 'supplier.name', 'bank_acc.bank_name as bank_name')
                ->join('supplier_ledger', 'supplier_ledger.supplier_id', '=', 'supplier.id')
                ->leftJoin('bank_acc', 'supplier_ledger.bank_id', '=', 'bank_acc.id')
                ->where('supplier_ledger.supplier_id', $id)
                ->orderby('supplier_ledger.id','ASC')
                ->get();
        $supplierDetails = DB::table('supplier')
                ->where('id', $id)
                ->orderby('id','ASC')
                ->first();
//        dd($supplier);


        return view('admin.supplier.supplier_ledger')
                        ->with('supplierDetails', $supplierDetails)
                        ->with('supplier_ledger', $supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $supplier = DB::table('supplier')->where('id', $id)->first();
        $cities = DB::table('cities')->get();
        $countries = DB::table('countries')->get();

        return view('admin.supplier.edit_supplier')
                        ->with('supplier', $supplier)
                        ->with('countries', $countries)
                        ->with('cities', $cities);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        DB::table('supplier')->where('id', $id)->update([
            'name' => $request->name,
            'cnic' => $request->cnic,
            'contact_no' => $request->contact_no,
            'address' => $request->address,
            'city' => $request->city,
            'country' => $request->country,
            'dues' => $request->dues,
            'currency' => $request->currency,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        Session::flash('success', 'Supplier Updated Successfully');
        return redirect()->route('supplier.index', ['status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('supplier')->where('id', $id)->delete();
        DB::table('supplier_ledger')->where('supplier_id', $id)->delete();

        Session::flash('success', 'Supplier Deleted Successfully');
        return redirect()->back();
    }

    public function supplier_payment($id) {
        $supplier = DB::table('supplier')->where('id', $id)->first();
        return view('admin.supplier.supplier_payment')->with('supplier', $supplier);
    }

    public function get_payment_type(Request $request) {
        // dd('hi');
        if ($request->pay_type == 'bank') {
            $banks = DB::table('bank_acc')->where('status',1)->get();

            echo ' <div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Select Bank</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="bank_name" id="bank_name" required>';
            foreach ($banks as $bank) :
                echo '<option value="' . $bank->id . '"> ' . $bank->bank_name . ' | Acc No:' . $bank->account_no . ' | Balance: ' . $bank->current_amount . '</option>';
            endforeach;
            echo ' </select>
                </div>
            </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Pay</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="pay" id="pay_price" required>
                    </div>
                </div>';
        }

        if ($request->pay_type == 'cash') {

            $cash = DB::table('cash_in_hand')->first();

            echo ' <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Cash in Hand</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <input class="form-control" type="text" name="cash" value="' . $cash->total_amount . '" required readonly>
                        </div>
                    </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Pay</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="pay" id="pay_price" required>
                    </div>
                  </div>';
        }
        if ($request->pay_type == 'cheque') {
            $banks = DB::table('bank_acc')->get();

            echo ' <div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Transfer From</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="tranfer_to" id="transfer_to" required>
                    <option value="">Transfer cheque amount from</option>
                    <option value="bank">Bank</option>
                    <option value="cash">Cash</option>
                    </select>
                </div>
            </div>';
        }
    }

    public function save_cheque_payments() {
        try {
            DB::beginTransaction();
            $date = date('Y-m-d');
            $bankCheques = DB::table('cheques')
                    ->where('is_clear', 0)
                    ->where('release_date', '<=', $date)
                    ->where('bank_id', '!=', null)
                    ->where('supplier_id', '!=', null)
                    ->orderBy('id', 'DESC')
                    ->get();
            $cashCheques = DB::table('cheques')
                    ->where('is_clear', 0)
                    ->where('release_date', '<=', $date)
                    ->where('supplier_id', '!=', null)
                    ->where('bank_id', '=', null)
                    ->orderBy('id', 'DESC')
                    ->get();
            if ($bankCheques->count()) {
                foreach ($bankCheques as $bankCheque) {

                    $bank = DB::table('bank_acc')->where('id', $bankCheque->bank_id)->first();


                    $cut_bal = $bank->current_amount - $bankCheque->amount;

                    DB::table('bank_acc')->where('id', $bankCheque->bank_id)->update([
                        'current_amount' => $cut_bal
                    ]);

                    DB::table('bank_ledger')->insert([
                        'bank_id' => $bankCheque->bank_id,
                        'cheque_id' => $bankCheque->id,
                        'total_amount' => $cut_bal,
                        'deposit' => 0,
                        'withdrawal' => $bankCheque->amount,
                        'remaining' => $cut_bal,
                        'description' => '',
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s'),
                    ]);

                    DB::table('journal_vocher')->insert([
                        'u_id' => $bankCheque->supplier_id,
                        'cheque_id' => $bankCheque->id,
                        'head' => "supplier",
                        'sub_head' => "supplier",
                        'total' => $bank->current_amount,
                        'pay' => $bankCheque->amount,
                        'balance' => $cut_bal,
                        'description' => '',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);

                    $supplier = DB::table('supplier')->where('id', $bankCheque->supplier_id)->first();

                    $supplier_bal = $supplier->dues - $bankCheque->amount;
                    DB::table('supplier')->where('id', $bankCheque->supplier_id)->update([
                        'dues' => $supplier_bal,
                        'last_payment' => date('Y-m-d')
                    ]);

                    DB::table('supplier_ledger')->insert([
                        'supplier_id' => $bankCheque->supplier_id,
                        'cheque_id' => $bankCheque->id,
                        'invoice_id' => 0,
                        'narration' => '',
                        'purchase' => 0,
                        'return_purchase' => 0,
                        'payment' => $bankCheque->amount,
                        'balance' => $supplier_bal,
                        'currency' => $supplier->currency,
                        'currency_rate' => '100',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    DB::table('cheques')->where('id', $bankCheque->id)->update([
                        'is_clear' => 1
                    ]);
                }
            }
            if ($cashCheques->count()) {

                foreach ($cashCheques as $bankCheque) {
                    $cash = DB::table('cash_in_hand')->first();

                    $cut_bal = $cash->total_amount - $bankCheque->amount;
                    DB::table('cash_in_hand')->update([
                        'total_amount' => $cut_bal
                    ]);

                    DB::table('cash_in_hand_ledger')->insert([
                        'cash_in_hand_id' => 1,
                        'cheque_id' => $bankCheque->id,
                        'total_amount' => $cut_bal,
                        'deposit' => 0,
                        'withdrawal' => $bankCheque->amount,
                        'remaining' => $cut_bal,
                        'description' => '',
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s'),
                    ]);

                    DB::table('journal_vocher')->insert([
                        'u_id' => $bankCheque->supplier_id,
                        'cheque_id' => $bankCheque->id,
                        'head' => "supplier",
                        'sub_head' => "supplier",
                        'total' => $cash->total_amount,
                        'pay' => $bankCheque->amount,
                        'balance' => $cut_bal,
                        'description' => '',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);

                    $supplier = DB::table('supplier')->where('id', $bankCheque->supplier_id)->first();

                    // if ($request->currency == 'usd') {
                    //     $dollar = $request->pay * Session::get('today_currency');
                    //     $supplier_bal = $supplier->dues - $dollar;
                    // } else {
                    $supplier_bal = $supplier->dues - $bankCheque->amount;
                    // }

                    DB::table('supplier')->where('id', $bankCheque->supplier_id)->update([
                        'dues' => $supplier_bal,
                        'last_payment' => date('Y-m-d')
                    ]);

                    DB::table('supplier_ledger')->insert([
                        'supplier_id' => $bankCheque->supplier_id,
                        'invoice_id' => 0,
                        'cheque_id' => $bankCheque->id,
                        'narration' => '',
                        'purchase' => 0,
                        'return_purchase' => 0,
                        'payment' => $bankCheque->amount,
                        'balance' => $supplier_bal,
                        'currency' => $supplier->currency,
                        'currency_rate' => '100',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    DB::table('cheques')->where('id', $bankCheque->id)->update([
                        'is_clear' => 1
                    ]);
                }
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function supplier_cheques() {
        $date = date('Y-m-d');
        $cheques = DB::table('cheques')
                ->join('supplier', 'supplier.id', '=', 'cheques.supplier_id')
                ->where('supplier_id', '!=', null)
                ->get();
        $unclearcheques = DB::table('cheques')
                ->select('cheques.*', 'supplier.name')
                ->join('supplier', 'supplier.id', '=', 'cheques.supplier_id')
                ->where('is_clear', 0)
                ->where('release_date', '<=', $date)
                ->where('supplier_id', '!=', null)
                ->orderBy('cheques.id', 'DESC')
                ->get();
        return view('admin/supplier/supplier_cheques')
                        ->with('unclearcheques', $unclearcheques)
                        ->with('cheques', $cheques);
    }

    public function suppliercheque_cleared() {
        $status = $_GET['status'];
//        dd($status);
        $id = $_GET['id'];
        $bankCheque = DB::table('cheques')
                ->where('bank_id', '!=', null)
                ->where('id', '=', $id)
                ->first();
//        dd($bankCheque);
        $cashCheque = DB::table('cheques')
                ->where('bank_id', '=', null)
                ->where('id', '=', $id)
                ->first();
//        dd($cashCheques);
        if ($status == 'notclear') {
            $c = DB::table('cheques')->where('id', $id)->update([
                'is_clear' => 2
            ]);
            if ($c) {

                echo json_encode(['flag' => 1, 'message' => 'Cheque Uncleared']);
            } else {
                echo json_encode(['flag' => 0, 'message' => 'Not res']);
            }
            exit;
        }
//        exit;

        $date = date('Y-m-d');
        $bankCheque = DB::table('cheques')
                ->where('bank_id', '!=', null)
                ->where('id', '=', $id)
                ->where('is_clear', '=', 0)
                ->first();
        $cashCheque = DB::table('cheques')
                ->where('bank_id', '=', null)
                ->where('id', '=', $id)
                ->where('is_clear', '=', 0)
                ->first();
//        dd($cashCheque);
        if ($bankCheque) {
//            foreach ($bankCheques as $bankCheque) {

            $bank = DB::table('bank_acc')->where('id', $bankCheque->bank_id)->first();


            $cut_bal = $bank->current_amount - $bankCheque->amount;

            DB::table('bank_acc')->where('id', $bankCheque->bank_id)->update([
                'current_amount' => $cut_bal
            ]);

            DB::table('bank_ledger')->insert([
                'bank_id' => $bankCheque->bank_id,
                'cheque_id' => $bankCheque->id,
                'total_amount' => $cut_bal,
                'deposit' => 0,
                'withdrawal' => $bankCheque->amount,
                'remaining' => $cut_bal,
                'description' => '',
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);

            DB::table('journal_vocher')->insert([
                'u_id' => $bankCheque->supplier_id,
                'cheque_id' => $bankCheque->id,
                'head' => "supplier",
                'sub_head' => "supplier",
                'total' => $bank->current_amount,
                'pay' => $bankCheque->amount,
                'balance' => $cut_bal,
                'description' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $supplier = DB::table('supplier')->where('id', $bankCheque->supplier_id)->first();

            $supplier_bal = $supplier->dues - $bankCheque->amount;
            DB::table('supplier')->where('id', $bankCheque->supplier_id)->update([
                'dues' => $supplier_bal,
                'last_payment' => date('Y-m-d')
            ]);

            DB::table('supplier_ledger')->insert([
                'supplier_id' => $bankCheque->supplier_id,
                'cheque_id' => $bankCheque->id,
                'invoice_id' => 0,
                'narration' => '',
                'purchase' => 0,
                'return_purchase' => 0,
                'payment' => $bankCheque->amount,
                'balance' => $supplier_bal,
                'currency' => $supplier->currency,
                'currency_rate' => '100',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            DB::table('cheques')->where('id', $bankCheque->id)->update([
                'is_clear' => 1
            ]);
//            }
            echo json_encode(['flag' => 1, 'message' => 'Cheque Cleared']);
        }
        if ($cashCheque) {

//            foreach ($cashCheques as $bankCheque) {
            $bankCheque = $cashCheque;
            $cash = DB::table('cash_in_hand')->first();

            $cut_bal = $cash->total_amount - $bankCheque->amount;
            DB::table('cash_in_hand')->update([
                'total_amount' => $cut_bal
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'cheque_id' => $bankCheque->id,
                'total_amount' => $cut_bal,
                'deposit' => 0,
                'withdrawal' => $bankCheque->amount,
                'remaining' => $cut_bal,
                'description' => '',
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);

            DB::table('journal_vocher')->insert([
                'u_id' => $bankCheque->supplier_id,
                'cheque_id' => $bankCheque->id,
                'head' => "supplier",
                'sub_head' => "supplier",
                'total' => $cash->total_amount,
                'pay' => $bankCheque->amount,
                'balance' => $cut_bal,
                'description' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $supplier = DB::table('supplier')->where('id', $bankCheque->supplier_id)->first();

            // if ($request->currency == 'usd') {
            //     $dollar = $request->pay * Session::get('today_currency');
            //     $supplier_bal = $supplier->dues - $dollar;
            // } else {
            $supplier_bal = $supplier->dues - $bankCheque->amount;
            // }

            DB::table('supplier')->where('id', $bankCheque->supplier_id)->update([
                'dues' => $supplier_bal,
                'last_payment' => date('Y-m-d')
            ]);

            DB::table('supplier_ledger')->insert([
                'supplier_id' => $bankCheque->supplier_id,
                'invoice_id' => 0,
                'cheque_id' => $bankCheque->id,
                'narration' => '',
                'purchase' => 0,
                'return_purchase' => 0,
                'payment' => $bankCheque->amount,
                'balance' => $supplier_bal,
                'currency' => $supplier->currency,
                'currency_rate' => '100',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            DB::table('cheques')->where('id', $bankCheque->id)->update([
                'is_clear' => 1
            ]);
//            }
            echo json_encode(['flag' => 1, 'message' => 'Cheque Cleared']);
        }
    }

    public function get_cheque_fields(Request $request) {
        if ($request->pay_type == 'bank') {
            $banks = DB::table('bank_acc')->get();

            echo ' <div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Select Bank</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="bank_name" id="bank_name" required>';
            foreach ($banks as $bank) :
                echo '<option value="' . $bank->id . '"> ' . $bank->bank_name . ' | Acc No:' . $bank->account_no . ' | Balance: ' . $bank->current_amount . '</option>';
            endforeach;
            echo ' </select>
                </div>
            </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Cheque Number</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="cheque_no" required>
                    </div>
                </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Amount</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="pay" required>
                    </div>
                </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">cheque Date</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="date" name="cheque_date" required>
                    </div>
                </div>';
        }

        if ($request->pay_type == 'cash') {

            $cash = DB::table('cash_in_hand')->first();

            echo ' <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Cash in Hand</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <input class="form-control" type="text" name="cash" value="' . $cash->total_amount . '" required readonly>
                        </div>
                    </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Cheque Number</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="cheque_no" required>
                    </div>
                </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Amount</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="pay" required>
                    </div>
                </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">cheque Date</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="date" name="cheque_date" required>
                    </div>
                </div>';
        }
    }

    public function dues_payment(Request $request) {
        try {
            DB::beginTransaction();

            $time = date('h:i:s');
            $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));

            $new_amount = $request->pay;
            if ($request->dtl != '' || $request->dtl != 0) {
                $new_amount = $request->dtl + $request->pay;
            }
//        dd($new_amount);
            if ($request->pay_from == 'bank') {

                $supplier = DB::table('supplier')->where('id', $request->supplier_id)->first();

                $supplier_bal = $supplier->dues - $new_amount;
                DB::table('supplier')->where('id', $request->supplier_id)->update([
                    'dues' => $supplier_bal,
                    'last_payment' => date('Y-m-d')
                ]);

                DB::table('supplier_ledger')->insert([
                    'supplier_id' => $request->supplier_id,
                    'bank_id' => $request->bank_name,
                    'invoice_id' => 0,
                    'narration' => $request->description,
                    'purchase' => 0,
                    'return_purchase' => 0,
                    'payment' => $request->pay,
                    'dtl' => $request->dtl,
                    'balance' => $supplier_bal,
                    'currency' => $supplier->currency,
                    'currency_rate' => Session::get('today_currency'),
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
                $last_id = DB::getPdo()->lastInsertId();
//dd($last_id);
                $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();
                $cut_bal = $bank->current_amount - $request->pay;
                DB::table('bank_acc')->where('id', $request->bank_name)->update([
                    'current_amount' => $cut_bal
                ]);

                DB::table('bank_ledger')->insert([
                    'bank_id' => $request->bank_name,
                    'supplier_id' => $request->supplier_id,
                    'supplier_ledger_id' => $last_id,
                    'total_amount' => $cut_bal,
                    'deposit' => 0,
                    'withdrawal' => $request->pay,
                    'remaining' => $cut_bal,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

                DB::table('journal_vocher')->insert([
                    'u_id' => $request->supplier_id,
                    'head' => "supplier",
                    'sub_head' => "supplier",
                    'total' => $bank->current_amount,
                    'pay' => $request->pay,
                    'balance' => $cut_bal,
                    'description' => $request->description,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
            }
            if ($request->pay_from == 'cash') {
                $supplier = DB::table('supplier')->where('id', $request->supplier_id)->first();

                $supplier_bal = $supplier->dues - $new_amount;

                DB::table('supplier')->where('id', $request->supplier_id)->update([
                    'dues' => $supplier_bal,
                    'last_payment' => date('Y-m-d')
                ]);

                DB::table('supplier_ledger')->insert([
                    'supplier_id' => $request->supplier_id,
                    'invoice_id' => 0,
                    'narration' => $request->description,
                    'purchase' => 0,
                    'return_purchase' => 0,
                    'payment' => $request->pay,
                    'dtl' => $request->dtl,
                    'balance' => $supplier_bal,
                    'currency' => $supplier->currency,
                    'currency_rate' => Session::get('today_currency'),
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
                $last_id = DB::getPdo()->lastInsertId();

                $cash = DB::table('cash_in_hand')->first();

                $cut_bal = $cash->total_amount - $request->pay;
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cut_bal
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'supplier_id' => $request->supplier_id,
                    'supplier_ledger_id' => $last_id,
                    'total_amount' => $cut_bal,
                    'deposit' => 0,
                    'withdrawal' => $request->pay,
                    'remaining' => $cut_bal,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

                DB::table('journal_vocher')->insert([
                    'u_id' => $request->supplier_id,
                    'head' => "supplier",
                    'sub_head' => "supplier",
                    'total' => $cash->total_amount,
                    'pay' => $request->pay,
                    'balance' => $cut_bal,
                    'description' => $request->description,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
            }
            if ($request->pay_from == 'cheque') {
                $arrayToSave = array();
                if ($request->tranfer_to == 'bank') {
                    $arrayToSave['bank_id'] = $request->bank_name;
                    $arrayToSave['transfer_to'] = 'bank';
                }
                if ($request->tranfer_to == 'cash') {
                    //cash logic goes here
                    $arrayToSave['transfer_to'] = 'cash';
                }
                $arrayToSave['supplier_id'] = $request->supplier_id;
                $arrayToSave['cheque_no'] = $request->cheque_no;
                $arrayToSave['amount'] = $request->pay;
                $arrayToSave['release_date'] = $request->cheque_date;
                $arrayToSave['created_at'] = $created_at;
                $arrayToSave['updated_at'] = $created_at;
//            dd($arrayToSave);
                DB::table('cheques')->insert($arrayToSave);
            }

            DB::commit();
            Session::flash('success', 'Payment Transaction Completed Successfully');
            return redirect()->route('supplier.index', ['status' => 1]);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function delete_payment($id) {
        try {
            DB::beginTransaction();
            $supplier_ledger = DB::table('supplier_ledger')->where('id', $id)->first();
            if ($supplier_ledger->bank_id != 0) {
                $bank_ledger = DB::table('bank_ledger')->where('supplier_ledger_id', $id)->first();
                if (!$bank_ledger) {
                    Session::flash('error', 'This transaction can not be deleted');
                    return redirect()->back();
                }
                $bank = DB::table('bank_acc')->where('id', $supplier_ledger->bank_id)->first();
                $cut_bal = $bank->current_amount + $bank_ledger->withdrawal;
                //update bank account
                DB::table('bank_acc')->where('id', $supplier_ledger->bank_id)->update([
                    'current_amount' => $cut_bal
                ]);
//            delete bank ledger

                $bLedger = DB::table('bank_ledger')->where('supplier_ledger_id', $id);
                $this->updateBankLedger($bLedger);

                $supplier = DB::table('supplier')->where('id', $supplier_ledger->supplier_id)->first();
                $supplier_bal = $supplier->dues + $supplier_ledger->payment + $supplier_ledger->dtl;
                DB::table('supplier')->where('id', $supplier_ledger->supplier_id)->update([
                    'dues' => $supplier_bal
                ]);
                $supplier_ledger = DB::table('supplier_ledger')->where('id', $id);
                $this->updateSupplierLedger($supplier_ledger);
            } else {
                $bank_ledger = DB::table('cash_in_hand_ledger')->where('supplier_ledger_id', $id)->first();
                if (!$bank_ledger) {
                    Session::flash('error', 'This transaction can not be deleted');
                    return redirect()->back();
                }

                $bank = DB::table('cash_in_hand')->where('id', 1)->first();
                $cut_bal = $bank->total_amount + $bank_ledger->withdrawal;
                //update bank account
                DB::table('cash_in_hand')->where('id', 1)->update([
                    'total_amount' => $cut_bal
                ]);

                $cashRecord = DB::table('cash_in_hand_ledger')->where('supplier_ledger_id', $id);
                $this->updateCashLedger($cashRecord);

//            delete Journal voucher
//update suppliers ledger
                $supplier = DB::table('supplier')->where('id', $supplier_ledger->supplier_id)->first();
                $supplier_bal = $supplier->dues + $supplier_ledger->payment + $supplier_ledger->dtl;
                DB::table('supplier')->where('id', $supplier_ledger->supplier_id)->update([
                    'dues' => $supplier_bal
                ]);
                $supplier_ledger = DB::table('supplier_ledger')->where('id', $id);
                $this->updateSupplierLedger($supplier_ledger);
            }

            DB::commit();
            Session::flash('success', 'Sale Transaction reverted Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function customer_cheque_delete($id) {

        $cheque = DB::table('cheques')->where('id', $id)->delete();

        if ($cheque) {
            Session::flash('success', 'Cheque Delete Successfully');
            return redirect()->back();
        }
    }

    public function cheque_delete($id) {
        dd($id);
    }

    public function change_status() {
        $id = $_GET['id'];
        $status = $_GET['status'];
        $msg = ($_GET['status']) ? 'Company moved to Active Successfully.' : 'Company moved to Inactive Successfully.';
        $cheque = DB::table('supplier')->where('id', $id)->update(['status' => $status]);

        if ($cheque) {
            Session::flash('success', $msg);
            return redirect()->back();
        }
    }

    public function update_ledgers($id) {

        try {
            DB::beginTransaction();
            $company = DB::table('supplier_ledger')
                    ->where('supplier_id', $id)
                    ->orderBy('id', 'asc')
                    ->first();
            $remainingAmount = $this->supplierLedgerNextRecords($company->balance, $company->supplier_id, $company->id);
            DB::table('supplier')->where('id', $id)->update([
                'dues' => $remainingAmount
            ]);
            DB::commit();
            Session::flash('success', 'Supplier Ledger Updated Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }


        return redirect()->back();
    }

}
