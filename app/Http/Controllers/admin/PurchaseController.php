<?php

namespace App\Http\Controllers\admin;

use DB;
use Session;
use App\Purchase;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\UpdateLedgersTrait;

class PurchaseController extends Controller {

    use UpdateLedgersTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        try {
            DB::beginTransaction();
            $time = date('h:i:s');
            $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));

            if (!$request->product_id || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }

            DB::table('supplier_purchase')->insert([
                'vr_no' => $request->vr_no,
                'supplier_id' => $request->supplier_id,
                'total_amount' => $request->line_cost_total,
                'discount' => $request->discount,
                'net_amount' => $request->net_amount,
                'paid' => $request->paidamount,
                'balance' => $request->due_amount,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $last_id = DB::getPdo()->lastInsertId();
            if ($request->paidamount > 0) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount - $request->paidamount,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'invoice_id' => $last_id,
                    'supplier_id' => $request->supplier_id,
                    'total_amount' => $cash->total_amount - $request->paidamount,
                    'deposit' => 0,
                    'withdrawal' => $request->paidamount,
                    'remaining' => $cash->total_amount - $request->paidamount,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
            }
            $product_id = $request->product_id;
            $unit_price = $request->unit_price;
            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $discount = $request->discount_per_item;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);

            for ($i = $count - 1; $i >= 0; $i--) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();
                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'discount' => $discount[$i],
                    'unit_price' => $unit_price[$i],
                    'actual_amount' => $unit_price[$i] * $qty[$i],
                    'total_price' => $line_cost[$i],
                    'purchase_id' => $last_id,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                );

                $data1 = array(
                    'product_id' => $product_id[$i],
                    'invoice_id' => $last_id,
                    'purchase_id' => $last_id,
                    'description' => $request->description,
                    'total_qty' => $get_record->stock_qty,
                    'purchased_qty' => $qty[$i],
                    'return_purchased' => 0,
                    'sale' => 0,
                    'return_sale' => 0,
                    'remaning' => $get_record->stock_qty + $qty[$i],
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                );

                $insert_data[] = $data;
                $insert_data1[] = $data1;


                $update_qty = $get_record->stock_qty + $qty[$i];
                $sale_details[] = array('product' => $get_record->product_name, 'qty' => $qty[$i], 'unit_price' => $unit_price[$i], 'cotton' => round($qty[$i] / $get_record->qty_per_cotton, 1));

                DB::table('products')->where('id', $product_id[$i])->update([
                    'product_cost_price' => $unit_price[$i],
                    'stock_qty' => $update_qty
                ]);
            }
            // dd($sale_details);
//        dd($request->all());
            $sale_details = \json_encode($sale_details);

            DB::table('supplier_purchase_detail')->insert($insert_data);
            DB::table('product_ledger')->insert($insert_data1);
            // dd([$insert_data, $insert_data1]);

            DB::table('supplier')->where('id', $request->supplier_id)->update([
                'dues' => $request->due_amount + $request->dues
            ]);

            DB::table('supplier_ledger')->insert([
                'supplier_id' => $request->supplier_id,
                'invoice_id' => $last_id,
                'purchase_details' => $sale_details,
                'narration' => $request->description,
                'purchase' => $request->net_amount,
                'return_purchase' => 0,
                'payment' => $request->paidamount,
                'balance' => $request->due_amount + $request->dues,
                'currency' => 'pkr',
                'currency_rate' => Session::get('today_currency'),
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);
//            dd($request->all());
            DB::commit();
            Session::flash('success', 'Purchased Transaction Completed Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function fetch_history() {
        $id = $_GET['invoiceNo'];
        $purchase_ledger = DB::table('supplier_purchase')
                ->select('products.product_name', 'products.product_code', 'supplier_purchase.total_amount', 'supplier_purchase.discount', 'supplier_purchase.net_amount', 'supplier_purchase.paid', 'supplier_purchase_detail.unit_price', 'supplier_purchase_detail.qty', 'supplier_purchase_detail.created_at')
                ->join('supplier_purchase_detail', 'supplier_purchase_detail.purchase_id', '=', 'supplier_purchase.id')
                ->join('products', 'supplier_purchase_detail.product_id', '=', 'products.id')
                ->where('supplier_purchase_detail.purchase_id', $id)
                ->get();
//        dd($purchase_ledger);
        $html = ' <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>P-code</th>
                                    <th>P-Name</th>
                                    <th>Cost Price</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach ($purchase_ledger as $ledger):

            $html .= '<tr>
                                    <td>' . $ledger->product_code . '</td>
                                    <td>' . $ledger->product_name . '</td>
                                    <td>' . $ledger->unit_price . '</td>
                                    <td>' . $ledger->qty . '</td>
                                    </tr>';
        endforeach;
        $html .= ' </tbody></table>';
        echo $html;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $supplier = DB::table('supplier')->where('id', $id)->first();
        $products = DB::table('products')->select('id', 'qty', 'product_name')
                ->get();
//dd($products);
        return view('admin.supplier.supplier_purchase')
                        ->with('supplier', $supplier)
                        ->with('products', $products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $supplier_purchases = DB::table('supplier_purchase')
                ->select('supplier_purchase.id', 'supplier_purchase.total_amount', 'supplier_purchase.discount', 'supplier_purchase.net_amount', 'supplier_purchase.paid', 'supplier_purchase.created_at', 'supplier.name')
                ->join('supplier', 'supplier.id', '=', 'supplier_purchase.supplier_id')
                ->where('supplier_purchase.supplier_id', $id)
                ->get();
        return view('admin.supplier.purchase_ledger')
                        ->with('supplier_id', $id)
                        ->with('supplier_purchases', $supplier_purchases);
    }
    public function editPurchase($id) {

        $getSaleDetails = Purchase::with('PurchaseDetails', 'PurchaseDetails.Product')->where('id', $id)->first();

        $supplierId = $getSaleDetails->supplier_id;
        $supplier = DB::table('supplier')->where('id', $supplierId)
        ->first();

        $products = DB::table('products')->select('id', 'qty', 'product_name')->get();
       
        $ledger = DB::table('supplier_ledger')->where('invoice_id', $id)
        ->where('purchase','>',0)
        ->first();
       
        $date=\Carbon\Carbon::parse($getSaleDetails->created_at)->format('y-m-d');

        return view('admin.companies.edit_purchase')
            ->with('supplier', $supplier)

            ->with('getSaleDetails', $getSaleDetails)
            ->with('ledger', $ledger)
            ->with('date', $date)
            ->with('products', $products);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        try {

            DB::beginTransaction();
            if ($request->product_id == null || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }
            $oldsalesData = DB::table('supplier_purchase')->where('id', $id)->first();


            //start product updation
            $oldsalesDetails = DB::table('supplier_purchase_detail')->where('purchase_id', $id)->select('product_id')->get();
            foreach ($oldsalesDetails as $sdetail) {
                $get_data =   DB::table('product_ledger')->where('product_id', $sdetail->product_id)->where('purchase_id', $id)->first();
            //dd($get_data);

                $product_qty = DB::table('products')->where('id', $sdetail->product_id)->first();
                $qty = $product_qty->stock_qty - $get_data->purchased_qty;
                DB::table('products')
                    ->where('id', $sdetail->product_id)
                    ->update(['stock_qty' => $qty]);
                $remning_qty = $get_data->remaning - $get_data->purchased_qty;
                //  dd($remning_qty);
                DB::table('product_ledger')
                    ->where('id', $get_data->id)
                    ->update(['purchased_qty' => '0', 'remaning' => $remning_qty]);
                $get_max_data =   DB::table('product_ledger')->where('product_id', $sdetail->product_id)->where('id', '>', $get_data->id)->get();

                foreach ($get_max_data as $more_details) {
                    $totalqty = $more_details->total_qty - $get_data->purchased_qty;
                    $totalremaining = $more_details->remaning - $get_data->purchased_qty;
                    DB::table('product_ledger')
                        ->where('id', $more_details->id)
                        ->update(['total_qty' => $totalqty, 'remaning' => $totalremaining]);
                }

            }

            //end old product Updation
            //end foreach

            //customer old Entry Update
            $get_customer_data =   DB::table('supplier_ledger')->where('supplier_id', $oldsalesData->supplier_id)->where('invoice_id', $id)->where('purchase', '>', 0)->first();
            $remaining = ($get_customer_data->balance + $get_customer_data->payment) - $get_customer_data->purchase;

            $oldbalance = DB::table('supplier')->where('id', $oldsalesData->supplier_id)->first();
            $customer_balance = $oldbalance->dues - $get_customer_data->purchase;

            DB::table('supplier')->where('id', $oldsalesData->supplier_id)->update([
                'dues' => $customer_balance
            ]);


            $resu =  DB::table('supplier_ledger')
                ->where('id', $get_customer_data->id)
                ->where('supplier_id', $oldsalesData->supplier_id)
                ->update([
                    'purchase' => '0',
                    'purchase_details' => '',
                    'balance' => $remaining
                ]);

            $get_customer_data_max =   DB::table('supplier_ledger')->where('supplier_id', $oldsalesData->supplier_id)->where('id', '>', $get_customer_data->id)->get();
            foreach ($get_customer_data_max as $more_details_customer) {
                $remaining = ($more_details_customer->balance + $get_customer_data->payment) - $get_customer_data->purchase;
                DB::table('supplier_ledger')
                    ->where('id', $more_details_customer->id)
                    ->where('supplier_id', $oldsalesData->supplier_id)
                    ->update([
                        'balance' => $remaining
                    ]);
            }
            // end customer old data update

            //start cash ledger Updation
            $get_cash_in_hand_data =   DB::table('cash_in_hand_ledger')->where('supplier_id', $oldsalesData->supplier_id)->where('invoice_id', $id)->first();
            //dd($get_cash_in_hand_data);
            if($get_cash_in_hand_data!=null){
            $cash = DB::table('cash_in_hand')->first();
            DB::table('cash_in_hand')->update([
                'total_amount' => $cash->total_amount + $oldsalesData->paid,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            try {
                $cash_remaining = ($get_cash_in_hand_data->remaining - $oldsalesData->paid);

                DB::table('cash_in_hand_ledger')
                    ->where('id', $get_cash_in_hand_data->id)
                    ->where('supplier_id', $oldsalesData->supplier_id)
                    ->update(['withdrawal' => '0', 'remaining' => $cash_remaining]);
                //dd('hi');
                $get_cash_in_hand_data_max =   DB::table('cash_in_hand_ledger')->where('id', '>', $get_cash_in_hand_data->id)->get();
                foreach ($get_cash_in_hand_data_max as $more_details_cash_in_hand) {
                    $remaining_cash = ($more_details_cash_in_hand->remaining + $oldsalesData->paid);
                    $total_cash = ($more_details_cash_in_hand->total_amount+ $oldsalesData->paid);

                    DB::table('cash_in_hand_ledger')
                        ->where('id', $more_details_cash_in_hand->id)
                        ->where('supplier_id', $oldsalesData->supplier_id)
                        ->update(['remaining' => $remaining_cash,  'total_amount' => $total_cash]);
                }
            } catch (Exception $ex) {
            }
        }
            //delete sales details
            DB::table('supplier_purchase_detail')
                ->where('purchase_id', $oldsalesData->id)
                ->delete();

            //end sales details
            //end cash ledger Updation

            //New Updation Start from Here

            //****New sales updation */
            DB::table('supplier_purchase')
                ->where('id', $oldsalesData->id)
                ->update([
                    'vr_no' => $request->vr_no,

                    'total_amount' => $request->line_cost_total,
                    'discount' => $request->discount,
                    'net_amount' => $request->net_amount,
                    'paid' => $request->paidamount,
                   'balance'=>$request->net_amount-$request->paidamount,


                    'updated_at' => date('y-m-d H:i:s'),
                ]);


            //****New sales Cash updation */
            if ($request->paidamount > 0) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount - $request->paidamount,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                $get_cash_in_hand_data =   DB::table('cash_in_hand_ledger')->where('purchase_id', $oldsalesData->supplier_id)->where('invoice_id', $id)->first();
                $cash_remaining = ($get_cash_in_hand_data->remaining - $oldsalesData->paid);
                DB::table('cash_in_hand_ledger')
                    ->where('id', $get_cash_in_hand_data->id)
                    ->where('supplier_id', $oldsalesData->supplier_id)
                    ->update([
                        'cash_in_hand_id' => 1,

                        'total_amount' => $cash->total_amount + $request->paidamount,
                        'deposit' => 0,
                        'withdrawal' => $request->paidamount,
                        'remaining' => $cash->total_amount - $request->paidamount,
                        'description' => $request->description,
                        'updated_at' => date('y-m-d H:i:s')
                    ]);

                $get_cash_in_hand_data_max =   DB::table('cash_in_hand_ledger')->where('purchase_id', $oldsalesData->supplier_id)->where('id', '>', $get_cash_in_hand_data->id)->get();
                foreach ($get_cash_in_hand_data_max as $more_details_cash_in_hand) {
                    $remaining_cash = ($more_details_cash_in_hand->remaining - $request->paidamount);
                    $total_cash = ($more_details_cash_in_hand->total_amount - $request->paidamount);

                    DB::table('cash_in_hand_ledger')
                        ->where('id', $more_details_cash_in_hand->id)
                        ->where('supplier_id', $oldsalesData->supplier_id)
                        ->update(['remaining' => $remaining_cash,  'total_amount' => $total_cash]);
                }
            }

            $customer = DB::table('supplier')->where('id', $oldsalesData->supplier_id)->first();

            $dues_remaining = $request->due_amount + $customer->dues;


            DB::table('supplier')->where('id', $oldsalesData->supplier_id)->update([
                'dues' => $dues_remaining
            ]);

                // kindly cheack record from here
            //*********Naqeeb Code******* */
            $product_id = $request->product_id;


                $request->unit_price = $request->retail_price;

            $unit_price = $request->unit_price;

            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $discount = $request->discount_per_item;
            // $whole = $request->whole_price;
            // $retail = $request->retail_price;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);
            for ($i = $count - 1; $i >= 0; $i--) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();
                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'discount' => $discount[$i],

                    'unit_price' => $unit_price[$i],
                    'total_price' => $line_cost[$i],
                    'purchase_id' => $oldsalesData->id,


                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                );

                $insert_data[] = $data;

                //            dd($get_record);
                $sale_details1[] = array('product' => $get_record->product_name, 'qty' => $qty[$i], 'unit_price' => round($line_cost[$i] / $qty[$i], 2), 'cotton' => round($qty[$i] / $get_record->qty_per_cotton, 1));
                $update_qty = $get_record->stock_qty + $qty[$i];

                $product_record = $product_record->update([
                    'stock_qty' => $update_qty,
                    //                'product_whole_sale_price' => $whole[$i],
                    //                'product_sale_price' => $retail[$i]
                    //                'stock_qty' => $update_qty
                ]);

                $checking_data_in_table = DB::table('product_ledger')->where('purchase_id', $oldsalesData->id)->where('product_id', $product_id[$i])->count();
                //dd($checking_data_in_table);
                if ($checking_data_in_table > 0) {
                    $cheaking_record = DB::table('product_ledger')
                        ->where('purchase_id', $oldsalesData->id)->where('product_id', $product_id[$i])->first();

                    $totalqty = $cheaking_record->remaning + $qty[$i];
                    DB::table('product_ledger')
                        ->where('purchase_id', $oldsalesData->id)->where('product_id', $product_id[$i])
                        ->update([
                            'purchased_qty' => $qty[$i],
                            'remaning' => $totalqty,
                            'updated_at' => date('y-m-d H:i:s')
                        ]);





                    $cheaking_max_record = DB::table('product_ledger')
                        ->where('id', '>', $cheaking_record->id)->get();
                    foreach ($cheaking_max_record as $record_Detail) {

                        $data_max = DB::table('product_ledger')
                            ->where('id', $get_record->id)->first();;
                        if ($data_max) {

                            $totalqty1 = $data_max->remaning + $qty[$i];
                            $totalqty2 = $data_max->total_qty + $qty[$i];

                            DB::table('product_ledger')
                                ->where('id', $get_record->id)
                                ->update([
                                    'total_qty' => $totalqty2,
                                    'remaning' => $totalqty1,
                                    'updated_at' => date('y-m-d H:i:s')
                                ]);
                        }
                    }
                } else {
                    DB::table('product_ledger')->insert([
                        'product_id' => $product_id[$i],
                        'invoice_id' => $oldsalesData->id,
                        'purchase_id' => $oldsalesData->id,
                        'description' => $request->description,
                        'total_qty' => $get_record->stock_qty,
                        'purchased_qty' => $qty[$i],
                        'return_purchased' => 0,
                        'sale' =>0,
                        'return_sale' => 0,
                        'remaning' => $get_record->stock_qty + $qty[$i],
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s'),
                    ]);


                }
            }


            //        dd($insert_data);

            DB::table('supplier_purchase_detail')->insert($insert_data);
            //  $customer = DB::table('company')->where('id', $oldsalesData->supplier_id)->first();
            // $dues_remaining = $request->due_amount + $customer->dues;
            // DB::table('company')->where('id', $oldsalesData->supplier_id)->update([
            //     'dues' => $dues_remaining
            // ]);
            DB::table('supplier_ledger')->where('supplier_id', $oldsalesData->supplier_id)->where('invoice_id', $oldsalesData->id)
                ->update([

                    'narration' => $request->description,
                    'purchase' => $request->net_amount,
                    'return_purchase' => 0,
                    'payment' => $request->paidamount,
                    'balance' => $dues_remaining,
                    'purchase_details' => json_encode($sale_details1),
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

            //New Updation Ends here

            // $this->deleteSale($id);

            Session::flash('success', 'Sale Transaction Updated Successfully');
            DB::commit();
            if (isset($request->print)) {
                return redirect()->route('sale/print_invoice', ['id' => $oldsalesData->id]);
            } else {
                return redirect()->route('supplierPurchase.edit', $oldsalesData->supplier_id);
            }
        } catch (QueryException $e) {
            DB::rollback();
        }







    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function get_product_details(Request $request) {
        if (isset($request->saleman) && !empty($request->saleman)) {
            if ($request->saleman != 0) {
                $product_details = DB::table('area_person_sales')
                        ->select('products.*', 'area_person_sales.qty as saleman_stock')
                        ->join('products', 'products.id', '=', 'area_person_sales.product_id')
                        ->where('area_person_sales.product_id', $request->id)
                        ->where('area_person_sales.sale_man_id', $request->saleman)
                        ->first();
                return response()->json($product_details);
            }
        }
        $product_details = DB::table('products')
                ->select('id', 'product_name', 'product_cost_price', 'product_whole_sale_price', 'product_sale_price', 'stock_qty')
                ->where('id', $request->id)
                ->first();
        return response()->json($product_details);
    }

    public function purchase_details($id) {
        $purchase_details = DB::table('supplier_purchase_detail')
                ->select('supplier_purchase_detail.unit_price', 'supplier_purchase_detail.discount as item_discount', 'supplier_purchase_detail.actual_amount', 'supplier_purchase_detail.qty', 'supplier_purchase_detail.total_price', 'supplier_purchase_detail.created_at', 'products.product_name', 'products.qty as pack', 'products.product_code')
                ->join('products', 'products.id', '=', 'supplier_purchase_detail.product_id')
                ->where('supplier_purchase_detail.purchase_id', $id)
                ->get();

        return view('admin.supplier.purchase_details')
                        ->with('purchase_id', $id)
                        ->with('purchase_details', $purchase_details);
    }

    public function delete_purchase($id) {
        try {
            DB::beginTransaction();
            $sale = DB::table('supplier_purchase')->where('id', $id)->first();

            //update product and its ledger
            $product_ledger = DB::table('product_ledger')
                    ->where('invoice_id', $id)
                    ->where('purchased_qty', '!=', 0)
                    ->get();
            foreach ($product_ledger as $pl) {
                $this->updateProductLedger($pl);
                $product = DB::table('products')->where('id', $pl->product_id);
                $get_product = $product->first();
                $add = $get_product->stock_qty - $pl->purchased_qty;
                // dump($add);
                $product->update(['stock_qty' => $add]);
            }
//        exit;
            // subtract the sale amount from the customer $sale->balance
            $get_company = DB::table('supplier')->where('id', $sale->supplier_id)->first();
//        dd($sale);
            $sub_bal = $get_company->dues - $sale->balance;
//        dd($get_company);
            $update_company = DB::table('supplier')->where('id', $sale->supplier_id)
                    ->update(['dues' => $sub_bal]);
//        delete product ledger
            $delete_product_ledger = DB::table('product_ledger')
                    ->where('invoice_id', $id)
                    ->where('purchased_qty', '!=', 0)
                    ->delete();
//        delete purchase and supplier purchase details
            DB::table('supplier_purchase')->where('id', $id)->delete();
            DB::table('supplier_purchase_detail')->where('purchase_id', $id)->delete();
// delete company ledger
            $delete_company_ledger = DB::table('supplier_ledger')
                    ->where('invoice_id', $id)
                    ->where('purchase', '!=', 0)
                    ->where('supplier_id', $sale->supplier_id);
            $this->updateSupplierLedger($delete_company_ledger);
            if ($sale->paid) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount + $sale->paid,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                $cashLedger = DB::table('cash_in_hand_ledger')
                        ->where('invoice_id', $id)
                        ->where('supplier_id', $sale->supplier_id);
//                dd($cashLedger->first());
                $this->updateCashLedger($cashLedger);
            }

            DB::commit();
            Session::flash('success', 'Purchase Transaction reverted Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function delete_return_purchase($id) {

        try {
            DB::beginTransaction();
            $sale = DB::table('return_purchase')->where('id', $id)->first();
            //update product and its ledger
            $product_ledger = DB::table('product_ledger')->where('invoice_id', $id)
                    ->where('return_purchased', '!=', 0)
                    ->get();
//        dd($product_ledger);
            foreach ($product_ledger as $pl) {
                $this->updateProductLedger($pl);
                $product = DB::table('products')->where('id', $pl->product_id);
                $get_product = $product->first();
                $add = $get_product->stock_qty + $pl->return_purchased;
                $product->update(['stock_qty' => $add]);
            }
//        exit;
            // subtract the sale amount from the customer $sale->balance
            $get_company = DB::table('supplier')->where('id', $sale->supplier_id)->first();
//        dd($sale);
            $sub_bal = $get_company->dues + $sale->balance;
//        dd($get_company);
            $update_company = DB::table('supplier')->where('id', $sale->supplier_id)
                    ->update(['dues' => $sub_bal]);
//        delete product ledger
            DB::table('product_ledger')
                    ->where('invoice_id', $id)
                    ->where('return_purchased', '!=', 0)
                    ->delete();

//        delete purchase and supplier purchase details
            DB::table('return_purchase')->where('id', $id)->delete();
            DB::table('return_purchase_details')->where('return_purchase_id', $id)->delete();
// delete company ledger
            $delete_company_ledger = DB::table('supplier_ledger')
                    ->where('invoice_id', $id)
                    ->where('return_purchase', '!=', 0)
                    ->where('supplier_id', $sale->supplier_id);
            $this->updateSupplierLedger($delete_company_ledger);
            if ($sale->paid) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount - $sale->paid,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                $cashLedger = DB::table('cash_in_hand_ledger')
                        ->where('invoice_id', $id)
                        ->where('supplier_id', $sale->supplier_id);
                $this->updateCashLedger($cashLedger);
            }
//            dd($delete_company_ledger);

            DB::commit();
            Session::flash('success', 'Return Purchase has been  reverted Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

}
