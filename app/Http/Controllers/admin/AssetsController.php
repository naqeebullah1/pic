<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Session;

class AssetsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = DB::table('assets')->get();
        return view('admin/personal_assets/index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
                    'title' => "required",
                    'price' => "required",
        ]);
        if ($validator->fails()) {
//         dd($validator->errors());
            Session::flash('error', $validator->errors());
            return redirect()->back();
        }
//        dd($request->all());
        $save = DB::table('assets')->insert([
            'title' => $request->title,
            'price' => $request->price,
            'description' => $request->description,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        if ($save) {
            Session::flash('success', 'User created successfully');
            return redirect('assets');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = DB::table('assets')->where('id', $id)->first();
        return view('admin.personal_assets.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'title' => "required",
                    'price' => "required",
        ]);
        if ($validator->fails()) {
//         dd($validator->errors());
            Session::flash('error', $validator->errors());
            return redirect()->back();
        }

//        dd($request->all());

        $save_array = [
            'title' => $request->title,
            'price' => $request->price,
            'description' => $request->description,
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $update = DB::table('assets')->where('id', $id)->update($save_array);
        if ($update) {
            Session::flash('success', 'Asset Updated Successfully');

            return redirect()->route('assets.index');
        } else {
            dd($update);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('assets')->where('id', $id)->delete();
        Session::flash('success', 'Asset Deleted Successfully');
        return redirect()->back();
    }

}
