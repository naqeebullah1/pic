<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ManufacturingCompaniesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $employees = DB::table('manufacturing_companies')->orderBy('id', 'desc')->get();
        return view('admin.manufacturing_companies.index')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->city);
        DB::table('manufacturing_companies')->insert([
            'company_name' => $request->company_name,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Company Added successfully');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetch_company() {
        $id = $_GET['id'];
        $employees = DB::table('manufacturing_companies')
                        ->where('id', '=', $id)
                        ->orderBy('id', 'desc')->first();
        echo $employees->company_name;
    }
     public function update_company(Request $request) {
        $id = $request->company_id;
        DB::table('manufacturing_companies')->where('id', $id)->update([
            'company_name' => $request->company_name,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Company Updated successfully');

        return redirect()->back();
    }

    public function destroy($id) {
        DB::table('manufacturing_companies')->where('id', $id)->delete();
        Session::flash('success', 'Company Deleted Successfully');
        return redirect()->back();
    }


}
