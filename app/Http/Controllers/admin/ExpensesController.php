<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ExpensesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $head_expenses = DB::table('expenses')->select('id', 'expenses')->where('level', 0)->get();

        $expneses = DB::table('expenses')
                ->where('level', 1)
                ->orderBy('date','asc')->get();
        $bank = DB::table('bank_acc')->pluck(DB::raw("concat(bank_name,'|',account_no)"), 'id');
        $all_expenses = $expneses;
//        dd($bank);
        return view('admin.expenses.index')
                        ->with('banks', $bank)
                        ->with('head_expenses', $head_expenses)
                        ->with('expneses', $all_expenses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->all());
        if ($request->pay_type == 'bank') {
//            dd($request->all());
            $bank = DB::table('bank_acc')->where('id', $request->bank_id)->first();
            $request_array = array();
            $amount = 0;
            foreach ($request->expense_title as $k => $exp) {
                $amount += $request->pay[$k];
                $request_array[$k]['expenses'] = $exp;
                $request_array[$k]['expenses_slug'] = str_slug($exp);
                $request_array[$k]['level'] = 1;
                $request_array[$k]['parent_id'] = $request->head;
                $request_array[$k]['description'] = $request->desc[$k];
                $request_array[$k]['payment_type'] = $request->pay_type;
                $request_array[$k]['bank_id'] = $request->bank_id;
                $request_array[$k]['amount'] = $request->pay[$k];
                $request_array[$k]['date'] = $request->created_at;
                $request_array[$k]['created_at'] = date('Y-m-d H:i:s');
                $request_array[$k]['updated_at'] = date('Y-m-d H:i:s');
            }
            $old_balance = $bank->current_amount;
            $cut_bal = $bank->current_amount - $amount;
            DB::table('bank_acc')->where('id', $request->bank_id)->update([
                'current_amount' => $cut_bal
            ]);
            foreach ($request_array as $key => $s):
                $expense = DB::table('expenses')->insert($request_array[$key]);
                $last_id = DB::getPdo()->lastInsertId();
                DB::table('bank_ledger')->insert([
                    'bank_id' => $request->bank_id,
                    'expense_id' => $last_id,
                    'total_amount' => $old_balance - $s['amount'],
                    'deposit' => 0,
                    'withdrawal' => $s['amount'],
                    'remaining' => $old_balance - $s['amount'],
                    'description' => $s['description'],
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
                $old_balance -= $s['amount'];
            endforeach;
            Session::flash('success', 'Expense Added Successfully');
            return redirect()->back();
        } else {

            $cash = DB::table('cash_in_hand')->first();
//            dd($request->all());
            $request_array = array();
            $amount = 0;
            foreach ($request->expense_title as $k => $exp) {
                $amount += $request->pay[$k];
                $request_array[$k]['expenses'] = $exp;
                $request_array[$k]['expenses_slug'] = str_slug($exp);
                $request_array[$k]['level'] = 1;
                $request_array[$k]['parent_id'] = $request->head;
                $request_array[$k]['description'] = $request->desc[$k];
                $request_array[$k]['payment_type'] = $request->pay_type;
                $request_array[$k]['bank_id'] = 1;
                $request_array[$k]['amount'] = $request->pay[$k];
                $request_array[$k]['date'] = $request->created_at;
                $request_array[$k]['created_at'] = date('Y-m-d H:i:s');
                $request_array[$k]['updated_at'] = date('Y-m-d H:i:s');
            }
            $old_balance = $cash->total_amount;
            $cut_bal = $cash->total_amount - $amount;
            DB::table('cash_in_hand')->update([
                'total_amount' => $cut_bal
            ]);
            foreach ($request_array as $key => $s):
                $expense = DB::table('expenses')->insert($request_array[$key]);
                $last_id = DB::getPdo()->lastInsertId();
                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'expense_id' => $last_id,
                    'total_amount' => $old_balance - $s['amount'],
                    'deposit' => 0,
                    'withdrawal' => $s['amount'],
                    'remaining' => $old_balance - $s['amount'],
                    'description' => $s['description'],
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
                $old_balance -= $s['amount'];
            endforeach;
            Session::flash('success', 'Expense Added Successfully');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $head_expense = DB::table('expenses')->where('id', $id)->select('id', 'expenses')->first();
        return view('admin.expenses.head_edit')
                        ->with('head_expense', $head_expense);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        DB::table('expenses')->where('id', $id)->update([
            'expenses' => $request->expense
        ]);

        Session::flash('success', 'Head Expense Updated Successfully');
        return redirect()->route('expenses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('expenses')->where('id', $id)->delete();

        Session::flash('success', 'Expense Deleted Successfully');
        return redirect()->back();
    }

    public function get_payment_type(Request $request) {
        if ($request->pay_type == 'bank') {
            $banks = DB::table('bank_acc')->get();

            echo '<div class="form-group">
                    <label>Select Bank.</label>
                    <select name="bank_id" class="form-control" required>

                    <option value="">Select Bank</option>';
            foreach ($banks as $bank) :
                echo '<option value="' . $bank->id . '"> ' . $bank->bank_name . ' | Acc No:' . $bank->account_no . ' | Balance: ' . $bank->current_amount . '</option>';
            endforeach;
            echo '</select>
            </div>';
//            echo '<div class="form-group">
//                        <label>Pay Amount</label>
//                        <input class="form-control" type="text" name="pay" id="pay_price" required>
//                    </div>';
        } else {
            $cash = DB::table('cash_in_hand')->first();

            echo '<div class="form-group">
                        <label>Cash Total Amount</label>
                        <input class="form-control" type="text" name="cash" value="' . $cash->total_amount . '" required readonly>
                    </div>';

//            echo '<div class="form-group">
//                    <label>Pay</label>
//                    <input class="form-control" type="text" name="pay" id="pay_price" required>
//                </div>';
        }
    }

    public function expense_delete($id) {
        $getexp = DB::table('expenses')->where('id', $id)->first();
        if ($getexp->payment_type == 'bank') {

            $bank = DB::table('bank_acc')->where('id', $getexp->bank_id)->first();
            $cut_bal = $bank->current_amount + $getexp->amount;
            DB::table('bank_acc')->where('id', $getexp->bank_id)->update([
                'current_amount' => $cut_bal
            ]);
//            dd($bank);
            DB::table('bank_ledger')->where('expense_id', $id)->delete();

            DB::table('expenses')->where('id', $id)->delete();

//            dd($getexp);

            Session::flash('success', 'Expense Added Successfully');
            return redirect()->back();
        } else {

            $cash = DB::table('cash_in_hand')->first();
            $cut_bal = $cash->total_amount + $getexp->amount;

            DB::table('cash_in_hand')->update([
                'total_amount' => $cut_bal
            ]);
            DB::table('cash_in_hand_ledger')->where('expense_id', $id)->update(['is_deleted' => 1]);
            DB::table('expenses')->where('id', $id)->delete();
//            dd($ledger);
            Session::flash('success', 'Expense Deleted Successfully');
            return redirect()->back();
        }
    }

}
