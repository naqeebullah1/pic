<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Http\Traits\UpdateLedgersTrait;

class CompaniesController extends Controller {

    use UpdateLedgersTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        exit;
        $status = 1;
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        }
        $customers = DB::table('company')->where('status', $status)
                        ->orderBy('name', 'asc')->get();
        return view('admin.companies.companies')
                        ->with('customers', $customers);
    }

    public function inactiveCompanies() {
        $customers = DB::table('company')->where('status', 0)
                        ->orderBy('id', 'desc')->get();
        return view('admin.companies.companies')
                        ->with('customers', $customers);
    }

    public function changeStatus($id) {

        $customer = DB::table('company')->find($id);
        if ($customer->status == 1) {
            DB::table('company')->where('id', $id)->update(['status' => 0]);
            // $customer->update(['status'=>0]);
        } else {
            DB::table('company')->where('id', $id)->update(['status' => 1]);
        }

        Session::flash('success', 'Customer Status Changed Successfully');
        return redirect()->back();
    }

    public function map(Request $request) {
        $users = DB::table('users')
                ->get();
        $user = "";
        if ($request->all()) {
            $user = DB::table('users')
                    ->where('id', '=', $request->user_id)
                    ->first();
        }
        $myusers = array();
        foreach ($users as $u):
            $myusers[$u->phone_no] = $u->name;
        endforeach;

        $myusers = json_encode($myusers);
        return view('admin.companies.map')
                        ->with('myusers', $myusers)
                        ->with('users', $users)
                        ->with('user', $user);
    }

    public function request_payment(Request $request) {
        $customer = DB::table('company')->where('id', '=', $request->customer_id)->first();
        if ($customer->contact) {
            $APIKey = '50d109f93eacd84034a04f3f9241500e202c8982';
            $receiver = $customer->contact;
            $sender = '8583';
            $textmessage = $request->message;
            $url = "http://api.smilesn.com/sendsms?hash=" . $APIKey . "&receivenum=" . $receiver . "&sendernum=" . urlencode($sender) . "&textmessage=" . urlencode($textmessage);
#----CURL Request Start
            $ch = curl_init();
            $timeout = 30;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $response = curl_exec($ch);
            curl_close($ch);
#----CURL Request End, Output Response
            $response = json_decode($response);
            if ($response) {
                Session::flash('success', 'Message hasbeen sent successfully');
            } else {
                Session::flash('error', 'Message can not be sent due to unknown reason');
            }
        } else {
            Session::flash('error', 'Message can not be sent because no contact number found');
        }
        return redirect()->back();
    }

    public function get_addresses() {
        $users = DB::table('users')->where('latitude', '!=', null)->where('longitude', '!=', null)->get();
        $setResponce = array();
        foreach ($users as $key => $user):
            $setResponce[$key] = array(ucwords($user->name), $user->latitude, $user->longitude);
        endforeach;
//        $a = [
//            ['Bondi Beach', -33.890542, 151.274856, 4],
//            ['Coogee Beach', -33.923036, 151.259052, 5],
//            ['Cronulla Beach', -34.028249, 151.157507, 3],
//            ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//            ['Maroubra Beach', -33.950198, 151.259302, 1]
//        ];
//        dd($setResponce);
        echo json_encode($setResponce);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $cities = DB::table('cities')->get();
        $countries = DB::table('countries')->get();
        $area_person = DB::table('area_person')->get();
        return view('admin.companies.add')->with('cities', $cities)->with('countries', $countries)->with('area_persons', $area_person);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function customer_payment($id) {
        $supplier = DB::table('company')->where('id', $id)->first();
        return view('admin.companies.customer_payment')->with('supplier', $supplier);
    }

    public function get_payment_type(Request $request) {
        if ($request->pay_type == 'bank') {
            $banks = DB::table('bank_acc')->where('status',1)->get();
            echo '<div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Select Bank</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="bank_name" id="bank_name" required>';
            foreach ($banks as $bank):
                echo '<option value="' . $bank->id . '"> ' . $bank->bank_name . ' | Acc No:' . $bank->account_no . ' | Balance: ' . $bank->current_amount . '</option>';
            endforeach;
            echo ' </select>
                </div>
            </div>';
        }
        if ($request->pay_type == 'cash') {

            $cash = DB::table('cash_in_hand')->first();

            echo ' <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Cash in Hand</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <input class="form-control" type="text" name="cash" value="' . $cash->total_amount . '" required readonly>
                        </div>
                    </div>';
        }
        if ($request->pay_type == 'cheque') {
            $banks = DB::table('bank_acc')->get();

            echo ' <div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Transfer To</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="tranfer_to" id="transfer_to" required>
                    <option value="">Transfer cheque amount To</option>
                    <option value="bank">Bank</option>
                    <option value="cash">Cash</option>
                    </select>
                </div>
            </div>';
        }
    }

    public function get_cheque_fields(Request $request) {
        if ($request->pay_type == 'bank') {
            $banks = DB::table('bank_acc')->get();

            echo ' <div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Select Bank</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="bank_name" id="bank_name" required>';
            foreach ($banks as $bank) :
                echo '<option value="' . $bank->id . '"> ' . $bank->bank_name . ' | Acc No:' . $bank->account_no . ' | Balance: ' . $bank->current_amount . '</option>';
            endforeach;
            echo ' </select>
                </div>
            </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Cheque Number</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="cheque_no" required>
                    </div>
                </div>';

            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Cheque Date</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="date" name="cheque_date" required>
                    </div>
                </div>';
        }

        if ($request->pay_type == 'cash') {

            $cash = DB::table('cash_in_hand')->first();

            echo ' <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Cash in Hand</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <input class="form-control" type="text" name="cash" value="' . $cash->total_amount . '" required readonly>
                        </div>
                    </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Cheque Number</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="text" name="cheque_no" required>
                    </div>
                </div>';
            echo '<div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">cheque Date</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <input class="form-control" type="date" name="cheque_date" required>
                    </div>
                </div>';
        }
    }

    public function save_cheque_payments() {
//        dd('yes');

        $date = date('Y-m-d');

        $bankCheques = DB::table('cheques')
                ->where('is_clear', 0)
                ->where('release_date', '<=', $date)
                ->where('bank_id', '!=', null)
                ->where('customer_id', '!=', null)
                ->orderBy('id', 'DESC')
                ->get();
        $cashCheques = DB::table('cheques')
                ->where('is_clear', 0)
                ->where('release_date', '<=', $date)
                ->where('bank_id', '=', null)
                ->where('customer_id', '!=', null)
                ->orderBy('id', 'DESC')
                ->get();
//        dd($bankCheques);
        if ($bankCheques->count()) {
            foreach ($bankCheques as $bankCheque) {

                $bank = DB::table('bank_acc')->where('id', $bankCheque->bank_id)->first();
                $add_bal = $bank->current_amount + $bankCheque->amount;

                DB::table('bank_acc')->where('id', $bankCheque->bank_id)->update([
                    'current_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('bank_ledger')->insert([
                    'bank_id' => $bankCheque->bank_id,
                    'cheque_id' => $bankCheque->id,
                    'total_amount' => $add_bal,
                    'deposit' => $bankCheque->amount,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => '',
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);


                DB::table('journal_vocher')->insert([
                    'u_id' => $bankCheque->customer_id,
                    'cheque_id' => $bankCheque->id,
                    'head' => "customer",
                    'sub_head' => "customer",
                    'total' => $bank->current_amount,
                    'pay' => $bankCheque->amount,
                    'balance' => $add_bal,
                    'description' => '',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                $customer = DB::table('company')->where('id', $bankCheque->customer_id)->first();

                $customer_bal = $customer->dues - $bankCheque->amount;

                DB::table('company')->where('id', $bankCheque->customer_id)->update([
                    'dues' => $customer_bal,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                DB::table('company_ledger')->insert([
                    'company_id' => $bankCheque->customer_id,
                    'invoice_no' => 0,
                    'cheque_id' => $bankCheque->id,
                    'narration' => '',
                    'sale' => 0,
                    'return_sale' => 0,
                    'payment' => $bankCheque->amount,
                    'remaining' => $customer_bal,
                    'created_by' => auth()->user()->id,
                    'table' => 'users',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                DB::table('cheques')->where('id', $bankCheque->id)->update([
                    'is_clear' => 1
                ]);
                echo "done";
            }
        }

        if ($cashCheques->count()) {

            foreach ($cashCheques as $bankCheque) {

                $cash = DB::table('cash_in_hand')->first();


                $add_bal = $cash->total_amount + $bankCheque->amount;
                DB::table('cash_in_hand')->update([
                    'total_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'cheque_id' => $bankCheque->id,
                    'total_amount' => $add_bal,
                    'deposit' => $bankCheque->amount,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => '',
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

                DB::table('journal_vocher')->insert([
                    'u_id' => $bankCheque->customer_id,
                    'cheque_id' => $bankCheque->id,
                    'head' => "customer",
                    'sub_head' => "customer",
                    'total' => $cash->total_amount,
                    'pay' => $bankCheque->amount,
                    'balance' => $add_bal,
                    'description' => '',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                $customer = DB::table('company')->where('id', $bankCheque->customer_id)->first();
                $customer_bal = $customer->dues - $bankCheque->amount;

                DB::table('company')->where('id', $bankCheque->customer_id)->update([
                    'dues' => $customer_bal,
                    'last_payment' => date('Y-m-d'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                DB::table('company_ledger')->insert([
                    'company_id' => $bankCheque->customer_id,
                    'invoice_no' => 0,
                    'cheque_id' => $bankCheque->id,
                    'narration' => '',
                    'sale' => 0,
                    'return_sale' => 0,
                    'payment' => $bankCheque->amount,
                    'remaining' => $customer_bal,
                    'created_by' => auth()->user()->id,
                    'table' => 'users',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                DB::table('cheques')->where('id', $bankCheque->id)->update([
                    'is_clear' => 1
                ]);
            }
        }
//        dd($cheques);
    }

    public function customer_cheques() {
        $date = date('Y-m-d');

        $cheques = DB::table('cheques')
                ->select('cheques.*', 'company.name')
                ->join('company', 'company.id', '=', 'cheques.customer_id')
                ->where('customer_id', '!=', null)
                ->orderBy('cheques.id', 'desc')
                ->get();
        $unclearcheques = DB::table('cheques')
                ->select('cheques.*', 'company.name')
                ->join('company', 'company.id', '=', 'cheques.customer_id')
                ->where('is_clear', 0)
                ->where('release_date', '<=', $date)
                ->where('customer_id', '!=', null)
                ->orderBy('cheques.id', 'DESC')
                ->get();
//        dd($unclearcheques);
        return view('admin/companies/customer_cheques')
                        ->with('unclearcheques', $unclearcheques)
                        ->with('cheques', $cheques);
    }

    public function cheque_cleared() {
        $status = $_GET['status'];

        $id = $_GET['id'];
        $bankCheque = DB::table('cheques')
                ->where('bank_id', '!=', null)
                ->where('id', '=', $id)
                ->first();
//        dd($bankCheque);
        $cashCheque = DB::table('cheques')
                ->where('bank_id', '=', null)
                ->where('id', '=', $id)
                ->first();
//        dd($cashCheques);
        if ($status == 'notclear') {
            $c = DB::table('cheques')->where('id', $id)->update([
                'is_clear' => 2
            ]);
            if ($c) {

                echo json_encode(['flag' => 1, 'message' => 'Cheque Uncleared']);
            } else {
                echo json_encode(['flag' => 0, 'message' => 'Not res']);
            }
            exit;
        }


        if ($bankCheque) {

            $bank = DB::table('bank_acc')->where('id', $bankCheque->bank_id)->first();
//            dd($bankCheque->bank_id);
            $add_bal = $bank->current_amount + $bankCheque->amount;

            DB::table('bank_acc')->where('id', $bankCheque->bank_id)->update([
                'current_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('bank_ledger')->insert([
                'bank_id' => $bankCheque->bank_id,
                'cheque_id' => $bankCheque->id,
                'total_amount' => $add_bal,
                'deposit' => $bankCheque->amount,
                'withdrawal' => 0,
                'remaining' => $add_bal,
                'description' => '',
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);


            DB::table('journal_vocher')->insert([
                'u_id' => $bankCheque->customer_id,
                'cheque_id' => $bankCheque->id,
                'head' => "customer",
                'sub_head' => "customer",
                'total' => $bank->current_amount,
                'pay' => $bankCheque->amount,
                'balance' => $add_bal,
                'description' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $customer = DB::table('company')->where('id', $bankCheque->customer_id)->first();

            $customer_bal = $customer->dues - $bankCheque->amount;

            DB::table('company')->where('id', $bankCheque->customer_id)->update([
                'dues' => $customer_bal,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            DB::table('company_ledger')->insert([
                'company_id' => $bankCheque->customer_id,
                'invoice_no' => 0,
                'cheque_id' => $bankCheque->id,
                'narration' => '',
                'sale' => 0,
                'return_sale' => 0,
                'payment' => $bankCheque->amount,
                'remaining' => $customer_bal,
                'created_by' => auth()->user()->id,
                'table' => 'users',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            DB::table('cheques')->where('id', $bankCheque->id)->update([
                'is_clear' => 1
            ]);
            echo json_encode(['flag' => 1, 'message' => 'Cheque Cleared']);
        }

        if ($cashCheque) {
            $bankCheque = $cashCheque;
            $cash = DB::table('cash_in_hand')->first();


            $add_bal = $cash->total_amount + $bankCheque->amount;
            DB::table('cash_in_hand')->update([
                'total_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'cheque_id' => $bankCheque->id,
                'total_amount' => $add_bal,
                'deposit' => $bankCheque->amount,
                'withdrawal' => 0,
                'remaining' => $add_bal,
                'description' => '',
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);

            DB::table('journal_vocher')->insert([
                'u_id' => $bankCheque->customer_id,
                'cheque_id' => $bankCheque->id,
                'head' => "customer",
                'sub_head' => "customer",
                'total' => $cash->total_amount,
                'pay' => $bankCheque->amount,
                'balance' => $add_bal,
                'description' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $customer = DB::table('company')->where('id', $bankCheque->customer_id)->first();
            $customer_bal = $customer->dues - $bankCheque->amount;

            DB::table('company')->where('id', $bankCheque->customer_id)->update([
                'dues' => $customer_bal,
                'last_payment' => date('Y-m-d'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            DB::table('company_ledger')->insert([
                'company_id' => $bankCheque->customer_id,
                'invoice_no' => 0,
                'cheque_id' => $bankCheque->id,
                'narration' => '',
                'sale' => 0,
                'return_sale' => 0,
                'payment' => $bankCheque->amount,
                'discount' => 0,
                'remaining' => $customer_bal,
                'created_by' => auth()->user()->id,
                'table' => 'users',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            DB::table('cheques')->where('id', $bankCheque->id)->update([
                'is_clear' => 1
            ]);
            echo json_encode(['flag' => 1, 'message' => 'Cheque Cleared']);
        }
    }

    public function save_payment(Request $request) {

        try {
            DB::beginTransaction();
            $time = date('h:i:s');
            $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));
//   dd($created_at);
            $new_amount = $request->pay;
            if ($request->discount != '' || $request->discount != 0) {
                $new_amount = $request->discount + $request->pay;
            }

            if ($request->pay_from == 'bank') {
                $customer = DB::table('company')->where('id', $request->customer_id)->first();

                $customer_bal = $customer->dues - $new_amount;

                DB::table('company')->where('id', $request->customer_id)->update([
                    'dues' => $customer_bal,
                    'last_payment' => $request->created_at,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                DB::table('company_ledger')->insert([
                    'company_id' => $request->customer_id,
                    'bank_id' => $request->bank_name,
                    'invoice_no' => 0,
                    'narration' => $request->description,
                    'sale' => 0,
                    'return_sale' => 0,
                    'payment' => $request->pay,
                    'discount' => $request->discount,
                    'remaining' => $customer_bal,
                    'created_by' => auth()->user()->id,
                    'table' => 'users',
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
                $last_id = DB::getPdo()->lastInsertId();


                $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();
                $add_bal = $bank->current_amount + $request->pay;

                DB::table('bank_acc')->where('id', $request->bank_name)->update([
                    'current_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('bank_ledger')->insert([
                    'bank_id' => $request->bank_name,
                    'company_id' => $request->customer_id,
                    'company_ledger_id' => $last_id,
                    'total_amount' => $add_bal,
                    'deposit' => $request->pay,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);


                DB::table('journal_vocher')->insert([
                    'u_id' => $request->customer_id,
                    'head' => "customer",
                    'sub_head' => "customer",
                    'total' => $bank->current_amount,
                    'pay' => $request->pay,
                    'balance' => $add_bal,
                    'description' => $request->description,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
            }
            if ($request->pay_from == 'cash') {
// dd($request->all());
                $customer = DB::table('company')->where('id', $request->customer_id)->first();
                $customer_bal = $customer->dues - $new_amount;

                DB::table('company')->where('id', $request->customer_id)->update([
                    'dues' => $customer_bal,
                    'last_payment' => $request->created_at,
                    'updated_at' => $created_at
                ]);

                DB::table('company_ledger')->insert([
                    'company_id' => $request->customer_id,
                    'invoice_no' => 0,
                    'narration' => $request->description,
                    'sale' => 0,
                    'return_sale' => 0,
                    'payment' => $request->pay,
                    'discount' => $request->discount,
                    'remaining' => $customer_bal,
                    'created_by' => auth()->user()->id,
                    'table' => 'users',
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
                $last_id = DB::getPdo()->lastInsertId();

                $cash = DB::table('cash_in_hand')->first();


                $add_bal = $cash->total_amount + $request->pay;
                DB::table('cash_in_hand')->update([
                    'total_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'company_id' => $request->customer_id,
                    'company_ledger_id' => $last_id,
                    'total_amount' => $add_bal,
                    'deposit' => $request->pay,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

                DB::table('journal_vocher')->insert([
                    'u_id' => $request->customer_id,
                    'head' => "customer",
                    'sub_head' => "customer",
                    'total' => $cash->total_amount,
                    'pay' => $request->pay,
                    'balance' => $add_bal,
                    'description' => $request->description,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);
            }

            if ($request->pay_from == 'cheque') {
                $arrayToSave = array();
                if ($request->tranfer_to == 'bank') {
                    $arrayToSave['bank_id'] = $request->bank_name;
                    $arrayToSave['transfer_to'] = 'bank';
                }
                if ($request->tranfer_to == 'cash') {
                    //cash logic goes here
                    $arrayToSave['transfer_to'] = 'cash';
                }
                $arrayToSave['customer_id'] = $request->customer_id;
                $arrayToSave['cheque_no'] = $request->cheque_no;
                $arrayToSave['amount'] = $request->pay;
                $arrayToSave['release_date'] = $request->cheque_date;
                $arrayToSave['created_at'] = $created_at;
                $arrayToSave['updated_at'] = $created_at;
//            dd($arrayToSave);
                DB::table('cheques')->insert($arrayToSave);
            }
            if (isset($request->send_msg)) {
                $companyDetails = $customer;
//        dd($companyDetails);
                if ($companyDetails->contact) {
                    $APIKey = '50d109f93eacd84034a04f3f9241500e202c8982';
                    $receiver = $companyDetails->contact;
                    $sender = '8583';
                    $textmessage = 'Dear Customer,
Thank you for your payment.
Below are your balance details.

Paid Amount: ' . $request->pay . ' PKR.    
Remaining Dues : ' . $customer_bal . ' PKR.

Thank You
Abdan traders';
                    $url = "http://api.smilesn.com/sendsms?hash=" . $APIKey . "&receivenum=" . $receiver . "&sendernum=" . urlencode($sender) . "&textmessage=" . urlencode($textmessage);
#----CURL Request Start
                    $ch = curl_init();
                    $timeout = 30;
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                    $response = curl_exec($ch);
                    curl_close($ch);
#----CURL Request End, Output Response
                    $response = json_decode($response);
                    if (!$response) {
                        Session::flash('error', 'Message can not be sent due to unknown reason');
                    }
                } else {
                    Session::flash('error', 'Message can not be sent because no contact number found');
                }
            }
            DB::commit();
            if (isset($request->print)) {
                if (isset($last_id)) {

                    return redirect()->route('print.payment', $last_id);
                }
            }
            Session::flash('success', 'Payment Transaction Completed Successfully');
            return redirect()->route('companies');
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function store(Request $request) {
        DB::table('company')->insert([
            'type' => $request->type,
            'name' => $request->name,
            'area_person_id' => $request->area_person_id,
            'cnic' => $request->cnic,
            'contact' => $request->contact,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'country' => $request->country,
            'dues' => $request->dues,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
//        dd($product_id);

        $product_id = DB::getPdo()->lastInsertId();


        DB::table('company_ledger')->insert([
            'company_id' => $product_id,
            'invoice_no' => 0,
            'narration' => 'opening account',
            'sale' => 0,
            'return_sale' => 0,
            'payment' => 0,
            'remaining' => $request->dues,
            'created_by' => auth()->user()->id,
            'table' => 'users',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

//        dd($product_id);
        Session::flash('success', 'Customer Added Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $customerSales = DB::table('company')
                ->select('company.name', 's.vr_no', 'company_ledger.*', 'company.id as company_id', 's.discount as sale_discount', 's.total_amount', 'sd.total_price', 'sd.qty','company_ledger.sale_details', 'p.product_name')
                ->join('company_ledger', 'company_ledger.company_id', '=', 'company.id')
                ->leftjoin('sales as s', 'company_ledger.invoice_no', '=', 's.id')
                ->leftjoin('sale_details as sd', 'sd.sale_id', '=', 's.id')
                ->leftjoin('products as p', 'p.id', '=', 'sd.product_id')
                ->where('company_ledger.company_id', $id)
                ->orderBy('company_ledger.id', 'asc')
                ->get();
                
                // dd($customerSales);
        $customer = array();
        foreach ($customerSales as $k => $c):
            $customer[$c->id][$k] = $c;
        endforeach;

        $banks = DB::table('bank_acc')->pluck('bank_name', 'id');
        $users = DB::table('users')->pluck('name', 'id');
        $salesman = DB::table('area_person')->pluck('name', 'id');
        $customerName = DB::table('company')->where('id', $id)->first();
        return view('admin.companies.customer_ledger')
                        ->with('customerName', $customerName)
                        ->with('users', $users)
                        ->with('banks', $banks)
                        ->with('salesman', $salesman)
                        ->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $company = DB::table('company')->where('id', $id)->first();
        $cities = DB::table('cities')->get();
        $countries = DB::table('countries')->get();
        $area_person = DB::table('area_person')->get();
        return view('admin.companies.edit_company')->with('cities', $cities)->with('countries', $countries)->with('area_persons', $area_person)->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
//        dd($request->all());
        DB::table('company')->where('id', $request->id)->update([
            'name' => $request->name,
            'area_person_id' => $request->area_person_id,
            'cnic' => $request->cnic,
            'contact' => $request->contact,
            'type' => $request->type,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'country' => $request->country,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        Session::flash('success', 'Company Updated Successfully');
        return redirect()->route('companies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('company')->delete($id);
        DB::table('company_ledger')->where('company_id', $id)->delete();

        Session::flash('success', 'Company & Company Ledger Deleted Successfully');
        return redirect()->back();
    }

    public function delete_payment($id) {
//        dd($id);
        try {
            DB::beginTransaction();
            /*
             * All COde Will goes here
             */

            $supplier_ledger = DB::table('company_ledger')->where('id', $id)->first();
//        dd($supplier_ledger->payment);
            if ($supplier_ledger->bank_id != 0) {
                $bank_ledger = DB::table('bank_ledger')->where('company_ledger_id', $id)->first();
                if (!$bank_ledger) {
                    Session::flash('error', 'This transaction can not be deleted');
                    return redirect()->back();
                }
                $bank = DB::table('bank_acc')->where('id', $supplier_ledger->bank_id)->first();
                $cut_bal = $bank->current_amount - $bank_ledger->deposit;
//            dd($bank_ledger);
                //update bank account
                DB::table('bank_acc')->where('id', $supplier_ledger->bank_id)->update([
                    'current_amount' => $cut_bal
                ]);
//            delete bank ledger
                $bankRec = DB::table('bank_ledger')->where('company_ledger_id', $id);
                $this->updateBankLedger($bankRec);
//            delete Journal voucher
//update suppliers ledger
                $supplier = DB::table('company')->where('id', $supplier_ledger->company_id)->first();
                $supplier_bal = $supplier->dues + $supplier_ledger->payment;
                DB::table('company')->where('id', $supplier_ledger->company_id)->update([
                    'dues' => $supplier_bal
                ]);
                $supplier_ledger = DB::table('company_ledger')->where('id', $id);
                $this->updateCustomerLedger($supplier_ledger);
//                dd($bankRec);
            } else {
                $bank_ledger = DB::table('cash_in_hand_ledger')->where('company_ledger_id', $id)->first();
                if (!$bank_ledger) {
                    Session::flash('error', 'This transaction can not be deleted');
                    return redirect()->back();
                }
                $bank = DB::table('cash_in_hand')->where('id', 1)->first();
                $cut_bal = $bank->total_amount - $bank_ledger->deposit;

                //update bank account
                DB::table('cash_in_hand')->where('id', 1)->update([
                    'total_amount' => $cut_bal
                ]);
//            delete bank ledger
                $cashRec = DB::table('cash_in_hand_ledger')
                        ->where('company_ledger_id', $id);
                $this->updateCashLedger($cashRec);
//            delete Journal voucher
//update suppliers ledger
                $supplier = DB::table('company')->where('id', $supplier_ledger->company_id)->first();
                $supplier_bal = $supplier->dues + $supplier_ledger->payment;
                DB::table('company')->where('id', $supplier_ledger->company_id)->update([
                    'dues' => $supplier_bal
                ]);
                $supplier_ledger = DB::table('company_ledger')->where('id', $id);

                $this->updateCustomerLedger($supplier_ledger);
            }
//            dd('yes');
            DB::commit();
            Session::flash('success', 'Payment Transaction Completed Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function recovery($id) {
        $customer = DB::table('company')
                ->select('company.name', 'company_ledger.*', 'company.id as company_id')
                ->join('company_ledger', 'company_ledger.company_id', '=', 'company.id')
                ->where('company_ledger.sale_man_id', $id)
                ->get();
        return view('admin.area_person.admin_customer_ledger')->with('customer', $customer);
    }

    public function update_ledger_payment($id) {
        try {
            DB::beginTransaction();
            $customer = DB::table('company_ledger')
                    ->where('company_id', $id)
                    ->orderBy('company_ledger.id', 'asc')
                    ->first();
            $remainingAmount = $this->customerLedgerNextRecords($customer->id, $customer->company_id, $customer->remaining);
            DB::table('company')->where('id', $id)->update([
                'dues' => $remainingAmount
            ]);
            DB::commit();
            Session::flash('success', 'Ledger Updated Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function printPayment($id) {
//        dd($id);
        $paymentDetails = DB::table('company_ledger')
                ->select('company_ledger.*', 'company.name')
                ->join('company', 'company.id', '=', 'company_ledger.company_id')
                ->where('company_ledger.id', $id)
                ->first();
        return view('admin.companies.payment_invoice', ['paymentDetails' => $paymentDetails]);
    }

}
