<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class CitiesController extends Controller 
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $employees = DB::table('cities')->orderBy('id', 'desc')->get();
        return view('admin.cities.index')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->city);
        DB::table('cities')->insert([
            'city' => $request->city,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'City Added');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    public function fetch_city() {
        $id = $_GET['id'];
        $employees = DB::table('cities')
                        ->where('id', '=', $id)
                        ->orderBy('id', 'desc')->first();
        echo $employees->city;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_city(Request $request) {
        $id = $request->city_id;
        DB::table('cities')->where('id', $id)->update([
            'city' => $request->city,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'City Updated');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('cities')->where('id', $id)->delete();
        Session::flash('success', 'City Deleted Successfully');
        return redirect()->back();
    }

}
