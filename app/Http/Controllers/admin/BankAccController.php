<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Http\Traits\UpdateLedgersTrait;

class BankAccController extends Controller {

    use UpdateLedgersTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $status = 1;
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        }

        $accounts = DB::table('bank_acc')
                        ->where('status', $status)
                        ->orderBy('id', 'desc')->get();

        return view('admin.bank_acc.index')->with('accounts', $accounts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.bank_acc.create');
    }

    public function transfer_amount() {
        $status = 1;
        // if (isset($_GET['status'])) {
        //     $status = $_GET['status'];
        // }
        $banks = DB::table('bank_acc') ->where('status', $status)->get();
        // dd($banks);
        return view('admin.bank_acc.transfer_amount')->with('banks', $banks);
    }

    public function transfer_amount_store(Request $request) {

        try {
            DB::beginTransaction();
            $time = date('h:i:s');
            $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));
            if ($request->pay_from == 'bank') {
                // transfer to cash
                $bank = DB::table('cash_in_hand')->where('id', 1)->first();

                $add_bal = $bank->total_amount + $request->pay;
//            dd($bank);

                DB::table('cash_in_hand')->where('id', 1)->update([
                    'total_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'transfer_from_bank_id' => $request->bank_name,
                    'total_amount' => $add_bal,
                    'deposit' => $request->pay,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => $request->description,
                    'created_at' => $created_at,
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

                $cihId = DB::getPdo()->lastInsertId();

                // transfer From bank
                $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();
                $sub_bal = $bank->current_amount - $request->pay;
                DB::table('bank_acc')->where('id', $request->bank_name)->update([
                    'current_amount' => $sub_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                DB::table('bank_ledger')->insert([
                    'bank_id' => $request->bank_name,
                    'total_amount' => $sub_bal,
                    'transfer_to_cash' => 1,
                    'deposit' => 0,
                    'withdrawal' => $request->pay,
                    'remaining' => $sub_bal,
                    'description' => $request->description,
                    'cihl_id' => $cihId,
                    'created_at' => $created_at,
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
                $blId = DB::getPdo()->lastInsertId();

                DB::table('cash_in_hand_ledger')->where('id', $cihId)->update([
                    'bl_id' => $blId,
                ]);
            }
            if ($request->pay_from == 'cash') {
//            dd($request->all());
                // transfer to bank
                $bank = DB::table('bank_acc')->where('id', $request->transfer_to)->first();
                $add_bal = $bank->current_amount + $request->pay;
                DB::table('bank_acc')->where('id', $request->transfer_to)->update([
                    'current_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                DB::table('bank_ledger')->insert([
                    'bank_id' => $request->transfer_to,
                    'transfer_from_cash' => 1,
                    'total_amount' => $add_bal,
                    'deposit' => $request->pay,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => $request->description,
                    'created_at' => $created_at,
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
                $blId = DB::getPdo()->lastInsertId();


                // transfer From cash
                $bank = DB::table('cash_in_hand')->where('id', 1)->first();

                $sub_bal = $bank->total_amount - $request->pay;
//            dd($bank);

                DB::table('cash_in_hand')->where('id', 1)->update([
                    'total_amount' => $sub_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'bank_id' => $request->transfer_to,
                    'total_amount' => $sub_bal,
                    'deposit' => 0,
                    'withdrawal' => $request->pay,
                    'remaining' => $sub_bal,
                    'description' => $request->description,
                    'bl_id' => $blId,
                    'created_at' => $created_at,
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
                $cihId = DB::getPdo()->lastInsertId();

                DB::table('bank_ledger')->where('id', $blId)->update([
                    'cihl_id' => $cihId,
                ]);
            }
            DB::commit();
            Session::flash('success', 'Transaction Completed');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function transfer_payment(Request $request) {
        // transfer to bank
        $bank = DB::table('bank_acc')->where('id', $request->transfer_to)->first();
        $add_bal = $bank->current_amount + $request->amount;
        DB::table('bank_acc')->where('id', $request->transfer_to)->update([
            'current_amount' => $add_bal,
            'updated_at' => date('y-m-d H:i:s')
        ]);
        DB::table('bank_ledger')->insert([
            'bank_id' => $request->transfer_to,
            'total_amount' => $add_bal,
            'deposit' => $request->amount,
            'withdrawal' => 0,
            'remaining' => $add_bal,
            'description' => $request->description,
            'created_at' => date('y-m-d H:i:s'),
            'updated_at' => date('y-m-d H:i:s'),
        ]);


        // transfer From bank
        $bank = DB::table('bank_acc')->where('id', $request->transfer_from)->first();
        $sub_bal = $bank->current_amount - $request->amount;
        DB::table('bank_acc')->where('id', $request->transfer_from)->update([
            'current_amount' => $sub_bal,
            'updated_at' => date('y-m-d H:i:s')
        ]);
        DB::table('bank_ledger')->insert([
            'bank_id' => $request->transfer_from,
            'total_amount' => $sub_bal,
            'deposit' => 0,
            'withdrawal' => $request->amount,
            'remaining' => $sub_bal,
            'description' => $request->description,
            'created_at' => date('y-m-d H:i:s'),
            'updated_at' => date('y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Transaction Completed');
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        DB::table('bank_acc')->insert([
            'bank_name' => $request->name,
            'account_no' => $request->account_no,
            'current_amount' => $request->current_amount,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
//        dd($product_id);

        $product_id = DB::getPdo()->lastInsertId();


        DB::table('bank_ledger')->insert([
            'bank_id' => $product_id,
            'total_amount' => $request->current_amount,
            'description' => 'opening account',
            'deposit' => 0,
            'withdrawal' => 0,
            'remaining' => $request->current_amount,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

//        dd($product_id);
        Session::flash('success', 'Bank Account Added Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
//        dd($id);
        $accountDetails = DB::table('bank_acc')
                ->select('bank_ledger.*', 'bank_acc.bank_name', 'bank_acc.account_no', 'bank_acc.current_amount', 'expenses.parent_id')
                ->join('bank_ledger', 'bank_ledger.bank_id', '=', 'bank_acc.id')
                ->leftjoin('expenses', 'bank_ledger.expense_id', '=', 'expenses.id')
                ->where('bank_ledger.bank_id', $id)->
                orderBy('bank_ledger.id', 'asc')
                ->get();
//        dd($accountDetails);
        $banks = DB::table('bank_acc')->where('id', $id)->first();
        $companies = DB::table('company')->pluck('name', 'id');
        $suppliers = DB::table('supplier')->pluck('name', 'id');
        $expenses = DB::table('expenses')
                ->where('level', 0)
                ->pluck('expenses', 'id');

        return view('admin.bank_acc.show')
                        ->with('companies', $companies)
                        ->with('suppliers', $suppliers)
                        ->with('expenses', $expenses)
                        ->with('bank', $banks)
                        ->with('accountDetails', $accountDetails);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $company = DB::table('bank_acc')->where('id', $id)->first();
        return view('admin.bank_acc.edit')->with('account', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
//        dd($request->all());
        DB::table('bank_acc')->where('id', $request->id)->update([
            'bank_name' => $request->name,
            'account_no' => $request->account_no,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Bank Account Updated Successfully');
        return redirect()->route('bank_acc');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('bank_acc')->where('id', $id)->delete();
        DB::table('bank_ledger')->where('bank_id', $id)->delete();

        Session::flash('success', 'Bank Account Deleted Successfully');
        return redirect()->back();
    }

    public function delete_ledger($id) {

        try {
            DB::beginTransaction();
            $get = DB::table('bank_ledger')->where('id', $id)->first();

            $cash_in_hand_get = DB::table('cash_in_hand')->first();


            if ($get->transfer_from_cash) {
                $add_bal = $cash_in_hand_get->total_amount + $get->deposit;
                DB::table('cash_in_hand')
                        ->where('id', 1)
                        ->update(['total_amount' => $add_bal]);

                $cashLedger = DB::table('cash_in_hand_ledger')->where('bl_id', $id);
                $this->updateCashLedger($cashLedger);


                $get_bank = DB::table('bank_acc')->where('id', $get->bank_id)->first();
                $sub_bal = $get_bank->current_amount - $get->deposit;
                DB::table('bank_acc')->where('id', $get->bank_id)->update([
                    'current_amount' => $sub_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                $bankLedger = DB::table('bank_ledger')->where('id', $id);
                $this->updateBankLedger($bankLedger);
            }

            if ($get->transfer_to_cash) {

                $add_bal = $cash_in_hand_get->total_amount - $get->withdrawal;
                DB::table('cash_in_hand')
                        ->where('id', 1)
                        ->update(['total_amount' => $add_bal]);
                $cashLedger = DB::table('cash_in_hand_ledger')->where('bl_id', $id);
                $this->updateCashLedger($cashLedger);


                $get_bank = DB::table('bank_acc')->where('id', $get->bank_id)->first();
                $sub_bal = $get_bank->current_amount + $get->withdrawal;
                DB::table('bank_acc')->where('id', $get->bank_id)->update([
                    'current_amount' => $sub_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                $bankLedger = DB::table('bank_ledger')->where('id', $id);
                $this->updateBankLedger($bankLedger);
            }

            DB::commit();
            Session::flash('success', 'Ladger Deleted');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function update_ledger($id) {
        try {
            DB::beginTransaction();
            $bankLedger = DB::table('bank_ledger')
                    ->where('bank_id', $id)
                    ->orderBy('id', 'asc')
                    ->first();
            $remainingAmount = $this->bankLedgerNextRecords($bankLedger->id, $bankLedger->bank_id, $bankLedger->remaining);
            DB::table('bank_acc')->where('id', $id)->update([
                'current_amount' => $remainingAmount
            ]);
//            dd($remainingAmount);
//            exit;
            DB::commit();
            Session::flash('success', 'Ledger Updated Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function changeStatus($id) {

        $customer = DB::table('bank_acc')->find($id);
        if ($customer->status == 1) {
            DB::table('bank_acc')->where('id', $id)->update(['status' => 0]);
            // $customer->update(['status'=>0]);
        } else {
            DB::table('bank_acc')->where('id', $id)->update(['status' => 1]);
        }

        Session::flash('success', 'Bank Status Changed Successfully');
        return redirect()->back();
    }

}
