<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Validator;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = DB::table('users')->get();
        return view('admin/users/index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'phone_no' => "unique:users,phone_no",
                    'email' => "unique:users,email",
        ]);
        if ($validator->fails()) {
//         dd($validator->errors());
            Session::flash('error', $validator->errors());
            return redirect()->back();
        }
//        dd($request->all());
        $save = DB::table('users')->insert([
            'user_type' => $request->role,
            'name' => $request->name,
            'email' => $request->email,
            'phone_no' => $request->phone_no,
            'password' => bcrypt($request->pass)
        ]);
        if ($save) {
            Session::flash('success', 'User created successfully');
            return redirect('users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = DB::table('users')->where('id', $id)->first();
        return view('admin.users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'phone_no' => "unique:users,phone_no," . $id,
                    'email' => "unique:users,email," . $id,
        ]);
        if ($validator->fails()) {
//         dd($validator->errors());
            Session::flash('error', $validator->errors());
            return redirect()->back();
        }


        $save_array = [
            'name' => $request->name,
            'email' => $request->email,
            'phone_no' => $request->phone_no,
            'user_type' => $request->role,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
//        dd($request->all());
        if (!empty($request->pass)) {
            $save_array['password'] = bcrypt($request->pass);
        }
//        dd($save_array);

        $update = DB::table('users')->where('id', $id)->update($save_array);
        if ($update) {

            Session::flash('success', 'User Updated Successfully');

            return redirect()->route('users.index');
        } else {
            dd($update);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('users')->where('id', $id)->delete();
        Session::flash('success', 'User Deleted Successfully');
        return redirect()->back();
    }

}
