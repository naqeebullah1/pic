<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ReportsController extends Controller {

    public function index() {
        $suppliers = DB::table('supplier')->select('id', 'name')
                        ->orderBy('name')->get();

        return view('admin.reports.supplier.supplier_report')
                        ->with('suppliers', $suppliers);
    }

    public function general_report() {
        $suppliers = DB::table('supplier')->select('id', 'name')->orderBy('name')->get();

        return view('admin.reports.supplier.general_report')
                        ->with('suppliers', $suppliers);
    }

    public function supplier_ledger_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('supplier_ledger as a')
                        ->join('supplier as b', 'b.id', '=', 'a.supplier_id')
                        ->leftJoin('supplier_purchase', 'supplier_purchase.id', '=', 'a.invoice_id')
                        ->where('a.supplier_id', '=', $request->supplier_id)
                        ->whereDate('a.created_at', '>=', $start_date)
                        ->whereDate('a.created_at', '<=', $end_date)
                        ->select('a.*', 'b.name', 'supplier_purchase.vr_no as vr_no')
                        ->orderBy('a.id', 'ASC')->get();
//dd($ledger_report);
        return view('admin.reports.supplier.supplier_ledger_report')
                        ->with('ledger_report', $ledger_report);
    }

    public function generate_supplier_general_report(Request $request) {
//        dump($request->all());
//        exit;
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('supplier as b');
        if (isset($request->name) && !empty($request->name)) {
            $ledger_report->where('b.name', 'LIKE', '%' . $request->name . '%');
        }
        if (isset($request->less) && !empty($request->less)) {
            $ledger_report->where('b.dues', '<', $request->less);
        }
        if (isset($request->grater) && !empty($request->grater)) {
            $ledger_report->where('b.dues', '>', $request->grater);
        }
        if (isset($request->from_date) && !empty($request->from_date)) {
            $ledger_report->whereDate('b.last_payment', '<', $start_date);
        }
        if (isset($request->to_date) && !empty($request->to_date)) {
            $ledger_report->whereDate('b.last_payment', '>', $end_date);
        }
        if (isset($request->days) && !empty($request->days)) {
            $prev_date = date('Y-m-d', strtotime('-' . $request->days . ' day'));

            $ledger_report->whereDate('b.last_payment', '<', $prev_date);
        }
//        dump($request->all());
//        dd($prev_date);
//        dd($ledger_report->get());

        $ledger_report = $ledger_report->orderBy('b.name')->get();
        return view('admin.reports.supplier.generate_supplier_general_report')
                        ->with('ledger_report', $ledger_report);
    }

    public function supplier_city_report(Request $request) {
        $cities = DB::table('cities')->select('id', 'city')->orderBy('city')->get();

        return view('admin.reports.supplier_city_report.supplier_city_report')
                        ->with('cities', $cities);
    }

    public function get_supplier_city_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('supplier as b');
        if (isset($request->name) && !empty($request->name)) {
            $ledger_report->where('b.name', 'LIKE', '%' . $request->name . '%');
        }
        if (isset($request->less) && !empty($request->less)) {
            $ledger_report->where('b.dues', '<', $request->less);
        }
        if (isset($request->grater) && !empty($request->grater)) {
            $ledger_report->where('b.dues', '>', $request->grater);
        }
        if (isset($request->from_date) && !empty($request->from_date)) {
            $ledger_report->whereDate('b.last_payment', '<', $start_date);
        }
        if (isset($request->to_date) && !empty($request->to_date)) {
            $ledger_report->whereDate('b.last_payment', '<', $end_date);
        }
        if (isset($request->city)) {
            $ledger_report->where('b.city', '=', $request->city);
        }
        if (isset($request->days) && !empty($request->days)) {
            $prev_date = date('Y-m-d', strtotime('-' . $request->days . ' day'));

            $ledger_report->whereDate('b.last_payment', '<', $prev_date);
        }
//        dump($request->all());
//        dd($ledger_report->get());
        $ledger_report = $ledger_report->orderBy('b.name')->get();
        return view('admin.reports.supplier_city_report.get_supplier_city_report')
                        ->with('cities', $ledger_report);
    }

    public function customer_report() {
        $customers = DB::table('company')->select('id', 'name')->orderBy('name')->get();
        $cities = DB::table('cities')->select('id', 'city')->orderBy('city')->get();

        return view('admin.reports.customers.customer_report')
                        ->with('customers', $customers)
                        ->with('cities', $cities);
    }

    public function customer_ledger_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('company_ledger as a')
                ->join('company as b', 'b.id', '=', 'a.company_id')
                ->leftJoin('sales', 'sales.id', '=', 'a.invoice_no')
                ->where('a.company_id', '=', $request->customer_id)
                ->whereDate('a.created_at', '>=', $start_date)
                ->whereDate('a.created_at', '<=', $end_date)
                ->select('a.*', 'b.name', 'sales.vr_no as vr_no', 'sales.discount as sale_discount')
                ->orderBy('a.id', 'ASC')
                ->get();

    //   dd($ledger_report);
    $sno=DB::table('company_ledger')

    ->where('company_id', '=', $request->customer_id)
    ->whereDate('created_at', '<', $start_date)->latest()->value('id');
    
     if($sno){
        
    }
    else{
         $sno=DB::table('company_ledger')

    ->where('company_id', '=', $request->customer_id)
    ->first();
    $sno=$sno->id;
   
    }
    
    
    $remaining_balance = DB::table('company_ledger')->where('id', $sno)->select('remaining')->first();
    // dd($sno);
        return view('admin.reports.customers.customer_ledger_report')
                        ->with('ledger_report', $ledger_report)
                        ->with('remaining_balance', $remaining_balance);
    }

    public function customer_city_report(Request $request) {
        $cities = DB::table('cities')->select('id', 'city')
                        ->orderBy('city')->get();

        return view('admin.reports.customer_city_report.customer_city_report')
                        ->with('cities', $cities);
    }

    public function get_customer_city_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('company as b');
        if (isset($request->name) && !empty($request->name)) {
            $ledger_report->where('b.name', 'LIKE', '%' . $request->name . '%');
        }
        if (isset($request->less) && !empty($request->less)) {
            $ledger_report->where('b.dues', '<', $request->less);
        }
        if ($request->has('grater') && $request->grater != '') {
            $gt = (int) $request->grater;
            $ledger_report->where('b.dues', '>', $gt);
        }
//        dd($ledger_report->get());
//        if (isset($request->from_date) && !empty($request->from_date)) {
//            $ledger_report->whereDate('b.last_payment', '<', $start_date);
//        }
        if (isset($request->to_date) && !empty($request->to_date)) {
            $ledger_report->whereDate('b.last_payment', '<', $end_date);
        }
        if (isset($request->city)) {
            $ledger_report->where('b.city', '=', $request->city);
        }
        if (isset($request->days) && !empty($request->days)) {
            $prev_date = date('Y-m-d', strtotime('-' . $request->days . ' day'));

            $ledger_report->whereDate('b.last_payment', '<', $prev_date);
        }
//        dump($request->all());
        $ledger_report = $ledger_report->orderBy('b.name')->get();

        return view('admin.reports.customer_city_report.get_customer_city_report')
                        ->with('cities', $ledger_report);
    }

    public function product_report() {
        $products = DB::table('products')->select('id', 'product_name')->orderBy('product_name')->get();

        return view('admin.reports.product.product_report')
                        ->with('products', $products);
    }

    public function product_ledger_report(Request $request) {
        // dd($request->product_id);
        if (!empty($request->product_id) && !empty($request->from_date) && !empty($request->to_date)) {
            $start_date = date("Y-m-d", strtotime($request->from_date));
            $end_date = date("Y-m-d", strtotime($request->to_date));

            $ledger_report = DB::table('product_ledger as a')
                            ->select('sales.vr_no as sale_no', 'return_sales.vr_no as return_sale_no', 'supplier_purchase.vr_no as purchase_no', 'return_purchase.vr_no as return_purchase_no', 'sales.company_id as customer_sale_id', 'supplier_purchase.supplier_id as supplier_purchase_id', 'return_sales.company_id as return_sale_customer_id', 'return_purchase.supplier_id as return_purchase_supplier_id', 'a.*', 'b.product_name', 'b.qty_per_cotton')
                            ->join('products as b', 'b.id', '=', 'a.product_id')
                            ->leftjoin('sales', 'sales.id', '=', 'a.sale_id')
                            ->leftjoin('return_sales', 'return_sales.id', '=', 'a.return_sale_id')
                            ->leftjoin('supplier_purchase', 'supplier_purchase.id', '=', 'a.purchase_id')
                            ->leftjoin('return_purchase', 'return_purchase.id', '=', 'a.return_purchase_id')
                            ->where('a.product_id', '=', $request->product_id)
                            ->whereDate('a.created_at', '>=', $start_date)
                            ->whereDate('a.created_at', '<=', $end_date)
                            ->orderBy('b.product_name')
                            ->orderBy('a.id', 'ASC')->get();

            // dd($ledger_report);
        } elseif (empty($request->product_id) && !empty($request->from_date) && !empty($request->to_date)) {
            $start_date = date("Y-m-d", strtotime($request->from_date));
            $end_date = date("Y-m-d", strtotime($request->to_date));

            $ledger_report = DB::table('product_ledger as a')
                            ->select('sales.vr_no as sale_no', 'return_sales.vr_no as return_sale_no', 'supplier_purchase.vr_no as purchase_no', 'return_purchase.vr_no as return_purchase_no', 'sales.company_id as customer_sale_id', 'supplier_purchase.supplier_id as supplier_purchase_id', 'return_sales.company_id as return_sale_customer_id', 'return_purchase.supplier_id as return_purchase_supplier_id', 'a.*', 'b.product_name')
                            ->join('products as b', 'b.id', '=', 'a.product_id')
                            ->leftjoin('sales', 'sales.id', '=', 'a.sale_id')
                            ->leftjoin('return_sales', 'return_sales.id', '=', 'a.return_sale_id')
                            ->leftjoin('supplier_purchase', 'supplier_purchase.id', '=', 'a.purchase_id')
                            ->leftjoin('return_purchase', 'return_purchase.id', '=', 'a.return_purchase_id')
                            ->whereDate('a.created_at', '>=', $start_date)
                            ->whereDate('a.created_at', '<=', $end_date)
                            ->orderBy('a.id', 'asc')->get();
        } else {

            $ledger_report = DB::table('product_ledger as a')
                            ->select('sales.vr_no as sale_no', 'return_sales.vr_no as return_sale_no', 'supplier_purchase.vr_no as purchase_no', 'return_purchase.vr_no as return_purchase_no', 'sales.company_id as customer_sale_id', 'supplier_purchase.supplier_id as supplier_purchase_id', 'return_sales.company_id as return_sale_customer_id', 'return_purchase.supplier_id as return_purchase_supplier_id', 'a.*', 'b.product_name')
                            ->join('products as b', 'b.id', '=', 'a.product_id')
                            ->leftjoin('sales', 'sales.id', '=', 'a.sale_id')
                            ->leftjoin('return_sales', 'return_sales.id', '=', 'a.return_sale_id')
                            ->leftjoin('supplier_purchase', 'supplier_purchase.id', '=', 'a.purchase_id')
                            ->leftjoin('return_purchase', 'return_purchase.id', '=', 'a.return_purchase_id')
                            ->orderBy('a.id', 'asc')->get();
        }
//        dd($ledger_report);
        $suppliers = DB::table('supplier')->pluck('name', 'id');
        $customers = DB::table('company')->pluck('name', 'id');
        return view('admin.reports.product.product_ledger_report')
                        ->with('suppliers', $suppliers)
                        ->with('customers', $customers)
                        ->with('ledger_report', $ledger_report);
    }

    public function purchase_report() {
        return view('admin.reports.purchase.purchase_report');
    }

    public function purchase_ledger_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('supplier_purchase as a')
                        ->join('supplier as b', 'b.id', '=', 'a.supplier_id')
                        ->whereDate('a.created_at', '>=', $start_date)
                        ->whereDate('a.created_at', '<=', $end_date)
                        ->select('a.*', 'b.name')
                        ->orderBy('a.created_at', 'desc')->get();

        return view('admin.reports.purchase.purchase_ledger_report')->with('ledger_report', $ledger_report);
    }

    public function return_purchase_report() {
        return view('admin.reports.return_purchase.return_purchase_report');
    }

    public function return_purchase_ledger_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('return_purchase as a')
                        ->join('supplier as b', 'b.id', '=', 'a.supplier_id')
                        ->whereDate('a.created_at', '>=', $start_date)
                        ->whereDate('a.created_at', '<=', $end_date)
                        ->select('a.*', 'b.name')
                        ->orderBy('a.created_at', 'desc')->get();

        return view('admin.reports.return_purchase.return_purchase_ledger_report')->with('ledger_report', $ledger_report);
    }

    public function sale_report() {
        return view('admin.reports.sale.sale_report');
    }

    public function sale_ledger_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('sales as a')
                ->select('a.*', 'b.name')
                ->leftjoin('company as b', 'b.id', '=', 'a.company_id')
                ->whereBetween('a.created_at', [$start_date, $end_date]);
        // ->whereDate('a.created_at', '<=', $end_date);
        if ($request->customer_type == 0) {
            $ledger_report->where('a.company_id', '=', 0);
        }
        if ($request->customer_type == 1) {
            $ledger_report->where('a.company_id', '!=', 0);
        }


        return view('admin.reports.sale.sale_ledger_report')->with('ledger_report', $ledger_report->orderBy('a.id', 'dsc')->get());
    }

    public function return_sale_report() {
        return view('admin.reports.return_sale.return_sale_report');
    }

    public function return_sale_ledger_report(Request $request) {
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $ledger_report = DB::table('return_sales as a')
                        ->join('company as b', 'b.id', '=', 'a.company_id')
                        ->whereDate('a.created_at', '>=', $start_date)
                        ->whereDate('a.created_at', '<=', $end_date)
                        ->select('a.*', 'b.name')
                        ->orderBy('a.created_at', 'desc')->get();

        return view('admin.reports.return_sale.return_sale_ledger_report')->with('ledger_report', $ledger_report);
    }

    public function expenses_report() {
        $expenses = DB::table('expenses')->select('id', 'expenses')->where('level', 0)->orderBy('expenses')->orderBy('expenses')->get();

        return view('admin.reports.expenses.expenses_report')->with('expenses', $expenses);
    }

    public function expense_ledger_report(Request $request) {
//        dd($request->all());
        $expense_report = DB::table('expenses as a')->where('level', 1);
        if (!empty($request->from_date) && !empty($request->to_date)) {
            $start_date = date("Y-m-d", strtotime($request->from_date));
            $end_date = date("Y-m-d", strtotime($request->to_date));
            $expense_report = $expense_report->whereDate('a.date', '>=', $start_date)
                    ->whereDate('a.date', '<=', $end_date);
        }
        if (!empty($request->expense_id)) {
            $expense_report = $expense_report->where('parent_id', $request->expense_id);
        }
        if (!empty($request->slug)) {
            $expense_report->where('expenses', $request->slug);
        }
        $expense_report = $expense_report->latest()->get();

//        $expense_report = $expense_report->get();
        return view('admin.reports.expenses.expense_ledger_report')->with('expense_report', $expense_report);
    }

    public function bank_report() {

        $banks = DB::table('bank_acc')->select('id', 'bank_name')->where('status',1)->orderBy('bank_name')->get();

        return view('admin.reports.banks.banks_report')->with('banks', $banks);
    }

    public function bank_ledger_report(Request $request) {
        if (!empty($request->bank_id)) {
            $start_date = date("Y-m-d", strtotime($request->from_date));
            $end_date = date("Y-m-d", strtotime($request->to_date));

            $bank_report = DB::table('bank_ledger as a')
                            ->join('bank_acc as b', 'b.id', '=', 'a.bank_id')
                            ->leftjoin('expenses', 'a.expense_id', '=', 'expenses.id')
                            ->whereDate('a.created_at', '>=', $start_date)
                            ->whereDate('a.created_at', '<=', $end_date)
                            ->select('a.*', 'b.bank_name', 'expenses.parent_id')
                            ->where('a.bank_id', $request->bank_id)
                            ->orderBy('a.id', 'asc')->get();
        } else {
            $bank_report = DB::table('bank_ledger as a')
                            ->join('bank_acc as b', 'b.id', '=', 'a.bank_id')
                            ->select('a.*', 'b.bank_name')
                            ->orderBy('a.id', 'asc')->get();
        }
        $amount = DB::table('bank_acc')->where('id', $request->bank_id)->pluck('current_amount');
        $companies = DB::table('company')->pluck('name', 'id');
        $suppliers = DB::table('supplier')->pluck('name', 'id');
        $expenses = DB::table('expenses')
                ->where('level', 0)
                ->pluck('expenses', 'id');

        return view('admin.reports.banks.bank_ledger_report')->with('bank_report', $bank_report)->with('companies', $companies)->with('amount', $amount)
                        ->with('suppliers', $suppliers)
                        ->with('expenses', $expenses);
    }

    public function employee_report() {
        $employess = DB::table('employees')->select('id', 'name')->orderBy('name')->get();

        return view('admin.reports.employees.employees_report')->with('employess', $employess);
    }

    public function employee_ledger_report(Request $request) {
        if (!empty($request->employee_id)) {
            $start_date = date("Y-m-d", strtotime($request->from_date));
            $end_date = date("Y-m-d", strtotime($request->to_date));

            $employee_report = DB::table('employee_ledger as a')
                            ->join('employees as b', 'b.id', '=', 'a.employee_id')
                            ->whereDate('a.created_at', '>=', $start_date)
                            ->whereDate('a.created_at', '<=', $end_date)
                            ->select('a.*', 'b.name')
                            ->where('a.employee_id', $request->employee_id)
                            ->orderBy('a.created_at', 'desc')->get();
        } else {
            $employee_report = DB::table('employee_ledger as a')
                            ->join('employees as b', 'b.id', '=', 'a.employee_id')
                            ->select('a.*', 'b.name')
                            ->orderBy('a.created_at', 'desc')->get();
        }

        return view('admin.reports.employees.employee_ledger_report')->with('employee_report', $employee_report);
    }

    public function cash_report() {
        return view('admin.reports.cash.cash_report');
    }

    public function cash_ledger_report(Request $request) {
//        dd('jhgj');
        $start_date = date("Y-m-d", strtotime($request->from_date));
        $end_date = date("Y-m-d", strtotime($request->to_date));

        $cash_report = DB::table('cash_in_hand')
                        ->select('expenses.parent_id', 'cash_in_hand.total_amount', 'cash_in_hand_ledger.*')
                        ->join('cash_in_hand_ledger', 'cash_in_hand_ledger.cash_in_hand_id', '=', 'cash_in_hand.id')
                        ->leftjoin('expenses', 'cash_in_hand_ledger.expense_id', '=', 'expenses.id')
                        ->where('cash_in_hand_ledger.cash_in_hand_id', 1)
                        ->whereDate('cash_in_hand_ledger.created_at', '>=', $start_date)
                        ->whereDate('cash_in_hand_ledger.created_at', '<=', $end_date)
                        ->where('cash_in_hand_ledger.cash_in_hand_id', 1)
                        ->orderBy('cash_in_hand_ledger.id', 'desc')->get();


//        dd($accountDetails);
        $companies = DB::table('company')->pluck('name', 'id');
        $employees = DB::table('employees')->pluck('name', 'id');
        $suppliers = DB::table('supplier')->pluck('name', 'id');
        $banks = DB::table('bank_acc')->pluck('bank_name', 'id');
        $expenses = DB::table('expenses')
                ->where('level', 0)
                ->pluck('expenses', 'id');

        return view('admin.reports.cash.cash_ledger_report')
                        ->with('bank_report', $cash_report)
                        ->with('companies', $companies)
                        ->with('employees', $employees)
                        ->with('suppliers', $suppliers)
                        ->with('banks', $banks)
                        ->with('expenses', $expenses);
    }

    public function area_person_report() {
        $area_person = DB::table('area_person')->select('id', 'name')->orderBy('name')->get();
        return view('admin.reports.area_persons.area_person_report')->with('area_person', $area_person);
    }

    public function area_person_ledger_report(Request $request) {


        if (!empty($request->area_person_id)) {
            $start_date = date("Y-m-d", strtotime($request->from_date));
            $end_date = date("Y-m-d", strtotime($request->to_date));

            $area_person = DB::table('company_ledger as a')
                    ->join('company as b', 'b.id', '=', 'a.company_id')
                    ->leftJoin('sales', 'sales.id', '=', 'a.invoice_no')
                    ->join('area_person as c', 'c.id', '=', 'b.area_person_id')

                    ->whereDate('a.created_at', '>=', $start_date)
                    ->whereDate('a.created_at', '<=', $end_date)
                    ->select('a.*', 'sales.vr_no','c.name', 'b.name as company_name')
                    ->where('b.area_person_id', $request->area_person_id)
                    ->orderBy('a.created_at', 'desc')
                    ->get();
                    // dd($area_person);

            $balance = DB::table('company as a')
                    ->select(DB::raw('SUM(a.dues) as total_dues'))
                    ->where('a.area_person_id', $request->area_person_id)
                    ->first();
//            dd($balance);
            $personDetails = DB::table('area_person')
                    ->where('id', $request->area_person_id)
                    ->first();
            $pName = $personDetails->name;
        } else {
            $area_person = DB::table('company_ledger as a')
                            ->join('company as b', 'b.id', '=', 'a.company_id')
                            ->join('area_person as c', 'c.id', '=', 'b.area_person_id')
                            ->select('a.*', 'c.name')
                            ->where('a.sale_man_id', 0)
                            ->orderBy('a.created_at', 'desc')->get();
            $balance = array();
            $pName = "All Salemans";
        }
        return view('admin.reports.area_persons.area_person_ledger_report')
                        ->with('area_person', $area_person)
                        ->with('pName', $pName)
                        ->with('balance', $balance);
    }

    public function current_summary() {
        $customers = DB::table('company')
                ->select(DB::raw('sum(dues) as customer_total'), 'company.type')
                ->groupBy(DB::raw('type'))
                ->get();

        $products = DB::table('products')
                ->select(DB::raw('product_cost_price * stock_qty as products_price'))
                ->get();

        $suppliers = DB::table('supplier')
                ->select(DB::raw('sum(dues) as supplier_total'), 'supplier.currency')
                ->groupBy(DB::raw('currency'))
                ->get();

        if (count($customers) == 0) {
            Session::flash('error', 'Customers OR Products OR Suppliers Data Not Found');
            return redirect()->back();
        }
        return view('admin.current_summary.current_summary')
                        ->with('customers', $customers)
                        ->with('products', $products)
                        ->with('suppliers', $suppliers);
    }

    public function daily_cash() {

        return view('admin.reports.supplier.daily_cash_report');
    }

    public function get_daily_cash(Request $request) {
        $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
        $to_date = date('Y-m-d 23:59:59', strtotime($request->from_date));

        $opening_bal = DB::table('cash_in_hand_ledger')
                ->where('created_at', '<', $from_date)
                ->orderBy('id', 'DESC')
                ->first();
        if ($opening_bal == null) {
            $opening_bal = DB::table('cash_in_hand_ledger')
                    ->first();
        }
        $supplier_bal = DB::table('cash_in_hand_ledger')->where('supplier_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->first();
//        dd($supplier_bal);
        $customer_bal = DB::table('cash_in_hand_ledger')->where('company_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(deposit),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->where('company_id', '!=', 0)
                ->first();
        $walking_customer_bal = DB::table('cash_in_hand_ledger')->where('company_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(deposit),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->where('company_id', '=', 0)
                ->first();
        $bank_bal = DB::table('cash_in_hand_ledger')->where('bank_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->first();
        $exp_bal = DB::table('cash_in_hand_ledger')->where('expense_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->first();

//        dd($qasar_bal);
        $emp_bal = DB::table('cash_in_hand_ledger')->where('employee_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->first();
        $cash_bal = DB::table('cash_in_hand_ledger')->where('transfer_from_bank_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(deposit),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->first();
        $zakat_bal = DB::table('cash_in_hand_ledger')->where('zakat_person_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->first();
//        dump($from_date);
//        dump($to_date);
        $closing_bal = DB::table('cash_in_hand_ledger')
                ->select('remaining')
                ->where('created_at', '>=', $from_date)
                ->where('created_at', '<=', $to_date)
                ->orderBy('id', 'DESC')
                ->first();
        if (!$closing_bal) {
            $closing_bal = DB::table('cash_in_hand_ledger')
                    ->select('remaining')
                    ->where('created_at', '<', $from_date)
                    ->orderBy('id', 'DESC')
                    ->first();
        }
//        dump($closing_bal);
//        exit;

        return view('admin.reports.supplier.get_daily_cash_report')->with(
                        [
                            'opening_bal' => $opening_bal,
                            'closing_bal' => $closing_bal,
                            'supplier_bal' => $supplier_bal,
                            'customer_bal' => $customer_bal,
                            'walking_customer_bal' => $walking_customer_bal,
                            'bank_bal' => $bank_bal,
                            'exp_bal' => $exp_bal,
                            'emp_bal' => $emp_bal,
                            'cash_bal' => $cash_bal,
                            'zakat_bal' => $zakat_bal]);
    }

    public function daily_bank() {
        $banks = DB::table('bank_acc')->orderBy('bank_name')->get();

        return view('admin.reports.supplier.daily_bank_report')
                        ->with('banks', $banks);
    }

    public function get_daily_bank(Request $request) {
        $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
        $to_date = date('Y-m-d 23:59:59', strtotime($request->from_date));
        $opening_bal = '';
        $closing_bal = '';
        if ($request->bank_id) {
            $opening_bal = DB::table('bank_ledger')
                    ->where('created_at', '<', $from_date)
                    ->where('bank_id', '=', $request->bank_id)
                    ->orderBy('id', 'DESC')
                    ->first();
            if ($opening_bal == null) {
                $opening_bal = DB::table('bank_ledger')
                        ->first();
            }
        }
        if ($request->bank_id) {
            $closing_bal = DB::table('bank_ledger')
                    ->select('remaining')
                    ->where('created_at', '>=', $from_date)
                    ->where('created_at', '<=', $to_date)
                    ->orderBy('id', 'DESC')
                    ->first();
            if (!$closing_bal) {
                $closing_bal = DB::table('bank_ledger')
                        ->select('remaining')
                        ->where('created_at', '<', $from_date)
                        ->orderBy('id', 'DESC')
                        ->first();
            }
        }
        $supplier_bal = DB::table('bank_ledger')->where('supplier_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date);
        $customer_bal = DB::table('bank_ledger')->where('company_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(deposit),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date)
                ->where('company_id', '!=', 0);
        $bank_bal = DB::table('bank_ledger')->where('transfer_to_cash', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date);
        $exp_bal = DB::table('bank_ledger')->where('expense_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date);
        $qasar_bal = DB::table('bank_ledger')->where('expense_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(case when withdrawal > 0 then withdrawal else 0 end),0) as total_amount'), DB::raw('COALESCE(SUM(case when withdrawal < 0 then withdrawal else 0 end),0) as returned'), DB::raw('COALESCE(SUM(withdrawal),0) as remaining'))
                ->join('expenses', 'expenses.id', '=', 'bank_ledger.expense_id')
                ->where('bank_ledger.created_at', '>', $from_date)
                ->where('bank_ledger.created_at', '<', $to_date)
                ->where('parent_id', 151);
        if ($request->bank_id) {
            $qasar_bal = $qasar_bal->where('bank_ledger.bank_id', $request->bank_id);
        }
        $qasar_bal = $qasar_bal->first();
//        dd($qasar_bal);

        $emp_bal = DB::table('bank_ledger')->where('employee_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date);
        $cash_bal = DB::table('bank_ledger')->where('transfer_from_cash', '!=', null)
                ->select(DB::raw('COALESCE(SUM(deposit),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date);
        $zakat_bal = DB::table('bank_ledger')->where('zakat_person_id', '!=', null)
                ->select(DB::raw('COALESCE(SUM(withdrawal),0) as total_amount'))
                ->where('created_at', '>', $from_date)
                ->where('created_at', '<', $to_date);
        if ($request->bank_id) {

            $supplier_bal = $supplier_bal->where('bank_id', $request->bank_id);
            $customer_bal = $customer_bal->where('bank_id', $request->bank_id);
            $bank_bal = $bank_bal->where('bank_id', $request->bank_id);
            $exp_bal = $exp_bal->where('bank_id', $request->bank_id);
            $emp_bal = $emp_bal->where('bank_id', $request->bank_id);
            $cash_bal = $cash_bal->where('bank_id', $request->bank_id);
            $zakat_bal = $zakat_bal->where('bank_id', $request->bank_id);
        }
        return view('admin.reports.supplier.get_daily_bank_report')->with(
                        [
                            'opening_bal' => $opening_bal,
                            'closing_bal' => $closing_bal,
                            'supplier_bal' => $supplier_bal->first(),
                            'customer_bal' => $customer_bal->first(),
                            'bank_bal' => $bank_bal->first(),
                            'qasar_bal' => $qasar_bal,
                            'exp_bal' => $exp_bal->first(),
                            'emp_bal' => $emp_bal->first(),
                            'cash_bal' => $cash_bal->first(),
                            'zakat_bal' => $zakat_bal->first()]);
    }

    public function product_general_report() {
        $products = DB::table('products')->select('id', 'product_name')->orderBy('product_name')->get();
        $suppliers = DB::table('supplier')->select('id', 'name')->orderBy('name')->get();

        return view('admin.reports.product.product_general_report')
                        ->with('suppliers', $suppliers)
                        ->with('products', $products);
    }

    public function product_quotation() {
        $products = DB::table('products')->select('id', 'product_name')->where('status','=',1)->orderBy('product_name')->get();
        $suppliers = DB::table('supplier')->select('id', 'name')->orderBy('name')->get();

        return view('admin.reports.product.product_quotation')
                        ->with('suppliers', $suppliers)
                        ->with('products', $products);
    }

//    get_product_general_report
    public function get_product_general_report(Request $request) {
//        dd($request->all());
        $products = DB::table('products')
                ->select('products.*', 'supplier.name')
                ->leftJoin('supplier', 'supplier.id', '=', 'products.supplier_id');
        if ($request->product_id) {
            $products->where('products.id', $request->product_id);
        }
        if ($request->supplier_id) {
            $products->where('supplier_id', $request->supplier_id);
        }
        $products = $products->orderBy('product_name')->get();
//        dd($products);
        return view('admin.reports.product.get_product_general_report')
                        ->with('products', $products);
    }

    public function get_product_quotation(Request $request) {
        $products = DB::table('products')
                ->select('products.*', 'supplier.name')
                ->leftJoin('supplier', 'supplier.id', '=', 'products.supplier_id')->where('products.status','=',1);
        if ($request->product_id) {
            $products->whereIn('products.id', $request->product_id);
        }
        if ($request->supplier_id) {
            $products->where('supplier_id', $request->supplier_id);
        }
        $products = $products->orderBy('product_name')->get();
//        dd($products);
        return view('admin.reports.product.get_product_quotation')
                        ->with('products', $products);
    }

    public function profit() {
        $products = DB::table('company')->select('id', 'name')->orderBy('name')->get();

        return view('admin.reports.profit_report.profit')
                        ->with('products', $products);
    }

    public function get_profit_report(Request $request) {

        /*
         * tcp= Calculate Total Cost Prices of the products
         * tsp= Calculate Total sale Prices of the products
         * $t_qty= Calculate Total quantity Sold in that time period
         * $tSaleDisc= calculate discount from sale table which is total bill discount+ sale details table which is percentage discount.
         *
         */
        $tcp = DB::raw("(select SUM(total_cost_price) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as tcp");
        $tsp = DB::raw("(select SUM(actual_price) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as tsp");
        $t_qty = DB::raw("(select SUM(qty) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as t_qty");
        $tSaleDisc = DB::raw("(select SUM(sale_details.total_price) from sale_details
where products.id=sale_details.product_id and DATE(sale_details.created_at) >='" . $request->from_date . "' and DATE(sale_details.created_at)<='" . $request->to_date . "') as total_price");
        $get_sale = DB::table('products')
                ->select('products.product_name', 'products.id', $tcp, $tsp, $t_qty, $tSaleDisc)->where('products.status',1)
                ->groupBy('products.id');
        $exp = DB::table('expenses as p')
                ->select('p.*', DB::raw("(select SUM(amount) as exp_amount from expenses where expenses.parent_id=p.id and date >='" . $request->from_date . "' and date<='" . $request->to_date . "') as exp_amount"))
                ->where('p.level', 0);
//        dd($exp->get());
        $supplier_dtl = DB::table('supplier_ledger')
                ->select(DB::raw('COALESCE(SUM(dtl),0) as total'));
        $customer_discount = DB::table('company_ledger')
                ->select(DB::raw('COALESCE(SUM(discount),0) as total'));

        /*
         * Employees Sallaries
         */
        $employeeSallaries = DB::table('employee_ledger')
                ->join('employees', 'employees.id', '=', 'employee_ledger.employee_id')
                ->select('employees.name', 'employee_ledger.payment', 'employee_ledger.advance')
                ->where(
                function ($query) {
            $query->where('payment', '!=', 0)
            ->orWhere('advance', '!=', 0);
        });


        $sales=DB::select("select sum(discount) as discount from sales where DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "'");

        if (!empty($request->from_date) && !empty($request->to_date)) {
            $supplier_dtl = $supplier_dtl->whereDate('created_at', '>=', $request->from_date);
            $supplier_dtl = $supplier_dtl->whereDate('created_at', '<=', $request->to_date);

            $customer_discount = $customer_discount->whereDate('created_at', '>=', $request->from_date);
            $customer_discount = $customer_discount->whereDate('created_at', '<=', $request->to_date);
            /*
             * Employees Checks
             */
            $employeeSallaries = $employeeSallaries->whereDate('employee_ledger.created_at', '>=', $request->from_date);
            $employeeSallaries = $employeeSallaries->whereDate('employee_ledger.created_at', '<=', $request->to_date);
        }
        $customer_discount = $customer_discount->first();
        $supplier_dtl = $supplier_dtl->first();

        $products = $get_sale->get();
        $exp = $exp->get();
        $employeeSallaries = $employeeSallaries->get();

        return view('admin.reports.profit_report.get_profit_report')
                        ->with('exp', $exp)
                        ->with('customer_discount', $customer_discount)
                        ->with('supplier_dtl', $supplier_dtl)
                        ->with('products', $products)
                        ->with('from_date', $request->from_date)
                        ->with('to_date', $request->to_date)
                        ->with('sales', $sales)
                        ->with('employeeSallaries', $employeeSallaries);
    }

    public function item_profit(Request $request) {
//        dd($request->all());
        $purchase_ledger = '';
        if (!empty($request->all())) {
            if (!empty($request->product_id)) {

                $purchase_ledger = DB::table('products')
                                ->select('products.product_name', 'products.id', DB::raw("(select SUM(total_cost_price) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as tcp"), DB::raw("(select SUM(total_price) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as tsp"), DB::raw("(select SUM(qty) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as t_qty"))
                                ->where('id', $request->product_id)
                                ->groupBy('products.id')->get();
            } else {
                $purchase_ledger = DB::table('products')
                                ->select('products.product_name', 'products.id', DB::raw("(select SUM(total_cost_price) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as tcp"), DB::raw("(select SUM(total_price) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as tsp"), DB::raw("(select SUM(qty) from sale_details where products.id=sale_details.product_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "') as t_qty"))
                                ->groupBy('products.id')->get();
            }
        }
        $products = DB::table('products')->select('id', 'product_name')->orderBy('product_name')->get();

        return view('admin.reports.profit_report.item_profit')
                        ->with('purchase_ledger', $purchase_ledger)
                        ->with('products', $products)
                        ->with('from_date', $request->from_date)
                        ->with('to_date', $request->to_date);
    }
    
      public function discount_profit(Request $request) {
//        dd($request->all());
        $purchase_ledger = '';
        if (!empty($request->all())) {
          
          $purchase_ledger= DB::table('company')
            ->join('company_ledger', 'company.id', '=', 'company_ledger.company_id')
            
            ->select('company.name', 'company_ledger.discount','company_ledger.created_at')
            ->where('company_ledger.created_At', '>', $request->from_date)
             ->where('company_ledger.created_At', '<', $request->to_date)
             ->where('company_ledger.discount', '>', 0)
            ->get();
                // $purchase_ledger = DB::table('company')
                //                 ->select('company.name', 'company.id', DB::raw("(select discount from company_ledger where company.id=company_ledger.company_id and DATE(created_at) >='" . $request->from_date . "' and DATE(created_at)<='" . $request->to_date . "')"))
                //               ->get();
         
        }
       
        return view('admin.reports.profit_report.discount_profit')
                         ->with('purchase_ledger', $purchase_ledger)
                        ->with('from_date', $request->from_date)
                        ->with('to_date', $request->to_date);
    }

    public function company_current_status() {
        $from_date = date('Y-m-d');
        $opening_bal = DB::table('cash_in_hand')
                ->where('created_at', '<', $from_date)
                ->first();
        $assets = DB::table('assets')
                ->select(DB::raw('SUM(price) as assets'))
                ->first();
        $banks = DB::table('bank_acc')
                ->select(DB::raw('SUM(current_amount) as amount'))
                ->first();
        $stock = DB::table('products')
                ->select(DB::raw('SUM(product_cost_price * stock_qty) as amount'))
                ->first();
        $customers_reciveable = DB::table('company')
                ->select(DB::raw('SUM(dues) as amount'))
                ->first();
        $s_payables = DB::table('supplier')
                ->select(DB::raw('SUM(dues) as amount'))
                ->first();
        return view('admin.reports.supplier.get_daily_cash_report2')->with(
                        [
                            'opening_bal' => $opening_bal,
                            'assets' => $assets,
                            'stock' => $stock,
                            'banks' => $banks,
                            's_payables' => $s_payables,
                            'customers_reciveable' => $customers_reciveable,
        ]);
    }

}
