<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ReturnPurchaseController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $time = date('h:i:s');
            $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));
            if (count($request->product_id) < 0 || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }

            DB::table('return_purchase')->insert([
                'vr_no' => $request->vr_no,
                'supplier_id' => $request->supplier_id,
                'total_amount' => $request->line_cost_total,
                'discount' => $request->discount,
                'net_amount' => $request->net_amount,
                'paid' => $request->paidamount,
                'balance' => $request->due_amount,
                'remarks' => $request->remarks,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $last_id = DB::getPdo()->lastInsertId();
            if ($request->paidamount > 0) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount + $request->paidamount,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'invoice_id' => $last_id,
                    'supplier_id' => $request->supplier_id,
                    'total_amount' => $cash->total_amount + $request->paidamount,
                    'deposit' => $request->paidamount,
                    'withdrawal' => 0,
                    'remaining' => $cash->total_amount + $request->paidamount,
                    'description' => $request->remarks,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
            }

            $product_id = $request->product_id;
            $unit_price = $request->unit_price;
            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $discount = $request->discount_per_item;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);

            for ($i = $count - 1; $i >= 0; $i--) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();
                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'discount' => $discount[$i],
                    'actual_amount' => $unit_price[$i] * $qty[$i],
                    'unit_price' => $unit_price[$i],
                    'total_price' => $line_cost[$i],
                    'return_purchase_id' => $last_id,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                );

                $data1 = array(
                    'product_id' => $product_id[$i],
                    'invoice_id' => $last_id,
                    'return_purchase_id' => $last_id,
                    'description' => $request->remarks,
                    'total_qty' => $get_record->stock_qty,
                    'purchased_qty' => 0,
                    'return_purchased' => $qty[$i],
                    'sale' => 0,
                    'return_sale' => 0,
                    'remaning' => $get_record->stock_qty - $qty[$i],
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                );

                $insert_data[] = $data;
                $insert_data1[] = $data1;

                $update_qty = $get_record->stock_qty - $qty[$i];

                DB::table('products')->where('id', $product_id[$i])->update([
                    'stock_qty' => $update_qty
                ]);
            }

            DB::table('return_purchase_details')->insert($insert_data);
            DB::table('product_ledger')->insert($insert_data1);
            // dd([$insert_data, $insert_data1]);

            DB::table('supplier')->where('id', $request->supplier_id)->update([
                'dues' => $request->dues - $request->due_amount
            ]);

            DB::table('supplier_ledger')->insert([
                'supplier_id' => $request->supplier_id,
                'invoice_id' => $last_id,
                'narration' => $request->remarks,
                'purchase' => 0,
                'return_purchase' => $request->net_amount,
                'payment' => $request->paidamount,
                'balance' => $request->dues - $request->due_amount,
                'currency' => 'pkr',
                'currency_rate' => Session::get('today_currency'),
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            DB::commit();
            Session::flash('success', 'Return Purchased Transaction Completed Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $supplier = DB::table('supplier')->where('id', $id)->first();
        $products = DB::table('products')
                ->select('id', 'qty', 'product_name')
//                ->where('supplier_id', $id)
                ->get();

        return view('admin.supplier.return_supplier_purchase')
                        ->with('supplier', $supplier)
                        ->with('products', $products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $supplier_purchases = DB::table('return_purchase')
                ->select('return_purchase.id', 'return_purchase.total_amount', 'return_purchase.discount', 'return_purchase.net_amount', 'return_purchase.paid', 'return_purchase.created_at', 'supplier.name')
                ->join('supplier', 'supplier.id', '=', 'return_purchase.supplier_id')
                ->where('return_purchase.supplier_id', $id)
                ->get();
//dd($id);
        return view('admin.supplier.return_purchase_ledger')
                        ->with('supplier_id', $id)
                        ->with('purchase_ledger', $supplier_purchases);
    }

    public function return_purchase_details($id, $redirect) {
        $purchase_ledger = DB::table('return_purchase')
                ->select('products.product_name', 'return_purchase_details.discount as item_discount', 'return_purchase_details.actual_amount', 'return_purchase_details.total_price', 'products.qty as pack', 'products.product_code', 'return_purchase.total_amount', 'return_purchase.discount', 'return_purchase.net_amount', 'return_purchase.paid', 'return_purchase_details.unit_price', 'return_purchase_details.qty', 'return_purchase.remarks', 'return_purchase_details.created_at')
                ->join('return_purchase_details', 'return_purchase_details.return_purchase_id', '=', 'return_purchase.id')
                ->join('products', 'return_purchase_details.product_id', '=', 'products.id')
                ->where('return_purchase_details.return_purchase_id', $id)
                ->get();
//        dd($purchase_ledger);

        return view('admin.supplier.return_purchase_detail')
                        ->with('redirect', $redirect)
                        ->with('purchase_ledger', $purchase_ledger);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
