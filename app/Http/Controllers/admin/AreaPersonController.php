<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Validator;

class AreaPersonController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cities = DB::table('cities')->get();
        $status = 1;
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        }
        $employees = DB::table('area_person')
                        ->where('status', $status)
                        ->orderBy('id', 'desc')->get();
        return view('admin.area_person.index')
                        ->with('cities', $cities)
                        ->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->city);
        $validator = Validator::make($request->all(), [
                    'email' => "unique:area_person,email",
        ]);
        if ($validator->fails()) {
//         dd($validator->errors());
            Session::flash('error', $validator->errors());
            return redirect()->back();
        }
        DB::table('area_person')->insert([
            'name' => $request->name,
            'cnic' => $request->cnic,
            'contact' => $request->contact,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'city' => $request->city,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Area Person Added successfully');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $employee = DB::table('area_person')
                        ->where('id', '=', $id)
                        ->orderBy('id', 'desc')->first();
        $cities = DB::table('cities')->get();
//        dd($employee);
        return view('admin.area_person.edit')
                        ->with('cities', $cities)
                        ->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
//        dd($request->all());
        $save = [
            'name' => $request->name,
            'cnic' => $request->cnic,
            'contact' => $request->contact,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        if ($request->pass) {
            $save['password'] = bcrypt($request->pass);
        }
//        dd($save);
        DB::table('area_person')->where('id', $id)->update($save);

        Session::flash('success', 'Area Person Updated Successfully');
        return redirect()->route('area_person.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('area_person')->where('id', $id)->delete();
        Session::flash('success', 'Area Person Deleted Successfully');
        return redirect()->back();
    }

    public function sale($id) {

        $supplier = DB::table('area_person')->where('id', $id)->first();
        $products = DB::table('products')->select('id', 'qty', 'product_name')->get();
        return view('admin.area_person.sales')
                        ->with('supplier', $supplier)
                        ->with('products', $products);
    }

    public function store_sale(Request $request, $id) {

        $time = date('h:i:s');
        $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));




        if (empty($request->product_id)) {
            Session::flash('error', 'Please select atleast 1 product');
            return redirect()->back();
        }
        $saleman_sale = array();
        $saleman_sale_details = array();
        foreach ($request->product_id as $key => $product):
//            dd($product);
            $area_person_sales = DB::table('area_person_sales')->where(['sale_man_id' => $id, 'product_id' => $product]);
            $get = $area_person_sales->first();
            if ($get) {
                $area_person_sales->update([
                    'qty' => $get->qty + $request->qty[$key],
                    'balance' => $get->balance + ($request->retail_price[$key] * $request->qty[$key]),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                DB::table('area_person_ledger')->insert([
                    'saleman_id' => $id,
                    'product_id' => $product,
                    'total_stock' => $get->qty,
                    'assign_stock' => $request->qty[$key],
                    'remaining' => $get->qty + $request->qty[$key],
                    'sale_stock' => 0,
                    'return_stock' => 0,
                    'description' => $request->description,
                    'invoice_no' =>$request->vr_no,
                    'date' => date('Y-m-d H:i:s'),
                ]);

                $last_id = $get->id;
            } else {
                DB::table('area_person_sales')->insert([
                    'sale_man_id' => $id,
                    'product_id' => $product,
                    'qty' => $request->qty[$key],
                    'total_amount' => 0,
                    'discount' => 0,
                    'net_amount' => 0,
                    'balance' => $request->retail_price[$key] * $request->qty[$key],
                    'narration' => $request->description,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                ]);

                DB::table('area_person_ledger')->insert([
                    'saleman_id' => $id,
                    'product_id' => $product,
                    'total_stock' => 0,
                    'assign_stock' => $request->qty[$key],
                    'remaining' => $request->qty[$key],
                    'sale_stock' => 0,
                    'return_stock' => 0,
                    'description' => 'Opening Stock',
                    'invoice_no' =>0,
                    'date' => date('Y-m-d H:i:s'),
                ]);

                $last_id = DB::getPdo()->lastInsertId();
            }

            $getp = DB::table('products')->where('id', $product)->first();

            $data1 = array(
                'product_id' => $product,
                'area_person_sale_id' => $last_id,
                'unit_price' => $request->retail_price[$key],
                'actual_price' => $getp->product_cost_price,
                'qty' => $request->qty[$key],
                'discount' => 0,
                'vr_no' => $request->vr_no,
                'description' => $request->description,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            );

            $saleman_sale[] = $data1;
        endforeach;
//        dd($saleman_sale);

        DB::table('area_person_sale_details')->insert($saleman_sale);
        Session::flash('success', 'Sale Transaction Completed Successfully');
        if (isset($request->print)) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function sales_view($id) {
        $purchase_ledger = DB::table('area_person_sales')
                ->select('area_person.name', 'area_person_sales.*', 'products.id', 'products.product_name')
                ->join('area_person', 'area_person.id', '=', 'area_person_sales.sale_man_id')
                ->join('products', 'area_person_sales.product_id', '=', 'products.id')
                ->where('sale_man_id', $id)
                ->get();
//        dd($purchase_ledger);
        return view('admin.area_person.sale_details')->with('purchase_ledger', $purchase_ledger);
    }

    public function invoices($id) {

        $purchase_ledger = DB::table('sales')
                ->select('area_person.name as area_person_name', 'company.name', 'sales.*')
                ->join('company', 'company.id', '=', 'sales.company_id')
                ->join('area_person', 'area_person.id', '=', 'sales.sale_man_id')
                ->where('sales.sale_man_id', $id)
                ->get();
//        dd($purchase_ledger);
        return view('admin.area_person.invoices')->with('purchase_ledger', $purchase_ledger);
    }

    public function get_product_list($id) {
        $getproducts = DB::table('area_person_sales')
                ->select('products.id', 'products.product_name', 'area_person_sales.qty')
                ->join('products', 'products.id', '=', 'area_person_sales.product_id')
                ->where('area_person_sales.qty', '>', 0)
                ->where('area_person_sales.sale_man_id', $id);
        if ($id == 0) {
            $getproducts = DB::table('products')
                    ->select('products.id', 'products.product_name', 'stock_qty as qty');
        }
        $getproducts = $getproducts->get();
//        dd($getproducts);
        echo json_encode($getproducts);
//        exit;
    }

    public function dashboard() {
        $user_id = auth()->user()->id;
        $count_products = DB::table('sales')->where('sale_man_id', $user_id)->count();
        $count_company = DB::table('company')->count();

        $stock = DB::table('area_person_sales')
                        ->select(DB::raw('SUM(qty) as "stock"'))
                        ->where('sale_man_id', $user_id)->first();
//        dd($stock);
        return view('admin.area_person.dashboard')
                        ->with('product', $count_products)
                        ->with('stock', $stock)
                        ->with('company', $count_company);
    }

    public function stock() {
        $user_id = auth()->user()->id;
        $cities = DB::table('cities')->get();
        $employees = DB::table('area_person_sales')
                ->select('area_person_sales.*', 'products.product_name', 'products.id as p_id')
                ->join('products', 'products.id', '=', 'area_person_sales.product_id')
                ->where('sale_man_id', $user_id)
                ->get();
        return view('admin.area_person.stock')
                        ->with('cities', $cities)
                        ->with('employees', $employees);
    }

    public function stock_details($id) {

        $employees = DB::table('area_person_sale_details')
                ->select('area_person_sale_details.*', 'products.product_name')
                ->join('products', 'products.id', '=', 'area_person_sale_details.product_id')
                ->where('area_person_sale_id', $id)
                ->get();

//        dd($employees);
        return view('admin.area_person.stock_details')
                        ->with('employees', $employees);
    }

    public function admin_stock_details($id, $red) {

        $employees = DB::table('area_person_ledger')
                ->where('product_id', $id)
                ->where('saleman_id', $red)
                ->get();
        return view('admin.area_person.admin_stock_ledger')
                        ->with('employees', $employees);
    }

    public function saleman_sale() {
        $id = auth()->user()->id;
        $salesman = DB::table('company')->get();
        $products = DB::table('area_person_sales')
                ->select('products.id', 'area_person_sales.qty', 'products.product_name')
                ->join('products', 'products.id', '=', 'area_person_sales.product_id')
                ->where('area_person_sales.qty', '!=', 0)
                ->where('area_person_sales.sale_man_id', $id)
                ->get();
//        dd($products);
        return view('admin.area_person.saleman_sales')
                        ->with('salesman', $salesman)
                        ->with('products', $products);
    }

    public function store_saleman_sale(Request $request) {

        try {
            DB::beginTransaction();


            if (count($request->product_id) < 0 || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }
            $request->salesman = auth()->user()->id;
            $request->supplier_id = $request->customer_id;

//        dd($request->salesman);
            DB::table('sales')->insert([
                'vr_no' => $request->vr_no,
                'sale_man_id' => $request->salesman,
                'company_id' => $request->supplier_id,
                'sale_type' => $request->sale_type,
                'total_amount' => $request->line_cost_total,
                'discount' => $request->discount,
                'net_amount' => $request->net_amount,
                'paid' => $request->paidamount,
                'balance' => $request->due_amount,
                'narration' => $request->description,
                'created_by' => auth()->user()->id,
                'table' => 'salesman',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $last_id = DB::getPdo()->lastInsertId();



            if (!empty($request->salesman) && $request->salesman != 0) {
                if (!empty($request->product_id)) {
                    foreach ($request->product_id as $k => $p):
                        $s_products = DB::table('area_person_sales')
                                ->where('sale_man_id', $request->salesman)
                                ->where('product_id', $p);
                        $get_p = $s_products->first();
                        $s_products->update(['qty' => $get_p->qty - $request->qty[$k]]);


                        DB::table('area_person_ledger')->insert([
                            'saleman_id' => $request->salesman,
                            'product_id' => $p,
                            'total_stock' => $get_p->qty,
                            'assign_stock' => 0,
                            'remaining' => $get_p->qty - $request->qty[$k],
                            'sale_stock' => $request->qty[$k],
                            'return_stock' => 0,
                            'description' => $request->description,
                            'invoice_no' => $last_id,
                            'date' => date('Y-m-d H:i:s'),
                        ]);
                    endforeach;
                }
            }


            $product_id = $request->product_id;
            if ($request->paidamount > 0) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount + $request->paidamount,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'invoice_id' => $last_id,
                    'company_id' => $request->supplier_id,
                    'total_amount' => $cash->total_amount + $request->paidamount,
                    'deposit' => $request->paidamount,
                    'withdrawal' => 0,
                    'remaining' => $cash->total_amount + $request->paidamount,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
            }


            if ($request->sale_type == 'retail') {
                $request->unit_price = $request->retail_price;
            } else {
                $request->unit_price = $request->whole_price;
            }

            $unit_price = $request->unit_price;

            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $discount = $request->discount_per_item;
            $whole = $request->whole_price;
            $retail = $request->retail_price;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);
            for ($i = $count - 1; $i >= 0; $i--) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();
                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'discount' => $discount[$i],
                    'actual_price' => $unit_price[$i] * $qty[$i],
                    'unit_price' => $unit_price[$i],
                    'total_price' => $line_cost[$i],
                    'sale_id' => $last_id,
                    'unit_cost_price' => $get_record->product_cost_price,
                    'total_cost_price' => $get_record->product_cost_price * $qty[$i],
                    'description' => '',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                $data1 = array(
                    'product_id' => $product_id[$i],
                    'invoice_id' => $last_id,
                    'sale_id' => $last_id,
                    'description' => $request->description,
                    'total_qty' => $get_record->stock_qty,
                    'purchased_qty' => 0,
                    'return_purchased' => 0,
                    'sale' => $qty[$i],
                    'return_sale' => 0,
                    'remaning' => $get_record->stock_qty - $qty[$i],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $insert_data[] = $data;
                $insert_data1[] = $data1;

                $sale_details[] = array('product' => $get_record->product_name, 'qty' => $qty[$i], 'unit_price' => round($line_cost[$i] / $qty[$i], 2), 'cotton' => round($qty[$i] / $get_record->qty_per_cotton, 1));
                $update_qty = $get_record->stock_qty - $qty[$i];
                $product_record = $product_record->update([
                    'stock_qty' => $update_qty,
                ]);
            }

            DB::table('sale_details')->insert($insert_data);
            DB::table('product_ledger')->insert($insert_data1);
            $customer = DB::table('company')->where('id', $request->supplier_id)->first();
            $dues_remaining = $request->due_amount + $customer->dues;
            DB::table('company')->where('id', $request->supplier_id)->update([
                'dues' => $request->due_amount + $customer->dues,
            ]);
            $saleman_id = auth()->user()->id;

            DB::table('company_ledger')->insert([
                'company_id' => $request->supplier_id,
                'sale_man_id' => $saleman_id,
                'invoice_no' => $last_id,
                'narration' => $request->description,
                'sale' => $request->net_amount,
                'return_sale' => 0,
                'payment' => $request->paidamount,
                'sale_details' => json_encode($sale_details),
                'remaining' => $dues_remaining,
                'created_by' => auth()->user()->id,
                'table' => 'salesman',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            Session::flash('success', 'Sale Transaction Completed Successfully');
            DB::commit();
            if (isset($request->print)) {
                return redirect()->route('sale/print_invoice', ['id' => $last_id]);
            } else {
                return redirect()->back();
            }
            return redirect()->route('companies');
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function sale_details($c_id) {
        $id = auth()->user()->id;
        $purchase_ledger = DB::table('sales')
                ->select('company.name', 'sales.*')
                ->join('company', 'company.id', '=', 'sales.company_id')
                ->where('sales.sale_man_id', $id)
                ->where('sales.company_id', $c_id)
                ->get();
//        dd($purchase_ledger);
        return view('admin.area_person.saleman_sale_details')->with('purchase_ledger', $purchase_ledger);
    }

    public function customers() {
//        exit;
        $customers = DB::table('company')
                        ->orderBy('id', 'desc')->get();
        return view('admin.area_person.customers')
                        ->with('customers', $customers);
    }

    public function customer_payment($id) {
        $supplier = DB::table('company')->where('id', $id)->first();
        return view('admin.area_person.customer_payment')->with('supplier', $supplier);
    }

    public function get_payment_type(Request $request) {
        if ($request->pay_type == 'bank') {

            $banks = DB::table('bank_acc')->where('status', 1)->get();

            echo ' <div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Select Bank</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="bank_name" id="bank_name" required>';
            foreach ($banks as $bank) :
                echo '<option value="' . $bank->id . '"> ' . $bank->bank_name . ' | Acc No:' . $bank->account_no . '</option>';
            endforeach;
            echo ' </select>
                </div>
            </div>';
            echo '
                </div>';
        }

        if ($request->pay_type == 'cash') {

            $cash = DB::table('cash_in_hand')->first();

            echo ' <div class="form-group row d-none">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Cash in Hand</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <input class="form-control" type="text" name="cash" value="' . $cash->total_amount . '" required readonly>
                        </div>
                    </div>';
            echo '
                  </div>';
        }
        if ($request->pay_type == 'cheque') {
            $banks = DB::table('bank_acc')->get();

            echo ' <div class="form-group row">
                <label class="col-12 col-sm-3 col-form-label text-sm-right">Transfer From</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control" name="tranfer_to" id="transfer_to" required>
                    <option value="">Transfer cheque amount from</option>
                    <option value="bank">Bank</option>
                    <option value="cash">Cash</option>
                    </select>
                </div>
            </div>';
        }
    }

    public function save_payment(Request $request) {


        try {
            DB::beginTransaction();




            $saleman_id = auth()->user()->id;
            $new_amount = $request->pay;
            if ($request->discount != '' || $request->discount != 0) {
                $new_amount = $request->discount + $request->pay;
            }
//        dd($new_amount);
            if ($request->pay_from == 'bank') {
                $customer = DB::table('company')->where('id', $request->customer_id)->first();

                $customer_bal = $customer->dues - $new_amount;

                DB::table('company')->where('id', $request->customer_id)->update([
                    'dues' => $customer_bal,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                DB::table('company_ledger')->insert([
                    'company_id' => $request->customer_id,
                    'sale_man_id' => $saleman_id,
                    'bank_id' => $request->bank_name,
                    'invoice_no' => 0,
                    'narration' => $request->description,
                    'sale' => 0,
                    'return_sale' => 0,
                    'payment' => $request->pay,
                    'discount' => $request->discount,
                    'remaining' => $customer_bal,
                    'created_by' => auth()->user()->id,
                    'table' => 'salesman',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                $last_id = DB::getPdo()->lastInsertId();


                $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();
                $add_bal = $bank->current_amount + $request->pay;

                DB::table('bank_acc')->where('id', $request->bank_name)->update([
                    'current_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('bank_ledger')->insert([
                    'bank_id' => $request->bank_name,
                    'company_id' => $request->customer_id,
                    'company_ledger_id' => $last_id,
                    'total_amount' => $add_bal,
                    'deposit' => $request->pay,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);


                DB::table('journal_vocher')->insert([
                    'u_id' => $request->customer_id,
                    'head' => "customer",
                    'sub_head' => "customer",
                    'total' => $bank->current_amount,
                    'pay' => $request->pay,
                    'balance' => $add_bal,
                    'description' => $request->description,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            if ($request->pay_from == 'cash') {
// dd($request->all());
                $customer = DB::table('company')->where('id', $request->customer_id)->first();
                $customer_bal = $customer->dues - $new_amount;

                DB::table('company')->where('id', $request->customer_id)->update([
                    'dues' => $customer_bal,
                    'last_payment' => date('Y-m-d'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                DB::table('company_ledger')->insert([
                    'company_id' => $request->customer_id,
                    'sale_man_id' => $saleman_id,
                    'invoice_no' => 0,
                    'narration' => $request->description,
                    'sale' => 0,
                    'return_sale' => 0,
                    'payment' => $request->pay,
                    'discount' => $request->discount,
                    'remaining' => $customer_bal,
                    'created_by' => auth()->user()->id,
                    'table' => 'salesman',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                $last_id = DB::getPdo()->lastInsertId();

                $cash = DB::table('cash_in_hand')->first();


                $add_bal = $cash->total_amount + $request->pay;
                DB::table('cash_in_hand')->update([
                    'total_amount' => $add_bal,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'company_id' => $request->customer_id,
                    'company_ledger_id' => $last_id,
                    'total_amount' => $add_bal,
                    'deposit' => $request->pay,
                    'withdrawal' => 0,
                    'remaining' => $add_bal,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

                DB::table('journal_vocher')->insert([
                    'u_id' => $request->customer_id,
                    'head' => "customer",
                    'sub_head' => "customer",
                    'total' => $cash->total_amount,
                    'pay' => $request->pay,
                    'balance' => $add_bal,
                    'description' => $request->description,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }

            if ($request->pay_from == 'cheque') {
                $arrayToSave = array();
                if ($request->tranfer_to == 'bank') {
                    $arrayToSave['bank_id'] = $request->bank_name;
                    $arrayToSave['transfer_to'] = 'bank';
                }
                if ($request->tranfer_to == 'cash') {
                    //cash logic goes here
                    $arrayToSave['transfer_to'] = 'cash';
                }
                $arrayToSave['customer_id'] = $request->customer_id;
                $arrayToSave['cheque_no'] = $request->cheque_no;
                $arrayToSave['amount'] = $request->pay;
                $arrayToSave['release_date'] = $request->cheque_date;
                $arrayToSave['created_at'] = date('Y-m-d H:i:s');
                $arrayToSave['updated_at'] = date('Y-m-d H:i:s');
//            dd($arrayToSave);
                DB::table('cheques')->insert($arrayToSave);
            }
            Session::flash('success', 'Payment Transaction Completed Successfully');

            DB::commit();
            return redirect()->route('saleman.customers');
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function customer_ledger($id) {
        $saleman = auth()->user()->id;
//        dd($saleman);
        $customer = DB::table('company')
                ->select('company.name', 'company_ledger.*', 'company.id as company_id')
                ->join('company_ledger', 'company_ledger.company_id', '=', 'company.id')
                ->where('company_ledger.company_id', $id)
                ->where('company_ledger.sale_man_id', $saleman)
                ->get();
        return view('admin.area_person.customer_ledger')->with('customer', $customer);
    }

    public function stock_sale_details($id, $redirect) {

        $fromDate = date('Y-m-d', strtotime('-1 month'));
        $toDate = date('Y-m-d');
        if (isset($_GET['from_date']) && isset($_GET['to_date'])) {
            $fromDate = $_GET['from_date'];
            $toDate = $_GET['to_date'];
//        dd($fromDate);
        }

        $purchase_ledger = DB::table('sales')
                ->select('products.product_name', 'company.name as company_id', 'products.qty as pack', 'products.product_code', 'sales.total_amount', 'sales.net_amount', 'sales.paid', 'sale_details.unit_price', 'sale_details.total_price', 'sale_details.actual_price', 'sale_details.discount', 'sale_details.qty', 'sale_details.created_at', 'sales.vr_no', 'sales.narration')
                ->join('company', 'company.id', '=', 'sales.company_id')
                ->join('sale_details', 'sale_details.sale_id', '=', 'sales.id')
                ->join('products', 'sale_details.product_id', '=', 'products.id')
                ->where('sales.sale_man_id', $redirect)
                ->where('sale_details.product_id', $id)
                ->whereDate('sales.created_at', '>=', $fromDate)
                ->whereDate('sales.created_at', '<=', $toDate)
                ->get();

        return view('admin.area_person.stock_sale_details')
                        ->with('customer_id', $id)
                        ->with('redirect', $redirect)
                        ->with('purchase_ledger', $purchase_ledger);
    }

    public function changeStatus($id) {

        $customer = DB::table('area_person')->find($id);
        if ($customer->status == 1) {
            DB::table('area_person')->where('id', $id)->update(['status' => 0]);
            // $customer->update(['status'=>0]);
        } else {
            DB::table('area_person')->where('id', $id)->update(['status' => 1]);
        }

        Session::flash('success', 'Salesman Status Changed Successfully');
        return redirect()->back();
    }

}
