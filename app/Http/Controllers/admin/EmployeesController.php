<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Employee;
use Session;

class EmployeesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $status = 1;
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        }
        $employees = Employee::where('status', $status)->get();
        return view('admin.employees.index')->with('employees', $employees);
    }

    public function employee_ledger($id) {
        $customer = DB::table('employee_ledger')
                ->select('employees.name', 'employee_ledger.date', 'employee_ledger.narration', 'employee_ledger.salary', 'employee_ledger.advance', 'employee_ledger.payment', 'employee_ledger.remaining', 'employee_ledger.created_at')
                ->join('employees', 'employee_ledger.employee_id', '=', 'employees.id')
                ->where('employee_ledger.employee_id', $id)
                ->orderBy('employee_ledger.id', 'desc')
                ->get();
//        dd($customer);

        return view('admin.employees.employee_ledger')->with('customer', $customer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $cities = DB::table('cities')->get();
        return view('admin.employees.create')->with('cities', $cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->all());
        DB::table('employees')->insert([
            'name' => $request->name,
            'cnic' => $request->cnic,
            'contact' => $request->contact,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'designation' => $request->designation,
            'salary' => $request->salary,
            'joining_date' => $request->joining_date,
            'last_salary_date' => $request->joining_date,
            'remainings' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        $employee_id = DB::getPdo()->lastInsertId();
        $arrayToSave = array();
        $arrayToSave['employee_id'] = $employee_id;
        $arrayToSave['date'] = date('Y-m-d');
        $arrayToSave['narration'] = 'openinig balance';
        $arrayToSave['salary'] = $request->salary;
        $arrayToSave['advance'] = 0;
        $arrayToSave['loan'] = 0;
        $arrayToSave['payment'] = 0;
        $arrayToSave['remaining'] = 0;
        $arrayToSave['created_at'] = date('Y-m-d H:i:s');
        $arrayToSave['updated_at'] = date('Y-m-d H:i:s');
        DB::table('employee_ledger')->insert($arrayToSave);
        Session::flash('success', 'Employee Added Successfully');
        return redirect()->back();
    }

    public function employee_payments() {
//        dd($arrayToSave);
        $date = date('Y-m-d', strtotime('-1 month'));
        // dd($date);
        // get users that have joined newly
        $new_employees = DB::table('employees')
                ->whereDate('last_salary_date', '>', $date)
                ->whereDate('last_salary_date', '<', date('Y-m-d'))
                ->where('leaving_date', null);
        $get_new_employees = $new_employees->get();

        if ($get_new_employees) {
            $new_employees->update(['last_salary_date' => date('Y-m-d')]);
            $arrayToSave = array();
            foreach ($get_new_employees as $key => $emp):
                $calculate_days = date('d', strtotime(date('Y-m-d', strtotime('-1 day'))) - strtotime($emp->last_salary_date));
                $amount = round(($emp->salary / 30) * $calculate_days);
                $ledger = DB::table('employee_ledger')
                                ->where('employee_id', '=', $emp->id)
                                ->orderBy('id', 'desc')->first();
                $empRec = DB::table('employees')
                        ->where('id', '=', $emp->id)
                        ->update([
                    'remainings' => $ledger->remaining + $amount,
                    'last_salary_date' => date('y-m-d')
                ]);

                $arrayToSave[$key]['employee_id'] = $emp->id;
                $arrayToSave[$key]['date'] = date('Y-m-d');
                $arrayToSave[$key]['narration'] = 'salary to pay';
                $arrayToSave[$key]['salary'] = $emp->salary;
                $arrayToSave[$key]['advance'] = 0;
                $arrayToSave[$key]['loan'] = 0;
                $arrayToSave[$key]['payment'] = 0;
                $arrayToSave[$key]['remaining'] = $ledger->remaining + $amount;
                $arrayToSave[$key]['created_at'] = date('Y-m-d H:i:s');
                $arrayToSave[$key]['updated_at'] = date('Y-m-d H:i:s');
            endforeach;
//            dd($arrayToSave);
            DB::table('employee_ledger')->insert($arrayToSave);
        }
        // if sallary date exceded and payment is not stored
        $oldBalancedEmployees = DB::table('employees')
                ->whereDate('last_salary_date', '<', $date)
                ->where('leaving_date', null);
        dump($date);
        dd($oldBalancedEmployees->get());








        $updateemployees = DB::table('employees')
                ->whereDate('last_salary_date', '=', $date)
                ->where('leaving_date', null);

        $employees = $updateemployees->orderBy('id', 'desc')->get();
        dd($employees);
        if ($employees) {
            $updateemployees->update(['last_salary_date' => date('Y-m-d')]);
            $arrayToSave = array();
            foreach ($employees as $key => $emp):

                $ledger = DB::table('employee_ledger')
                                ->where('employee_id', '=', $emp->id)
                                ->orderBy('id', 'desc')->first();
                $empRec = DB::table('employees')
                        ->where('id', '=', $emp->id)
                        ->update(['remainings' => $ledger->remaining + $emp->salary]);

                $arrayToSave[$key]['employee_id'] = $emp->id;
                $arrayToSave[$key]['date'] = date('Y-m-d');
                $arrayToSave[$key]['narration'] = 'salary to pay';
                $arrayToSave[$key]['salary'] = $emp->salary;
                $arrayToSave[$key]['advance'] = 0;
                $arrayToSave[$key]['loan'] = 0;
                $arrayToSave[$key]['payment'] = 0;
                $arrayToSave[$key]['remaining'] = $ledger->remaining + $emp->salary;
                $arrayToSave[$key]['created_at'] = date('Y-m-d H:i:s');
                $arrayToSave[$key]['updated_at'] = date('Y-m-d H:i:s');
            endforeach;
            DB::table('employee_ledger')->insert($arrayToSave);
        }
        Session::flash('success', 'Salary assigned Completed Successfully');
        return back();
    }

    public function save_last_payment(Request $request) {

//        dd($request->all());
//        dd($request->all());
        if ($request->pay_from == 'bank') {
//            dd($request->all());
            $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();
            $add_bal = $bank->current_amount - $request->amount;

            DB::table('bank_acc')->where('id', $request->bank_name)->update([
                'current_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('bank_ledger')->insert([
                'bank_id' => $request->bank_name,
                'employee_id' => $request->employee_id,
                'total_amount' => $add_bal,
                'deposit' => 0,
                'withdrawal' => $request->amount,
                'remaining' => $add_bal,
                'description' => $request->narration,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);
            Session::flash('success', 'Payment Transaction Completed Successfully');
        } else {
            $cash = DB::table('cash_in_hand')->first();
            if (!$cash) {
                Session::flash('error', 'Please insert cash amount to pay from cash.');
                return redirect()->back();
            }


            $add_bal = $cash->total_amount - $request->amount;
            DB::table('cash_in_hand')->update([
                'total_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'employee_id' => $request->employee_id,
                'total_amount' => $add_bal,
                'deposit' => 0,
                'withdrawal' => $request->amount,
                'remaining' => $add_bal,
                'description' => $request->narration,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);
            Session::flash('success', 'Payment Transaction Completed Successfully');
        }
        //First Add amount of remaining balance
        $emp = DB::table('employee_ledger')
                ->where('employee_id', '=', $request->employee_id)
                ->orderBy('id', 'desc')
                ->first();

        $empRec = DB::table('employees')
                ->where('id', '=', $request->employee_id)
                ->update(['remainings' => $emp->remaining + $request->new_amount]);
        //        dd($emp);
        $arrayToSave = array();
        $arrayToSave['employee_id'] = $request->employee_id;
        $arrayToSave['date'] = date('Y-m-d');
        $arrayToSave['narration'] = $request->narration;
        $arrayToSave['salary'] = $emp->salary;
        $arrayToSave['advance'] = 0;
        $arrayToSave['loan'] = 0;
        $arrayToSave['payment'] = 0;
        $arrayToSave['remaining'] = $emp->remaining + $request->new_amount;
        $arrayToSave['created_at'] = date('Y-m-d H:i:s');
        $arrayToSave['updated_at'] = date('Y-m-d H:i:s');
        DB::table('employee_ledger')->insert($arrayToSave);

//        then subtract aount of remaining balance
        $emp = DB::table('employee_ledger')
                ->where('employee_id', '=', $request->employee_id)
                ->orderBy('id', 'desc')
                ->first();

        $empRec = DB::table('employees')
                ->where('id', '=', $request->employee_id)
                ->update(['remainings' => $emp->remaining - $request->amount, 'leaving_date' => $request->leaving_date]);
        //        dd($emp);
        $arrayToSave = array();
        $arrayToSave['employee_id'] = $request->employee_id;
        $arrayToSave['date'] = date('Y-m-d');
        $arrayToSave['narration'] = $request->narration;
        $arrayToSave['salary'] = $emp->salary;
        $arrayToSave['advance'] = 0;
        $arrayToSave['loan'] = 0;
        $arrayToSave['payment'] = $request->amount;
        $arrayToSave['remaining'] = $emp->remaining - $request->amount;
        $arrayToSave['created_at'] = date('Y-m-d H:i:s');
        $arrayToSave['updated_at'] = date('Y-m-d H:i:s');
        DB::table('employee_ledger')->insert($arrayToSave);

        return redirect()->back();
    }

    public function get_date_diff() {
        $id = $_GET['emp'];
        $leaving_date = $_GET['leaving_date'];
        $e = DB::table('employees')->where('id', $id)->first();
//        dump($e);
        $diff = date('d', strtotime($leaving_date) - strtotime($e->last_salary_date));
        $res = array();
        $res['days'] = $diff;
        $res['amount'] = ceil($diff * ($e->salary / 30));
        $res['last_salary'] = $e->last_salary_date;
        $res['remainings'] = $e->remainings;
        echo json_encode($res);
    }

    public function advance(Request $request) {
        $time = date('h:i:s');
        $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));
//        dd($created_at);
        if ($request->pay_from == 'bank') {
//            dd($request->all());
            $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();
            $add_bal = $bank->current_amount - $request->advance;

            DB::table('bank_acc')->where('id', $request->bank_name)->update([
                'current_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('bank_ledger')->insert([
                'bank_id' => $request->bank_name,
                'employee_id' => $request->employee_id,
                'total_amount' => $add_bal,
                'deposit' => 0,
                'withdrawal' => $request->advance,
                'remaining' => $add_bal,
                'description' => $request->narration,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);
            Session::flash('success', 'Payment Transaction Completed Successfully');
        } else {
            $cash = DB::table('cash_in_hand')->first();
            if (!$cash) {
                Session::flash('error', 'Please insert cash amount to pay from cash.');
                return redirect()->back();
            }


            $add_bal = $cash->total_amount - $request->advance;
            DB::table('cash_in_hand')->update([
                'total_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'employee_id' => $request->employee_id,
                'total_amount' => $add_bal,
                'deposit' => 0,
                'withdrawal' => $request->advance,
                'remaining' => $add_bal,
                'description' => $request->narration,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);
            Session::flash('success', 'Payment Transaction Completed Successfully');
        }

        $emp = DB::table('employee_ledger')
                ->where('employee_id', '=', $request->employee_id)
                ->orderBy('id', 'desc')
                ->first();
        $empRec = DB::table('employees')
                ->where('id', '=', $request->employee_id)
                ->update(['remainings' => $emp->remaining - $request->advance]);
        //        dd($emp);
        $arrayToSave = array();
        $arrayToSave['employee_id'] = $request->employee_id;
        $arrayToSave['date'] = date('Y-m-d');
        $arrayToSave['narration'] = $request->narration;
        $arrayToSave['salary'] = $emp->salary;
        $arrayToSave['advance'] = $request->advance;
        $arrayToSave['loan'] = 0;
        $arrayToSave['payment'] = 0;
        $arrayToSave['remaining'] = $emp->remaining - $request->advance;
        $arrayToSave['created_at'] = $created_at;
        $arrayToSave['updated_at'] = $created_at;
        DB::table('employee_ledger')->insert($arrayToSave);
        return redirect()->back();
    }

    public function save_paid_payment(Request $request) {
        $emp = DB::table('employee_ledger')
                ->where('employee_id', '=', $request->employee_id)
                ->orderBy('id', 'desc')
                ->first();
        $employee = DB::table('employees')
                ->where('id', '=', $request->employee_id)
                ->first();
//        dump($request->all());
        $empRec = DB::table('employees')
                ->where('id', '=', $emp->employee_id)
                ->update(['remainings' => $emp->remaining - $request->amount]);
        if ($request->pay_from == 'bank') {
            $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();
            $add_bal = $bank->current_amount - $request->amount;

            DB::table('bank_acc')->where('id', $request->bank_name)->update([
                'current_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('bank_ledger')->insert([
                'bank_id' => $request->bank_name,
                'employee_id' => $request->employee_id,
                'total_amount' => $add_bal,
                'deposit' => 0,
                'withdrawal' => $request->amount,
                'remaining' => $add_bal,
                'description' => $request->narration,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);
            Session::flash('success', 'Payment Transaction Completed Successfully');
        } else {
            $cash = DB::table('cash_in_hand')->first();

            if (!$cash) {
                Session::flash('error', 'Please insert cash amount to pay from cash.');
                return redirect()->back();
            }
            $add_bal = $cash->total_amount - $request->amount;
//            dd($request->all());
            DB::table('cash_in_hand')->update([
                'total_amount' => $add_bal,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'employee_id' => $request->employee_id,
                'total_amount' => $add_bal,
                'deposit' => 0,
                'withdrawal' => $request->amount,
                'remaining' => $add_bal,
                'description' => $request->narration,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);
            Session::flash('success', 'Payment Transaction Completed Successfully');
        }
        $arrayToSave = array();
        $arrayToSave['employee_id'] = $request->employee_id;
        $arrayToSave['date'] = $employee->last_salary_date;
        $arrayToSave['narration'] = $request->narration;
        $arrayToSave['salary'] = $emp->salary;
        $arrayToSave['advance'] = 0;
        $arrayToSave['loan'] = 0;
        $arrayToSave['payment'] = $request->amount;
        $arrayToSave['remaining'] = $emp->remaining - $request->amount;
        $arrayToSave['created_at'] = date('Y-m-d H:i:s');
        $arrayToSave['updated_at'] = date('Y-m-d H:i:s');
//        dd($arrayToSave);
        DB::table('employee_ledger')->insert($arrayToSave);

        return redirect()->back();
    }

    public function get_payment_type(Request $request) {
        if ($request->pay_type == 'bank') {
            $banks = DB::table('bank_acc')->get();

            echo ' <div class="form-group row">
                <label class="col-lg-12">Select Bank</label>
                <div class="col-lg-12">
                    <select class="form-control" name="bank_name" id="bank_name" required>';
            foreach ($banks as $bank) :
                echo '<option value="' . $bank->id . '"> ' . $bank->bank_name . ' | Acc No:' . $bank->account_no . ' | Balance: ' . $bank->current_amount . '</option>';
            endforeach;
            echo ' </select>
                </div>
            </div>';
        } else {
            $cash = DB::table('cash_in_hand')->first();

            echo ' <div class="form-group row">
                        <label class="col-lg-12">Cash in Hand</label>
                        <div class="col-lg-12">
                            <input class="form-control" type="text" name="cash" value="' . $cash->total_amount . '" required readonly>
                        </div>
                    </div>';
        }
    }

    public function salary_details() {
        $emp = DB::table('employee_ledger')
                ->where('employee_id', '=', $_GET['id'])
                ->where('date', '<=', date('Y-m-d'))
                ->orderBy('id', 'DESC')
                ->limit('6')
                ->get();
//        dd($emp);

        $html = '<table class="table table-bordered"><tr><th>Salary</th><th>Description</th><th>Advance</th><th>Loan</th><th>Remaining Amount</th><th>Date</th></tr>';
        foreach ($emp as $e):
            $html .= "<tr>"
                    . "<td>" . $e->salary . "</td>"
                    . "<td>" . $e->narration . "</td>"
                    . "<td>" . $e->advance . "</td>"
                    . "<td>" . $e->loan . "</td>"
                    . "<td>" . $e->remaining . "</td>"
                    . "<td>" . date('M d, Y', strtotime($e->date)) . "</td>"
                    . "</tr>";
        endforeach;
        $html .= '</table>';
        echo $html;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $employee = DB::table('employees')
                        ->where('id', '=', $id)
                        ->orderBy('id', 'desc')->first();
        $cities = DB::table('cities')->get();
//        dd($employee);
        return view('admin.employees.edit')
                        ->with('cities', $cities)
                        ->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
//        dd($request->all());
        DB::table('employees')->where('id', $id)->update([
            'name' => $request->name,
            'cnic' => $request->cnic,
            'contact' => $request->contact,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'designation' => $request->designation,
            'salary' => $request->salary,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        Session::flash('success', 'Employee Updated Successfully');
        return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
//        dd($id);
        DB::table('employees')->where('id', $id)->delete();
        DB::table('employee_ledger')->where('employee_id', $id)->delete();

        Session::flash('success', 'Employee Deleted Successfully');
        return redirect()->back();
    }

    public function changeStatus($id) {
        $customer = DB::table('employees')->find($id);
        if ($customer->status == 1) {
            DB::table('employees')->where('id', $id)->update(['status' => 0]);
            // $customer->update(['status'=>0]);
        } else {
            DB::table('employees')->where('id', $id)->update(['status' => 1]);
        }

        Session::flash('success', 'Employee Status Changed Successfully');
        return redirect()->back();
    }

}
