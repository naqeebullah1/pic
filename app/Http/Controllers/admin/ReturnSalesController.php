<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ReturnSalesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $time = date('h:i:s');
            $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));
            if (count($request->product_id) < 0 || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }
            DB::table('return_sales')->insert([
                'vr_no' => $request->vr_no,
                'company_id' => $request->supplier_id,
                'return_sale_type' => $request->sale_type,
                'total_amount' => $request->line_cost_total,
                'discount' => $request->discount,
                'net_amount' => $request->net_amount,
                'paid' => $request->paidamount,
                'balance' => $request->due_amount,
                'remarks' => $request->description,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);
//        dd($request->sale_type);


            $last_id = DB::getPdo()->lastInsertId();
            if ($request->paidamount > 0) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount - $request->paidamount,
                    'updated_at' => date('y-m-d H:i:s')
                ]);

                DB::table('cash_in_hand_ledger')->insert([
                    'cash_in_hand_id' => 1,
                    'invoice_id' => $last_id,
                    'company_id' => $request->supplier_id,
                    'total_amount' => $cash->total_amount - $request->paidamount,
                    'deposit' => 0,
                    'withdrawal' => $request->paidamount,
                    'remaining' => $cash->total_amount - $request->paidamount,
                    'description' => $request->description,
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);
            }

            $product_id = $request->product_id;

            if ($request->sale_type == 'retail') {
                $request->unit_price = $request->retail_price;
            } else {
                $request->unit_price = $request->whole_price;
            }

            $unit_price = $request->unit_price;

            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $discount = $request->discount_per_item;
            $whole = $request->whole_price;
            $retail = $request->retail_price;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);
            for ($i = 0; $i < $count; $i++) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();
                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'discount' => $discount[$i],
                    'actual_amount' => $unit_price[$i] * $qty[$i],
                    'unit_price' => $unit_price[$i],
                    'total_price' => $line_cost[$i],
                    'return_sale_id' => $last_id,
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                );

                $data1 = array(
                    'product_id' => $product_id[$i],
                    'invoice_id' => $last_id,
                    'return_sale_id' => $last_id,
                    'description' => $request->description,
                    'total_qty' => $get_record->stock_qty,
                    'purchased_qty' => 0,
                    'return_purchased' => 0,
                    'sale' => 0,
                    'return_sale' => $qty[$i],
                    'remaning' => $get_record->stock_qty + $qty[$i],
                    'created_at' => $created_at,
                    'updated_at' => $created_at,
                );

                $insert_data[] = $data;
                $insert_data1[] = $data1;

                $update_qty = $get_record->stock_qty + $qty[$i];
                DB::table('products')->where('id', $product_id[$i])->update([
                    'stock_qty' => $update_qty,
//                'product_whole_sale_price' => $whole[$i],
//                'product_sale_price' => $retail[$i]
                ]);
            }

            DB::table('return_sale_details')->insert($insert_data);
            DB::table('product_ledger')->insert($insert_data1);
            // dd([$insert_data, $insert_data1]);

            DB::table('company')->where('id', $request->supplier_id)->update([
                'dues' => $request->dues - $request->due_amount
            ]);

            DB::table('company_ledger')->insert([
                'company_id' => $request->supplier_id,
                'invoice_no' => $last_id,
                'narration' => $request->description,
                'sale' => 0,
                'return_sale' => $request->net_amount,
                'payment' => $request->paidamount,
                'remaining' => $request->dues - $request->due_amount,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            DB::commit();
            Session::flash('success', 'Return Sale Transaction Completed Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function walkingCustomerSaveReturnsale(Request $request) {
        try {
            DB::beginTransaction();

            $checkcash = DB::table('cash_in_hand')
                    ->first();
            if (!$checkcash) {
                Session::flash('error', 'Please Enter cash to your account');
                return redirect()->back();
            }
            if (count($request->product_id) < 0 || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }
            DB::table('return_sales')->insert([
                'vr_no' => $request->vr_no,
                'company_id' => $request->supplier_id,
                'return_sale_type' => $request->sale_type,
                'total_amount' => $request->line_cost_total,
                'discount' => $request->discount,
                'net_amount' => $request->net_amount,
                'paid' => $request->paidamount,
                'balance' => $request->due_amount,
                'remarks' => $request->description,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
//        dd($request->sale_type);


            $last_id = DB::getPdo()->lastInsertId();
            $product_id = $request->product_id;

            if ($request->sale_type == 'retail') {
                $request->unit_price = $request->retail_price;
            } else {
                $request->unit_price = $request->whole_price;
            }

            $unit_price = $request->unit_price;

            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $whole = $request->whole_price;
            $retail = $request->retail_price;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);
            for ($i = 0; $i < $count; $i++) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();
                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'unit_price' => $unit_price[$i],
                    'total_price' => $line_cost[$i],
                    'return_sale_id' => $last_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $data1 = array(
                    'product_id' => $product_id[$i],
                    'invoice_id' => $last_id,
                    'description' => $request->description,
                    'total_qty' => $get_record->stock_qty,
                    'purchased_qty' => 0,
                    'return_purchased' => 0,
                    'sale' => 0,
                    'return_sale' => $qty[$i],
                    'remaning' => $get_record->stock_qty + $qty[$i],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $insert_data[] = $data;
                $insert_data1[] = $data1;

                $update_qty = $get_record->stock_qty + $qty[$i];
                DB::table('products')->where('id', $product_id[$i])->update([
                    'stock_qty' => $update_qty,
//                'product_whole_sale_price' => $whole[$i],
//                'product_sale_price' => $retail[$i]
                ]);
            }

            DB::table('return_sale_details')->insert($insert_data);
            DB::table('product_ledger')->insert($insert_data1);
            // dd([$insert_data, $insert_data1]);
            $cash = DB::table('cash_in_hand')
                    ->first();
            DB::table('cash_in_hand')->update([
                'total_amount' => $cash->total_amount - $request->paidamount,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'return_sale_id' => $last_id,
                'total_amount' => $cash->total_amount - $request->paidamount,
                'deposit' => 0,
                'withdrawal' => $request->paidamount,
                'remaining' => $cash->total_amount - $request->paidamount,
                'description' => $request->description,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);
            DB::commit();
            Session::flash('success', 'Return Sale Transaction Completed Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $supplier = DB::table('company')->where('id', $id)->first();
        $products = DB::table('products')->select('id', 'qty', 'product_name')->get();
        return view('admin.companies.return_sale')
                        ->with('supplier', $supplier)
                        ->with('products', $products);
    }

    public function walkingCustomerReturnsale() {
        $products = DB::table('products')->select('id', 'qty', 'product_name')->get();
        return view('admin.companies.walking_customer_returnsale')
                        ->with('products', $products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $supplier_purchases = DB::table('return_sales')
                ->select('return_sales.id', 'return_sales.total_amount', 'return_sales.discount', 'return_sales.net_amount', 'return_sales.paid', 'return_sales.created_at', 'company.name')
                ->join('company', 'company.id', '=', 'return_sales.company_id')
                ->where('return_sales.company_id', $id)
                ->get();
//        dd($supplier_purchases);
        return view('admin.companies.return_purchase_ledger')
                        ->with('supplier_id', $id)
                        ->with('purchase_ledger', $supplier_purchases);
    }

    public function walkingCustomersReturn() {
        $supplier_purchases = DB::table('return_sales')
                ->select('return_sales.id', 'return_sales.total_amount', 'return_sales.discount', 'return_sales.net_amount', 'return_sales.paid', 'return_sales.created_at')
                ->where('return_sales.company_id', 0)
                ->get();
//        dd($supplier_purchases);
        return view('admin.companies.walking_customers_return')
                        ->with('supplier_id', 0)
                        ->with('purchase_ledger', $supplier_purchases);
    }

    public function return_sale_details($id, $redirect) {
        $purchase_ledger = DB::table('return_sales')
                ->select('products.product_name', 'return_sale_details.total_price', 'return_sale_details.discount as item_discount', 'return_sale_details.actual_amount', 'products.qty as pack', 'products.product_code', 'return_sales.total_amount', 'return_sales.discount', 'return_sales.net_amount', 'return_sales.paid', 'return_sale_details.unit_price', 'return_sale_details.qty', 'return_sales.remarks', 'return_sale_details.created_at')
                ->join('return_sale_details', 'return_sale_details.return_sale_id', '=', 'return_sales.id')
                ->join('products', 'return_sale_details.product_id', '=', 'products.id')
                ->where('return_sale_details.return_sale_id', $id)
                ->get();
//        dd($purchase_ledger);

        return view('admin.companies.return_purchase_detail')
                        ->with('redirect', $redirect)
                        ->with('purchase_ledger', $purchase_ledger);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
