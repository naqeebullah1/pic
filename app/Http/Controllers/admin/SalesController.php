<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Http\Traits\UpdateLedgersTrait;
use App\Models\Sale;
use App\Models\SaleDetail;
use Exception;
use Illuminate\Support\Facades\DB as FacadesDB;

class SalesController extends Controller
{

    use UpdateLedgersTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->product_id == null || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }
            $last_id = $this->saveSaleData();

            if (isset($request->send_msg)) {
                $companyDetails = DB::table('company')->where('id', $request->supplier_id)->first();
                if ($companyDetails->contact) {
                    $APIKey = '50d109f93eacd84034a04f3f9241500e202c8982';
                    $receiver = $companyDetails->contact;
                    $sender = '8583';
                    $textmessage = 'Dear Customer,
Choosing us means a lot to us, thank you for shopping us.
Below are the details of your current and previous balances.

Current purchase Amount: ' . $request->net_amount . ' PKR.
Paid Amount: ' . $request->paidamount . ' PKR.
Remaining Dues : ' . $companyDetails->dues . ' PKR.

Thank you
Abdan Traders';

                    $url = "http://api.smilesn.com/sendsms?hash=" . $APIKey . "&receivenum=" . $receiver . "&sendernum=" . urlencode($sender) . "&textmessage=" . urlencode($textmessage);

                    #----CURL Request Start
                    $ch = curl_init();
                    $timeout = 30;
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    #----CURL Request End, Output Response
                    $response = json_decode($response);
                    //                dd($response);
                    if (!isset($response->status)) {
                        //                if ($response->status != "ACCEPTED") {
                        Session::flash('error', 'Message can not be sent due to unknown reason');
                        //                }
                    }
                } else {
                    Session::flash('error', 'Message can not be sent because no contact number found');
                }
            }
            Session::flash('success', 'Sale has been created successfully');
            DB::commit();
            if (isset($request->print)) {
                return redirect()->route('sale/print_invoice', ['id' => $last_id]);
            } else {
                return redirect()->back();
            }
        } catch (QueryException $e) {
            DB::rollback();
            //dd($e);
        }
    }

    public function saveWalkingCustomer(Request $request)
    {

        try {
            DB::beginTransaction();

            $checkcash = DB::table('cash_in_hand')->first();
            if (!$checkcash) {
                Session::flash('error', 'Please Enter cash to your account');
                return redirect()->back();
            }

            if (count($request->product_id) < 0 || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }

            DB::table('sales')->insert([
                'vr_no' => $request->vr_no,
                'company_id' => $request->supplier_id,
                'customer_name' => $request->customer_name,
                'sale_type' => $request->sale_type,
                'total_amount' => $request->line_cost_total,
                'discount' => $request->discount,
                'net_amount' => $request->net_amount,
                'paid' => $request->paidamount,
                'balance' => $request->due_amount,
                'created_by' => auth()->user()->id,
                'table' => 'users',
                'created_at' => $request->created_at,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $last_id = DB::getPdo()->lastInsertId();
            $product_id = $request->product_id;

            if ($request->sale_type == 'retail') {
                $request->unit_price = $request->retail_price;
            } else {
                $request->unit_price = $request->whole_price;
            }

            $unit_price = $request->unit_price;

            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $whole = $request->whole_price;
            $retail = $request->retail_price;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);
            for ($i = 0; $i < $count; $i++) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();


                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'unit_price' => $unit_price[$i],
                    'total_price' => $line_cost[$i],
                    'sale_id' => $last_id,
                    'unit_cost_price' => $get_record->product_cost_price,
                    'total_cost_price' => $get_record->product_cost_price * $qty[$i],
                    'created_at' => $request->created_at,
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $data1 = array(
                    'product_id' => $product_id[$i],
                    'invoice_id' => $last_id,
                    'sale_id' => $last_id,
                    'description' => $request->description,
                    'total_qty' => $get_record->stock_qty,
                    'purchased_qty' => 0,
                    'return_purchased' => 0,
                    'sale' => $qty[$i],
                    'return_sale' => 0,
                    'remaning' => $get_record->stock_qty - $qty[$i],
                    'created_at' => $request->created_at,
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $insert_data[] = $data;
                $insert_data1[] = $data1;

                $update_qty = $get_record->stock_qty - $qty[$i];

                $product_record = $product_record->update([
                    'stock_qty' => $update_qty,
                    //                'product_whole_sale_price' => $whole[$i],
                    //                'product_sale_price' => $retail[$i]
                ]);
            }

            DB::table('sale_details')->insert($insert_data);
            DB::table('product_ledger')->insert($insert_data1);
            // dd([$insert_data, $insert_data1]);
            $cash = DB::table('cash_in_hand')->first();
            //dd($cash);
            DB::table('cash_in_hand')->update([
                'total_amount' => $cash->total_amount + $request->paidamount,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'invoice_id' => $last_id,
                'company_id' => 0,
                'total_amount' => $cash->total_amount + $request->paidamount,
                'deposit' => $request->paidamount,
                'withdrawal' => 0,
                'remaining' => $cash->total_amount + $request->paidamount,
                'description' => $request->description,
                'created_at' => $request->created_at,
                'updated_at' => date('y-m-d H:i:s'),
            ]);
            DB::commit();
            Session::flash('success', 'Sale Transaction Completed Successfully');

            if (isset($request->print)) {
                return redirect()->route('sale/print_invoice', ['id' => $last_id]);
            } else {
                return redirect()->back();
            }
        } catch (QueryException $e) {
            DB::rollback();
            //dd($e);
        }
    }

    public function print_invoice($id)
    {
        $checkcustomer = DB::table('sales')->where('sales.id', $id)->first();
        //        dd($checkcustomer);
        if ($checkcustomer->company_id != 0) {

            $sale = DB::table('sales')->select('company.name', 'sales.narration', 'company.contact', 'company.dues', 'company.address', 'company.city', 'sales.id', 'sales.total_amount', 'sales.discount', 'sales.net_amount', 'sales.paid', 'sales.created_at')->join('company', 'sales.company_id', '=', 'company.id')->where('sales.id', $id)->first();
            $purchase_ledger = DB::table('sales')->select('products.product_name', 'products.qty_per_cotton', 'products.qty as p_qty', 'products.product_code', 'sales.total_amount', 'sales.discount', 'sale_details.discount as item_discount', 'sale_details.actual_price', 'sales.net_amount', 'sales.paid', 'sale_details.unit_price', 'sale_details.qty', 'sale_details.total_price', 'sale_details.created_at')->join('sale_details', 'sale_details.sale_id', '=', 'sales.id')->join('products', 'sale_details.product_id', '=', 'products.id')->where('sale_details.sale_id', $id)->get();
            $sale_expenses = DB::table('sales')->select('sale_details.*')->join('sale_details', 'sale_details.sale_id', '=', 'sales.id')->where('sale_details.sale_id', $id)->where('sale_details.product_id', 0)->get();
            //dd($sale_expenses);
            return view('admin.companies.print_invoice')->with('sales', $sale)->with('customer_id', $id)->with('sale_expenses', $sale_expenses)->with('purchase_ledger', $purchase_ledger);
        } else {
            $sale = DB::table('sales')->where('sales.id', $id)->first();
            $purchase_ledger = DB::table('sales')->select('products.product_name', 'products.qty as p_qty', 'products.product_code', 'sales.total_amount', 'sales.discount', 'sale_details.discount as item_discount', 'sale_details.actual_price', 'sales.net_amount', 'sales.paid', 'sale_details.unit_price', 'sale_details.qty', 'sale_details.total_price', 'sale_details.created_at')->join('sale_details', 'sale_details.sale_id', '=', 'sales.id')->join('products', 'sale_details.product_id', '=', 'products.id')->where('sale_details.sale_id', $id)->get();

            //            dd('yes');
            return view('admin.companies.w_c_invoice')->with('sales', $sale)->with('customer_id', $id)->with('purchase_ledger', $purchase_ledger);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = DB::table('company')->where('id', $id)->first();
        $salesman = DB::table('area_person')->get();
        $getSaleDetails = new Sale;

        $products = DB::table('products')->select('id', 'qty', 'product_name')->get();
        return view('admin.companies.sales')
            ->with('supplier', $supplier)
            ->with('getSaleDetails', $getSaleDetails)
            ->with('salesman', $salesman)
            ->with('products', $products);
    }

    public function edit_sale($id)
    {
        $getSaleDetails = Sale::with('SaleDetails', 'SaleDetails.Product')->where('id', $id)->first();
        $customerId = $getSaleDetails->company_id;
        $supplier = DB::table('company')->where('id', $customerId)->first();
        $salesman = DB::table('area_person')->get();

        $products = DB::table('products')->select('id', 'qty', 'product_name')->get();

        return view('admin.companies.edit_sale')
            ->with('supplier', $supplier)
            ->with('salesman', $salesman)
            ->with('getSaleDetails', $getSaleDetails)
            ->with('products', $products);
    }

    public function walkingCustomerSale()
    {
        $products = DB::table('products')->select('id', 'qty', 'product_name')->get();
        return view('admin.companies.walking_customer_sale')->with('products', $products);
    }

    public function edit($id)
    {
        $purchase_ledger = DB::table('sales')->select('company.name', 'sales.*')->join('company', 'company.id', '=', 'sales.company_id')->where('sales.company_id', $id)->get();
        $users = DB::table('users')->pluck('name', 'id');
        $salesman = DB::table('area_person')->pluck('name', 'id');
        return view('admin.companies.sale_details')
            ->with('purchase_ledger', $purchase_ledger)
            ->with('salesman', $salesman)
            ->with('users', $users);
    }

    public function walkingCustomers()
    {
        //        dd('kjh');
        $purchase_ledger = DB::table('sales')->select('sales.id', 'sales.total_amount', 'sales.discount', 'sales.net_amount', 'sales.paid', 'sales.created_at')->where('sales.company_id', 0)->orderBy('sales.created_at', 'ASC')->get();
        // dd($purchase_ledger);
        return view('admin.companies.walking_customers', compact('purchase_ledger'));
    }

    public function saleDetails($id)
    {
        //        dd($id);
        $purchase_ledger = DB::table('sales')->select('products.product_name', 'products.qty as pack', 'products.product_code', 'sales.total_amount', 'sales.net_amount', 'sales.paid', 'sales.vr_no', 'sale_details.unit_price', 'sale_details.total_price', 'sale_details.actual_price', 'sale_details.discount', 'sale_details.qty', 'sale_details.created_at')->join('sale_details', 'sale_details.sale_id', '=', 'sales.id')->join('products', 'sale_details.product_id', '=', 'products.id')->where('sale_details.sale_id', $id)->get();
        $sale_expenses = DB::table('sales')->select('sale_details.*')->join('sale_details', 'sale_details.sale_id', '=', 'sales.id')->where('sale_details.sale_id', $id)->where('sale_details.product_id', 0)->get();

        return view('admin.companies.details')->with('customer_id', $id)->with('sale_expenses', $sale_expenses)->with('purchase_ledger', $purchase_ledger);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveSaleData()
    {

        $request = request();
        $time = date('h:i:s');
        $created_at = date('Y-m-d h:i:s', strtotime($request->created_at . ' ' . $time));
        //dd($request->product_id==null);

        //         if (!empty($request->salesman) && $request->salesman != 0) {
        //             if (!empty($request->product_id)) {
        //                 foreach ($request->product_id as $k => $p):
        //                     $s_products = DB::table('area_person_sales')
        //                             ->where('sale_man_id', $request->salesman)
        //                             ->where('product_id', $p);
        //                     $get_p = $s_products->first();


        // //                    dump($s_products);
        //                 endforeach;
        //             }
        // }
        DB::table('sales')->insert([
            'vr_no' => $request->vr_no,
            'sale_man_id' => $request->salesman,
            'company_id' => $request->supplier_id,
            'sale_type' => $request->sale_type,
            'total_amount' => $request->line_cost_total,
            'discount' => $request->discount,
            'net_amount' => $request->net_amount,
            'paid' => $request->paidamount,
            'balance' => $request->due_amount,
            'narration' => $request->description,
            'created_by' => auth()->user()->id,
            'table' => 'users',
            'created_at' => $created_at,
            'updated_at' => $created_at,
        ]);


        $last_id = DB::getPdo()->lastInsertId();

        if (!empty($request->salesman) && $request->salesman != 0) {
            if (!empty($request->product_id)) {
                foreach ($request->product_id as $k1 => $p1) :
                    $s_products1 = DB::table('area_person_sales')
                        ->where('sale_man_id', $request->salesman)
                        ->where('product_id', $p1);
                    $get_p1 = $s_products1->first();

                    DB::table('area_person_ledger')->insert([
                        'saleman_id' => $request->salesman,
                        'product_id' => $p1,
                        'total_stock' => $get_p1->qty,
                        'assign_stock' => 0,
                        'remaining' => $get_p1->qty - $request->qty[$k1],
                        'sale_stock' => $request->qty[$k1],
                        'return_stock' => 0,
                        'description' => $request->description,
                        'invoice_no' => $request->vr_no,
                        'date' => date('Y-m-d H:i:s'),
                    ]);


                    $s_products1->update(['qty' => $get_p1->qty - $request->qty[$k1]]);
                //                    dump($s_products);
                endforeach;
            }
        }


        if ($request->paidamount > 0) {
            $cash = DB::table('cash_in_hand')->first();
            DB::table('cash_in_hand')->update([
                'total_amount' => $cash->total_amount + $request->paidamount,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'invoice_id' => $last_id,
                'company_id' => $request->supplier_id,
                'total_amount' => $cash->total_amount + $request->paidamount,
                'deposit' => $request->paidamount,
                'withdrawal' => 0,
                'remaining' => $cash->total_amount + $request->paidamount,
                'description' => $request->description,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
            ]);
        }

        $product_id = $request->product_id;

        if ($request->sale_type == 'retail') {
            $request->unit_price = $request->retail_price;
        } else {
            $request->unit_price = $request->whole_price;
        }

        $unit_price = $request->unit_price;

        $current_qty = $request->current_qty;
        $qty = $request->qty;
        $discount = $request->discount_per_item;
        $whole = $request->whole_price;
        $retail = $request->retail_price;
        $line_cost = $request->line_cost;
        // dd($qty);
        if (count($qty) > count($product_id))
            $count = count($qty);
        else
            $count = count($product_id);
        for ($i = $count - 1; $i >= 0; $i--) {
            $product_record = DB::table('products')->where('id', $product_id[$i]);
            $get_record = $product_record->first();
            $data = array(
                'product_id' => $product_id[$i],
                'qty' => $qty[$i],
                'discount' => $discount[$i],
                'actual_price' => $unit_price[$i] * $qty[$i],
                'unit_price' => $unit_price[$i],
                'total_price' => $line_cost[$i],
                'sale_id' => $last_id,
                'unit_cost_price' => $get_record->product_cost_price,
                'total_cost_price' => $get_record->product_cost_price * $qty[$i],
                'description' => '',
                'created_at' => $created_at,
                'updated_at' => $created_at,
            );
            $data1 = array(
                'product_id' => $product_id[$i],
                'invoice_id' => $last_id,
                'sale_id' => $last_id,
                'description' => $request->description,
                'total_qty' => $get_record->stock_qty,
                'purchased_qty' => 0,
                'return_purchased' => 0,
                'sale' => $qty[$i],
                'return_sale' => 0,
                'remaning' => $get_record->stock_qty - $qty[$i],
                'created_at' => $created_at,
                'updated_at' => $created_at,
            );

            $insert_data[] = $data;
            $insert_data1[] = $data1;
            //            dd($get_record);
            $sale_details[] = array('product' => $get_record->product_name, 'qty' => $qty[$i], 'unit_price' => round($line_cost[$i] / $qty[$i], 2), 'cotton' => round($qty[$i] / $get_record->qty_per_cotton, 1));
            $update_qty = $get_record->stock_qty - $qty[$i];

            $product_record = $product_record->update([
                'stock_qty' => $update_qty,
                //                'product_whole_sale_price' => $whole[$i],
                //                'product_sale_price' => $retail[$i]
                //                'stock_qty' => $update_qty
            ]);
        }
        if ($request->extra_title) {
            foreach ($request->extra_title as $k => $amount) {
                array_push(
                    $insert_data,
                    array(
                        'product_id' => 0,
                        'qty' => 0,
                        'discount' => 0,
                        'actual_price' => $amount,
                        'unit_price' => $amount,
                        'total_price' => $amount,
                        'sale_id' => $last_id,
                        'unit_cost_price' => $amount,
                        'total_cost_price' => $amount,
                        'description' => $request->extra_description[$k],
                        'created_at' => $created_at,
                        'updated_at' => $created_at,
                    )
                );
            }
        }
        //        dd($insert_data);

        DB::table('sale_details')->insert($insert_data);
        DB::table('product_ledger')->insert($insert_data1);
        // dd([$insert_data, $insert_data1]);
        $customer = DB::table('company')->where('id', $request->supplier_id)->first();
        $dues_remaining = $request->due_amount + $customer->dues;
        DB::table('company')->where('id', $request->supplier_id)->update([
            'dues' => $request->due_amount + $customer->dues
        ]);
        DB::table('company_ledger')->insert([
            'company_id' => $request->supplier_id,
            'invoice_no' => $last_id,
            'narration' => $request->description,
            'sale' => $request->net_amount,
            'return_sale' => 0,
            'payment' => $request->paidamount,
            'remaining' => $dues_remaining,
            'sale_details' => json_encode($sale_details),
            'created_at' => $created_at,
            'updated_at' => $created_at,
        ]);
        return $last_id;
    }

    public function deleteSale($id)
    {

        $sale = DB::table('sales')->where('id', $id)->first();
        // This logic is for add stock again to salesman account
        if ($sale->sale_man_id) {

            $sale_details = DB::table('sale_details')->where('sale_id', $id)->get();
            //            dd($sale_details);
            $saleman_sale = array();
            $saleman_sale_details = array();
            foreach ($sale_details as $key => $product) :
                $area_person_sales = DB::table('area_person_sales')
                    ->where(['sale_man_id' => $sale->sale_man_id, 'product_id' => $product->product_id]);
                $get = $area_person_sales->first();

                //               dd($product);
                if ($get) {
                    $p_qty = 0;
                    if (!empty($product->qty)) {
                        $p_qty = $product->qty;
                    }
                    $area_person_sales->update([
                        'qty' => $get->qty + $p_qty,
                        'balance' => $get->balance + $product->total_price,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);

                    $area_person_ledger = DB::table('area_person_ledger')
                    ->where(['saleman_id' => $sale->sale_man_id, 'product_id' => $product->product_id,'invoice_no'=>$sale->vr_no]);
                $getdata = $area_person_ledger->first();

                        $rem=$getdata->remaining+$getdata->sale_stock;

                        DB::table('area_person_ledger')->where('id',$getdata->id)
                        ->update([

                            'remaining'=>$rem,
                            'sale_stock'=>0
                        ]);
                      $greater_data=  DB::table('area_person_ledger')->where('id','>',$getdata->id)
                        ->where(['saleman_id' => $sale->sale_man_id, 'product_id' => $product->product_id])->get();
                            if($greater_data)
                            {
                                foreach($greater_data as $record)
                           {
                               DB::table('area_person_ledger')->where('id',$record->id)
                                ->update([
                                'total_stock'=>$record->total_stock+$getdata->sale_stock,
                                'remaining'=>$record->remaining+$getdata->sale_stock,
                            ]);
                                }
                            }

                    $last_id = $get->id;


                }

                $getp = DB::table('products')->where('id', $product->product_id)->first();
                $data1 = array(
                    'product_id' => $product->product_id,
                    'area_person_sale_id' => $last_id,
                    'unit_price' => $product->unit_price,
                    'actual_price' => $product->unit_cost_price,
                    'qty' => $product->qty,
                    'discount' => 0,
                    'description' => 'Deleted Record Recovered',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $saleman_sale[] = $data1;
            endforeach;
            //        dd($saleman_sale);

            DB::table('area_person_sale_details')->insert($saleman_sale);
        }

        //update product and its ledger
        $product_ledger = DB::table('product_ledger')->where('invoice_id', $id)->where('sale', '!=', 0)->get();
        foreach ($product_ledger as $pl) {
            $this->updateProductLedger($pl);
            $product = DB::table('products')->where('id', $pl->product_id);
            $get_product = $product->first();
            $add = $get_product->stock_qty + $pl->sale;
            $product->update(['stock_qty' => $add]);
        }

        //        exit;
        // subtract the sale amount from the customer $sale->balance
        //        dd($sale);
        $get_company = DB::table('company')->where('id', $sale->company_id)->first();
        $sub_bal = $get_company->dues - $sale->balance;
        //        dd($sub_bal);
        $update_company = DB::table('company')->where('id', $sale->company_id)->update(['dues' => $sub_bal]);

        //        delete product ledger
        $delete_product_ledger = DB::table('product_ledger')->where('invoice_id', $id)->where('sale', '!=', 0)->delete();

        //        delete sale and sale details
       
        DB::table('sales')->where('id', $id)->delete();
        DB::table('sale_details')->where('sale_id', $id)->delete();



        // delete company ledger
        $delete_company_ledger = DB::table('company_ledger')
            ->where('invoice_no', $id)
            ->where('company_id', $sale->company_id);
        $this->updateCustomerLedger($delete_company_ledger);
        /*
         * Please delete paid amount in invoice
         */
        if ($sale->paid) {
            $cash = DB::table('cash_in_hand')->first();
            //update cash_in_hand

            DB::table('cash_in_hand')->update([
                'total_amount' => $cash->total_amount - $sale->paid,
                'updated_at' => date('y-m-d H:i:s')
            ]);
            $cashLedger = DB::table('cash_in_hand_ledger')->where('invoice_id', $id)->where('company_id', $sale->company_id);
            $this->updateCashLedger($cashLedger);
        }
    }

    public function update(Request $request, $id)
    {

        try {

            DB::beginTransaction();
            if ($request->product_id == null || $request->line_cost_total == 0 || $request->net_amount == 0) {
                Session::flash('error', 'Please Select at least One Product');
                return redirect()->back();
            }
            $oldsalesData = DB::table('sales')->where('id', $id)->first();
            //start product updation
            $oldsalesDetails = DB::table('sale_details')->where('sale_id', $id)->select('product_id')->get();

            foreach ($oldsalesDetails as $sdetail) {
                $get_data =   DB::table('product_ledger')->where('product_id', $sdetail->product_id)->where('sale_id', $id)->first();
                $product_qty = DB::table('products')->where('id', $sdetail->product_id)->first();
                $qty = $product_qty->stock_qty + $get_data->sale;
                DB::table('products')
                    ->where('id', $sdetail->product_id)
                    ->update(['stock_qty' => $qty]);
                $remning_qty = $get_data->remaning + $get_data->sale;
                //  dd($remning_qty);
              
                DB::table('product_ledger')
                    ->where('id', $get_data->id)
                    ->update(['sale' => '0', 'remaning' => $remning_qty]);
                $get_max_data =   DB::table('product_ledger')->where('product_id', $sdetail->product_id)->where('id', '>', $get_data->id)->get();

                foreach ($get_max_data as $more_details) {
                    $totalqty = $more_details->total_qty + $get_data->sale;
                    
                    $totalremaining = $more_details->remaning + $get_data->sale;
                     
                    DB::table('product_ledger')
                        ->where('id', $more_details->id)
                        ->update(['total_qty' => $totalqty, 'remaning' => $totalremaining]);
                      
                }
              
                if (!empty($oldsalesData->sale_man_id) && $oldsalesData->sale_man_id != 0) {
                    $s_products = DB::table('area_person_sales')
                        ->where('sale_man_id', $oldsalesData->sale_man_id)
                        ->where('product_id', $sdetail->product_id);
                    $get_p = $s_products->first();
                    $s_products->update(['qty' => $get_p->qty + $get_data->sale]);
                    //                    dump($s_products);
                    $saleman_ledger_id = DB::table('area_person_ledger')->where('invoice_no', $oldsalesData->vr_no)->where('product_id', $sdetail->product_id)->first();

                    DB::table('area_person_ledger')->where('id', $saleman_ledger_id->id ?? 0)
                        ->update([

                            'remaining' => $saleman_ledger_id->remaining ?? 0 + $get_data->sale ?? 0,
                            'sale_stock' => 0
                        ]);

                    DB::table('area_person_ledger')->where('id', '>', $saleman_ledger_id->id ?? 0)
                        ->where('saleman_id', $saleman_ledger_id->saleman_id ?? 0)
                        ->where('product_id', $sdetail->product_id)
                        ->update([
                            'total_stock' => $saleman_ledger_id->total_stock ?? 0 + $get_data->sale ?? 0,
                            'remaining' => $saleman_ledger_id->remaining ?? 0 + $get_data->sale ?? 0,

                        ]);
                }
            }

            //end old product Updation
            //end foreach

            //customer old Entry Update
            $get_customer_data =   DB::table('company_ledger')->where('company_id', $oldsalesData->company_id)->where('invoice_no', $id)->where('sale', '>', 0)->first();
            $remaining = ($get_customer_data->remaining + $get_customer_data->payment) - $get_customer_data->sale;

            $oldbalance = DB::table('company')->where('id', $oldsalesData->company_id)->first();
            $customer_balance = $oldbalance->dues - $get_customer_data->sale;

            DB::table('company')->where('id', $oldsalesData->company_id)->update([
                'dues' => $customer_balance
            ]);


            $resu =  DB::table('company_ledger')
                ->where('id', $get_customer_data->id)
                ->where('company_id', $oldsalesData->company_id)
                ->update([
                    'sale' => '0',
                    'sale_details' => '',
                    'remaining' => $remaining
                ]);

            $get_customer_data_max =   DB::table('company_ledger')->where('company_id', $oldsalesData->company_id)->where('id', '>', $get_customer_data->id)->get();
            foreach ($get_customer_data_max as $more_details_customer) {
                $remaining = ($more_details_customer->remaining + $get_customer_data->payment) - $get_customer_data->sale;
                DB::table('company_ledger')
                    ->where('id', $more_details_customer->id)
                    ->where('company_id', $oldsalesData->company_id)
                    ->update([
                        'remaining' => $remaining
                    ]);
            }
            // end customer old data update

            //start cash ledger Updation
            $get_cash_in_hand_data =   DB::table('cash_in_hand_ledger')->where('company_id', $oldsalesData->company_id)->where('invoice_id', $id)->first();
            $cash = DB::table('cash_in_hand')->first();
            DB::table('cash_in_hand')->update([
                'total_amount' => $cash->total_amount - $oldsalesData->paid,
                'updated_at' => date('y-m-d H:i:s')
            ]);

            try {
                $cash_remaining = ($get_cash_in_hand_data->remaining - $oldsalesData->paid);

                DB::table('cash_in_hand_ledger')
                    ->where('id', $get_cash_in_hand_data->id)
                    ->where('company_id', $oldsalesData->company_id)
                    ->update(['deposit' => '0', 'remaining' => $cash_remaining]);
                //dd('hi');
                $get_cash_in_hand_data_max =   DB::table('cash_in_hand_ledger')->where('company_id', $oldsalesData->company_id)->where('id', '>', $get_cash_in_hand_data->id)->get();
                foreach ($get_cash_in_hand_data_max as $more_details_cash_in_hand) {
                    $remaining_cash = ($more_details_cash_in_hand->remaining - $oldsalesData->paid);
                    $total_cash = ($more_details_cash_in_hand->total_amount - $oldsalesData->paid);

                    DB::table('cash_in_hand_ledger')
                        ->where('id', $more_details_cash_in_hand->id)
                        ->where('company_id', $oldsalesData->company_id)
                        ->update(['remaining' => $remaining_cash,  'total_amount' => $total_cash]);
                }
            } catch (Exception $ex) {
            }
            //delete sales details
            DB::table('sale_details')
                ->where('sale_id', $oldsalesData->id)
                ->delete();

            //end sales details
            //end cash ledger Updation

            //New Updation Start from Here

            //****New sales updation */
            DB::table('sales')
                ->where('id', $oldsalesData->id)
                ->update([
                    'vr_no' => $request->vr_no,
                    'sale_man_id' => $request->salesman,
                    'sale_type' => $request->sale_type,
                    'total_amount' => $request->line_cost_total,
                    'discount' => $request->discount,
                    'net_amount' => $request->net_amount,
                    'paid' => $request->paidamount,
                    'balance'=>$request->net_amount-$request->paidamount,
                    'narration' => $request->description,
                    'created_by' => auth()->user()->id,
                    'table' => 'users',

                    'updated_at' => date('y-m-d H:i:s'),
                ]);

            //****New sales Cash updation */
            if ($request->paidamount > 0) {
                $cash = DB::table('cash_in_hand')->first();
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount + $request->paidamount,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                $get_cash_in_hand_data =   DB::table('cash_in_hand_ledger')->where('company_id', $oldsalesData->company_id)->where('invoice_id', $id)->first();
                $cash_remaining = ($get_cash_in_hand_data->remaining + $oldsalesData->paid);
                DB::table('cash_in_hand_ledger')
                    ->where('id', $get_cash_in_hand_data->id)
                    ->where('company_id', $oldsalesData->company_id)
                    ->update([
                        'cash_in_hand_id' => 1,

                        'total_amount' => $cash->total_amount + $request->paidamount,
                        'deposit' => $request->paidamount,
                        'withdrawal' => 0,
                        'remaining' => $cash->total_amount + $request->paidamount,
                        'description' => $request->description,
                        'updated_at' => date('y-m-d H:i:s')
                    ]);

                $get_cash_in_hand_data_max =   DB::table('cash_in_hand_ledger')->where('company_id', $oldsalesData->company_id)->where('id', '>', $get_cash_in_hand_data->id)->get();
                foreach ($get_cash_in_hand_data_max as $more_details_cash_in_hand) {
                    $remaining_cash = ($more_details_cash_in_hand->remaining + $request->paidamount);
                    $total_cash = ($more_details_cash_in_hand->total_amount + $request->paidamount);

                    DB::table('cash_in_hand_ledger')
                        ->where('id', $more_details_cash_in_hand->id)
                        ->where('company_id', $oldsalesData->company_id)
                        ->update(['remaining' => $remaining_cash,  'total_amount' => $total_cash]);
                }
            }

            $customer = DB::table('company')->where('id', $oldsalesData->company_id)->first();

            $dues_remaining = $request->due_amount + $customer->dues;


            DB::table('company')->where('id', $oldsalesData->company_id)->update([
                'dues' => $dues_remaining
            ]);


            //*********Naqeeb Code******* */
            $product_id = $request->product_id;

            if ($request->sale_type == 'retail') {
                $request->unit_price = $request->retail_price;
            } else {
                $request->unit_price = $request->whole_price;
            }

            $unit_price = $request->unit_price;

            $current_qty = $request->current_qty;
            $qty = $request->qty;
            $discount = $request->discount_per_item;
            $whole = $request->whole_price;
            $retail = $request->retail_price;
            $line_cost = $request->line_cost;

            if (count($qty) > count($product_id))
                $count = count($qty);
            else
                $count = count($product_id);
            for ($i = $count - 1; $i >= 0; $i--) {
                $product_record = DB::table('products')->where('id', $product_id[$i]);
                $get_record = $product_record->first();
                $data = array(
                    'product_id' => $product_id[$i],
                    'qty' => $qty[$i],
                    'discount' => $discount[$i],
                    'actual_price' => $unit_price[$i] * $qty[$i],
                    'unit_price' => $unit_price[$i],
                    'total_price' => $line_cost[$i],
                    'sale_id' => $oldsalesData->id,
                    'unit_cost_price' => $get_record->product_cost_price,
                    'total_cost_price' => $get_record->product_cost_price * $qty[$i],
                    'description' => '',
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                );

                $insert_data[] = $data;

                //            dd($get_record);
                $sale_details1[] = array('product' => $get_record->product_name, 'qty' => $qty[$i], 'unit_price' => round($line_cost[$i] / $qty[$i], 2), 'cotton' => round($qty[$i] / $get_record->qty_per_cotton, 1));
                $update_qty = $get_record->stock_qty - $qty[$i];

                $product_record = $product_record->update([
                    'stock_qty' => $update_qty,
                    //                'product_whole_sale_price' => $whole[$i],
                    //                'product_sale_price' => $retail[$i]
                    //                'stock_qty' => $update_qty
                ]);

                $checking_data_in_table = DB::table('product_ledger')->where('sale_id', $oldsalesData->id)->where('product_id', $product_id[$i])->count();
                //dd($checking_data_in_table);
                if ($checking_data_in_table > 0) {
                    $cheaking_record = DB::table('product_ledger')
                        ->where('sale_id', $oldsalesData->id)->where('product_id', $product_id[$i])->first();

                    $totalqty = $cheaking_record->remaning - $qty[$i];
                    DB::table('product_ledger')
                        ->where('sale_id', $oldsalesData->id)->where('product_id', $product_id[$i])
                        ->update([
                            'sale' => $qty[$i],
                            'remaning' => $totalqty,
                            'updated_at' => date('y-m-d H:i:s')
                        ]);


                    if (!empty($oldsalesData->sale_man_id) && $oldsalesData->sale_man_id != 0) {
                        $s_products1 = DB::table('area_person_sales')
                            ->where('sale_man_id', $oldsalesData->sale_man_id)
                            ->where('product_id', $product_id[$i]);
                        $get_p1 = $s_products1->first();
                        $s_products->update(['qty' => $get_p1->qty - $qty[$i]]);
                        //


                        $saleman_ledger_id1 = DB::table('area_person_ledger')->where('invoice_no', $oldsalesData->id)->where('product_id', $product_id[$i])->first();

                        DB::table('area_person_ledger')->where('id', $saleman_ledger_id1->id ??0)
                            ->update([
                                'remaining' => $saleman_ledger_id1->remaining ?? 0 - $qty[$i],
                                'sale_stock' => $qty[$i]
                            ]);

                        DB::table('area_person_ledger')->where('id', '>', $saleman_ledger_id1->id ?? 0)
                            ->where('saleman_id', $saleman_ledger_id1->saleman_id ?? 0)
                            ->where('product_id', $product_id[$i])
                            ->update([
                                'total_stock' => $saleman_ledger_id1->total_stock ?? 0 - $qty[$i],
                                'remaining' => $saleman_ledger_id1->remaining ?? 0 - $qty[$i],
                            ]);
                    }



                    $cheaking_max_record = DB::table('product_ledger')
                        ->where('id', '>', $cheaking_record->id)->get();
                    foreach ($cheaking_max_record as $record_Detail) {

                        $data_max = DB::table('product_ledger')
                            ->where('id', $get_record->id)->first();;
                        if ($data_max) {

                            $totalqty1 = $data_max->remaning - $qty[$i];
                            $totalqty2 = $data_max->total_qty - $qty[$i];

                            DB::table('product_ledger')
                                ->where('id', $get_record->id)
                                ->update([
                                    'total_qty' => $totalqty2,
                                    'remaning' => $totalqty1,
                                    'updated_at' => date('y-m-d H:i:s')
                                ]);
                        }
                    }
                } else {
                    DB::table('product_ledger')->insert([
                        'product_id' => $product_id[$i],
                        'invoice_id' => $oldsalesData->id,
                        'sale_id' => $oldsalesData->id,
                        'description' => $request->description,
                        'total_qty' => $get_record->stock_qty,
                        'purchased_qty' => 0,
                        'return_purchased' => 0,
                        'sale' => $qty[$i],
                        'return_sale' => 0,
                        'remaning' => $get_record->stock_qty - $qty[$i],
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s'),
                    ]);

                    if (!empty($request->salesman) && $request->salesman != 0) {
                        if (!empty($request->product_id)) {
                            foreach ($request->product_id as $k1 => $p1) :
                                $s_products1 = DB::table('area_person_sales')
                                    ->where('sale_man_id', $request->salesman)
                                    ->where('product_id', $p1);
                                $get_p1 = $s_products1->first();

                                $a = DB::table('area_person_ledger')->insert([
                                    'saleman_id' => $request->salesman,
                                    'product_id' => $p1,
                                    'total_stock' => $get_p1->qty,
                                    'assign_stock' => 0,
                                    'remaining' => $get_p1->qty - $request->qty[$k1],
                                    'sale_stock' => $request->qty[$k1],
                                    'return_stock' => 0,
                                    'description' => $request->description,
                                    'invoice_no' => $oldsalesData->vr_no,
                                    'date' => date('Y-m-d H:i:s'),
                                ]);

                                $s_products1->update(['qty' => $get_p1->qty - $request->qty[$k1]]);
                            //                    dump($s_products);
                            endforeach;
                        }
                    }
                }
            }

            if ($request->extra_title) {
                foreach ($request->extra_title as $k => $amount) {
                    array_push(
                        $insert_data,
                        array(
                            'product_id' => 0,
                            'qty' => 0,
                            'discount' => 0,
                            'actual_price' => $amount,
                            'unit_price' => $amount,
                            'total_price' => $amount,
                            'sale_id' => $oldsalesData->id,
                            'unit_cost_price' => $amount,
                            'total_cost_price' => $amount,
                            'description' => $request->extra_description[$k],
                            'created_at' => date('y-m-d H:i:s'),
                            'updated_at' => date('y-m-d H:i:s'),
                        )
                    );
                }
            }
            //        dd($insert_data);

            DB::table('sale_details')->insert($insert_data);
            //  $customer = DB::table('company')->where('id', $oldsalesData->company_id)->first();
            // $dues_remaining = $request->due_amount + $customer->dues;
            // DB::table('company')->where('id', $oldsalesData->company_id)->update([
            //     'dues' => $dues_remaining
            // ]);
            DB::table('company_ledger')->where('company_id', $oldsalesData->company_id)->where('invoice_no', $oldsalesData->id)
                ->update([

                    'narration' => $request->description,
                    'sale' => $request->net_amount,
                    'return_sale' => 0,
                    'payment' => $request->paidamount,
                    'remaining' => $dues_remaining,
                    'sale_details' => json_encode($sale_details1),
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ]);

            //New Updation Ends here

            // $this->deleteSale($id);

            Session::flash('success', 'Sale Transaction Updated Successfully');
            DB::commit();
            if (isset($request->print)) {
                return redirect()->route('sale/print_invoice', ['id' => $oldsalesData->id]);
            } else {
                return redirect()->route('sales.edit', $request->supplier_id);
            }
        } catch (QueryException $e) {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetch_history()
    {
        $type = $_GET['type'];
        $id = $_GET['invoiceNo'];
        if ($type == 'sale') {
            $purchase_ledger = DB::table('sales')->select('products.product_name', 'products.product_code', 'sales.total_amount', 'sales.discount', 'sales.net_amount', 'sales.paid', 'sale_details.unit_price', 'sale_details.qty', 'sale_details.created_at')->join('sale_details', 'sale_details.sale_id', '=', 'sales.id')->join('products', 'sale_details.product_id', '=', 'products.id')->where('sale_details.sale_id', $id)->get();
            $html = ' <table class="table table-striped table-hover table-fw-widget" id="table1">
    <thead>
        <tr>
            <th>P-code</th>
            <th>P-Name</th>
            <th>Unit Price</th>
            <th>Qty</th>
        </tr>
    </thead>
    <tbody>';
            foreach ($purchase_ledger as $ledger) :

                $html .= '<tr>
            <td>' . $ledger->product_code . '</td>
            <td>' . $ledger->product_name . '</td>
            <td>' . $ledger->unit_price . '</td>
            <td>' . $ledger->qty . '</td>
        </tr>';
            endforeach;
            $html .= ' </tbody></table>';
        } else {

            $purchase_ledger = DB::table('return_sales')->select('products.product_name', 'products.product_code', 'return_sales.total_amount', 'return_sales.discount', 'return_sales.net_amount', 'return_sales.paid', 'return_sale_details.unit_price', 'return_sale_details.qty', 'return_sale_details.created_at')->join('return_sale_details', 'return_sale_details.return_sale_id', '=', 'return_sales.id')->join('products', 'return_sale_details.product_id', '=', 'products.id')->where('return_sale_details.return_sale_id', $id)->get();
            $html = ' <table class="table table-striped table-hover table-fw-widget" id="table1">
    <thead>
        <tr>
            <th>P-code</th>
            <th>P-Name</th>
            <th>Unit Price</th>
            <th>Qty</th>
        </tr>
    </thead>
    <tbody>';
            foreach ($purchase_ledger as $ledger) :

                $html .= '<tr>
            <td>' . $ledger->product_code . '</td>
            <td>' . $ledger->product_name . '</td>
            <td>' . $ledger->unit_price . '</td>
            <td>' . $ledger->qty . '</td>
        </tr>';
            endforeach;
            $html .= ' </tbody></table>';
        }
        echo $html;
    }

    public function delete_sale($id)
    {
        try {
            DB::beginTransaction();

            $this->deleteSale($id);

            DB::commit();
            Session::flash('success', 'Sale Transaction reverted Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            //dd($e);
        }
    }

    public function delete_return_sale($id)
    {
        try {
            DB::beginTransaction();
            $sale = DB::table('return_sales')->where('id', $id)->first();
            //update product and its ledger

            $product_ledger = DB::table('product_ledger')->where('invoice_id', $id)->where('return_sale', '!=', 0)->get();
            foreach ($product_ledger as $pl) {
                $this->updateProductLedger($pl);
                $product = DB::table('products')->where('id', $pl->product_id);
                $get_product = $product->first();
                $add = $get_product->stock_qty - $pl->return_sale;
                $product->update(['stock_qty' => $add]);
            }
            // subtract the sale amount from the customer $sale->balance
            $get_company = DB::table('company')->where('id', $sale->company_id)->first();
            $sub_bal = $get_company->dues + $sale->balance;
            //        dd($sub_bal);
            $update_company = DB::table('company')->where('id', $sale->company_id)->update(['dues' => $sub_bal]);

            //        delete product ledger
            $delete_product_ledger = DB::table('product_ledger')->where('invoice_id', $id)->where('return_sale', '!=', 0)->delete();

            //        delete sale and sale details
            DB::table('return_sales')->where('id', $id)->delete();
            DB::table('return_sale_details')->where('return_sale_id', $id)->delete();



            // delete company ledger
            $delete_company_ledger = DB::table('company_ledger')
                ->where('invoice_no', $id)
                ->where('company_id', $sale->company_id);
            $this->updateCustomerLedger($delete_company_ledger);


            //        dd($delete_company_ledger);
            if ($sale->paid) {
                $cash = DB::table('cash_in_hand')->first();
                //update cash_in_hand
                DB::table('cash_in_hand')->update([
                    'total_amount' => $cash->total_amount + $sale->paid,
                    'updated_at' => date('y-m-d H:i:s')
                ]);
                $cashLedger = DB::table('cash_in_hand_ledger')->where('invoice_id', $id)->where('company_id', $sale->company_id);
                $this->updateCashLedger($cashLedger);
            }
            Session::flash('success', 'Return Sale Transaction reverted Successfully');
            DB::commit();
            Session::flash('success', 'Sale Transaction reverted Successfully');
            return redirect()->back();
        } catch (QueryException $e) {
            DB::rollback();
            //dd($e);
        }
    }

    public function delete_walking_sale($id)
    {

        $cinl = DB::table('cash_in_hand_ledger')->where('invoice_id', $id)->first();
        if (!$cinl) {
            Session::flash('error', 'This Record can not be deleted');
            return redirect()->back();
        }
        $cash = DB::table('cash_in_hand')->first();
        //update cash_in_hand

        DB::table('cash_in_hand')->update([
            'total_amount' => $cash->total_amount - $cinl->deposit,
            'updated_at' => date('y-m-d H:i:s')
        ]);
        DB::table('cash_in_hand_ledger')->where('invoice_id', $id)->update(['is_deleted' => 1]);

        $sale = DB::table('sales')->where('id', $id)->first();
        //update product and its ledger
        $product_ledger = DB::table('product_ledger')->select('product_ledger.*')->join('sales', 'sales.id', '=', 'product_ledger.invoice_id')->where('invoice_id', $id)->where('sale', '!=', 0)->get();

        foreach ($product_ledger as $pl) {
            $product = DB::table('products')->where('id', $pl->product_id);
            $get_product = $product->first();
            $add = $get_product->stock_qty + $pl->sale;
            $product->update(['stock_qty' => $add]);
        }

        //        delete product ledger
        $delete_product_ledger = DB::table('product_ledger')->where('invoice_id', $id)->join('sales', 'sales.id', '=', 'product_ledger.invoice_id')->where('sale', '!=', 0)->delete();

        //        delete sale and sale details
        DB::table('sales')->where('id', $id)->delete();
        DB::table('sale_details')->where('sale_id', $id)->delete();

        Session::flash('success', 'Sale Transaction reverted Successfully');
        if (isset($request->print)) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function delete_walking_return_sale($id)
    {

        $cinl = DB::table('cash_in_hand_ledger')->where('return_sale_id', $id)->first();
        if (!$cinl) {
            Session::flash('error', 'This Record can not be deleted');
            return redirect()->back();
        }
        $cash = DB::table('cash_in_hand')->first();
        //update cash_in_hand
        DB::table('cash_in_hand')->update([
            'total_amount' => $cash->total_amount + $cinl->deposit,
            'updated_at' => date('y-m-d H:i:s')
        ]);
        $cinl = DB::table('cash_in_hand_ledger')->where('return_sale_id', $id)->update(['is_deleted' => 1]);

        $sale = DB::table('return_sales')->where('id', $id)->first();
        //update product and its ledger
        $product_ledger = DB::table('product_ledger')->select('product_ledger.*')->join('return_sales', 'return_sales.id', '=', 'product_ledger.invoice_id')->where('return_sale', '!=', 0)->where('invoice_id', $id)->get();

        foreach ($product_ledger as $pl) {
            $product = DB::table('products')->where('id', $pl->product_id);
            $get_product = $product->first();
            $add = $get_product->stock_qty - $pl->sale;
            $product->update(['stock_qty' => $add]);
        }

        //        delete product ledger
        $delete_product_ledger = DB::table('product_ledger')->select('product_ledger.*')->join('return_sales', 'return_sales.id', '=', 'product_ledger.invoice_id')->where('return_sale', '!=', 0)->where('invoice_id', $id)->delete();

        //        delete sale and sale details
        DB::table('return_sales')->where('id', $id)->delete();
        DB::table('return_sale_details')->where('return_sale_id', $id)->delete();

        Session::flash('success', 'Return Sale Transaction reverted Successfully');
        if (isset($request->print)) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function edit_vr()
    {
        $getName = explode('-', request()->name);
        if ($getName[0] == 'sales') {

            $update = DB::table('sales')->where('id', request()->pk)->update([$getName[1] => request()->value]);
            exit;
        }
    }

    public function test_cron()
    {
        $rec = DB::table('cron_jobs')->first();
        DB::table('cron_jobs')->where('id', $rec->id)->update(['test' => $rec->test + 1]);
    }
}
