<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ProductQtyController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $employees = DB::table('product_qty')->orderBy('id', 'desc')->get();
        return view('admin.product_qty.index')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->quantity);
        DB::table('product_qty')->insert([
            'quantity' => $request->quantity,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Quantity added successfully');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    public function fetch_qty() {
        $id = $_GET['id'];
        $employees = DB::table('product_qty')
                        ->where('id', '=', $id)
                        ->orderBy('id', 'desc')->first();
        echo $employees->quantity;
    }

    public function update_qty(Request $request) {
//        dd($request->all());
        $id = $request->quantity_id;
        DB::table('product_qty')->where('id', $id)->update([
            'quantity' => $request->quantity,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Quantity Updated successfully');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id) {
        DB::table('product_qty')->where('id', $id)->delete();
        Session::flash('success', 'Quantity Deleted Successfully');
        return redirect()->back();
    }

}
