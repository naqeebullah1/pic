<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Http\Traits\UpdateLedgersTrait;

class CashInHandController extends Controller {

    use UpdateLedgersTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $fromDate = date('Y-m-d', strtotime('-1 month'));
        $toDate = date('Y-m-d');
        if (isset($_GET['from_date']) && isset($_GET['to_date'])) {
            $fromDate = $_GET['from_date'];
            $toDate = $_GET['to_date'];
//        dd($fromDate);
        }


        $accountDetails = DB::table('cash_in_hand')
                ->select('expenses.parent_id', 'cash_in_hand.total_amount', 'cash_in_hand_ledger.*')
                ->join('cash_in_hand_ledger', 'cash_in_hand_ledger.cash_in_hand_id', '=', 'cash_in_hand.id')
                ->leftjoin('expenses', 'cash_in_hand_ledger.expense_id', '=', 'expenses.id')
                ->where('cash_in_hand_ledger.cash_in_hand_id', 1)
                ->whereDate('cash_in_hand_ledger.created_at', '>=', $fromDate)
                ->whereDate('cash_in_hand_ledger.created_at', '<=', $toDate)
                ->get();
        $cashInHand = DB::table('cash_in_hand')->first();
        $banks = DB::table('bank_acc')->pluck('bank_name', 'id');
//        dd($bank);
        $companies = DB::table('company')->pluck('name', 'id');
        $suppliers = DB::table('supplier')->pluck('name', 'id');
        $personal_expenses = DB::table('zakat_persons')->pluck('name', 'id');
        $expenses = DB::table('expenses')
                ->where('level', 0)
                ->pluck('expenses', 'id');
//        dd($expenses);
//        dd($accountDetails);
        return view('admin.cash_in_hand.index')
                        ->with('companies', $companies)
                        ->with('suppliers', $suppliers)
                        ->with('expenses', $expenses)
                        ->with('banks', $banks)
                        ->with('cashInHand', $cashInHand)
                        ->with('personal_expenses', $personal_expenses)
                        ->with('accountDetails', $accountDetails);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.cash_in_hand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        DB::table('cash_in_hand')->insert([
            'total_amount' => $request->total_amount,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
//        dd($product_id);

        $product_id = DB::getPdo()->lastInsertId();
        DB::table('cash_in_hand_ledger')->insert([
            'cash_in_hand_id' => $product_id,
            'total_amount' => $request->total_amount,
            'deposit' => 0,
            'withdrawal' => 0,
            'remaining' => $request->total_amount,
            'description' => "opening balance",
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

//        dd($product_id);
        Session::flash('success', 'Cash Amount Added Successfully');
        return redirect()->route('cash_in_hand.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
