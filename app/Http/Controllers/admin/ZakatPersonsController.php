<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ZakatPersonsController extends Controller {

    public function index() {
        $accounts = DB::table('zakat_persons')->orderBy('id', 'desc')->get();
        return view('admin.zakat_persons.index')->with('accounts', $accounts);
    }

    public function create() {
        return view('admin.zakat_persons.create');
    }

    public function store(Request $request) {
//        dd($request->all());
        DB::table('zakat_persons')->insert([
            'name' => $request->name,
            'contact_no' => $request->contact_no,
            'balance' => $request->current_amount,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
//        dd($product_id);

        $product_id = DB::getPdo()->lastInsertId();


        DB::table('zakat_person_ledger')->insert([
            'zakat_person_id' => $product_id,
            'total_amount' => $request->current_amount,
            'description' => 'opening account',
            'withdrawal' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

//        dd($product_id);
        Session::flash('success', 'Zakat Person Added Successfully');
        return redirect()->back();
    }

    public function edit($id) {
        $company = DB::table('zakat_persons')->where('id', $id)->first();
        return view('admin.zakat_persons.edit')->with('account', $company);
    }

    public function update(Request $request) {
//        dd($request->all());
        DB::table('zakat_persons')->where('id', $request->id)->update([
            'name' => $request->name,
            'contact_no' => $request->contact_no,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Session::flash('success', 'Record Updated Successfully');
        return redirect()->route('zakat_persons.index');
    }

    public function destroy($id) {

       $ledger = DB::table('zakat_person_ledger')->where('id', $id)->first();




       if ($ledger->transection_type == 'Bank') {


        $bank = DB::table('bank_acc')->where('id', $ledger->transection_id)->first();


        $cut_bal = $bank->current_amount + $ledger->withdrawal;

        DB::table('bank_acc')->where('id', $ledger->transection_id)->update([
            'current_amount' => $cut_bal
        ]);

        DB::table('bank_ledger')->insert([
            'bank_id' => $ledger->transection_id,
            'zakat_person_id' => $ledger->zakat_person_id,
            'total_amount' => $cut_bal,
            'deposit' => $ledger->withdrawal,
            'withdrawal' => 0,
            'remaining' => $cut_bal,
            'description' => 'Expanse Deleted',
            'created_at' => \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->toDateTimeString(),
            'updated_at' => date('y-m-d H:i:s'),
        ]);

        DB::table('journal_vocher')->insert([
            'u_id' => $ledger->zakat_person_id,
            'head' => "zakat_persons",
            'sub_head' => "zakat_persons",
            'total' => $bank->current_amount,
            'pay' => $ledger->withdrawal,
            'balance' => $cut_bal,
            'description' => 'Expense Deleted',
            'created_at' => \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->toDateTimeString(),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        $supplier = DB::table('zakat_persons')->where('id', $ledger->zakat_person_id)->first();


        $supplier_bal = $supplier->balance - $ledger->withdrawal;

        DB::table('zakat_persons')->where('id', $ledger->zakat_person_id)->update([
            'balance' => $supplier_bal,
//                'last_payment' => date('Y-m-d')
        ]);

       $a= DB::table('zakat_person_ledger')
        ->delete($ledger->id);

    }
    if ($ledger->transection_type == 'Cash') {

        $cash = DB::table('cash_in_hand')->first();

        $cut_bal = $cash->total_amount + $ledger->withdrawal;
        DB::table('cash_in_hand')->update([
            'total_amount' => $cut_bal
        ]);

        DB::table('cash_in_hand_ledger')->insert([
            'cash_in_hand_id' => 1,
            'zakat_person_id' =>$ledger->zakat_person_id,
            'total_amount' => $cut_bal,
            'deposit' => $ledger->withdrawal,
            'withdrawal' => 0,
            'remaining' => $cut_bal,
            'description' => 'expanse Deleted',
            'created_at' => \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->toDateTimeString(),
            'updated_at' => date('y-m-d H:i:s'),
        ]);

        DB::table('journal_vocher')->insert([
            'u_id' => $ledger->zakat_person_id,
            'head' => "zakat_persons",
            'sub_head' => "zakat_persons",
            'total' => $cash->total_amount,
            'pay' => $ledger->withdrawal,
            'balance' => $cut_bal,
            'description' => 'expanse deleted',
            'created_at' => \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->toDateTimeString(),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $supplier = DB::table('zakat_persons')->where('id', $ledger->zakat_person_id)->first();


        $supplier_bal = $supplier->balance - $ledger->withdrawal;

        DB::table('zakat_persons')->where('id', $ledger->zakat_person_id)->update([
            'balance' => $supplier_bal,
//                'last_payment' => date('Y-m-d')
        ]);

        DB::table('zakat_person_ledger')
        ->delete($ledger->id);
    }
//
//        $person = DB::table('zakat_persons')->where('id', $ledger->zakat_person_id);
//        $get_p = $person->first();
////        $person->update(['balance' => $get_p->balance - $ledger->withdrawal]);
//        $ledger = DB::table('zakat_person_ledger')->where('id', $id)->first();
//
//        dd($person);


        Session::flash('Deleted', 'Deleted Successfully');
        return redirect()->back();
    }

    public function show($id) {
        $accountDetails = DB::table('zakat_persons')
                ->select('zakat_persons.name', 'zakat_person_ledger.id', 'zakat_persons.balance', 'zakat_person_ledger.total_amount', 'zakat_person_ledger.withdrawal', 'zakat_person_ledger.description', 'zakat_person_ledger.created_at')
                ->join('zakat_person_ledger', 'zakat_person_ledger.zakat_person_id', '=', 'zakat_persons.id')
                ->where('zakat_person_ledger.zakat_person_id', $id)
                ->get();
//        dd($accountDetails);

        return view('admin.zakat_persons.show')->with('accountDetails', $accountDetails);
    }

    public function payment($id) {

        $supplier = DB::table('zakat_persons')->where('id', $id)->first();

        return view('admin.zakat_persons.supplier_payment')->with('supplier', $supplier);
    }

    public function save_payment(Request $request) {
//        dd($request->all());
        if ($request->pay_from == 'bank') {

            $bank = DB::table('bank_acc')->where('id', $request->bank_name)->first();


            $cut_bal = $bank->current_amount - $request->pay;

            DB::table('bank_acc')->where('id', $request->bank_name)->update([
                'current_amount' => $cut_bal
            ]);

            DB::table('bank_ledger')->insert([
                'bank_id' => $request->bank_name,
                'zakat_person_id' => $request->supplier_id,
                'total_amount' => $cut_bal,
                'deposit' => 0,
                'withdrawal' => $request->pay,
                'remaining' => $cut_bal,
                'description' => $request->description,
                'created_at' => \Carbon\Carbon::parse($request->created_at)->toDateTimeString(),
                'updated_at' => date('y-m-d H:i:s'),
            ]);

            DB::table('journal_vocher')->insert([
                'u_id' => $request->supplier_id,
                'head' => "zakat_persons",
                'sub_head' => "zakat_persons",
                'total' => $bank->current_amount,
                'pay' => $request->pay,
                'balance' => $cut_bal,
                'description' => $request->description,
                'created_at' => \Carbon\Carbon::parse($request->created_at)->toDateTimeString(),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $supplier = DB::table('zakat_persons')->where('id', $request->supplier_id)->first();


            $supplier_bal = $supplier->balance + $request->pay;

            DB::table('zakat_persons')->where('id', $request->supplier_id)->update([
                'balance' => $supplier_bal,
//                'last_payment' => date('Y-m-d')
            ]);

            DB::table('zakat_person_ledger')->insert([
                'zakat_person_id' => $request->supplier_id,
                'description' => $request->description,
                'withdrawal' => $request->pay,
                'total_amount' => $supplier_bal,
                'created_at' => \Carbon\Carbon::parse($request->created_at)->toDateTimeString(),
                'updated_at' => date('Y-m-d H:i:s'),
                'transection_type'=>'Bank',
                'transection_id'=>$request->bank_name
            ]);
        }
        if ($request->pay_from == 'cash') {

            $cash = DB::table('cash_in_hand')->first();

            $cut_bal = $cash->total_amount - $request->pay;
            DB::table('cash_in_hand')->update([
                'total_amount' => $cut_bal
            ]);

            DB::table('cash_in_hand_ledger')->insert([
                'cash_in_hand_id' => 1,
                'zakat_person_id' => $request->supplier_id,
                'total_amount' => $cut_bal,
                'deposit' => 0,
                'withdrawal' => $request->pay,
                'remaining' => $cut_bal,
                'description' => $request->description,
                'created_at' => \Carbon\Carbon::parse($request->created_at)->toDateTimeString(),
                'updated_at' => date('y-m-d H:i:s'),
            ]);

            DB::table('journal_vocher')->insert([
                'u_id' => $request->supplier_id,
                'head' => "zakat_persons",
                'sub_head' => "zakat_persons",
                'total' => $cash->total_amount,
                'pay' => $request->pay,
                'balance' => $cut_bal,
                'description' => $request->description,
                'created_at' => \Carbon\Carbon::parse($request->created_at)->toDateTimeString(),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $supplier = DB::table('zakat_persons')->where('id', $request->supplier_id)->first();


            $supplier_bal = $supplier->balance + $request->pay;

            DB::table('zakat_persons')->where('id', $request->supplier_id)->update([
                'balance' => $supplier_bal,
//                'last_payment' => date('Y-m-d')
            ]);

            DB::table('zakat_person_ledger')->insert([
                'zakat_person_id' => $request->supplier_id,
                'description' => $request->description,
                'withdrawal' => $request->pay,
                'total_amount' => $supplier_bal,
                'created_at' => \Carbon\Carbon::parse($request->created_at)->toDateTimeString(),
                'updated_at' => date('Y-m-d H:i:s'),
                'transection_type'=>'Cash',

            ]);
        }
        if ($request->pay_from == 'cheque') {
            $arrayToSave = array();
            if ($request->tranfer_to == 'bank') {
                $arrayToSave['bank_id'] = $request->bank_name;
                $arrayToSave['transfer_to'] = 'bank';
            }
            if ($request->tranfer_to == 'cash') {
                //cash logic goes here
                $arrayToSave['transfer_to'] = 'cash';
            }
            $arrayToSave['supplier_id'] = $request->supplier_id;
            $arrayToSave['cheque_no'] = $request->cheque_no;
            $arrayToSave['amount'] = $request->pay;
            $arrayToSave['release_date'] = $request->cheque_date;
            $arrayToSave['created_at'] = \Carbon\Carbon::parse($request->created_at)->toDateTimeString();
            $arrayToSave['updated_at'] = date('Y-m-d H:i:s');
//            dd($arrayToSave);
            DB::table('cheques')->insert($arrayToSave);

            Session::flash('success', 'Payment Transaction Completed Successfully');
            return redirect()->route('zakat_persons.index');
        }
        Session::flash('success', 'Payment Transaction Completed Successfully');
        return redirect()->route('zakat_persons.index');
    }

}
