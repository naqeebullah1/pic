<?php

namespace App;

use App\AreaPersonLedger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AreaPerson extends Authenticatable {

    protected $table = 'area_person';
    protected $guard = 'saleman';



    public function areaperson_ledger(){
        return $this->hasMany('App\AreaPersonLedger');
    }

}
