<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class assignSallary extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sallary:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command  will assign sallaries to the Employees.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $query = DB::table('cron_jobs')->where('id', 1);
        $get = $query->first();
        $update = $query->update(['test'=>$get->test + 1]);
    }

}
