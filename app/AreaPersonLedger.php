<?php

namespace App;

use App\AreaPerson;
use Illuminate\Database\Eloquent\Model;

class AreaPersonLedger extends Model
{
    public function areaperson()
    {
        return $this->belongsTo('App\AreaPerson');
    }
}
