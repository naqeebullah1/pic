<?php

namespace App;

use App\Purchase;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    protected $table='supplier_purchase_detail';

    public function Purchases() {
        return $this->belongsTo('App\Purchase');
    }
    public function Product() {
        return $this->belongsTo('App\Models\Product');
    }
}
