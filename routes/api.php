<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user-login', 'UsersController@index')->name('user-login');
Route::post('verify-phone-no', 'UsersController@verify_phone_number')->name('verify-phone-no');
Route::post('update-password', 'UsersController@update_password')->name('update-password');
