<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('our_backup_database', 'ContactusController@our_backup_database')->name('our_backup_database');

Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::routes();
Route::resource('contactus', 'ContactusController');
Route::get('save_cheque_payments', 'admin\SupplierController@save_cheque_payments')->name('save_cheque_payments');
Route::get('customers/save_cheque_payments', 'admin\CompaniesController@save_cheque_payments');
//Route::post('/login', 'UsersController@login');
Route::get('test/cron', 'admin\SalesController@test_cron');

Route::group(['middleware' => ['auth', 'CheckRole']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

// Products Route
    Route::get('products', 'admin\ProductsController@index')->name('products');
    Route::get('zero-products', 'admin\ProductsController@zero_products')->name('zero-products');

    Route::get('add-products', 'admin\ProductsController@add_products')->name('add-products');
    Route::post('store', 'admin\ProductsController@store')->name('store');
    Route::get('delete-product/{id}', 'admin\ProductsController@delete_product')->name('delete-product');
    Route::get('edit-product/{id}', 'admin\ProductsController@edit_product')->name('edit-product');
    Route::post('update-product', 'admin\ProductsController@update_product')->name('update-product');
    Route::get('product-ledger/{id}', 'admin\ProductsController@update_ledger')->name('product-ledger');

    // Currency Route
    Route::post('add-currency', 'HomeController@add_currency')->name('add-currency');
    //map route
    Route::get('users/map', 'admin\CompaniesController@map')->name('users/map');
    Route::post('users/map', 'admin\CompaniesController@map')->name('users/map');
    Route::post('company/get_addresses', 'admin\CompaniesController@get_addresses')->name('company/get_addresses');
    Route::get('company/get_addresses', 'admin\CompaniesController@get_addresses')->name('company/get_addresses');

    // Supplier Route
    Route::resource('supplier', 'admin\SupplierController');
    Route::resource('assets', 'admin\AssetsController');
    Route::get('supplier-payment/{id}', 'admin\SupplierController@supplier_payment')->name('supplier-payment');
    Route::post('get-payment-type', 'admin\SupplierController@get_payment_type')->name('get-payment-type');
    Route::post('dues-payment', 'admin\SupplierController@dues_payment')->name('dues-payment');

    // Supplier Purchase Route
    Route::resource('supplierPurchase', 'admin\PurchaseController');
    Route::put('supplierPurchase/test/{id}', 'admin\PurchaseController@update')->name('supplierPurchase.update');
    Route::resource('supplierReturnPurchase', 'admin\ReturnPurchaseController');
    Route::post('get-product-details', 'admin\PurchaseController@get_product_details')->name('get-product-details');
    Route::get('purchase-details/{id}', 'admin\PurchaseController@purchase_details')->name('purchase=-details');
    Route::get('return_purchase_details/{id}/{redirect}', 'admin\ReturnPurchaseController@return_purchase_details')->name('return_purchase_details');
    Route::get('return_sale_details/{id}/{redirect}', 'admin\ReturnSalesController@return_sale_details')->name('return_sale_details');
    Route::get('purchase/editPurchase/{id}', 'admin\PurchaseController@editPurchase')->name('editPurchase');

    // customer Sales Route
    Route::resource('sales', 'admin\SalesController');
    Route::resource('return_sale', 'admin\ReturnSalesController');
    Route::get('sale/details/{id}', 'admin\SalesController@saleDetails')->name('sale/details');
    Route::get('walking_customers/sale', 'admin\SalesController@walkingCustomerSale')->name('walking_customers/sale');
    Route::get('walking_customers/returnsale', 'admin\ReturnSalesController@walkingCustomerReturnsale')->name('walking_customers/returnsale');
    Route::get('walking_customers', 'admin\SalesController@walkingCustomers')->name('walking_customers');
    Route::get('walking_customers/return', 'admin\ReturnSalesController@walkingCustomersReturn')->name('walking_customers/return');
    Route::post('walking_customers/save', 'admin\SalesController@saveWalkingCustomer')->name('save/walking_customers');
    Route::post('walking_customers/saveReturnSale', 'admin\ReturnSalesController@walkingCustomerSaveReturnsale')->name('save/walking_customers/saveReturnSale');

    //    Route::post('get-product-details', 'admin\PurchaseController@get_product_details')->name('get-product-details');
    // cash in hand Route
    Route::resource('cash_in_hand', 'admin\CashInHandController');

    Route::get('products', 'admin\ProductsController@index')->name('products');
    Route::get('add-products', 'admin\ProductsController@add_products')->name('add-products');
    Route::post('store', 'admin\ProductsController@store')->name('store');

    Route::get('companies', 'admin\CompaniesController@index')->name('companies');
    Route::get('change-company-status/{id}', 'admin\CompaniesController@changeStatus')->name('change.company.status');
    Route::get('change-bank-status/{id}', 'admin\BankAccController@changeStatus')->name('change.bank.status');
    Route::get('change-salesman-status/{id}', 'admin\AreaPersonController@changeStatus')->name('change.saleman.status');
    Route::get('change-employee-status/{id}', 'admin\EmployeesController@changeStatus')->name('change.employee.status');
    Route::get('inactive-companies', 'admin\CompaniesController@inactiveCompanies')->name('inactive.companies');
    Route::get('customer/ledger/{id}', 'admin\CompaniesController@show')->name('customer/ledger');

    Route::get('company/edit/{id}', 'admin\CompaniesController@edit')->name('company/edit');
    Route::get('company/delete/{id}', 'admin\CompaniesController@destroy')->name('company/delete');
    Route::post('company/update', 'admin\CompaniesController@update')->name('company/update');
    Route::get('Company/add', 'admin\CompaniesController@create')->name('Company/add');
    Route::post('company/store', 'admin\CompaniesController@store')->name('company/store');
    Route::get('company/payment/{id}', 'admin\CompaniesController@customer_payment')->name('company/payment');
    Route::post('company/payment_type', 'admin\CompaniesController@get_payment_type')->name('company/payment_type');
    Route::post('company/save_payment', 'admin\CompaniesController@save_payment')->name('company/save_payment');


    //Banks Routes

    Route::get('bank_acc', 'admin\BankAccController@index')->name('bank_acc');
    Route::get('bank_acc/add', 'admin\BankAccController@create')->name('bank_acc/add');
    Route::post('bank_acc/store', 'admin\BankAccController@store')->name('bank_acc/store');
    Route::get('bank_acc/edit/{id}', 'admin\BankAccController@edit')->name('bank_acc/edit');
    Route::post('bank_acc/update', 'admin\BankAccController@update')->name('bank_acc/update');

    Route::get('bank_acc/destroy/{id}', 'admin\BankAccController@destroy')->name('bank_acc/destroy');
    Route::get('bank_acc/show/{id}', 'admin\BankAccController@show')->name('bank_acc/show');

    //Employees Routes
    Route::resource('employee', 'admin\EmployeesController');
    Route::resource('cities', 'admin\CitiesController');
    Route::resource('manufacturing_companies', 'admin\ManufacturingCompaniesController');
    Route::resource('product_qty', 'admin\ProductQtyController');
    Route::get('employee/employee_ledger/{id}', 'admin\EmployeesController@employee_ledger')->name('employee/employee_ledger');
    Route::post('employee/advance', 'admin\EmployeesController@advance')->name('employee/advance');
    Route::post('employee/save_paid_payment', 'admin\EmployeesController@save_paid_payment')->name('employee/save_paid_payment');
    Route::post('employee/payment_type', 'admin\EmployeesController@get_payment_type')->name('employee/payment_type');
    Route::post('cities/update_city', 'admin\CitiesController@update_city')->name('cities/update_city');
    Route::get('salary_details', 'admin\EmployeesController@salary_details')->name('salary_details');
    Route::get('fetch_company', 'admin\ManufacturingCompaniesController@fetch_company')->name('fetch_company');
    Route::get('fetch_quantity', 'admin\ProductQtyController@fetch_qty')->name('fetch_quantity');
    Route::post('product_qty/update', 'admin\ProductQtyController@update_qty')->name('product_qty/update');
    Route::get('fetch_city', 'admin\CitiesController@fetch_city')->name('fetch_city');
    Route::post('manufacturing_companies/update', 'admin\ManufacturingCompaniesController@update_company')->name('manufacturing_companies/update');

    //cron job
    Route::get('employees/employee_payments', 'admin\EmployeesController@employee_payments')->name('assign.salary');

    // Expense Route
    Route::resource('expenses', 'admin\HeadExpensesController');
    Route::resource('subexpenses', 'admin\ExpensesController');
    Route::post('payment-type', 'admin\ExpensesController@get_payment_type')->name('payment-type');

    Route::get('sale/history', 'admin\SalesController@fetch_history')->name('sale/history');
    Route::get('purchase/history', 'admin\PurchaseController@fetch_history')->name('purchase/history');
    Route::get('sale/print_invoice/{id}', 'admin\SalesController@print_invoice')->name('sale/print_invoice');
    Route::resource('area_person', 'admin\AreaPersonController');

    // Supplier Reports Route
    Route::get('supplier-general-report', 'admin\ReportsController@general_report')->name('supplier-general-report');
    Route::post('generate-supplier-general-report', 'admin\ReportsController@generate_supplier_general_report')->name('generate-supplier-general-report');
    Route::get('supplier-report', 'admin\ReportsController@index')->name('supplier-report');
    Route::post('supplier-ledger-report', 'admin\ReportsController@supplier_ledger_report')->name('supplier-ledger-report');

    // Supplier City Reports
    Route::get('supplier-city-report', 'admin\ReportsController@supplier_city_report')->name('supplier-city-report');
    Route::post('get-supplier-city-report', 'admin\ReportsController@get_supplier_city_report')->name('get-supplier-city-report');

    //Customers Reports Route
    Route::get('customer-report', 'admin\ReportsController@customer_report')->name('customer-report');
    Route::post('customer-ledger-report', 'admin\ReportsController@customer_ledger_report')->name('customer-ledger-report');

    //Customers City Reports Route
    Route::get('customer-city-report', 'admin\ReportsController@customer_city_report')->name('customer-city-report');
    Route::post('get-customer-city-report', 'admin\ReportsController@get_customer_city_report')->name('get-customer-city-report');
    // Product Reports Route
    Route::get('product-report', 'admin\ReportsController@product_report')->name('product-report');
    Route::post('product-ledger-report', 'admin\ReportsController@product_ledger_report')->name('product-ledger-report');

    // Product Reports Route
//    Route::get('product-report', 'admin\ReportsController@product_report')->name('product-report');
//    Route::post('product-ledger-report', 'admin\ReportsController@product_ledger_report')->name('product-ledger-report');
    // Purchase Reports Route
//    Route::get('purchase-report', 'admin\ReportsController@purchase_report')->name('purchase-report');
//    Route::post('purchase-ledger-report', 'admin\ReportsController@purchase_ledger_report')->name('purchase-ledger-report');
    // Purchase Reports Route
    Route::get('purchase-report', 'admin\ReportsController@purchase_report')->name('purchase-report');
    Route::post('purchase-ledger-report', 'admin\ReportsController@purchase_ledger_report')->name('purchase-ledger-report');
    // Sale Reports Route
//    Route::get('sale-report', 'admin\ReportsController@sale_report')->name('sale-report');
//    Route::post('sale-ledger-report', 'admin\ReportsController@sale_ledger_report')->name('sale-ledger-report');
    // Return  Purchase Reports Route
    Route::get('return-purchase-report', 'admin\ReportsController@return_purchase_report')->name('return-purchase-report');
    Route::post('return-purchase-ledger-report', 'admin\ReportsController@return_purchase_ledger_report')->name('return-purchase-ledger-report');
    // Expenses Reports Route
//    Route::get('expenses-report', 'admin\ReportsController@expenses_report')->name('expenses-report');
//    Route::post('expense-ledger-report', 'admin\ReportsController@expense_ledger_report')->name('expense-ledger-report');
    // Sale Reports Route
    Route::get('sale-report', 'admin\ReportsController@sale_report')->name('sale-report');
    Route::post('sale-ledger-report', 'admin\ReportsController@sale_ledger_report')->name('sale-ledger-report');
    // Expenses Reports Route
//    Route::get('bank-report', 'admin\ReportsController@bank_report')->name('bank-report');
//    Route::post('bank-ledger-report', 'admin\ReportsController@bank_ledger_report')->name('bank-ledger-report');
    // Return Sale Reports Route
    Route::get('return-sale-report', 'admin\ReportsController@return_sale_report')->name('return-sale-report');
    Route::post('return-sale-ledger-report', 'admin\ReportsController@return_sale_ledger_report')->name('return-sale-ledger-report');
    // Expenses Reports Route
//    Route::get('employee-report', 'admin\ReportsController@employee_report')->name('employee-report');
//    Route::post('employee-ledger-report', 'admin\ReportsController@employee_ledger_report')->name('employee-ledger-report');
    // Expenses Reports Route
    Route::get('expenses-report', 'admin\ReportsController@expenses_report')->name('expenses-report');
    Route::post('expense-ledger-report', 'admin\ReportsController@expense_ledger_report')->name('expense-ledger-report');

    // Bank Reports Route
    Route::get('bank-report', 'admin\ReportsController@bank_report')->name('bank-report');
    Route::post('bank-ledger-report', 'admin\ReportsController@bank_ledger_report')->name('bank-ledger-report');

    // Bank Reports Route
    Route::get('cash-report', 'admin\ReportsController@cash_report')->name('cash-report');
    Route::post('cash-ledger-report', 'admin\ReportsController@cash_ledger_report')->name('cash-ledger-report');

    // Employee Reports Route
    Route::get('employee-report', 'admin\ReportsController@employee_report')->name('employee-report');
    Route::post('employee-ledger-report', 'admin\ReportsController@employee_ledger_report')->name('employee-ledger-report');

    // Area Person Reports Route
    Route::get('area-report', 'admin\ReportsController@area_person_report')->name('area-report');
    Route::post('area-ledger-report', 'admin\ReportsController@area_person_ledger_report')->name('area-ledger-report');
    Route::post('tranferPayment', 'admin\BankAccController@transfer_payment')->name('transferPayment');

    // Sales Man Reports Route
    Route::resource('saleman', 'admin\SalesManController');

    // Sales Man Reports Route
    Route::get('current-summary', 'admin\ReportsController@current_summary')->name('current-summary');

    Route::post('company/request_payment', 'admin\CompaniesController@request_payment')->name('company/request_payment');
    Route::post('get_cheque_fields', 'admin\SupplierController@get_cheque_fields')->name('get_cheque_fields');
    Route::post('get_cheque_fields_customer', 'admin\CompaniesController@get_cheque_fields')->name('get_cheque_fields_customer');
    Route::get('supplier_cheques', 'admin\SupplierController@supplier_cheques')->name('supplier_cheques');
    Route::get('customer_cheques', 'admin\CompaniesController@customer_cheques')->name('customer_cheques');

    // Sales Man Reports Route
    Route::resource('zakat_persons', 'admin\ZakatPersonsController');
    Route::resource('users', 'admin\UsersController');
    Route::get('zakat_persons/destroy/{id}', 'admin\ZakatPersonsController@destroy')->name('zakat_persons.destroy');
    Route::get('zakat_persons/ledger/{id}', 'admin\ZakatPersonsController@show')->name('zakat_persons.ledger');
    Route::get('zakat_persons/payment/{id}', 'admin\ZakatPersonsController@payment')->name('zakat_persons.payment');
    Route::post('zakat_person/save_payment', 'admin\ZakatPersonsController@save_payment')->name('zakat_person.save_payment');
    Route::get('cheque_cleared', 'admin\CompaniesController@cheque_cleared')->name('cheque_cleared');
    Route::get('suppliercheque_cleared', 'admin\SupplierController@suppliercheque_cleared')->name('suppliercheque_cleared');
    Route::get('transfer_amount', 'admin\BankAccController@transfer_amount')->name('transfer_amount');
    Route::post('transfer_amount/store', 'admin\BankAccController@transfer_amount_store')->name('transfer_amount/store');
    Route::get('daily_cash', 'admin\ReportsController@daily_cash')->name('daily_cash');
    Route::post('get_daily_cash', 'admin\ReportsController@get_daily_cash')->name('get_daily_cash');
    Route::get('daily_bank', 'admin\ReportsController@daily_bank')->name('daily_bank');
    Route::post('get_daily_bank', 'admin\ReportsController@get_daily_bank')->name('get_daily_bank');
    Route::get('product-general-report', 'admin\ReportsController@product_general_report');
    Route::post('get-product-general-report', 'admin\ReportsController@get_product_general_report')->name('get-product-general-report');
    Route::get('get_date_diff', 'admin\EmployeesController@get_date_diff')->name('get_date_diff');
    Route::post('save_last_payment', 'admin\EmployeesController@save_last_payment')->name('save_last_payment');
//regular customer sale delete
    Route::get('sale/delete_sale/{id}', 'admin\SalesController@delete_sale')->name('sale/delete_sale');
    Route::get('sale/edit_sale/{id}', 'admin\SalesController@edit_sale')->name('sale/edit_sale');
    Route::get('sale/delete_return_sale/{id}', 'admin\SalesController@delete_return_sale')->name('sale/delete_return_sale');
// Quotaion Report

    Route::get('product-quotation', 'admin\ReportsController@product_quotation');
    Route::post('get-product-quotation', 'admin\ReportsController@get_product_quotation')->name('get-product-quotation');



//walking customer sale delete
    Route::get('sale/delete_walking_sale/{id}', 'admin\SalesController@delete_walking_sale')->name('sale/delete_walking_sale');
    Route::get('sale/delete_walking_return_sale/{id}', 'admin\SalesController@delete_walking_return_sale')->name('sale/delete_walking_return_sale');

    //Supplier purchase delete
    Route::get('purchase/delete_purchase/{id}', 'admin\PurchaseController@delete_purchase')->name('purchase/delete_purchase');
    Route::get('purchase/delete_return_purchase/{id}', 'admin\PurchaseController@delete_return_purchase')->name('purchase/delete_return_purchase');
    Route::get('supplier/delete_payment/{id}', 'admin\SupplierController@delete_payment')->name('supplier/delete_payment');
    Route::get('customer/delete_payment/{id}', 'admin\CompaniesController@delete_payment')->name('customer/delete_payment');
    Route::get('profit', 'admin\ReportsController@profit')->name('profit');
    Route::post('get-profit-report', 'admin\ReportsController@get_profit_report')->name('get-profit-report');

    Route::get('item-profit', 'admin\ReportsController@item_profit')->name('item_profit');
    Route::post('item-profit', 'admin\ReportsController@item_profit')->name('item_profit');
     Route::get('item-discount', 'admin\ReportsController@discount_profit')->name('discount_profit');
    Route::post('item-discount', 'admin\ReportsController@discount_profit')->name('discount_profit');
    Route::get('cheque/delete', 'admin\SupplierController@cheque_delete')->name('cheque/delete');
    Route::get('customer/cheque/delete/{id}', 'admin\SupplierController@customer_cheque_delete')->name('customer/cheque/delete');
    Route::get('expense/delete/{id}', 'admin\ExpensesController@expense_delete')->name('expense/delete');
    Route::get('bank_acc/delete_ledger/{id}', 'admin\BankAccController@delete_ledger')->name('bank_acc/delete_ledger');
    Route::get('sale_man/sales/{id}', 'admin\AreaPersonController@sale')->name('sale_man/sales');
    Route::post('sale_man/store_sale/{id}', 'admin\AreaPersonController@store_sale')->name('sale_man/store_sale');
    Route::get('sale_man/sales/history/{id}', 'admin\AreaPersonController@sales_view')->name('sale_man/sales/history');
    Route::get('get-product-list/{id}', 'admin\AreaPersonController@get_product_list')->name('get-product-list');
    Route::get('sale_man/sales/invoices/{id}', 'admin\AreaPersonController@invoices');
    Route::get('saleman/stock/details/{id}/{redirect}', 'admin\AreaPersonController@admin_stock_details')->name('saleman/stock/details');
    Route::get('sale_man/recovery/{id}', 'admin\CompaniesController@recovery');
    Route::get('company-current-status', 'admin\ReportsController@company_current_status');
    Route::get('saleman/sale-details/{id}/{redirect}', 'admin\AreaPersonController@stock_sale_details')->name('saleman/sale-details');
    Route::get('ledger_payment/update/{id}', 'admin\CompaniesController@update_ledger_payment')->name('update.ledger_payment');
    Route::post('stock-adjustment', 'admin\ProductsController@stockAdjustment')->name('stock.adjustment');
    Route::post('admin/edit/vr-no', 'admin\SalesController@edit_vr')->name('admin.edit.vr_no');
    Route::get('company/change-status', 'admin\SupplierController@change_status')->name('admin.company.change.status');
    Route::get('product/change-status', 'admin\ProductsController@change_status')->name('admin.product.change.status');
    Route::get('company/update-ledger/{id}', 'admin\SupplierController@update_ledgers')->name('admin.company.update.ledgers');
    Route::get('bank_acc/update-ledger/{id}', 'admin\BankAccController@update_ledger');
    Route::get('payment/print/{id}', 'admin\CompaniesController@printPayment')->name('print.payment');


});
Route::group(['middleware' => ['auth:saleman', 'CheckRole'], 'prefix' => 'salesman'], function () {
    Route::get('my_dashboard', 'admin\AreaPersonController@dashboard')->name('my_dashboard');
    Route::get('stock', 'admin\AreaPersonController@stock')->name('stock');
    Route::get('stock-details/{id}', 'admin\AreaPersonController@stock_details')->name('stock-details');
    Route::get('sale-details/{id}', 'admin\AreaPersonController@sale_details')->name('sale-details');
    Route::get('sale', 'admin\AreaPersonController@saleman_sale')->name('saleman.sale');
    Route::post('get-product-details', 'admin\PurchaseController@get_product_details')->name('saleman.get-product-details');
    Route::post('store_sale', 'admin\AreaPersonController@store_saleman_sale')->name('saleman.store_sale');
    Route::get('sale/details/{id}', 'admin\SalesController@saleDetails')->name('saleman.saledetails');
    Route::get('sale/print_invoice/{id}', 'admin\SalesController@print_invoice')->name('saleman.print_invoice');
    Route::get('customers', 'admin\AreaPersonController@customers')->name('saleman.customers');
    Route::get('customer-payment/{id}', 'admin\AreaPersonController@customer_payment')->name('customer-payment');
    Route::post('company/save_payment', 'admin\AreaPersonController@save_payment')->name('saleman.save_payment');
    Route::post('get-payment-type', 'admin\AreaPersonController@get_payment_type')->name('saleman.payment-type');
    Route::get('customer-ledger/{id}', 'admin\AreaPersonController@customer_ledger')->name('saleman.customer_ledger');
});
