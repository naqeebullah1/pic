<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnSalesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('return_sales');
        Schema::create('return_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vr_no')->nullable();
            $table->integer('company_id'); //company_id
            $table->string('return_sale_type'); 
            $table->string('total_amount');
            $table->string('discount')->nullable();
            $table->string('net_amount');
            $table->string('paid');
            $table->string('balance');
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('return_sales');
    }

}
