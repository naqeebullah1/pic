<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnSaleDetailsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('return_sale_details');
        Schema::create('return_sale_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('return_sale_id');
            $table->string('unit_price');
            $table->string('qty');
            $table->string('total_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('return_sale_details');
    }

}
