<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_ledger', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id');
            $table->string('invoice_id');
            $table->string('narration')->nullable();
            $table->string('purchase');
            $table->string('return_purchase');
            $table->string('payment');
            $table->string('balance');
            $table->string('currency');
            $table->string('currency_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_ledger');
    }
}
