<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_code')->nullable();
            $table->string('qty')->nullable();
            $table->string('title');
            $table->string('product_name');
            $table->string('product_cost_price');
            $table->string('product_sale_price');
            $table->string('product_whole_sale_price');
            $table->string('manufacturing_company');
            $table->string('stock_qty');
            $table->string('product_img')->nullable();
            $table->text('product_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }

}
