<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_ledger', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id');
            $table->string('invoice_id');
            $table->string('description')->nullable();
            $table->string('total_qty');
            $table->string('purchased_qty');
            $table->string('return_purchased');
            $table->string('sale');
            $table->string('return_sale');
            $table->string('remaning');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_ledger');
    }
}
