<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierPurchaseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('supplier_purchase_detail');
        Schema::create('supplier_purchase_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('purchase_id');
            $table->string('unit_price');
            $table->string('qty');
            $table->string('total_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_purchase_detail');
    }
}
