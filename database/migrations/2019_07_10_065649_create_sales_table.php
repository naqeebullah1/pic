<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('sales');
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vr_no')->nullable();
            $table->integer('company_id');
            $table->string('sale_type');
            $table->string('total_amount');
            $table->string('discount')->nullable();
            $table->string('net_amount');
            $table->string('paid');
            $table->string('balance');
            $table->string('narration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sales');
    }

}
