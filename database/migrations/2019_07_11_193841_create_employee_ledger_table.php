<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLedgerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('employee_ledger');
        Schema::create('employee_ledger', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('date');
            $table->string('narration')->nullable();
            $table->string('salary');
            $table->string('advance');
            $table->string('loan');
            $table->string('payment');
            $table->string('remaining');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('employee_ledger');
    }

}
