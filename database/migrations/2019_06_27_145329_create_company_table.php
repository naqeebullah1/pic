<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('area_person_id')->nullable();
            $table->string('cnic')->nullable();
            $table->string('contact');
            $table->string('email')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('country');
            $table->string('type');
            $table->string('dues');
            $table->string('last_payment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('company');
    }

}
