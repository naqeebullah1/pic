<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalVocherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('journal_vocher');
        Schema::create('journal_vocher', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('u_id');
            $table->string('head');
            $table->string('sub_head');
            $table->string('total');
            $table->string('pay');
            $table->string('balance');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_vocher');
    }
}
