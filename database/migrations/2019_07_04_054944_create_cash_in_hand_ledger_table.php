<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashInHandLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('cash_in_hand_ledger');
        Schema::create('cash_in_hand_ledger', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_in_hand_id');
            $table->string('total_amount');
            $table->string('deposit');
            $table->string('withdrawal');
            $table->string('remaining');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_in_hand_ledger');
    }
}
